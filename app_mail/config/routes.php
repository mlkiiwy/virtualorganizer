<?php

use lithium\core\Environment;
use lithium\action\Response;
use lithium\net\http\Router;

/**
 * Request type detection
 */
Router::connect('/{:args}.{:type:ajax|json|rss|xml}', array(), array('continue' => true));

/**
 *
 */
$email = '{:email:[\w\_\-\@\%\. ]+}';

Router::connect(
	'/_ah/mail/' . $email,
	array('controller' => 'MailerHandler', 'action' => 'incoming')
);

if (!Environment::is('production')) {
	Router::connect('/test/{:args}', array('controller' => 'lithium\test\Controller'));
	Router::connect('/test', array('controller' => 'lithium\test\Controller'));
}

Router::connect(
	'/',
	array(), function($request) {

	return new Response(array(
			'location' => array(
				'controller' => 'Topics',
				'action' => 'index',
				'args' => array('#', 'create')
			),
			'status' => 301
		));
});

Router::connect('/{:controller}/{:action}/{:args}');

?>