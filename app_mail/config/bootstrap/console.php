<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

use lithium\console\Dispatcher;
use lithium\net\http\Router;
use lithium\core\Environment;
use lithium\core\Libraries;

Dispatcher::applyFilter('_call', function($self, $params, $chain) {
	$params['callable']->response->styles(array(
		'heading' => '\033[1;30;46m'
	));
	return $chain->next($self, $params, $chain);
});


Dispatcher::applyFilter('run', function($self, $params, $chain) {
	Environment::set($params['request']);

	foreach (array_reverse(Libraries::get()) as $name => $config) {
		if ($name === 'lithium') {
			continue;
		}
		$file = "{$config['path']}/config/routes.php";
		file_exists($file) ? include $file : null;
	}
	return $chain->next($self, $params, $chain);
});

?>