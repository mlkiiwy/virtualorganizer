<?php

use lithium\core\Environment;

/**
 * Fetch default settings and merge user settings.
 */
require 'environment.conf.php';
require dirname(LITHIUM_APP_PATH) . '/conf.php';

$environnements = array_keys($appSettings);
$baseEnvironment = empty($userEnvironment) ? $baseEnvironment : $userEnvironment;

foreach ($environnements as $environment) {
	if ($environment != 'production') {
		$appSettings[$environment] = array_merge($appSettings['production'], $appSettings[$environment]);
	}

	if (!empty($userSettings)) {
		if (!empty($userSettings['default']) && !empty($userSettings['default'][$environment])) {
			$appSettings[$environment] = array_merge($appSettings[$environment], $userSettings['default'][$environment]);
		}

		if (!empty($userSettings[APP_NAME]) && !empty($userSettings[APP_NAME][$environment])) {
			$appSettings[$environment] = array_merge($appSettings[$environment], $userSettings[APP_NAME][$environment]);
		}
	}

	Environment::set($environment, $appSettings[$environment]);
}

/**
 * Determine environment based on host name or by the overriden value in the settings file.
 */
Environment::is(function ($request) {
	global $baseEnvironment;

	return empty($baseEnvironment) ? 'production' : $baseEnvironment;
});

/**
 * Override environment for configuration that happens pre-request dispatch.
 */
Environment::set($baseEnvironment);

?>