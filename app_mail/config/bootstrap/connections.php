<?php

use lithium\core\Environment;
use lithium\data\Connections;

Connections::add('default', array(
	'type' => 'database',
	'adapter' => 'common\data\source\database\adapter\MySql',
	'host' => Environment::get('sql_master.host'),
	'socket' => Environment::get('sql_master.socket'),
	'login' => Environment::get('sql_master.login'),
	'password' => Environment::get('sql_master.password'),
	'database' => Environment::get('sql_master.dbName'),
	'encoding' => 'UTF-8',
	'persistent' => false,
	'classes' => array(
		'entity' => 'common\data\entity\Record',
		'set' => 'common\data\collection\RecordSet'
	)
));

/*
Connections::add('slave', array(
	'type' => 'database',
	'adapter' => 'common\data\source\database\adapter\MySql',
	'host' => Environment::get('sql_slave.host'),
	'login' => Environment::get('sql_slave.login'),
	'password' => Environment::get('sql_slave.password'),
	'database' => 'sc_2',
	'encoding' => 'UTF-8',
	'persistent' => false,
	'classes' => array(
		'entity' => 'common\data\entity\Record',
		'set' => 'common\data\collection\RecordSet'
	)
));
*/

?>