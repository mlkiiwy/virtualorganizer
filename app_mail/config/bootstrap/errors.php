<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

use lithium\core\Environment;
use lithium\core\ErrorHandler;
use lithium\action\Response;
use lithium\net\http\Media;

use common\util\ExceptionUtil;

use lithium\analysis\Logger;

Logger::config(
	array('default' => Environment::get('log'))
);

ErrorHandler::apply('lithium\action\Dispatcher::run', array(), function($info, $params) {
	$statusCode = $info['exception']->getCode();
	$ajax = in_array($params['request']->type(), array('ajax', 'json'));

	switch ($statusCode) {
		case 401:
			if (!$ajax) {
				$response['location'] = array('controller' => 'Auth', 'action' => 'login');
				$response = new Response($response);
				$response->status(301);
			} else {
				$response['message'] = '/auth/login';
				$response = new Response($response);
				$response->status($statusCode);

			}

			break;

		default:
			if (!Environment::is('production')) {
				$code = $info['exception']->getCode();
				$code = empty($code) ? 500 : $code;

				$response = new Response(array(
					'request' => $params['request'],
					'status' => $code,
				));

				Logger::write('error', ExceptionUtil::toString($info['exception']));

				Media::render($response, compact('info', 'params'), array(
					'controller' => '_errors',
					'template' => 'development',
					'layout' => 'error',
					'request' => $params['request']
				));

			} else {
				header("HTTP/1.0 404 Not Found");
				die;
			}
			break;
	}

	return $response;
});

ErrorHandler::run(array('convertErrors' => true));

?>