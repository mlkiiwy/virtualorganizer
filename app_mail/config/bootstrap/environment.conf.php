<?php
/**
 * Mode d'environnement disponibles:
 * - production
 * - preproduction
 * - development
 * - test
 */
$baseEnvironment = 'production';

$appSettings = array(
	'production' => array(
		'app' => array(
			'domain' => "mail.virtualorganizer.dev",
			'resources' => '/tmp/virtualorganizer/app_mail/'
		),

		'cache' => false,
		'cache_read' => false,

		'debugger' => array(
			'logQueries' => false,
			'logSlowPages' => false,
			'logCache' => false,
			'display' => false,
			'sendSyslog' => true,
			'displaySession' => false,
		),

		'log' => false,

		'domain' => array(
			'main' => 'virtualorganizer.dev',
			'www' => "www.virtualorganizer.dev"
		),

		'media' => false,

		'sql_master' => array(
			'dbName' => 'virtualorganizer',
			'host' => '127.0.0.1',
			'login' => 'virtualorganizer',
			'password' => 'virtualorganizer'
		),

		'sql_slave' => false,

		'mail' => array(
			'default_from' => 'default-from@test.com',
			'adapter' => 'Smtp',
			'host' => 'localhost',
			'port' => 25,
		),

		'delayed_insert' => array(
			'path' => '/scdata.local/delayed_insert/'
		),

		'assets' => array(
			'js_compression' => true,
			'css_compression' => true,
			'domain' => '',
		),
	),
	'preproduction' => array(
		'app' => array(
			'domain' => 'media.preprod.senscritique.com',
			'resources' => '/scdata.local/tmp.preprod'
		),

		'media' => array(
			'domain' => 'http://m.sens.sc'
		),

		'assets' => array(
			'js_compression' => true,
			'css_compression' => true,
			'domain' => '',
		),
	),
	'test' => array(),
	'development' => array(
		'app' => array(
			'domain' => 'media.senscritique.dev',
			'resources' => LITHIUM_APP_PATH . '/resources'
		),

		'cache' => false,

		'cache_read' => false,

		'debugger' => array(
			'logQueries' => true,
			'logSlowPages' => true,
			'logCache' => true,
			'display' => true,
			'sendSyslog' => false,
			'displaySession' => false,
		),

		'media' => array(
			'domain' => 'http://mv.sens.sc',
			'path' => "/tmp/lithium",
			'cache_path' => "/tmp/lithium.cache",
		),

		'sql_master' => array(
			'host' => 'sql-dev',
			'login' => 'www-dev',
			'password' => 'TO BE REPLACED'
		),

		'sql_slave' => array(
			'host' => 'sql-dev',
			'login' => 'www-dev',
			'password' => 'TO BE REPLACED'
		),

		'mail' => array(
			'default_from' => 'default-from@test.com',
			'adapter' => 'PhpMail',
			'overrideTo' => 'labrut@gmail.com',
		),

		'assets' => array(
			'js_compression' => true,
			'css_compression' => true,
			'domain' => '',
		),
	)
);

?>