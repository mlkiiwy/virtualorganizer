<?php
/**
 * Lithium: the most rad php framework
 *
 * @copyright     Copyright 2011, Union of RAD (http://union-of-rad.org)
 * @license       http://opensource.org/licenses/bsd-license.php The BSD License
 */

/**
 * This file creates a default cache configuration using the most optimized adapter available, and
 * uses it to provide default caching for high-overhead operations.
 */
use lithium\action\Dispatcher;

use lithium\core\Environment;
use lithium\core\Libraries;

use lithium\data\Connections;

use lithium\storage\Cache;
use lithium\storage\cache\adapter\Redis;

// Disable cache for php in cli
if (PHP_SAPI === 'cli') {
	return;
}

/**
 * Caches paths for auto-loaded and service-located classes and model schemas. Sets up Redis cache.
 * @todo move key namespace to Cache class.
 */
Dispatcher::applyFilter('run', function ($self, $params, $chain) {
	$useCache = Environment::get('cache');
	$useCacheRead = Environment::get('cache_read');

	if (!empty($useCache) && !empty($useCacheRead)) {
		switch (Environment::get()) {
			case 'production':
				$namespace = 'Prod.';
				break;

			case 'preproduction':
				$namespace = 'Preprod.';
				break;

			default:
				$namespace = 'Dev.';
		}

		Cache::applyFilter('read', function ($self, $params, $chain) use ($namespace) {
			$params['key'] = $namespace . $params['key'];
			return $chain->next($self, $params, $chain);
		});

		Cache::applyFilter('write', function ($self, $params, $chain) use ($namespace) {
			$params['key'] = $namespace . $params['key'];
			return $chain->next($self, $params, $chain);
		});

		Cache::applyFilter('delete', function ($self, $params, $chain) use ($namespace) {
			$params['key'] = $namespace . $params['key'];
			return $chain->next($self, $params, $chain);
		});

		if (is_array($useCache) && $useCache['adapter'] == 'redis' && Redis::enabled()) {
			Cache::config(array(
				'default' => array(
					'adapter' => 'Redis',
					'host' => $useCache['host'],
					'strategies' => array('Serializer')
				),
				'read_only' => array(
					'adapter' => 'Redis',
					'host' => $useCacheRead['host'],
					'strategies' => array('Serializer')
				)
			));
		} elseif ($useCache === true) {
			Cache::config(array(
				'default' => array(
					'adapter' => 'File',
					'strategies' => array('Serializer')
				)
			));
		}

		/**
		 * Loop over all defined `Connections` adapters and tack in our
		 * filter on the `_execute` method.
		 */
		foreach (Connections::get() as $connection) {
			$connection = Connections::get($connection);
			$connection->applyFilter('describe', function($self, $params, $chain) {
				if (!empty($params['fields'])) {
					$fields = $params['fields'];
					return $self->invokeMethod('_instance', array('schema', compact('fields')));
				}

				$key = 'SC.Schema.' . $self->invokeMethod('_entityName', array($params['entity']));
				$schema = Cache::read('read_only', $key);

				if ($schema) {
					$fields = $schema;
					return $self->invokeMethod('_instance', array('schema', compact('fields')));
				}

				$schema = $chain->next($self, $params, $chain);

				Cache::write('default', $key, $schema->fields(), '+1 day');

				return $schema;
			});
		}
	}

	$key = md5(LITHIUM_APP_PATH) . '.core.libraries';

	if ($cache = Cache::read('read_only', $key)) {
		$cache = (array) $cache + Libraries::cache();
		Libraries::cache($cache);
	}

	$result = $chain->next($self, $params, $chain);

	if ($cache != Libraries::cache()) {
		Cache::write('default', $key, Libraries::cache(), '+1 day');
	}

	return $result;
});

?>