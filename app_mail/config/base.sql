DROP TABLE IF EXISTS `vo_register_code`;

CREATE TABLE `vo_register_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `user_id_creation` int(10) NOT NULL COMMENT 'Generator',
  `user_id` int(10) NOT NULL COMMENT 'User who use the code',
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Waiting / 1: Used',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_topic`;

CREATE TABLE `vo_topic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(500) NOT NULL DEFAULT '' COMMENT 'Sujet du topic',
  `state` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT '',
  `user_id` int(10) NOT NULL COMMENT 'Creator',
  `gen_count_users` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Actives users',
  `gen_count_messages` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Global messages of topic',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_topic_user`;

CREATE TABLE `vo_topic_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Waiting / 1: In / -1: Out',
  `topic_id_mode` tinyint(4) NOT NULL DEFAULT 1,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`topic_id`, `user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_topic_mode`;

CREATE TABLE `vo_topic_mode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_message_type_id`;

CREATE TABLE `vo_message_type_id` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_gender`;

CREATE TABLE `vo_gender` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_user`;

CREATE TABLE `vo_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Draft / 1: Active / -1 : Deleted',
  `state_admin` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Basic / 1: Admin',
  `creation_state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Basic / 1: Import from api = Created from a sync by api like recup from friends',
  `login` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(32) DEFAULT NULL,
  `validation_code` varchar(32) DEFAULT NULL,
  `main_email` varchar(255) NOT NULL,
  `main_phone_number` varchar(255),
  `user_id_creation` int(10) unsigned DEFAULT NULL,
  `external_api_id_creation_import` int(10) unsigned DEFAULT NULL,
  `external_api_id_creation` int(10) unsigned DEFAULT NULL,
  `application_id_creation` int(10) unsigned DEFAULT 0,
  `date_activation` timestamp NULL DEFAULT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`main_email`),
  KEY(`main_phone_number`),
  UNIQUE(`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_user_email`;

CREATE TABLE `vo_user_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Draft / 1: Active / -1 : Deleted',
  `main` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Boolean',
  `email` varchar(255) NOT NULL,
  `user_id_creation` int(10) unsigned NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_user_phone_number`;

CREATE TABLE `vo_user_phone_number` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Draft / 1: Active / -1 : Deleted',
  `main` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Boolean',
  `phone_number` varchar(255) NOT NULL,
  `phone_type_id` tinyint(4) NOT NULL DEFAULT 0 COMMENT '1: Mobile / 2: Static / 3:Unknow',
  `user_id_creation` int(10) unsigned NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`phone_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_phone_type`;

CREATE TABLE `vo_phone_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_user_contact`;

CREATE TABLE `vo_user_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `user_id_contact` int(10) unsigned NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Draft / 1: Active',
  `user_id_creation` int(10) unsigned NOT NULL,
  `creation_state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Basic / 1: Import from api',
  `external_api_id` int(10) unsigned DEFAULT NULL,
  `gen_count_message` int(10) NOT NULL DEFAULT 0,
  `gen_count_topic` int(10) NOT NULL DEFAULT 0,
  `gen_count_group` int(10) NOT NULL DEFAULT 0,
  `gen_friend_force` int(10) NOT NULL DEFAULT 0,
  `gen_affinity` int(10) NOT NULL DEFAULT 0,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`user_id`, `user_id_contact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_group`;

CREATE TABLE `vo_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `group_type_id` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Basic / 1: shared',
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Draft / 1: Active / -1 : deleted',
  `creation_state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Basic / 1: Import from api / 2 : calculated',
  `user_id_creation` int(10) unsigned NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_group_type`;

CREATE TABLE `vo_group_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_group_user`;

CREATE TABLE `vo_group_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Member / 1: Admin',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`group_id`, `user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_application`;

CREATE TABLE `vo_application` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_user_token`;

CREATE TABLE `vo_user_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned DEFAULT 0,
  `user_id` int(10) unsigned NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `token` varchar(32) DEFAULT NULL,
  `ip` int(10) unsigned DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `date_expiration` timestamp NULL DEFAULT NULL,
  `date_creation` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`state`),
  UNIQUE KEY `idx_token_state` (`token`,`state`) USING BTREE,
  KEY `idx_state` (`state`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_date_expiration` (`date_expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
/*!50100 PARTITION BY LIST (state)
(PARTITION p0 VALUES IN (0) ENGINE = InnoDB,
 PARTITION p1 VALUES IN (1) ENGINE = InnoDB,
 PARTITION p2 VALUES IN (-1) ENGINE = InnoDB) */;

DROP TABLE IF EXISTS `vo_log_action`;

CREATE TABLE `vo_log_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `action_id` int(10) NOT NULL,
  `user_id` int(10),
  `topic_id` int(10),
  `message_id` int(10),
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Unknow / 1: Success / -1: Failed',
  `step` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: None / 1: Before / 2: After',
  `json` TEXT NOT NULL DEFAULT '' COMMENT 'Other data',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_action`;

CREATE TABLE `vo_action` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_message_status_user`;

CREATE TABLE `vo_message_status_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL,
  `message_id` int(10) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Unread / 1: Read',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`user_id`, `message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `vo_message`;

CREATE TABLE `vo_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) NOT NULL,
  `user_id` int(10) COMMENT 'Sender / Creator user',
  `agent_topic_id` int(10) COMMENT 'Sender / Creator agent',
  `user_dest_id` int(10) COMMENT 'Destination',
  `json` MEDIUMTEXT NOT NULL,
  `message_type_id` int(10) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_email_type`;

CREATE TABLE `vo_email_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_email`;

CREATE TABLE `vo_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_type_id` int(10) NOT NULL,
  `code` VARCHAR(32) NOT NULL,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `user_id` int(10) NOT NULL COMMENT 'user_id of destination',
  `subject` VARCHAR(255) NOT NULL,
  `content` MEDIUMTEXT NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Waiting / 1: Send / 2: Open',
  `message_id` int(10),
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE(`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_income_email`;

CREATE TABLE `vo_income_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` MEDIUMTEXT NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Pending / 1: Treated / -1: Error',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_income_email_analysed`;

CREATE TABLE `vo_income_email_analysed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT 'user_id of owner (=from)',
  `topic_id` int(10),
  `subject` VARCHAR(255) NOT NULL,
  `content_html` MEDIUMTEXT NOT NULL,
  `content_text` MEDIUMTEXT NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Pending / 1: Treated / -1: Error',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_event`;

CREATE TABLE `vo_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` TEXT NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Pending / 1: Open / 2: Close',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_event_user`;

CREATE TABLE `vo_event_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_event` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Pending / 1: In / 2: Out / 3: Unknow',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_agent`;

CREATE TABLE `vo_agent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_agent_topic`;

CREATE TABLE `vo_agent_topic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(10) NOT NULL,
  `agent_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: Pending / 1: Active / -1 : Inactive ',
  `date_last_execution` timestamp NULL DEFAULT NULL,
  `date_next_execution` timestamp NULL DEFAULT NULL,
  `date_end_execution` timestamp NULL DEFAULT NULL,
  `json` TEXT NOT NULL DEFAULT '' COMMENT 'Other data',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_agent_answer`;

CREATE TABLE `vo_agent_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agent_topic_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT 0 COMMENT '0: No answer / 1: Yes / -1 : No / 2 : Maybe ',
  `json` TEXT NOT NULL DEFAULT '' COMMENT 'Other data',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_agent_reask`;

CREATE TABLE `vo_agent_reask` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `agent_topic_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `agent_reask_reason_id` int(10) NOT NULL,
  `message_id` int(10) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_agent_reask_reason`;

CREATE TABLE `vo_agent_reask_reason` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api`;

CREATE TABLE `vo_external_api` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `json` TEXT NOT NULL DEFAULT '' COMMENT 'Other data',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_account`;

CREATE TABLE `vo_external_api_account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_api_id` int(10) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `uid` varchar(100) DEFAULT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `username` varchar(150) DEFAULT NULL,
  `first_name` varchar(128) DEFAULT NULL,
  `last_name` varchar(128) DEFAULT NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `json` TEXT NOT NULL DEFAULT '' COMMENT 'Other data',
  `date_last_sync` timestamp NULL DEFAULT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_uid` (`uid`, `external_api_id`),
  KEY `idx_user_id` (`user_id`,`external_api_id`) USING BTREE,
  KEY `idx_external_api_id` (`external_api_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_account_link`;

CREATE TABLE `vo_external_api_account_link` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gen_external_api_id` int(10) NOT NULL,
  `external_api_account_id` int(10) unsigned NOT NULL,
  `external_api_account_id_related` int(10) unsigned NOT NULL,
  `link_type_id` tinyint(4) NOT NULL DEFAULT 0,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY (`external_api_account_id`, `external_api_account_id_related`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_link_type`;

CREATE TABLE `vo_link_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_oauth_type`;

CREATE TABLE `vo_oauth_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_token`;

CREATE TABLE `vo_external_api_token` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_api_id` int(10) NOT NULL,
  `external_api_account_id` int(10) NOT NULL,
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `oauth_type_id` tinyint(4) NOT NULL DEFAULT '1',
  `access_token` varchar(500) DEFAULT NULL,
  `secret` varchar(200) DEFAULT NULL,
  `date_expiration` timestamp NULL DEFAULT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_external_api_id` (`external_api_id`,`state`, `date_expiration`),
  KEY `idx_external_api_account_id` (`external_api_account_id`,`state`, `date_expiration`),
  KEY `idx_date_expiration` (`date_expiration`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_right`;

CREATE TABLE `vo_external_api_right` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(255) NOT NULL,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_right_by_api`;

CREATE TABLE `vo_external_api_right_by_api` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_api_id` int(10) NOT NULL,
  `external_api_right_id` int(10) NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT 0,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`external_api_id`, `external_api_right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `vo_external_api_right_account_value`;

CREATE TABLE `vo_external_api_right_account_value` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_api_account_id` int(10) NOT NULL,
  `external_api_right_id` int(10) NOT NULL,
  `value` tinyint(4) NOT NULL DEFAULT 0,
  `date_creation` timestamp NULL DEFAULT NULL,
  `date_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_unique` (`external_api_account_id`, `external_api_right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;