<?php

namespace app\extensions\command;

use common\models\ExternalApiAccounts;

use common\actions\ActionSyncExternalApiAccountFriends;
use common\actions\ActionExecuteAgentTopic;
use common\actions\ActionAddAgentMessageToTopic;

use common\util\DataFake;

use common\util\Mail as MailUtil;

class Tests extends \common\extensions\command\Command {

	/**
	 * [parsing description]
	 * @return [type] [description]
	 */
	public function parsing() {

		var_dump(MailUtil::parse(DataFake::emailContent()));

		return true;
	}

	/**
	 * [sync description]
	 * @return [type] [description]
	 */
	public function sync() {
		$externalApiAccount = ExternalApiAccounts::first(1);
		$action = new ActionSyncExternalApiAccountFriends(compact('externalApiAccount'));
		return $action->execute();
	}

	public function agent() {
		$action = new ActionExecuteAgentTopic(array('agentTopic' => 1));
		$action->displayer($this);
		return $action->execute();
	}

	public function addMessage() {
		$user = 1;
		$agentTopic = 1;
		$message = array(
					'template' => 'reask_after_first_reading',
					'type' => 'reask',
					'agent_reask_reason_id' => 1,
				);
		$action = new ActionAddAgentMessageToTopic(compact('agentTopic', 'message', 'user'));
		$action->displayer($this);
		return $action->execute();
	}
}

?>