<?php

namespace app\extensions\command;

use common\data\database\Database as DataDatabase;

class Database extends \common\extensions\command\Command {

	/**
	 *
	 */
	public function reset() {
		$this->out('Reset database');
		return DataDatabase::reset();
	}
}

?>