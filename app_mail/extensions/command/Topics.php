<?php

namespace app\extensions\command;

use common\actions\ActionCreateTopic;

class Topics extends \common\extensions\command\Command {


	public $subject;

	/**
	 *
	 */
	public function create() {
		$params = $this->_params(array('subject' => 'Default subject'));

		$action = new ActionCreateTopic($params);
		return $action->execute();
	}
}

?>