<?php

namespace app\extensions\command;

use common\actions\base\ActionsQueue;

use common\actions\ActionCreateTopic;
use common\actions\ActionAddMessageToTopic;
use common\actions\ActionSendPendingMessageOfTopic;
use common\actions\ActionTestResultExecute;

use common\actions\ActionCreateIncomingEmail;
use common\actions\ActionAnalyseIncomeEmail;
use common\actions\ActionAddMessageToTopicFromEmail;

use common\actions\ActionOpenEmail;
use common\actions\ActionClickEmail;

use common\actions\queue\ActionBuildTopic;
use common\actions\queue\ActionCreateUsers;
use common\actions\queue\ActionAddUsersToTopic;

use common\actions\util\UseResult;

use common\util\Set as SetUtil;
use common\util\RecordSet as RecordSetUtil;
use common\util\DataFake;

use common\models\Topics;

class TopicSimulator extends \common\extensions\command\Simulator {

	/**
	 * Scénario : création d'un topic
	 */
	public function build() {

		$subject = DataFake::subject();
		$emails = DataFake::emails(array('count' => 2));
		$message = DataFake::message($this->_user());

		$action = new ActionBuildTopic(compact('subject', 'emails', 'message'));
		$action->user($this->_user());
		return $action->execute(array('displayer' => null));
	}

	/**
	 * Scénario :
	 * - Création d'un topic
	 * - Envoi des emails liés a ce topic
	 */
	public function simulation2() {

		// Simulate build action
		$result = $this->build();
		$result = $result->result();
		$topic = $result['topic'];

		$actionsQueue = new ActionsQueue();

		// --- On demande les messages a envoyer =>
		// une action de génération est faite pour chaque message
		// on l'execute immédiatement, on a donc les emails envoyés
		$actionsQueue->add(new ActionSendPendingMessageOfTopic,
			array('topic' => $topic), array('result' => 'sendTasks'));

		$actionsQueue->add(new ActionTestResultExecute,
			array('action' => new UseResult('parent.sendTasks')), array('result' => 'emails'));

		return $actionsQueue->execute();
	}

	/**
	 * Scénario :
	 * - Création d'un topic
	 * - Reception d'un message que l'on intègre au topic
	 *
	 * Simulation testée : OK !
	 */
	public function simulation3() {

		// Simulate build action
		$result = $this->build();
		$result = $result->result();
		$topic = $result['topic'];

		$topicUser  = RecordSetUtil::pickRandomElement($topic->populateUsers());

		$incomingEmailContent = DataFake::emailContent(array(
			'fromEmail' => $topicUser->related('user')->email,
			'toEmail' => $topic->getEmailFrom(),
		));

		$actionsQueue = new ActionsQueue();
		$actionsQueue->displayer($this);

		// --- On receptionne un message venant du topic
		$actionsQueue->add(new ActionCreateIncomingEmail,
			array('content' => $incomingEmailContent), array('result' => 'incomeEmail'));

		return $actionsQueue->execute();
	}

	/**
	 * Scénario :
	 * - Création d'un topic
	 * - Envoi des emails liés au topic
	 * - Simulation d'ouverture de l'un de ces emails
	 * @return [type] [description]
	 */
	public function simulation4() {

		// Simulate send emails
		$result = $this->simulation2();
		$result = $result->result();

		$email = SetUtil::pickRandomElement($result['emails']['all']);
		$code = $email->code;

		$action = new ActionOpenEmail(compact('code'));
		$action->displayer($this);
		return $action->execute();
	}

	/**
	 * Scénario :
	 * - Création d'un topic
	 * - Envoi des emails liés au topic
	 * - Simulation de click de l'un de ces emails
	 * @return [type] [description]
	 */
	public function simulation5() {

		// Simulate send emails
		$result = $this->simulation2();
		$result = $result->result();

		$email = SetUtil::pickRandomElement($result['emails']['all']);
		$code = $email->code;

		$action = new ActionClickEmail(compact('code'));
		$action->displayer($this);
		return $action->execute();
	}

	/**
	 * Scénario :
	 * - Création d'un topic
	 * - Association d'un agent "question" au topic
	 * - Envoi des emails liés a ce topic
	 */
	public function simulationAgent() {

		// Simulate build action
		$result = $this->build();
		$result = $result->result();
		$topic = $result['topic'];

		$actionsQueue = new ActionsQueue();
		$actionsQueue->user($this->_user());

		// Ajout d'un agent au topic
		$actionsQueue->add(new ActionCreateSimpleQuestionAgent,
			array('topic' => $topic), array('result' => 'agent'));

		/*
		// --- On demande les messages a envoyer =>
		// une action de génération est faite pour chaque message
		// on l'execute immédiatement, on a donc les emails envoyés
		$actionsQueue->add(new ActionSendPendingMessageOfTopic,
			array('topic' => $topic), array('result' => 'sendTasks'));

		$actionsQueue->add(new ActionTestResultExecute,
			array('action' => new UseResult('parent.sendTasks')), array('result' => 'emails'));
		*/

		return $actionsQueue->execute();
	}


	/**
	 * [analyseIncome description]
	 * @return [type] [description]
	 */
	public function analyseIncome() {
		$action = new ActionAnalyseIncomeEmail(array('incomeEmail' => 2));
		$action->displayer($this);
		return $action->execute();
	}
}

?>