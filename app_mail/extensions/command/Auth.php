<?php

namespace app\extensions\command;

use common\models\ExternalApis;
use common\models\Genders;

use common\actions\queue\ActionLoginProcessFromApiData;

use common\util\Date;

class Auth extends \common\extensions\command\Command {

	/**
	 *
	 */
	public function loginFromApi() {

		$userInfo = array(
			'external_api_id' => ExternalApis::FACEBOOK,
			'uid' => 1,
			'username' => 'username',
			'slug' => 'username',
			'first_name' => 'first_name',
			'last_name' => 'last_name',
			'avatar' => 'http://avatar.url',
			'email' => 'test@email.com',
			'other' => array(
				'gender' => Genders::MALE,
			),
			'info_token' => array(
				'access_token' => 'access_token',
				'date_expiration' => Date::sqlNow(),
			),
		);

		$action = new ActionLoginProcessFromApiData(compact('userInfo'));
		$action->displayer($this);
		return $action->execute();
	}
}

?>