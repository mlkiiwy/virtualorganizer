<?php

namespace app\extensions\command;

use common\actions\ActionCleanExternalApiTokens;

class ExternalApiTokens extends \common\extensions\command\Cron {

	/**
	 *
	 */
	public function clean() {
		$action = new ActionCleanExternalApiTokens();
		return $action->execute();
	}
}

?>