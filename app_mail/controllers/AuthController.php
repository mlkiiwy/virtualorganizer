<?php

namespace app\controllers;

use common\models\ExternalApis;

use common\actions\ActionLoginUser;
use common\actions\ActionConnectUser;
use common\actions\ActionCreateUser;
use common\actions\ActionActivateUser;
use common\actions\queue\ActionLoginProcessFromApiData;
use common\actions\ActionSendActivationEmail;
use common\actions\ActionDisconnectUser;

use common\models\Users;
use common\models\RegisterCodes;

use common\net\oauth\OAuthService;

use common\extensions\storage\Visitor;

use common\data\constants\State;

use common\util\Url;
use common\util\ExceptionUtil;

use common\extensions\exceptions\ControllerParameterException;
use Exception;

/**
 *
 */
class AuthController extends AppBaseController {

	const LOGIN_ERROR_MESSAGE = 'Identification impossible';
	const LOGIN_ERROR_INPUT = 'Email non valide';

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('login', 'callback', 'lostPassword', 'register', 'activate'),
		'xhrOnly' => array(),
		'xhr' => array('login', 'register', 'lostPassword'),
		'nofollow' => array('login', 'callback', 'register', 'lostPassword', 'activate'),
	);

	/**
	 * [_getCallbackUrl description]
	 * @param  [type] $api [description]
	 * @return [type]      [description]
	 */
	protected function _getCallbackUrl($api) {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'callback',
			'args' => array($api)
		), array('absolute' => true));
	}

	/**
	 * [register description]
	 * @return [type] [description]
	 */
	public function register() {

		if (!$this->isUserAnonymous()) {
			return $this->redirect('/');
		}

		if ($this->request->type() == 'json') {
			try {
				$data = $this->_getParameters(array(
					'email' => array(
						'required' => true,
						'type' => 'string_email',
						'error' => 'Email vide',
						'empty' => false,
					),
					'password' => array(
						'required' => true,
						'type' => 'string',
						'empty' => false,
						'error' => 'Mot de passe vide',
					),
					'repassword' => array(
						'required' => true,
						'type' => 'string',
						'empty' => false,
						'error' => 'Mot de passe re-entré vide',
					),
					'code' => array(
						'required' => true,
						'type' => 'string',
						'empty' => false,
						'error' => 'Code d\'activation vide',
					),
				));

				if ($data['password'] != $data['repassword']) {
					throw new ControllerParameterException('repassword', 'pwmatch' ,'Les deux mots de passe sont différents');
				} else if (mb_strlen($data['password']) < 5) {
					throw new ControllerParameterException('password', 'minlength', 'Le mot de passe doit avoir plus de 5 caractères');
				}

				if (!RegisterCodes::isCodeActive($data['code'])) {
					throw new ControllerParameterException('code', 'codeInvalid', 'Le code d\'invitation n\'est pas valide, entrez un autre code');
				}

				$action = new ActionCreateUser($data);
				$result = $action->execute();

				if ($result->success()) {
					$user = $result->result();

					if ($user->state == State::ACTIVE) {
						throw new Exception('Le compte avec l\'email `' . $data['email'] . '` existe déjà, veuillez vous connecter.');
					}

					RegisterCodes::setUsed($data['code'], $user);

					$sendActivationEmail = new ActionSendActivationEmail(compact('user'));
					$result = $sendActivationEmail->execute();

					if ($result->success()) {
						$this->_buildJsonResponse(array('message' => 'Un mail d\'activation a été envoyé à `' . $data['email'] . '`'));
					} else {
						$this->_buildJsonResponse($result->getException(), false);
					}
				} else {
					$this->_buildJsonResponse($result->getException(), false);
				}
			} catch (ControllerParameterException $e) {
				$this->_buildJsonResponse($e, false);
			} catch (Exception $e) {
				$this->_buildJsonResponse($e, false);
			}

		} else {
			try {
				$data = $this->_getParameters(array(
					'code' => array(
						'required' => false,
						'type' => 'string',
						'default' => '',
						'empty' => true,
					),
					'email' => array(
						'required' => false,
						'type' => 'string_email',
						'empty' => true,
						'default' => '',
					),
				));
			} catch (ControllerParameterException $e) {
				$data = array();
			}

			$this->set($data);
		}
	}

	/**
	 * [lostPassword description]
	 * @return [type] [description]
	 */
	public function lostPassword() {

		if (!$this->isUserAnonymous()) {
			return $this->redirect('/');
		}

		// TODO
	}

	/**
	 * [activate description]
	 * @param  [type] $code [description]
	 * @return [type]       [description]
	 */
	public function activate($code = null) {

		if (!$this->isUserAnonymous()) {
			return $this->redirect('/');
		}

		$user = Users::firstByValidationCode($code);

		if (!empty($user)) {
			$action = new ActionActivateUser(compact('user'));
			$result = $action->execute();

			if ($result->success()) {
				$action = new ActionConnectUser(compact('user'));
				$result = $action->execute();

				if ($result->success()) {
					$url = Visitor::getLastPage();
					return $this->redirect(empty($url) ? '/' : $url['url']);
				}
			}
		}
	}

	/**
	 * [logout description]
	 * @return [type] [description]
	 */
	public function logout() {

		$action = new ActionDisconnectUser();
		$action->user($this->_currentUser);
		$result = $action->execute();

		if ($result->success()) {
			$url = Visitor::getLastPage(true);
			return $this->redirect(empty($url) ? '/' : $url['url']);
		}
	}

	/**
	 * [login description]
	 * @param  [type] $api [description]
	 * @return [type]      [description]
	 */
	public function login($api = null) {
		if (!$this->isUserAnonymous()) {
			return $this->redirect('/');
		}

		if (!empty($api)) {
			// Auth process
			$oauthService = OAuthService::getService($api);

			if ($oauthService->isAuthentificated()) {
				$userInfo = $oauthService->getUserInfo();

				$action = new ActionLoginProcessFromApiData(compact('userInfo'));
				$result = $action->execute();

				if (!$result->success()) {
					$e = $result->getException();
					d(ExceptionUtil::toString($e));
				}

			} else {
				$urlRedirect = $oauthService->getUrlForAuthentificate(
					$this->_getCallbackUrl($api)
				);
				return $this->redirect($urlRedirect);
			}
		} else if ($this->request->type() == 'json') {

			try {

				$data = $this->_getParameters(array(
					'email' => array(
						'required' => true,
						'type' => 'string_email',
						'error' => 'Email vide',
						'empty' => false,
					),
					'password' => array(
						'required' => true,
						'type' => 'string',
						'empty' => false,
						'error' => 'Mot de passe vide',
					),
				));

				$action = new ActionLoginUser($data);
				$result = $action->execute();

				if ($result->success()) {
					$url = Visitor::getLastPage();
					$this->_buildJsonResponse(array('url' => empty($url) ? '/' : $url['url']));
				} else {
					$this->_buildJsonResponse(self::LOGIN_ERROR_MESSAGE, false);
				}
			} catch (ControllerParameterException $e) {
				$this->_buildJsonResponse(self::LOGIN_ERROR_INPUT, false);
			} catch (Exception $e) {
				$this->_buildJsonResponse(self::LOGIN_ERROR_MESSAGE, false);
			}

		}
	}

	/**
	 * [callback description]
	 * @param  [type]   $api [description]
	 * @return function      [description]
	 */
	public function callback($api) {
		$oauthService = OAuthService::getService($api);
		$oauthService->onAuthorizeCallback($this->request, $this->_getCallbackUrl($api));

		if ($oauthService->isAuthentificated()) {
			return $this->redirect(array(
				'controller' => 'Auth',
				'action' => 'login',
				'args' => array($api),
			));
		} else {
			throw new Exception("Error");
		}
	}
}

?>