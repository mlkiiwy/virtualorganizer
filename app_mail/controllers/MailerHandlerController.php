<?php

namespace app\controllers;

use common\actions\ActionCreateIncomingEmail;

use Exception;

/**
 *
 */
class MailerHandlerController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('incoming'),
		'xhrOnly' => array(),
	);

	/**
	 *
	 */
	public function incoming() {
		global $HTTP_RAW_POST_DATA_COPY;

		$action = new ActionCreateIncomingEmail(array(
			'content' => $HTTP_RAW_POST_DATA_COPY,
		));
		$result = $action->execute();

		if (!$result->success()) {
			throw $result->getException();
		}
	}
}

?>