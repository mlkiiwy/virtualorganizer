<?php

namespace app\controllers;

use common\actions\ActionAnswerToAgentTopic;

use common\util\Url;
use common\util\ExceptionUtil;

use common\extensions\storage\Visitor;

use common\models\AgentAnswers;

use lithium\analysis\Logger;

use Exception;

/**
 *
 */
class AgentAnswersController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('answer'),
		'xhrOnly' => array(),
	);

	/**
	 *
	 */
	public function answer($agentTopicId, $userId, $value) {
		$action = new ActionAnswerToAgentTopic(array(
			'user' => $userId,
			'agentTopic' => $agentTopicId,
			'answer' => $value,
		));
		$action->user($this->populateActualUser());
		$result = $action->execute();

		$data = $action->getParams();
		$topicId = $data['agentTopic']->topic_id;

		$message = array('type' => 'alert', 'message' => 'Impossible d\'enregistrer votre réponse, une erreur est survenue');

		if ($result->success()) {
			switch ($value) {
				case AgentAnswers::VALUE_YES_OR_POSITIVE:
					$message = array('type' => 'success', 'message' => 'Votre réponse <strong>positive</strong> a été enregistrée');
					break;

				case AgentAnswers::VALUE_NO_OR_NEGATIVE:
					$message = array('type' => 'success', 'message' => 'Votre réponse <strong>négative</strong> a été enregistrée');
					break;

				case AgentAnswers::VALUE_MAYBE:
					$message = array('type' => 'success', 'message' => 'Votre réponse a été enregistrée');
					break;
			}
		} else {
			Logger::write('error', ExceptionUtil::toString($result->getException()));
		}

		Visitor::addFlash($message);

		$url = Url::url(array(
			'controller' => 'Topics',
			'action' => 'index',
		), array('#' => array('view', $topicId)));

		return $this->redirect($url);
	}
}

?>