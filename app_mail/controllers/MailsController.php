<?php

namespace app\controllers;

use common\actions\ActionOpenEmail;

use Exception;

/**
 *
 */
class MailsController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('open'),
		'image' => array('open'),
	);

	/**
	 * [open description]
	 * @param  [type] $rand [description]
	 * @return [type]       [description]
	 */
	public function open($rand) {
		$action = new ActionOpenEmail(array('email' => $this->_currentEmail));
		$result = $action->execute();

		if ($result->success()) {
			return $this->_responseOnePixelPng();
		} else if ($result->getException()) {
			throw $result->getException();
		} else {
			throw new Exception('Unknow error');
		}
	}
}

?>