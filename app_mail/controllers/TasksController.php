<?php

namespace app\controllers;

use common\actions\base\Action;

use common\util\ExceptionUtil;

use lithium\analysis\Logger;

use Exception;

/**
 *
 */
class TasksController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('action'),
		'xhrOnly' => array('action'),
	);

	/**
	 * [action description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public function action($name) {
		$action = Action::createByName($name);
		$action->input($this->request->data);
		$result = $action->execute();

		if (!$result->success()) {
			if ($result->getException()) {
				Logger::write('error', ExceptionUtil::toString($result->getException()));
				throw $result->getException();
			} else {
				throw new Exception('unknow error');
			}
		} else {
			$this->_buildJsonResponse(true);
		}
	}
}

?>