<?php

namespace app\controllers;

use Exception;

/**
 *
 */
class HomeController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('index'),
		'xhrOnly' => array(),
		'xhr' => array(),
		'nofollow' => array(),
	);

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index() {

	}
}

?>