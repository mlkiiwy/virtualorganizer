<?php

namespace app\controllers;

use lithium\core\Environment;

use common\net\http\UrlException;
use common\net\http\PrivateUrlException;

use common\extensions\auth\Auth;

use common\extensions\storage\Visitor;

use common\util\Types;
use common\util\ExceptionUtil;

use common\models\Emails;
use common\actions\ActionClickEmail;

use common\extensions\exceptions\ControllerParameterException;
use Exception;

/**
 * Base controller. All other controllers should inherit from this base controller.
 */
class AppBaseController extends \lithium\action\Controller {

	/**
	 *
	 * @var array
	 */
	protected $_actionsConfig = array();

	/**
	 * Currently logged-in user.
	 *
	 * @see \common\models\Users
	 */
	protected $_currentUser = null;

	/**
	 * [$_currentEmail description]
	 * @var [type]
	 */
	protected $_currentEmail = null;

	/**
	 * [$_actualUser description]
	 * @var [type]
	 */
	protected $_actualUser = null;

	/**
	 *
	 */
	protected function _init() {
		$this->_render['negotiate'] = true;
		parent::_init();

		$this->_currentUser = Auth::getUser();
		$this->_identificateEmail();

		$this->populateActualUser();
	}

	/**
	 * [_identificateEmail description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	protected function _identificateEmail(array $options = array()) {
		$defaults = array('click' => true);
		$options += $defaults;

		$code = empty($this->request->query['mailCode'])
			? '' : $this->request->query['mailCode'];

		if (!empty($code)) {
			$email = Emails::getByField('code', $code);

			if (!empty($email)) {
				$this->_currentEmail = $email;

				Visitor::setEmailEntry($email);
			}
		}

		// Auto execute click action if not open email action
		if (!empty($this->_currentEmail) && !$this->_isFakeImageAction()) {
			$click = new ActionClickEmail(array('email' => $this->_currentEmail));
			$result = $click->execute();

			if (!$result->success() && $result->getException()) {
				throw $result->getException();
			}
		}

		return $this->_currentEmail;
	}

	/**
	 * Calls a method on this object with the given parameters. Provides an OO wrapper
	 * for call_user_func_array, and improves performance by using straight method calls
	 * in most cases.
	 *
	 * @param string $method  Name of the method to call
	 * @param array $params  Parameter list to use when calling $method
	 * @return mixed  Returns the result of the method call
	 */
	public function invokeMethod($method, $params = array()) {
		$this->_saveVisitorJourney();
		$this->_checkAccessibility();
		$this->_populateGlobalVariables();

		return parent::invokeMethod($method, $params);
	}

	/**
	 * [_saveVisitorJourney description]
	 * @return [type] [description]
	 */
	protected function _saveVisitorJourney() {
		$noSave = $this->_isXhrAction() || $this->_isUnfollowedAction();

		if (!$noSave) {
			Visitor::addPage($this->request, array('private' => !$this->_isPublicAction()));
		}
	}

	/**
	 * [_populateGlobalVariables description]
	 * @return [type] [description]
	 */
	protected function _populateGlobalVariables() {
		global $userEnvironment;

		if ($this->request->type() != 'json') {
			$global = array(
				'environment' => Environment::get(true),
				'inProduction' => $userEnvironment == 'production',
			);
			$this->set(compact('global'));
		}
	}

	/**
	 * [_isUnfollowedAction description]
	 * @return boolean [description]
	 */
	protected function _isUnfollowedAction() {
		$action = empty($action) ? $this->request->action : $action;
		$nofollowActions = empty($this->_actionsConfig['nofollow']) ?
			array() : $this->_actionsConfig['nofollow'];
		return in_array($action, $nofollowActions);

	}

	/**
	 * [_isFakeImageAction description]
	 * @return boolean [description]
	 */
	protected function _isFakeImageAction() {
		$action = empty($action) ? $this->request->action : $action;
		$images = empty($this->_actionsConfig['image']) ?
			array() : $this->_actionsConfig['image'];
		return in_array($action, $images);

	}

	/**
	 *
	 */
	protected function _checkAccessibility() {
		$public = $this->_isPublicAction();
		if ($this->isUserAnonymous() && !$public) {
			throw new PrivateUrlException;
		}

		$xhrAction = $this->_isXhrAction();
		if ($this->_isXhrRequest() && !$xhrAction) {
			throw new UrlException();
		}
	}

	/**
	 *
	 */
	protected function _isPublicAction($action = null) {
		$action = empty($action) ? $this->request->action : $action;
		$publicActions = empty($this->_actionsConfig['public']) ?
			array() : $this->_actionsConfig['public'];
		return in_array($action, $publicActions);
	}

	/**
	 *
	 */
	public function isUserAnonymous() {
		return empty($this->_currentUser);
	}

	/**
	 *
	 */
	protected function _isXhrAction($action = null) {
		$action = empty($action) ? $this->request->action : $action;
		$xhrActions = empty($this->_actionsConfig['xhr']) ?
			array() : $this->_actionsConfig['xhr'];
		$xhrActions += empty($this->_actionsConfig['xhrOnly']) ?
			array() : $this->_actionsConfig['xhrOnly'];
		return in_array($action, $xhrActions);
	}

	/**
	 *
	 */
	protected function _isXhrRequest() {
		return $this->request->type() == 'json' || $this->request->type() == 'ajax';
	}


	/**
	 * [_getParameter description]
	 * @param  [type] $name    [description]
	 * @param  [type] $config  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	protected function _getParameter($name, $config, array $options = array()) {
		$defaults = array(
			'type' => array('POST', 'GET'),
		);
		$options += $defaults;

		$defaultParameterConfig = array(
			'required' => false,
			'default' => null,
			'empty' => true,
			'type' => 'string',
			'preformat' => null,
			'error' => 'error',
		);
		$config += $defaultParameterConfig;

		$value = null;
		foreach ($options['type'] as $key => $type) {
			$last = $key == (count($options['type']) - 1);
			$input = null;
			switch ($type) {
				case 'POST':
					$input = $this->request->data;
					break;
				case 'GET':
					$input = $this->request->query;
					break;
				default:
					throw new Exception("http type : `$type` not implemented");
					break;
			}

			if (!isset($input[$name])) {
				if ($config['required']) {
					if ($last) {
						throw new ControllerParameterException($name,
							$config['error'] . '1', ControllerParameterException::REQUIRED);
					} else {
						continue;
					}
				} else if ($last) {
					$value = $config['default'];
					break;
				}
			} else {
				$value = $input[$name];
				break;
			}
		}

		if (empty($value) && !$config['empty']) {
			throw new ControllerParameterException($name, $config['error']. '2', ControllerParameterException::PARAM_EMPTY);
		}

		if (!empty($config['preformat'])) {
			$func = $config['preformat'];

			$value = $func($value);
		}

		if (!empty($value)) {
			try {
				$value = Types::test($value, $config['type'], array('tryToCast' => true, 'error' => 'exception'));
			} catch (Exception $e) {
				throw new ControllerParameterException($name, $e->getMessage(), ControllerParameterException::TYPE);
			}
		}

		return $value;
	}

	/**
	 *
	 */
	protected function _getParameters($config, array $options = array()) {
		$defaults = array(
			'type' => array('POST', 'GET'),
		);
		$options += $defaults;

		$data = array();
		$errors = array();
		foreach ($config as $key => $parameterConfig) {
			try {
				$data[$key] = $this->_getParameter($key, $parameterConfig, $options);
			} catch (Exception $e) {
				$errors[$key] = $e;
			}
		}

		if (!empty($errors)) {
			$exception = new ControllerParameterException('parameters errors');
			$exception->errors = $errors;

			throw $exception;
		}

		return $data;
	}

	/**
	 *
	 */
	protected function _buildJsonResponse($data, $success = true, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($success)) {
			$result = array();
			if ($data instanceof ControllerParameterException) {
				$message = $data->getMessage();
				$message = empty($message) ? 'Parameter error on : ' : $message;
				$result = array('message' => $message);
				if (!empty($data->errors)) {
					$result['details'] = array();
					foreach ($data->errors as $e) {
						if ($e instanceof ControllerParameterException) {
							$result['details'][$e->paramName] = array(
								'message' => $e->getMessage(),
								'type' => $e->type,
							);
						} else if ($e instanceof Exception) {
							$result['details'][] = $e->getMessage();
						}
					}
				} else {
					$result['details'][$data->paramName] = array(
						'message' => $data->getMessage(),
						'type' => $data->type,
					);
				}
			} elseif ($data instanceof Exception) {
				$result = array('message' => ExceptionUtil::toString($data));
			} else {
				$result = array('message' => $data);
			}
		} else {
			$result = $data;
		}

		$json = array(
			'success' => $success,
			'data' => $result,
		);

		$flashs = Visitor::getFlashs();
		if (!empty($flashs)) {
			$json += compact('flashs');
		}

		$this->set(compact('json'));
	}

	/**
	 *
	 */
	public function render(array $options = array()) {
		$this->_refreshCurrentUser();
		$this->_refreshActualUser();

		return parent::render($options);
	}

	/**
	 * [_refreshCurrentUser description]
	 * @return [type] [description]
	 */
	protected function _refreshCurrentUser() {
		if ($this->request->type() != 'json') {
			$data = $this->_currentUser ? $this->_currentUser->data() : null;
			$this->set(array('currentUser' => $data));
		}
	}

	/**
	 * [_refreshActualUser description]
	 * @return [type] [description]
	 */
	protected function _refreshActualUser() {
		if ($this->request->type() != 'json') {
			$actualUser = $this->populateActualUser();
			if (!empty($actualUser)) {
				$actualUser = $actualUser->data();
				$this->set(compact('actualUser'));
			}
		}
	}

	/**
	 * [_responseOnePixelPng description]
	 * @return [type] [description]
	 */
	protected function _responseOnePixelPng() {
		// Generate a transparent png 1px/1px
		header('content-type:image/png');

		$image = @imagecreatetruecolor(1, 1);
		imagealphablending($image, false);
		imagesavealpha($image, true);
		$trans_layer_overlay = imagecolorallocatealpha($image, 220, 220, 220, 127);
		imagefill($image, 0, 0, $trans_layer_overlay);
		imagepng($image);
		imagedestroy($image);

		die();
	}

	/**
	 * [populateActualUser description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	protected function populateActualUser(array $options = array()) {
		$defaults = array(
			'refresh' => false,
			'priorities' => array('connected', 'inSession', 'email'),
		);
		$options += $defaults;

		if ($this->_actualUser !== null && !$options['refresh']) {
			return $this->_actualUser;
		}

		$actualUser = false;
		foreach ($options['priorities'] as $type) {
			switch ($type) {
				case 'connected':
					if (!empty($this->_currentUser)) {
						$actualUser = $this->_currentUser;
					}
					break;

				case 'inSession':
					$actualUser = Visitor::getEmailUser();
					break;

				case 'email' :
					if (!empty($this->_currentEmail)) {
						$actualUser = $this->_currentEmail->related('user');
					}
					break;
			}
		}

		$this->_actualUser = $actualUser;

		return $this->_actualUser;
	}

	/**
	 * [_generateExtraUrlFromEmail description]
	 * @return [type] [description]
	 */
	protected function _generateExtraUrlFromEmail() {
		if (!empty($this->_currentEmail)) {
			return array('extra' => array('mailCode' => $this->_currentEmail->code));
		}
		return array('extra' => array());
	}
}

?>