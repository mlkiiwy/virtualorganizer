<?php

namespace app\controllers;

use common\actions\ActionShowAgent;

use Exception;

/**
 *
 */
class AgentsController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('show'),
		'image' => array('show'),
		'xhrOnly' => array(),
	);

	/**
	 * [show description]
	 * @param  [type] $agentTopicId [description]
	 * @param  [type] $rand         [description]
	 * @return [type]               [description]
	 */
	public function show($agentTopicId, $rand) {
		$action = new ActionShowAgent(array('agentTopic' => $agentTopicId));
		$action->user($this->populateActualUser());
		$result = $action->execute();

		if ($result->success()) {
			return $this->_responseOnePixelPng();
		} else {
			throw $result->getException();
		}
	}
}

?>