<?php

namespace app\controllers;

use Exception;

use common\models\Agents;
use common\models\Users;
use common\models\Emails;
use common\models\AgentTopics;
use common\models\Topics;

use common\agents\SimpleQuestionAgent;

use lithium\net\http\Media;
use lithium\net\http\Router;

use common\util\Url;
use common\extensions\mailer\Mail;

/**
 *
 */
class MailTestsController extends AppBaseController {

	/**
	 * [_init description]
	 * @return [type] [description]
	 */
	protected function _init() {
		global $_SERVER;

		if (!empty($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] !== '/mail_tests') {
			/**
			 * Change the media type
			 */
			Media::type('twig', array('text/html', 'application/xhtml+xml', '*/*'), array(
				'view' => 'li3_twig\template\View',
				'loader' => 'li3_twig\template\Loader',
				'renderer' => 'li3_twig\template\view\adapter\Twig',
				'paths' => array(
					'template' => array(
						'{:library}/views/mails/{:template}.mail.html.twig',
						'{:library}/views/layouts'
					)
				)
			));
		}

		parent::_init();

		$this->_render['data'] = array();
	}

	/**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index() {

		$templates = array(
			array('action' => 'simple', 'name' => 'Simple mail with content'),
			array('action' => 'simpleQuestion', 'name' => 'Mail with a question'),
			array('action' => 'activation', 'name' => 'System mail activation'),
		);

		$links = array();
		foreach ($templates as $data) {
			$link = array('controller' => 'MailTests', 'action' => $data['action']);
			if (isset($data['params'])) {
				$link['args'] = $data['params'];
			}
			$url = Router::match($link);
			$links[] = array('url' => $url, 'name' => $data['name'], 'url_send' => $url.'?send=1');
		}

		$this->set(compact('links'));
	}

	/**
	 * Override to send mail of needed before rendering view
	 */
	public function render(array $options = array()) {
		$action = $this->request->params['action'];
		if ($action == 'index') {
			return parent::render($options);
		}

		// Overide templare
		$this->_render['template'] = 'byBlocks';

		$email = $this->_render['data']['email'];

		// Add dest if not set
		if (empty($email->user)) {
			$email->related('user', Users::testUser());
		}

		if (empty($email->code)) {
			$email->code = 'test-code';
		}

		if (empty($email->to)) {
			$email->to = $this->_currentUser->main_email;
		}

		if (empty($email->subject)) {
			$email->subject = 'test subject';
		}

		// Send email too ?
		if (!empty($this->request->query['send'])) {
			$to = array($email->to => $email->user->label);
			Mail::sendByTemplate($to, $email->subject, 'byBlocks', $email->from, array('email' => $email->data()));
		}

		$this->_render['data']['email'] = $email->data();

		Url::toggleAbsolute(true);
		Url::addParam('mailCode', empty($email->code) ? '' : $email->code);

		return parent::render($options);
	}

	/**
	 *
	 */
	public function simple() {
		$email = Emails::create();
		$email->related('user', $this->_currentUser);
		$email->addBlock('message', array('content' => 'Mon message'));
		$this->set(compact('email'));
	}

	/**
	 *
	 */
	public function simpleQuestion() {
		$email = Emails::create();
		$email->related('user', $this->_currentUser);

		// Widget section
		$agentTopic = AgentTopics::create(array(
			'id' => 1,
			'agent_id' => Agents::SIMPLE_QUESTION,
			'data_agent' => array('question' => 'Ciné ce soir ?'),
		));
		$agent = SimpleQuestionAgent::instance($agentTopic);
		$agentTopic->insertBlock($email, $this->_currentUser);

		// Content section
		$email->addBlock('message', array('content' => 'Mon message'));
		$email->addBlock('footer', array(
			'template' => 'footerBar',
		));

		// Add topic
		$topic = Topics::create(array('id' => 1, 'gen_count_users' => 3));
		$email->topic = $topic->data();

		$this->set(compact('email'));
	}

	/**
	 *
	 */
	public function activation() {
		$email = Emails::create();
		$email->related('user', $this->_currentUser);

		$email->addBlock('activation', array());

		$this->set(compact('email'));
	}

}

?>