<?php

namespace app\controllers;

use common\actions\queue\ActionBuildTopic;
use common\actions\ActionAddMessageToTopic;
use common\actions\ActionCreateSimpleQuestionAgent;

use common\models\Topics;
use common\models\Users;
use common\models\Emails;

use lithium\net\http\Media;

use common\util\RecordSet as RecordSetUtil;

use Exception;

/**
 *
 */
class SimulationsController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array(),
		'xhrOnly' => array('buildTopic', 'addMessage', 'addQuestion'),
	);

	/**
	 * [_init description]
	 * @return [type] [description]
	 */
	protected function _init() {
		global $_SERVER;

		if (!empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'index') !== false) {
			/**
			 * Change the media type
			 */
			Media::type('twig', array('text/html', 'application/xhtml+xml', '*/*'), array(
				'view' => 'li3_twig\template\View',
				'loader' => 'li3_twig\template\Loader',
				'renderer' => 'li3_twig\template\view\adapter\Twig',
				'paths' => array(
					'template' => array(
						'{:library}/views/{:controller}/{:template}.mail.html.twig',
						'{:library}/views/layouts'
					)
				)
			));
		}

		parent::_init();

		$this->_render['data'] = array();
	}

	/**
	 * [addMessage description]
	 */
	public function addMessage() {
		$user = empty($this->request->data['userId']) ? $this->_currentUser : Users::first($this->request->data['userId']);
		$message = empty($this->request->data['message']) ?
		'Nouveau message de ' . $user->label : $this->request->data['message'];
		$topic = empty($this->request->data['topicId']) ? null : Topics::first($this->request->data['topicId']);

		$action = new ActionAddMessageToTopic(compact('message', 'topic'));
		$action->user($user);

		$result = $action->execute();

		if ($result->success()) {
			$this->_buildJsonResponse(true);
		} else {
			$this->_buildJsonResponse($result->getException(), false);
		}
	}

	/**
	 * [addQuestion description]
	 */
	public function addQuestion() {
		$user = empty($this->request->data['userId']) ? $this->_currentUser : Users::first($this->request->data['userId']);
		$question = empty($this->request->data['question']) ?
		'Question de ' . $user->label : $this->request->data['question'];
		$topic = empty($this->request->data['topicId']) ? null : Topics::first($this->request->data['topicId']);

		$action = new ActionCreateSimpleQuestionAgent(compact('question', 'topic'));
		$action->user($user);

		$result = $action->execute();

		if ($result->success()) {
			// Imidiately launch a message
			$message = 'Une petite question !';

			$action = new ActionAddMessageToTopic(compact('message', 'topic'));
			$action->user($user);
			$result = $action->execute();

			if ($result->success()) {
				$this->_buildJsonResponse(true);
			} else {
				$this->_buildJsonResponse($result->getException(), false);
			}
		} else {
			$this->_buildJsonResponse($result->getException(), false);
		}
	}

	/**
	 * [buildTopic description]
	 * @return [type] [description]
	 */
	public function buildTopic() {
		$subject = 'Test de chain-mail';
		$emails = array(
			'user1@gmail.com',
			'user2@yahoo.com',
		);
		$message = 'Message de test des emails';
		$agents = array('question');
		$agentParams = array('question' => array('question' => 'La question', 'maxDateTime' => date('c')));

		// Build topic
		$build = new ActionBuildTopic(compact('subject', 'emails', 'message', 'agents', 'agentParams'));
		$build->user($this->_currentUser);
		$result = $build->execute();

		if ($result->success()) {
			$data = $result->result();
			$this->_buildJsonResponse(array('topic' => $data['topic']->data()));
		} else {
			$this->_buildJsonResponse($result->getException(), false);
		}
	}

	/**
	 * [index description]
	 * @param  [type] $topicId [description]
	 * @param  [type] $userId  [description]
	 * @return [type]          [description]
	 */
	public function index($topicId = null, $userId = null) {

		// Topics
		$currentTopic = empty($topicId) ? null : Topics::first($topicId);
		$topics = Topics::all();

		// Users
		$topicUsers = empty($currentTopic) ? array() : $currentTopic->populateUsers();
		if (!empty($topicUsers)) {
			$topicUsers->related('user');
		}
		$selectedUser = empty($userId) ? null : Users::first($userId);

		// Emails
		$conditions = array('Messages.topic_id' => $topicId);
		$with = array('Messages');
		$order = array('Emails.date_creation' => 'DESC');

		if (!empty($userId)) {
			$conditions += array('Emails.user_id' => $userId);
		}

		$emails = Emails::all(compact('conditions', 'with', 'order'));

		$data = RecordSetUtil::toData(compact('currentTopic', 'topics', 'selectedUser', 'topicUsers', 'emails'));

		$this->set($data);
	}
}

?>