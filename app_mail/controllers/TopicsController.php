<?php

namespace app\controllers;

use common\util\Types;

use common\actions\queue\ActionBuildTopic;

use common\models\Topics;

use common\util\RecordSet as RecordSetUtil;

use common\extensions\exceptions\ControllerParameterException;
use Exception;

/**
 *
 */
class TopicsController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array('view', 'index', 'user'),
		'xhrOnly' => array('create', 'build', 'view', 'user'),
	);

	/**
	 *
	 */
	public function create() {

	}

	/**
	 *
	 */
	public function index() {

	}

	/**
	 * [user description]
	 * @param  [type] $userId [description]
	 * @return [type]         [description]
	 */
	public function user($userId) {

		if (empty($userId)) {
			throw new Exception('no userId given');
		}

		$topics = Topics::allByUser($userId);
		$topics = $topics->data();

		$this->_buildJsonResponse($topics);
	}

	/**
	 * [view description]
	 * @param  [type] $topicId [description]
	 * @return [type]          [description]
	 */
	public function view($topicId = null) {

		if ($this->request->type() == 'json') {
			try {
				$topic = Topics::first($topicId);

				// Security
				if (empty($topic)) {
					throw new Exception('not a valid topic');
				}

				if (!$topic->hasUser($this->populateActualUser())) {
					throw new Exception('user is invalid');
				}

				$topicUsers = $topic->populateUsers(array('with_users' => true));
				$agents = $topic->getAgents();

				if (!empty($agents)) {
					foreach ($agents as $agent) {
						$topicUsers = $agent->mergeUserStatus($topicUsers);
					}
				}

				$topicUsers->populateStatusLabel();

				$topic->users = $topicUsers;
				$topic = $topic->data();

				$this->_buildJsonResponse($topic);
			} catch (Exception $e) {
				$this->_buildJsonResponse($e, false);
			}
		}
	}

	/**
	 *
	 */
	public function build() {
		try {

			$funcCutEmails = function ($input) {
				return explode(';', trim($input));
			};

			$data = $this->_getParameters(array(
				'emails' => array(
					'required' => true,
					'type' => 'string[]',
					'error' => 'Empty emails',
					'empty' => false,
					'preformat' => $funcCutEmails,
				),
				'subject' => array(
					'required' => true,
					'type' => 'string',
					'empty' => false,
					'error' => 'Empty subject',
				),
				'message' => array(
					'required' => true,
					'type' => 'string',
					'empty' => false,
					'error' => 'Empty message',
				),
				'agents' => array(
					'required' => false,
					'type' => 'string[]',
					'error' => 'Incorrect agents',
					'empty' => true,
					'default' => array(),
				),
			));

			$parametersPerAgent = array();

			if (!empty($data['agents'])) {

				$configPerAgent = array(
					'question' => array(
						'question' => array(
							'required' => true,
							'type' => 'string',
							'error' => 'Empty question',
							'empty' => false,
						),
						'maxDateTime' => array(
							'required' => false,
							'type' => 'string',
							'error' => 'Empty emails',
							'empty' => true,
							'default' => date('c'),
						),
					)
				);

				foreach ($data['agents'] as $agent) {
					if (empty($configPerAgent[$agent])) {
						throw new Exception("agent `$agent` is not supported");
					}

					$parametersPerAgent[$agent] = $this->_getParameters($configPerAgent[$agent]);
				}
			}

			$data['agentParams'] = $parametersPerAgent;

			// Do creation of topic
			$action = new ActionBuildTopic($data);
			$action->user($this->_currentUser);
			$result = $action->execute();

			if ($result->success()) {
				$result = $result->result();
				$topic = $result['topic'];

				$this->_buildJsonResponse($topic->data());
			} else {
				$this->_buildJsonResponse($result->getException(), false);
			}
		} catch (ControllerParameterException $e) {
			$this->_buildJsonResponse($e, true);
		} catch (Exception $e) {
			$this->_buildJsonResponse($e, false);
		}
	}
}

?>