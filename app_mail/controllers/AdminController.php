<?php

namespace app\controllers;

use common\data\database\Database;

use Exception;

/**
 *
 */
class AdminController extends AppBaseController {

	/**
	 * @var array
	 */
	protected $_actionsConfig = array(
		'public' => array(),
		'xhrOnly' => array(),
	);

	/**
	 * [install description]
	 * @return [type] [description]
	 */
	public function install() {
		if ($this->_currentUser->state_admin == 1) {
			$success = Database::reset();
			$this->set(compact('success'));
		}
	}
}

?>