angular.module('topics',
	['ngRoute', 'ngResource', 'mgcrea.ngStrap'])

.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
})

.config(function($routeProvider) {
	$routeProvider
		.when(
			'/create',
			{templateUrl: '/topics/create.ajax', controller: 'CreateController'}
		)
		.when(
			'/view/:topicId',
			{templateUrl: '/topics/view.ajax', controller: 'ViewController'}
		)
		.otherwise({
			redirectTo:'/create'
		});
})

.service('FlashService', function() {
	this.setScope = function(scope) {
		this.$scope = scope;

		this.$scope.flashs = [];
	};

	this.setFlashs = function(flashs) {
		this.$scope.flashs = flashs;
	}
})

.factory('myHttpInterceptor', function($q, FlashService) {
	return {
		// optional method
		'request': function(config) {
			// do something on success
			return config || $q.when(config);
		},

		// optional method
		'requestError': function(rejection) {
			// do something on error
			if (canRecover(rejection)) {
				return responseOrNewPromise
			}
			return $q.reject(rejection);
		},

		// optional method
		'response': function(response) {
			if (response && response.config.url.indexOf('json') !== -1) {
				if (response.data.flashs && response.data.flashs.length > 0) {

					FlashService.setFlashs(response.data.flashs);

					delete response.data.flashs;
				}

				if (response.data.success == true) {
					response.data = response.data.data;
				} else {
					var message = response.data.data.message && response.data.data.message.length > 0 ?
						response.data.data.message : 'unknow error';
					alert('Error : ' + message);
					return $q.reject(response);
				}
			}
			// do something on success
			return response || $q.when(response);
		},

		// optional method
	 	'responseError': function(rejection) {
			// do something on error
			if (rejection.status == 401) {
				var data = rejection.data,
					splits = data.split('\t');

				window.location.href = splits[0];
			} else if (canRecover(rejection)) {
				return responseOrNewPromise
			}
			return $q.reject(rejection);
		}
	};
})

.config(function($httpProvider) {
	$httpProvider.interceptors.push('myHttpInterceptor');
})

.factory('Topics', ['$resource',
	function($resource){
		 return $resource('/topics/:topicId.json', {}, {
			query: {method:'GET', 'url' : '/topics/view/:topicId.json'},
			build: {method:'POST', 'url' : '/topics/build.json'},
			user: {method:'GET', 'url' : '/topics/user/:userId.json'}
	});
}])

.controller('MainController',function($scope, $routeParams, $timeout, Topics, FlashService) {
	$scope.topic = {'subject' : '---'};
	$scope.topics = [];

	$scope.userId = null;

	$scope.topicUsers = [];

	FlashService.setScope($scope);

	$scope.setUserId = function (value) {
		$scope.userId = value;
		$scope.topics = Topics.user({userId: $scope.userId});
	}
})

.controller('ViewController',function($scope, $routeParams, $timeout, Topics) {
	$scope.topic = Topics.query({topicId: $routeParams.topicId}, function(topic) {
		$scope.topicUsers = topic.users;

		for(i in $scope.topicUsers) {
			var label, cssClassLabel;
			switch ($scope.topicUsers[i].statusLabel.type) {
				case 'unknow' :
					label = $scope.topicUsers[i].statusLabel.label;
					cssClassLabel = 'default';
					break;
				case 'yes' :
					label = $scope.topicUsers[i].statusLabel.label;
					cssClassLabel = 'success';
					break;
				case 'no' :
					label = $scope.topicUsers[i].statusLabel.label;
					cssClassLabel = 'danger';
					break;
				case 'maybe' :
					label = $scope.topicUsers[i].statusLabel.label;
					cssClassLabel = 'info';
					break;
				case 'no_answer' :
					label = $scope.topicUsers[i].statusLabel.label;
					cssClassLabel = 'warning';
					break;
			}
			$scope.topicUsers[i].label = label;
			$scope.topicUsers[i].cssClassLabel = cssClassLabel;
		}
	});
})

.controller('CreateController', function($scope, $location, $timeout, Topics) {
	$scope.formBuild = {'agents' : ['question']};

	$scope.timePreSelections = [
		{ "value": 'tonight20', "label" : 'Ce soir 20h' },
		{ "value": 'tomorrow20', "label" : 'Demain soir 20h' },
		{ "value": 'in6hours', "label" : 'Dans 6 heures' },
		{ "value": 'in12Hours', "label" : 'Dans 12 Heures' },
		{ "value": 'custom', "label" : 'Custom' },
	];
	$scope.timePreSelection = 'tonight20';

	// Init
	var now = moment();
	now.hour(20);
	now.minute(0);
	now.second(0);
	$scope.formBuild.maxDateTime = now.format();

	$scope.$watch(
		'timePreSelection',
		function (newValue, oldValue) {
			if (newValue == oldValue) {
				return;
			}

			var now = moment();

			switch (newValue) {
				case 'tonight20':
					now.hour(20);
					now.minute(0);
					now.second(0);
					$scope.formBuild.maxDateTime = now.format();
					break;

				case 'tomorrow20':
					now.hour(20);
					now.minute(0);
					now.second(0);
					now.add('days', 1);
					$scope.formBuild.maxDateTime = now.format();
					break;

				case 'in6hours':
					now.second(0);
					now.add('hours', 6);
					$scope.formBuild.maxDateTime = now.format();
					break;

				case 'in12Hours':
					now.second(0);
					now.add('hours', 12);
					$scope.formBuild.maxDateTime = now.format();
					break;

				case 'custom':
					break;
			}
		}
	);

	$scope.save = function() {
		var result = Topics.build($scope.formBuild, function(topic) {
			$location.path('/view/' + topic.id);
		});
	}
});