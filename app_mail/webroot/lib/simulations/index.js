angular.module('simulation', ['ngResource'])

.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
})

.controller('SimulationController', function($scope, $http) {

	$scope.buildNewTopic = function() {
		$http.post('/simulations/buildTopic.json').success(function(result) {
			if (result.success) {
				window.location = '/simulations/index/' + result.data.topic.id;
			} else {
				alert(result.data.message);
			}
		});
	};

	$scope.addMessage = function() {
		var data = {
				message : $scope.messageText,
				userId: 0,
				topicId: 0
			},
			url = window.location.href.split('/');

		if (url.length != 7) {
			return;
		}
		data.userId = url[6];
		data.topicId = url[5];
		$http.post('/simulations/addMessage.json', data).success(function(result) {
			if (result.success) {
				window.location.reload();
			} else {
				alert(result.data.message);
			}
		});
	};

	$scope.addQuestion = function() {
		var data = {
				question : $scope.questionText,
				userId: 0,
				topicId: 0
			},
			url = window.location.href.split('/');

		if (url.length != 7) {
			return;
		}
		data.userId = url[6];
		data.topicId = url[5];
		$http.post('/simulations/addQuestion.json', data).success(function(result) {
			if (result.success) {
				window.location.reload();
			} else {
				alert(result.data.message);
			}
		});
	};
});