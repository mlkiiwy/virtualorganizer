angular.module('register', ['ngResource', 'ngSanitize'])

.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
})

.directive('pwCheck', [function() {
	return {
		require:'ngModel',
		link: function(scope, elem, attrs, ctrl) {
			var firstPassword = '#' + attrs.pwCheck;
			elem.add(firstPassword).on('keyup', function () {
				scope.$apply(function () {
					var v = elem.val() === $(firstPassword).val();
					ctrl.$setValidity('pwmatch', v);
				});
			});
		}
	}
}])

.controller('RegisterController', function($scope, $http) {
	$scope.account = {email: '', password: '', repassword:'', code: ''};
	$scope.errorMessage = false;
	$scope.registering = false;
	$scope.goodMessage = false;

	$scope.initForm = function(data) {
		$scope.account.code = data.code;
		$scope.account.email = data.email;
	}

	$scope.register = function() {
		$scope.errorMessage = false;
		$scope.registering = true;

		$http.post('register.json', $scope.account).success(function(result) {
			$scope.registering = false;

			if (result.success) {
				$scope.goodMessage = result.data.message;
				$scope.registering = true;
			} else {
				if (result.data.details) {
					var param  = '';
					$scope.errorMessage = '';
					for ( param in result.data.details) {
						var detail = result.data.details[param];
						if (detail.type && $scope.registerForm[param] && (detail.type in $scope.registerForm[param].$error)) {
							$scope.registerForm[param].$setValidity(detail.type, false);
						} else {
							$scope.errorMessage += $scope.errorMessage == '' ? '' : '<br>';
							$scope.errorMessage += detail.message;
						}
					}
				} else {
					$scope.errorMessage = result.data.message;
				}
			}
		});
	};
});