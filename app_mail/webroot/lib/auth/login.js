angular.module('login', ['ngResource'])

.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('{[{').endSymbol('}]}');
})

.controller('LoginController', function($scope, $http, $interval) {
	$scope.credential = {email: '', password: ''};
	$scope.errorMessage = false;
	$scope.logging = false;

	$scope.autoFillCheckFunc = function() {
		if ($scope.credential.password != loginForm.password.value) {
			$scope.loginForm.password.$setViewValue(loginForm.password.value);
			if ($scope.autoFillCheck) {
				$interval.cancel($scope.autoFillCheck);
				$scope.autoFillCheck = null;
			}
		}
		console.log('check');
	};

	// For auto-fill by navigator
	$(loginForm.email).on('blur', $scope.autoFillCheckFunc);

	// For auto-fill by navigator (check for change every 500ms)
	$scope.autoFillCheck = $interval($scope.autoFillCheckFunc, 500);

	$scope.login = function() {
		$scope.errorMessage = false;
		$scope.logging = true;

		$http.post('login.json', $scope.credential).success(function(result) {
			$scope.logging = false;

			if (result.success) {
				window.location = result.data.url;
			} else {
				$scope.errorMessage = result.data.message;
			}
		});
	};
});