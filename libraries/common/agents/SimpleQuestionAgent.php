<?php

namespace common\agents;

use common\models\AgentAnswers;
use common\models\AgentReaskReasons;
use common\models\AgentReasks;

use common\actions\ActionAddAgentMessageToTopic;

use Exception;

class SimpleQuestionAgent extends Agent {

	const NB_REASK = 3;
	const MIN_TIME_BETWEEN_REASK = 10800; // 60 * 60 * 6 = 6 Hours

	protected $_config = array(
		'hasBlock' => true,
		'blockDefault' => array(
			'name' => 'simpleQuestion',
			'data' => array(
				'agentTopicId' => null,
				'template' => 'question',
				'question' => '',
				'choices' => array(
					AgentAnswers::VALUE_YES_OR_POSITIVE => 'Oui',
					AgentAnswers::VALUE_NO_OR_NEGATIVE => 'Non',
					AgentAnswers::VALUE_MAYBE => 'Peut-être',
				),
				'selected' => false,
			),
			'position' => false,
		),
		'execution' => array(
			'period' => 60,
		),
		'messages' => array(
			'reasks' => array(
				AgentReaskReasons::AFTER_FIRST_READING => array(
					'template' => 'reask_after_first_reading',
					'type' => 'reask',
					'agent_reask_reason_id' => AgentReaskReasons::AFTER_FIRST_READING,
				),
				AgentReaskReasons::BASIC_AFTER_SOME_TIME => array(
					'template' => 'reask_after_some_time',
					'type' => 'reask',
					'agent_reask_reason_id' => AgentReaskReasons::BASIC_AFTER_SOME_TIME,
				),
			)
		),
	);

	/**
	 * [getBlock description]
	 * @interface IBlockMaker
	 * @param  [type] $topic   [description]
	 * @param  [type] $user    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function getBlock($user = null, array $options = array()) {
		$defaults = array('default' => false);
		$options += $defaults;

		$block = parent::getBlock($user, $options);

		if ($options['default']) {
			return $block;
		}

		if (empty($this->_agentTopic->data_agent['question'])) {
			throw new Exception('SimpleQuestionAgent data_agent question is empty');
		} else {
			$block['data']['question'] = $this->_agentTopic->data_agent['question'];
		}

		if (!empty($user)) {
			$answer = AgentAnswers::answerForAgentAndUser($this->_agentTopic->id, $user->id);
			$block['data']['selected'] = empty($answer) ? false : $answer->value;
		}

		return $block;
	}

	/**
	 * [userData description]
	 * @return [type] [description]
	 */
	public function userData() {
		$data = parent::userData();

		$answers = AgentAnswers::allForAgentTopicId($this->_agentTopic->id);

		foreach ($answers as $answer) {
			$data[$answer->user_id] = array('answer' => $answer->data());
		}

		return $data;
	}

	/**
	 * [execute description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function execute(array $options = array()) {
		$result = parent::execute($options);

		if (!$result) {
			return $result;
		}

		// Check responses and re-ask if not reply

		$topicUsers = $this->_agentTopic->allTopicUsersActives();
		$answers = AgentAnswers::allForAgentTopicId($this->_agentTopic->id);

		$dataByUserId = array();
		foreach ($topicUsers as $topicUser) {
			$dataByUserId[$topicUser->user_id] = array(
				'topicUser' => $topicUser,
				'answer' => null,
			);
		}

		foreach ($answers as $answer) {
			if (isset($dataByUserId[$answer->user_id])) {
				$dataByUserId[$answer->user_id]['answer'] = $answer;
			}
		}

		$lifetime = $this->_agentTopic->getTotalLifeTime();
		$nbReask = $lifetime / self::MIN_TIME_BETWEEN_REASK;
		$nbReask = $nbReask >= self::NB_REASK ? self::NB_REASK : $nbReask;

		$currentTime = time();

		$userIdsToReask = array();
		foreach ($dataByUserId as $userId => $userData) {
			if (empty($userData['answer']) || $userData['answer']->value == AgentAnswers::VALUE_NO_ANSWER) {

				$reason = empty($userData['answer']) ?
					AgentReaskReasons::BASIC_AFTER_SOME_TIME : AgentReaskReasons::AFTER_FIRST_READING;

				$userIdsToReask[$userId] = array(
					'reason' => $reason,
					'reasks' => array(),
				);
			}
		}

		$actions = array();
		if (!empty($userIdsToReask)) {

			// Check relances
			$reasks = AgentReasks::findByAgentTopicId($this->_agentTopic->id);

			foreach ($reasks as $reask) {
				if (isset($userIdsToReask[$reask->user_id])) {
					$userIdsToReask[$reask->user_id]['reasks'][] = $reask;
				}
			}

			foreach ($userIdsToReask as $userId => $reaskData) {
				if (count($reaskData['reasks']) > 0 && $reaskData['reason'] == AgentReaskReasons::AFTER_FIRST_READING) {
					$reaskData['reason'] = AgentReaskReasons::BASIC_AFTER_SOME_TIME;
				}

				if (count($reaskData['reasks']) >= $nbReask) {
					$reaskData['reason'] = null;
				}

				$newReaskCount = count($reaskData['reasks']) + 1;
				$checkTime = $newReaskCount * self::MIN_TIME_BETWEEN_REASK;

				if ($currentTime < (strtotime($this->_agentTopic->date_creation) + $checkTime)) {
					$reaskData['reason'] = null;
				}

				if (!empty($reaskData['reason'])) {
					// Do the Reask
					$action = new ActionAddAgentMessageToTopic(array(
							'agentTopic' => $this->_agentTopic,
							'user' => $userId,
							'message' => $this->_config['messages']['reasks'][$reaskData['reason']],
					));
					$action->delayed();

					$actions[] = $action;
				}
			}
		}

		return true;
	}

	/**
	 * [onMessageAdded description]
	 * @param  [type] $message [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function onMessageAdded($message, array $options = array()) {
		return AgentReasks::build($message);
	}

	/**
	 * [populateEndExecution description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function populateEndExecution(array $options = array()) {
		$this->_agentTopic->date_end_execution = $this->_agentTopic->data_agent['maxDateTime'];
		return $this->_agentTopic->date_end_execution;
	}

}

?>