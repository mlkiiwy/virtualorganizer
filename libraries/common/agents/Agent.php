<?php

namespace common\agents;

use common\interfaces\IBlockMaker;
use common\interfaces\ITaskable;

use common\actions\ActionExecuteAgentTopic;

use common\data\constants\State;

use common\util\Date as DateUtil;

use DateTime;
use DateInterval;

use Exception;

class Agent implements IBlockMaker, ITaskable {

	protected $_config = array(
		'hasBlock' => false,
		'execution' => array(
			'period' => 60,
		),
	);

	protected static $_instancesByAgentTopicId = array();

	protected $_agentTopic = null;

	/**
	 * [instance description]
	 * @param  [type] $agentTopic [description]
	 * @return [type]             [description]
	 */
	public static function instance($agentTopic) {
		if (empty(static::$_instancesByAgentTopicId[$agentTopic->id])) {
			$klass = get_called_class();
			$agent = new $klass($agentTopic);
			static::$_instancesByAgentTopicId[$agentTopic->id] = $agent;
		}
		return static::$_instancesByAgentTopicId[$agentTopic->id];
	}

	/**
	 * [__construct description]
	 * @param [type] $agentTopic [description]
	 */
	protected function __construct($agentTopic) {
		if (empty($agentTopic)) {
			throw new Exception('agentTopic is required');
		}
		$this->_agentTopic = $agentTopic;
	}

	/**
	 * [hasBlock description]
	 * @interface IBlockMaker
	 * @return boolean [description]
	 */
	public function hasBlock($user = null) {
		return $this->_config['hasBlock'];
	}

	/**
	 * [getBlock description]
	 * @interface IBlockMaker
	 * @param  [type] $topic   [description]
	 * @param  [type] $user    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function getBlock($user = null, array $options = array()) {
		$defaults = array('default' => false);
		$options += $defaults;

		if (!$this->hasBlock($user)) {
			return false;
		}

		if (empty($this->_config['blockDefault'])) {
			$block = array(
				'name' => 'agent' . $this->_agentTopic->id,
				'data' => array(
					'agentTopicId' => $this->_agentTopic->id,
					'template' => 'simple',
				),
				'position' => false,
			);
		} else {
			$block = $this->_config['blockDefault'];
			$block['name'] .= $this->_agentTopic->id;
			$block['data']['agentTopicId'] = $this->_agentTopic->id;
		}

		return $block;
	}

	/**
	 * [userData description]
	 * @return [type] [description]
	 */
	public function userData() {
		return array();
	}

	/**
	 * [delayed description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function delayed(array $options = array()) {
		$action = new ActionExecuteAgentTopic(array('agentTopic' => $this->_agentTopic));
		return $action->delayed($options);
	}

	/**
	 * [populateNextExecution description]
	 * @return [type] [description]
	 */
	public function populateNextExecution() {
		$now = new DateTime();
		$time = $this->_config['execution']['period'];
		$now->add(new DateInterval('PT' . $time . 'M'));
		$this->_agentTopic->date_next_execution = $now->format(DateUtil::SQL_DATETIME);
	}

	/**
	 * [execute description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function execute(array $options = array()) {
		if ($this->_agentTopic->state != State::ACTIVE) {
			return false;
		}

		// Calculate ending
		$timeEnd = strtotime($this->_agentTopic->date_end_execution);
		if ($timeEnd <= time()) {
			$this->_agentTopic->state = State::DELETED;
			$this->_agentTopic->save();

			return false;
		}

		$this->_agentTopic->date_last_execution = DateUtil::now();
		$this->populateNextExecution();

		return $this->_agentTopic->save();
	}

	/**
	 * [onMessageAdded description]
	 * @param  [type] $message [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function onMessageAdded($message, array $options = array()) {
		return true;
	}

	/**
	 * [populateEndExecution description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function populateEndExecution(array $options = array()) {
		return true;
	}

}

?>