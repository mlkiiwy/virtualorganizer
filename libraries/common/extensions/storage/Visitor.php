<?php

namespace common\extensions\storage;

use lithium\core\Environment;

use common\models\Users;

use common\util\String;
use common\util\Set;

use DateTime;


/**
 * Class for setting, getting and clearing info about actual visitor.
 */
class Visitor extends \lithium\core\StaticObject {

	/**
	 *
	 */
	const SESSION_PREFIX = "Visitor";

	/**
	 *
	 */
	const MAX_HISTORY = 6;

	/**
	 *
	 */
	const SESSION_LIVE_TIME = 6400;

	/**
	 * Possible session keys
	 */
	const KEY_PAGES = "pages";

	/**
	 *
	 */
	const USER_TOKEN_COOKIE_KEY = 'Visitor.UserTokenCookie';
	const USER_TOKEN_COOKIE_LIFETIME_DEFAULT = 2592000;	// 60 * 60 * 24 * 30 = 30 Days

	/**
	 *
	 */
	const FLASH_VARNAME = 'Flashs';

	const KEY_EMAIL_ENTRY = 'Visitor.EmailEntry';

	/**
	 * Class configuration.
	 *
	 * @var array
	 */
	protected static $_config = array(
		'storage_name' => 'default'
	);

	/**
	 * Class dependencies.
	 *
	 * @var array
	 */
	protected static $_classes = array(
		'session' => 'lithium\storage\Session'
	);

	/**
	 * [getUserToken description]
	 * @return [type] [description]
	 */
	public static function getUserToken() {
		global $_COOKIE;

		return !empty($_COOKIE[self::USER_TOKEN_COOKIE_KEY]) ? $_COOKIE[self::USER_TOKEN_COOKIE_KEY] : false;
	}

	/**
	 * [deleteUserToken description]
	 * @return [type] [description]
	 */
	public static function deleteUserToken() {
		global $_COOKIE;

		$domain = Environment::get('domain.main');
		setcookie(self::USER_TOKEN_COOKIE_KEY, "", time() - 3600, "/", $domain);

		if (!empty($_COOKIE[self::USER_TOKEN_COOKIE_KEY])) {
			unset($_COOKIE[self::USER_TOKEN_COOKIE_KEY]);
		}
	}

	/**
	 * [setUserToken description]
	 * @param [type] $value               [description]
	 * @param [type] $expirationTimestamp [description]
	 */
	public static function setUserToken($value, $expirationTimestamp = null) {
		global $_COOKIE;

		$expirationTimestamp = empty($expirationTimestamp) ? time() + self::USER_TOKEN_COOKIE_LIFETIME_DEFAULT : $expirationTimestamp;

		if (!empty($_COOKIE[self::USER_TOKEN_COOKIE_KEY])) {
			self::deleteCookieID();
		}

		$domain = Environment::get('domain.main');
		if (setcookie(self::USER_TOKEN_COOKIE_KEY, $value, $expirationTimestamp, '/', $domain) === false) {
			throw new Exception('cannot set the cookie');
		}

		return true;
	}

	/**
	 * Store a page
	 *
	 * @param request
	 * @return boolean True on successful write, false otherwise.
	 */
	public static function addPage($reqOrUrl, array $options = array()) {
		$defaults = array(
			'private' => false,
		);
		$options += $defaults;

		// Determinate page
		if (is_string($reqOrUrl)) {
			$options['url'] = $reqOrUrl;
		} else {
			$options['url'] = $reqOrUrl->env('REQUEST_URI');
		}

		$pages = static::get(self::KEY_PAGES);
		$pages = empty($pages) ? array() : $pages;
		array_unshift($pages, $options);
		if (count($pages) > self::MAX_HISTORY) {
			$pages = array_slice($pages, 0, self::MAX_HISTORY);
		}

		return static::set(self::KEY_PAGES, $pages);
	}

	/**
	 * Get last page
	 * @param boolean private or not page
	 * @return array The stored flash message.
	 */
	public static function getLastPage($onlyPublic = false) {
		$pages = static::get(self::KEY_PAGES);

		if ($onlyPublic) {
			foreach ($pages as $page) {
				if (!$page['private']) {
					return $page;
				}
			}
		} else {
			return Set::first($pages);
		}
	}

	/**
	 *
	 */
	public static function init() {
		$new = false;
		$lastTime = static::get('lastTime');
		if (!empty($lastTime) &&  $lastTime + self::SESSION_LIVE_TIME < time()) {
			// Clear all data
			self::clearData();
			$new = true;
		} else if (empty($lastTime)) {
			$new = true;
		}
		static::set('lastTime', time());
		return $new;
	}

	/**
	 *
	 */
	public static function get($key) {
		$session = static::$_classes['session'];
		return $session::read(self::_key($key));
	}

	/**
	 *
	 */
	public static function set($key, $value) {
		$session = static::$_classes['session'];
		return $session::write(self::_key($key), $value);
	}

	/**
	 *
	 */
	public static function delete($key) {
		$session = static::$_classes['session'];
		return $session::delete(self::_key($key));
	}

	/**
	 *
	 */
	protected static function _key($key) {
		return self::SESSION_PREFIX . "." . $key;
	}

	/**
	 * [getIp description]
	 * @return [type] [description]
	 */
	public static function getIp() {
		global $_SERVER;

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			// check ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			// to check ip is pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} elseif (!empty($_SERVER['REMOTE_ADDR'])) {
			$ip = $_SERVER['REMOTE_ADDR'];
		} else {
			$ip = null;
		}

		return ip2long($ip);
	}

	/**
	 * [getFlashs description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function getFlashs(array $options = array()) {
		$defaults = array('clean' => true);
		$options += $defaults;

		$flashs = static::get(self::FLASH_VARNAME);

		if ($options['clean']) {
			static::delete(self::FLASH_VARNAME);
		}

		return empty($flashs) ? array() : $flashs;
	}

	/**
	 * [addFlash description]
	 * @param [type] $data    [description]
	 * @param array  $options [description]
	 */
	public static function addFlash($data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$flashs = static::get(self::FLASH_VARNAME);
		$flashs = empty($flashs) ? array() : $flashs;

		$flashs[] = $data;

		return static::set(self::FLASH_VARNAME, $flashs);
	}

	/**
	 * [setEmailEntry description]
	 * @param [type] $email [description]
	 */
	public static function setEmailEntry($email) {
		$user = $email->related('user');
		return static::set(self::KEY_EMAIL_ENTRY, $email->data());
	}

	/**
	 * [getEmailUser description]
	 * @return [type] [description]
	 */
	public static function getEmailUser() {
		$data = static::get(self::KEY_EMAIL_ENTRY);
		$user = null;
		if (!empty($data['user'])) {
			$user = Users::create($data['user'], array('exists' => true));
		}
		return $user;
	}
}

?>