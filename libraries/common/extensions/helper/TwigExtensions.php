<?php

namespace common\extensions\helper;

use common\util\Date;
use common\util\Set;

use Twig_Extension;
use Twig_Filter_Method;
use Twig_Function_Method;
use Twig_Test_Method;

use DateTime;
use Exception;

/**
 *
 */
class TwigExtensions extends Twig_Extension {

	public function __construct(array $config = array()) {
	}

	public function getName() {
		return 'SensCritique';
	}

	public function getFilters() {
		return array(
			'truncate' => new Twig_Filter_Method($this, 'truncate'),
			'prefix' => new Twig_Filter_Method($this, 'prefix'),
			'datetime' => new Twig_Filter_Method($this, 'datetime'),
			'dict' => new Twig_Filter_Method($this, 'dict'),
			'encap' => new Twig_Filter_Method($this, 'encap'),
			'paragraph' => new Twig_Filter_Method($this, 'paragraph'),
			'firstCap' => new Twig_Filter_Method($this, 'firstCap'),
			'firstSmall' => new Twig_Filter_Method($this, 'firstSmall'),
			'levenshtein' => new Twig_Filter_Method($this, 'levenshtein'),
			'round' => new Twig_Filter_Method($this, 'round'),
			'd' => new Twig_Filter_Method($this, 'varExport'),
			'rome' => new Twig_Filter_Method($this, 'rome'),
			'stripTags' => new Twig_Filter_Method($this, 'stripTags'),
			'dump' => new Twig_Filter_Method($this, 'varDump'),
			'firstKey' => new Twig_Filter_Method($this, 'firstKey'),
			'clearSpace' => new Twig_Filter_Method($this, 'clearSpace')
		);
	}

	public function getTests() {
		return array(
			'inArray' => new Twig_Test_Method($this, 'inArray'),
			'array' => new Twig_Test_Method($this, 'isArray'),
			'oldTimestamp' => new Twig_Test_Method($this, 'isOldTimestamp')
		);
	}

	public static function stripTags($data) {
		return strip_tags($data);
	}

	public static function isArray($data) {
		return is_array($data);
	}

	public static function varExport($var) {
		return '<pre>' . print_r($var, true) . '</pre>';
	}

	public static function varDump($var) {
		return '<pre>' . var_export($var, true) . '</pre>';
	}

	public static function inArray($value, $array) {
		return in_array($value, $array);
	}

	public function datetime($d, $format = "%B %e, %Y %H:%M") {
		try {
			return Date::format($d, $format);
		} catch (Exception $e) {
			return '';
		}
	}

	/**
	 * Tronque une chaîne de caractères.
	 *
	 * {{{
	 * {% set value = "Le soleil vient de se lever" %}
	 * {{ value|truncate(10) }} // Le soleil...
	 * {{ value|truncate(10, " ", ".") }} // Le soleil. }}}
	 *
	 * @param string $string Chaîne de caractère à tronquer
	 * @param integer $limit Limite de caractères
	 * @param string $break Caractère de césure
	 * @param string $pad Terminaison
	 * @return string Contenu tronqué
	 */
	public static function truncate($string, $limit, $break = " ", $pad = "...") {
		// return with no change if string is shorter than $limit
		if( mb_strlen($string) <= (int) $limit) return $string;

		// is $break present between $limit and the end of the string?
		if( false !== ($breakpoint = mb_strpos($string, $break, $limit))) {
			if( $breakpoint < mb_strlen($string) - 1) {
				$string = mb_substr($string, 0, $breakpoint) . $pad;
			}
		}
		return $string;
	}


	/**
	 * Encapsule une valeur dans du code si elle est présente.
	 *
	 *	{{{
	 *	{% set value = "foo" %}
	 *	{{ value|encap("(", ")"); }} // (foo) }}}
	 *
	 * @param string $value Valeurs à encapsuler.
	 * @param string $before Valeur avant.
	 * @param string $after Valeur après.
	 */
	public static function encap($value, $before, $after) {
		if ($value) {
			return $before.$value.$after;
		}
		return false;
	}

	/**
	 * Génère des classes à partir d'un string et d'un préfixe.
	 *
	 *	{{{
	 *	{% set value = "button list" %}
	 *	{{ value|prefix("pref", "-"); }} // pref-button pref-list }}}
	 *
	 * @param string $value Valeurs à préfixer
	 * @param string $prefix Préfixe
	 * @param string $glue Caractère d'association ("-" par défaut)
	 * @return string Valeurs préfixées
	 */
	public static function prefix($value, $prefix, $glue = "-") {
		$vars = explode(" ", trim($value));
		$temp = null;
		foreach( $vars as $var) {
			$temp = $prefix.$glue.$var." ";
		}
		return $temp;
	}

	/**
	 * Première lettre en minuscule
	 */
	public static function firstSmall($value) {
		if (preg_match('/^[A-Z]{1}/', $value)) {
			if (mb_strlen($value) == 1) {
				return strtolower($value);
			} elseif (mb_strlen($value) > 1) {
				return strtolower(substr($value, 0, 1)) . substr($value, 1);
			}
		}
		return $value;
	}

	/**
	 * Première lettre en majuscule
	 */
	public static function firstCap($value) {
		if (is_array($value)) {
			return '';
		}

		if (preg_match('/^[a-zsàáâ,ãäåçèéêëìíîïðòóôõöùúûüýÿ]{1}/', $value)) {
			if (mb_strlen($value) == 1) {
				return strtoupper($value);
			} elseif (mb_strlen($value) > 1) {
				return strtoupper(substr($value, 0, 1)) . substr($value, 1);
			}
		}

		return $value;
	}

	/**
	 * Première lettre en majuscule
	 */
	public static function levenshtein($value, $target) {

		return levenshtein($value, $target);
	}

	/**
	 * Arrondit (round())
	 */
	public static function round($value, $decimal) {

		return round($value, $decimal);
	}

	/**
	 * Premiere clef d'un tableau
	 */
	public static function firstKey($value) {
		return is_array($value) ? Set::getFirstKey($value) : $value;
	}

	/**
	 * Convertit en chiffres romains
	 */
	public static function rome($N) {
		$c='IVXLCDM';
        for($a=5,$b=$s='';$N;$b++,$a^=7)
                for($o=$N%$a,$N=$N/$a^0;$o--;$s=$c[$o>2?$b+$N-($N&=-2)+$o=1:$b].$s);
        return $s;
	}

	/**
	 * Automatically applies "p" and "br" markup to text.
	 * Basically [nl2br](http://php.net/nl2br) on steroids.
	 *
	 *     echo Text::auto_p($text);
	 *
	 * [!!] This method is not foolproof since it uses regex to parse HTML.
	 *
	 * @param   string   subject
	 * @param   array    options
	 *			-`br` boolean : convert single linebreaks to <br>
	 *			-`class` string : rajoute des classes aux tags <p>
	 * @return  string
	 */
	public static function paragraph($str, $options = array())
	{
		$br = (empty($options['br'])) ? TRUE : $options['br'];
		$class = (empty($options['class'])) ? NULL : $options['class'];

		if ($class) {
			$class = ' class="' . $class . '" ';
		}

		// Trim whitespace
		if (($str = trim($str)) === '')
			return '';

		// Standardize newlines
		$str = str_replace(array("\r\n", "\r"), "\n", $str);

		// Trim whitespace on each line
		$str = preg_replace('~^[ \t]+~m', '', $str);
		$str = preg_replace('~[ \t]+$~m', '', $str);

		// The following regexes only need to be executed if the string contains html
		if ($html_found = (mb_strpos($str, '<') !== FALSE))
		{
			// Elements that should not be surrounded by p tags
			$no_p = '(?:p|div|h[1-6r]|ul|ol|li|blockquote|d[dlt]|pre|t[dhr]|t(?:able|body|foot|head)|c(?:aption|olgroup)|form|s(?:elect|tyle)|a(?:ddress|rea)|ma(?:p|th))';

			// Put at least two linebreaks before and after $no_p elements
			$str = preg_replace('~^<'.$no_p.'[^>]*+>~im', "\n$0", $str);
			$str = preg_replace('~</'.$no_p.'\s*+>$~im', "$0\n", $str);
		}

		// Do the <p> magic!
		$str = '<p' . $class . '>'.trim($str).'</p>';
		$str = preg_replace('~\n{2,}~', "</p>\n\n<p" . $class . ">", $str);

		// The following regexes only need to be executed if the string contains html
		if ($html_found !== FALSE)
		{
			// Remove p tags around $no_p elements
			$str = preg_replace('~<p>(?=</?'.$no_p.'[^>]*+>)~i', '', $str);
			$str = preg_replace('~(</?'.$no_p.'[^>]*+>)</p>~i', '$1', $str);
		}

		// Convert single linebreaks to <br />
		if ($br === TRUE)
		{
			$str = preg_replace('~(?<!\n)\n(?!\n)~', "<br>\n", $str);
		}

		return $str;
	}

	/**
	 *
	 */
	public function isOldTimestamp($val) {
		$date = new DateTime($val);
		$now = new DateTime();

		if ($date->diff($now, true)->days >= 90) {
			return true;
		}

		return false;
	}

	/**
	 *
	 */
	public function clearSpace($val) {
		$val = preg_replace('/\r+/u', ' ', $val);
		$val = preg_replace('/\s+/u', ' ', $val);
		return $val;
	}
}

?>
