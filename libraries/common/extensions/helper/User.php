<?php

namespace common\extensions\helper;

use DateTime;

use lithium\core\Environment;
use common\models\Users;

/**
 *
 */
class User extends \lithium\template\Helper {

	/**
	 * Retourne un avatar en random
	 *
	 *		Utilisation dans un template:
	 *		{% set total = view.("User").randomAvatar() %}
	 *
	 * @return string url
	 */
	public function randomAvatar() {
		return "/img/tmp/avatar/".rand(1,14).".png";
	}

	/**
	 *
	 */
	public function age($birth) {
		$now = new DateTime();
		$birthday = new DateTime($birth);

		return $now->diff($birthday)->y;
	}

	/**
	 *
	 */
	public function gender($gender) {
		return $gender == 2 ? 'Femme' : 'Homme';
	}

	/**
	 *
	 */
	public function concatRecipient($recipients, $userId){
		$return = '';
		$i = 0;
		foreach( $recipients as $k => $recipient){
			if( $userId != $recipient['user']['id']){
				$i++;
				if( $i != 1){
					$return .= $recipient['user']['displayed_name'] . ', ';
				}
			}
		}
		return substr($return, 0, -2);
	}

	/**
	 *
	 */
	public function displayGender($user) {
		return !empty($user['privacy_gender']);
	}

	/**
	 *
	 */
	public function displayCountry($user) {
		return !empty($user['privacy_geo']);
	}

	/**
	 *
	 */
	public function canManageUsers($user) {
		if (empty($user['state_admin'])) {
			return false;
		}

		return ($user['state_admin'] & Users::ACTION_MANAGE_USERS);
	}
}

?>