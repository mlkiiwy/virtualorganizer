<?php

namespace common\extensions\helper;

use common\util\Date as DateUtil;

use DateTime;
use Exception;

class Date extends \lithium\template\Helper {

	/**
	 * Affichage des heures en fonction du subtype_id
	 *
	 * Film, série, etc : 2 h 20 min.
	 * Track : 4:20
	 */
	public function length($seconds, $subtype_id = 1) {
		return DateUtil::length($seconds, $subtype_id);
	}

	public function convertFake($date, $type = 'date') {
		if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $matches)) {
	    	if ($type == 'date') {
		    	//convert date
		    	if ($matches[3] == '00') {
			    	$date_done = date("Y/m/00", strtotime($matches[1] .'-' . $matches[2] . '-1'));
		    	} elseif ($matches[2] == '00') {
			    	$date_done = date("Y/00/00", strtotime($matches[1] .'-1-1'));
		    	} else{
		    		$date_done = date("Y/m/d", strtotime($matches[1] .'-' . $matches[2] . '-' . $matches[3]));
		    	}
		    } elseif ($type == 'format') {
		    	//convert date
		    	if ($matches[3] == '00') {
			    	$date_done = date("m/Y", strtotime($matches[1] .'-' . $matches[2] . '-1'));
		    	} elseif ($matches[2] == '00') {
			    	$date_done = date("Y", strtotime($matches[1] .'-1-1'));
		    	} else{
		    		$date_done = date("d/m/Y", strtotime($matches[1] .'-' . $matches[2] . '-' . $matches[3]));
		    	}
		    } else{
	    		//label
		    	$date_done =  $matches[3] != '00' ? $matches[3] . ' ' : '';
		    	$month_fr = array('01' => 'janvier', '02' => 'février', '03' => 'mars', '04' => 'avril', '05' => 'mai', '06' => 'juin', '07' => 'juillet', '08' => 'août', '09' => 'septembre', '10' => 'octobre', '11' => 'novembre', '12' => 'décembre');
		    	$date_done .=  $matches[2] != '00' ? $month_fr[$matches[2]] . ' ' : '';
		    	$date_done .= $matches[1];
		    }
	    	return $date_done;
	    }

	}

	public function release($date, $options = array()) {
		try {
			return DateUtil::formatDateSc($date, $options);
		} catch (Exception $e) {
			return '';
		}
	}

	public function relativeDay($dateStr) {
		$date = new DateTime($dateStr);
		$today = new DateTime();
		$tomorrow = new DateTime('tomorrow');
		$afterTomorrow = new DateTime('tomorrow +1 day');
		if( $today->format('Y-m-d') == $date->format('Y-m-d') && $date->format('H') >= 4 && $date->format('H') < 20 ) {
			return 'aujourd\'hui';
		} else if( ($today->format('Y-m-d') == $date->format('Y-m-d') && ($date->format('H') < 4 || $date->format('H') >= 20) ) || ($tomorrow->format('Y-m-d') == $date->format('Y-m-d') && $date->format('H') < 4) ) {
			return 'ce soir';
		} else if(($tomorrow->format('Y-m-d') == $date->format('Y-m-d') && $date->format('H') >= 4) || ($afterTomorrow->format('Y-m-d') == $date->format('Y-m-d') && $date->format('H') < 4) ) {
			return 'demain';
		} else {
			return mb_strtolower(DateUtil::format($dateStr, '%A %e'));
		}
	}
}

?>