<?php

namespace common\extensions\helper;

/**
 *
 */
class Html extends \lithium\template\helper\Html {

	/**
	 * Imports local string definitions into rendering context.
	 *
	 * @return void
	 * @todo
	 */
	protected function _init() {
		$this->_strings['link-span-prefixed'] = '<a href="{:url}" {:a_options}><span {:span_options}>{:span_text}</span>{:title}</a>';

		parent::_init();
	}

	/**
	 * @todo
	 */
	public function link_span($title, $span_text, $url, array $options = array()) {
		$defaults = array(
			'span_options' => array(),
			'a_options' => array()
		);
		$options += $defaults;

		$a_options = $options['a_options'];
		$span_options = $options['span_options'];

		return $this->_render(__METHOD__, 'link-span-prefixed', compact('title', 'url', 'span_text', 'a_options', 'span_options'));
	}

	/**
	 * Overwritten to return an empty string instead of null when no string is returned.
	 * This is the case when `inline` is set to `false`, and made Twig throw an exception.
	 */
	public function style($path, array $options = array()) {
		$r = parent::style($path, $options);

		// Fix: Twig.
		if (is_null($r)) {
			return '';
		}

		return $r;
	}

	/**
	 * Overwritten to return an empty string instead of null when no string is returned.
	 * This is the case when `inline` is set to `false`, and made Twig throw an exception.
	 */
	public function script($path, array $options = array()) {
		$r = parent::script($path, $options);

		// Fix: Twig.
		if (is_null($r)) {
			return '';
		}

		return $r;
	}
}

?>