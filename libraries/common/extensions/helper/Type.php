<?php

namespace common\extensions\helper;

use common\models\Types;

/**
 * Helper principal pour l'affichage de données liés à un produit (film, série, bd, jeu, livre ...).
 */
class Type extends \lithium\template\Helper {

	/**
	 *
	 */
	public function equals($a, $b) {
		if (empty($a) && empty($b)) {
			return true;
		} elseif (empty($a) && !empty($b) || !empty($a) && empty($b)) {
			return false;
		}

		return $this->normalize($a) == $this->normalize($b);
	}

	/**
	 *
	 */
	public function normalize($input) {
		$normalize = Types::normalize($input);
		return empty($normalize) ? '' : $normalize;
	}

	/**
	 *
	 */
	public function getId($input) {
		return Types::getId($input);
	}

	/**
	 * @deprecated use Sc_d
	 */
	public function label($id) {
		switch ($id) {
			case 1:
				return 'Film';
			case 2:
				return 'Livre';
			case 3:
				return 'Jeu Vidéo';
			case 4:
				return 'Série TV';
			case 5:
				return 'BD';
			case 6:
				return 'Musique';
			default:
				return '';
		}
	}

	/**
	 *
	 */
	public function universes($id = null) {
		return Types::universes($id);
	}

	/**
	 * @deprecated use Types
	 */
	public function labelsUrl($id) {
		switch ($id) {
			case 1:
				return 'films';
			case 2:
				return 'livres';
			case 3:
				return 'jeuxvideo';
			case 4:
				return 'series';
			case 5:
				return 'bd';
			case 6:
				return 'musique';
			default:
				return '';
		}
	}

	/**
	 * masculin ou féminin
	 */
	public function genderByType($typeId) {
		return $typeId == 4 || $typeId == 6 ? 'f' : 'm';
	}
}

?>