<?php

namespace common\extensions\helper;

use common\models\Lists;

/**
 *
 */
class Listing extends \lithium\template\Helper {

	/**
	 *
	 */
	public function label($list) {
		if (empty($list['label']) && !empty($list['parent_id']) && !empty($list['parent_list']['label'])) {
			return $list['parent_list']['label'];
		}

		return empty($list['label']) ? '' : $list['label'];
	}

	/**
	 *
	 */
	public function description($list) {
		if (empty($list['description']) && !empty($list['parent_id']) && !empty($list['parent_list']['description'])) {
			return $list['parent_list']['description'];
		}

		return empty($list['description']) ? '' : $list['description'];
	}

	/**
	 *
	 */
	public function thumbnail($list) {
		if (!empty($list['parent_id']) && !empty($list['parent_list']['thumbnail'])) {
			return $list['parent_list']['thumbnail'];
		}

		return empty($list['thumbnail']) ? null : $list['thumbnail'];
	}

	/**
	 *
	 */
	public function cover($list, $cropped = true) {
		$tag = $cropped ? 'cover_cropped' : 'cover';
		return empty($list[$tag]) ? null : $list[$tag];
	}

	/**
	 *
	 */
	public function hasCover($list) {
		return !empty($list['cover']) && !empty($list['cover']['main']) && !empty($list['cover']['main']['filename']);
	}

	/**
	 *
	 */
	public function isPoll($list, $parent = null) {
		if ($parent === true) {
			return $list['subtype_id'] == Lists::TYPE_POLL && empty($list['parent_id']);
		} elseif ($parent === false) {
			return $list['subtype_id'] == Lists::TYPE_POLL && !empty($list['parent_id']);
		}

		return $list['subtype_id'] == Lists::TYPE_POLL;
	}

	/**
	 *
	 */
	public function count($list) {
		$count = empty($list['gen_count']) ? 0 : $list['gen_count'];

		if ($this->isPoll($list, false)) {
			return $count;
		}

		if (!empty($list['limitation']) || !empty($list['parent_list']['limitation'])) {
			$limit = !empty($list['limitation']) ? $list['limitation'] : $list['parent_list']['limitation'];
			$count = ($count > $limit) ? $limit : $count;
		}

		return $count;
	}
}

?>