<?php

namespace common\extensions\helper;

/**
 * Outils supplémentaires pour Twig. Essentiellement des tests et des outils de debug.
 */
class Tools extends \lithium\template\Helper {

	/**
	 * Teste si une valeur est un entier.
	 *
	 * {{{
	 * {% Tools_isInt(432432) %} // 1
	 * {% Tools_isInt("foo") %} // 0 }}}
	 *
	 * @param mixed $value Valeur à tester.
	 * @return integer 1 ou 0.
	 */
	public static function isInt($value) {
		return is_int($value);
	}

	/**
	 * Retourne le contenu d'une variable.
	 *
	 * {{{
	 * // Sans mise en forme
	 * {{ Tools_varExport(value) }} // array('key' => 'foo')
	 *
	 * // Avec mise en forme <pre>
	 * {{ Tools_varExport(value, true) }} // <pre>array('key' => 'foo')</pre> }}}
	 *
	 * @param mixed $value Valeur à afficher.
	 * @param boolean $pre Mise en page ou non.
	 * @return string Contenu de la valeur.
	 */
	public static function printR($value, $pre = true) {
		if ($pre == true) {
			return "<pre>".var_export($value, true)."</pre>";
		}
		return var_export($value, true);
	}
}
?>
