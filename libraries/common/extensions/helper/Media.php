<?php

namespace common\extensions\helper;

use lithium\core\Environment;

/**
 *
 */
class Media extends \lithium\template\Helper {

	/**
	 *
	 */
	protected $_webroot;

	/**
	 *
	 */
	protected function _init() {
		$this->_webroot = Environment::get('media.domain') . '/';
	}

	/**
	 *
	 */
	public function size($medium) {
		return '10Mo';
	}

	/**
	 *
	 */
	public function filename($medium) {
		return $medium['directory'] . $medium['filename'] . '.' . $medium['extension'];
	}

	/**
	 *
	 */
	public function width($medium, $format) {
		if (empty($medium)) {
			return '0';
		}

		foreach (array_shift($medium) as $tag => $file) {
			if ($tag == $format) {
				return $file['width'];
			}
		}

		return '0';
	}

	/**
	 *
	 */
	public function height($medium, $format) {
		if (empty($medium)) {
			return '0';
		}

		foreach (array_shift($medium) as $tag => $file) {
			if ($tag == $format) {
				return $file['height'];
			}
		}

		return '0';
	}

	/**
	 *
	 */
	public function image($medium, $format) {
		$filepath = '';

		if (empty($medium)) {
			return $filepath;
		}

		foreach (array_shift($medium) as $tag => $file) {
			if ($tag == $format) {
				$filepath = $file['filepath'];
				break;
			}
		}

		return empty($filepath) ? '' : $this->cdn($this->_webroot . $filepath);
	}

	/**
	 *
	 */
	public function cdn($path) {
		return $path;
	}
}

?>