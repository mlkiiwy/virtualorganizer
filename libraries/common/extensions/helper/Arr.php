<?php

namespace common\extensions\helper;

use common\util\Arr as ArrUtil;

use Exception;

class Arr extends \lithium\template\Helper {

	public function removeKey($array, $key) {
		if (!empty($array[$key])) {
			unset($array[$key]);
		}
		return $array;
	}

	/**
	 * Ajoute une valeur à un tableau (twig).
	 */
	public function push($array, $key, $value = null) {
		if (empty($array)) {
			$array = array();
		}
		if (!is_array($array)) {
			return false;
		}
		if (empty($value)) {
			$array[] = $key;
		} else {
			$array[$key] = $value;
		}
		return $array;
	}

}

?>