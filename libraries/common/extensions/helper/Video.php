<?php

namespace common\extensions\helper;

use common\util\Video as VideoUtil;

/**
 *
 */
class Video extends \lithium\template\Helper {

	/**
	 *
	 */
	public function show($id, $width, $height, $autoplay = false) {
		return VideoUtil::getVideoEmbedCode($id, $width, $height, $autoplay);
	}
}

?>