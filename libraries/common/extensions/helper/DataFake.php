<?php

namespace common\extensions\helper;

class DataFake extends \lithium\template\helper\Html {

	public function actions() {
		$return =  array(
			 'rating' => rand(1, 10),
			 'is_recommend' => rand(0, 1),
			 'is_done' => rand(0, 1),
			 'is_current' => rand(0, 1),
			 'is_wish_list' => rand(0, 1),
			 'is_shopping_list' => rand(0, 1),
			 'is_list' => rand(0, 1),
			 );
		return $return;
	}

	public function badges() {
		$badge = array("Bêta testeur", "Bienfaiteur", "Alpha testeur", "Bâtisseur", "Critique (Niveau 2)", "A voté", "Collectionneur (Niveau 3)");
		$img =	rand(1,7);
		return array(
				"title" => $badge[rand(0,count($badge)-1)],
				"img" => "/img/tmp/badge/".$img.".png",
			);
	}

	public function user() {
		$username = array("nanark", "prodigy", "Faskiel", "LaraCroft", "RogerLapoutre", "Almeida", "capap");
		$firstname = array("Jean-Michel", "Jean-Luc", "Jean-Louis", "Michel", "Michou", "Jean");
		$lastname = array("Dutrou", "Dupuis", "Dujardin", "Labrut", "Spychala", "Kuipers", "Marie");
		$sex = array("Homme", "Femme");
		$location = array("75009 (France)", "60180 (France)", "75003 (France)", "75006 (France)", "75002 (France)");
		$website = array("http://www.senscritique.com", "http://www.google.com", "http://www.siteduzero.com", "http://www.joulse.com");
		$age = rand(21,41);
		$id = rand(1,14);
		return array(
			"username" => $username[rand(0,count($username)-1)],
			"firstname" => $firstname[rand(0,count($firstname)-1)],
			"lastname" => $lastname[rand(0,count($lastname)-1)],
			"age" => $age,
			"sex" => $sex[rand(0,count($sex)-1)],
			"location" => $location[rand(0,count($location)-1)],
			"website" => $website[rand(0,count($website)-1)],
			"avatar" => "/img/tmp/avatar/".$id.".png",
			"about" =>
					"Certes, comme le souligne le grand Mr Raisin-ver, il y a un coté ridicule à vouloir faire passer une femme de 30 ans, au corps si...
					bref, à la faire passer pour un enfant de 12 ans, mais je ne sais pas... sans doute mon petit cœur d'enfant qui se laissait émouvoir.",
			"id" => $id
			);
	}

	public function collection($probability = 100) {
		$review = array(
			array(
				"review_title" => "L'artifice magique...",
				"review_content" =>
					"Certes, comme le souligne le grand Mr Raisin-ver, il y a un coté ridicule à vouloir faire passer une femme de 30 ans, au corps si... bref, à la faire passer pour un enfant de 12 ans, mais je ne sais pas... sans doute mon petit cœur d'enfant qui se laissait émouvoir par des dessins animés (et puis, je me suis fais avoir encore récemment par King Kong aussi), mon petit cœur donc s'est réveillé et s'est glissé dans cette farce comme on se glisse sous une bonne couette lors d'un hiver glacial, grâce, ce qui est une raison valable cette fois, au jeu de la belle Ginger Rogers.
					Comment ne l'avais-je pas repéré plus tôt cette incroyable femme qui joue les trois âges de la vie, qui peut être autant distante et inaccessible et dans la seconde suivante, brulante et irrésistible ?

					Il y a ici dans ce deuxième film de Billy Wilder en tant que réalisateur, la marque de fabrique de ses prochaines comédies, ce loufoque, ce comique de situation dans la pure lignée des comédies de Lubitsch qu'il a assisté, cet aspect magique qui supplante le réel (même procédé de travestissement dans Irma la douce ou Certains l'aiment chaud, ou encore Fedora), ce procédé qui ne se met en place que pour mieux nous faire rêver et rire, un humour toujours à la limite du grotesque, des situations cocasses...(Aussi, le thème initial, le travestissement d'une adulte en enfant, ne bascule pas sur une pédophilie sous-jacente, non l'artifice est ailleurs, au service de la comédie).
					En fait, ce cinéma correspond sans doute aussi à ce que j'en attends : m'offrir l'envie de sourire durant une demi-heure une fois le film fini, me transporter dans des situations irréelles, me laisser séduire par ces oldies qui ont pour stature une beauté éternelle...

					C'est sans doute ces raisons qui me ramènent aux films de cette période. Voir par exemple Ginger jouer la gamine durant une heure pour après, quand le moment est venu de révéler son âge, la voir se transformer, sans effet numérique, en femme divinement belle....
					Toute la magie du cinéma est là au final au service d'une belle comédie romantique.",
				"review_date_creation" => "08/02/2012",
				"review_last_update" => "08/02/2012",
				"review_link" => "",
				"review_like" => "10",
				"review_like_positive" => "7",
				"review_like_ratio" => "70",
				"review_comment" => rand(0,10)
			),
			array(
				"review_title" => "C'est vraiment un titre de critique qui est très long et qui prend de la place mais c'est nécessaire pour le test que je dois faire !",
				"review_content" =>
					"Une comédie qui paraît assez légère de loin et qui en fait traite du sujet un peu malsain qu'est l'adultère.
					L'adultère ici c'est une sorte de malédiction qui tombe sur les hommes dès que leur femme est partie en vacances, et ce quel que soit l'amour qu'ils portent à leur épouse ou leur volonté de résister à la tentation. D'un autre côté, lorsque l'adultère se présente sous la forme de la sexyssime Marylin Monroe...

					Le jeu de Marylin se limite pour la plupart du temps à jouer l'ingénue, plisser les yeux, faire des sourires pleins de dents ou la moue, mettre les seins en avant ou étendre les jambes (à part dans les désaxés certes). N'empêche qu'elle le fait à la perfection et qu'elle imprime l'écran comme aucune autre actrice avant ou après elle. Une véritable aura sexuelle se dégage de sa personne à chacun de ses mouvements.

					Donc on comprend un peu cet homme, somme toute très banal, qui se retrouve seul chez lui car sa femme et son fils sont partis en vacances et qui craque pour sa voisine. On se doute très vite que le film est tiré d'une pièce de théâtre puisqu'on est quasiment en huis clos et que le héros passe son temps à monologuer. Il monologue d'ailleurs très bien, ça n'en devient jamais lassant grâce à ses constants revirements entre son attitude de seducteur et son attitude d'époux fidèle.
					Très amusant aussi, son imagination débordante qui le conduit à vivre des scènes complètement inventées.

					Il est très intéressant de savoir que ce film a été énormément censuré et qu'il était censé aller beaucoup plus loin que ce qu'on voit (Apparemment la pièce d'origine était plus osée). En particulier, Tom Ewell et Marylin Monroe ne devait pas que s'embrasser. Ah on n'allait pas non plus les voir au lit, hein ! On ne rigolait pas avec l'adultère à l'époque ! Mais on aurait dû comprendre qu'ils y avaient été. De même, la fameuse scène où Marylin avec sa robe blanche se place au-dessus d'une grille de métro n'est pas très affriolante. Lorsqu'elle a été tournée pourtant, tout le monde a pu voir sa culotte. Nous on a le droit à ses genoux...

					Malgré une censure sévère, Billy Wilder a réussi un film drôle, réaliste et un peu osé.",
				"review_date_creation" => "10/02/2012",
				"review_last_update" => "08/02/2012",
				"review_link" => "",
				"review_like" => "5",
				"review_like_positive" => "5",
				"review_like_ratio" => "100",
				"review_comment" => rand(0,10)
			),
			array(
				"review_title" => "",
				"review_content" =>
					"Un huit-clos tout à fait maitrisé, digne du grand Billy Wilder !",
				"review_date_creation" => "10/01/2012",
				"review_last_update" => "08/02/2012",
				"review_link" => "",
				"review_like" => "14",
				"review_like_positive" => "0",
				"review_like_ratio" => "0",
				"review_comment" => rand(0,10)
			),
			array(
				"review_title" => "THE girl next door",
				"review_content" =>
					"New York, dans les années 50. Richard Sherman, un publiciste d'une maison d'édition, envoie sa femme et son fils en vacances pour l'été. Le bouquin qu'il doit faire vendre? Un essai sur le démon de midi chez l'homme après sept ans de mariage, la \"démangeaison des 7 ans\". A peine commence-t-il à goûter ces moments de solitude bien mérités qu'une superbe blonde un peu ingénue emménage juste au-dessus de chez lui ...

					Adapté d'une pièce de théâtre de Broadway écrite par George Axelrod, avec Tom Ewell reprenant le rôle du mari adultère à l'imagination débordante qu'il tenait dans la pièce, c'est l'exemple-type d'une bataille livrée à la censure du code Hays qui aura finalement tourné plutôt en faveur de celui-ci, car bien plus sage que la version de Broadway.

					L'origine théâtrale se ressent de par l'unité de lieu et les monologues, et les enjeux comiques sont bien moindres comparé au futur \"Certains l'aiment chaud\", mais le charme de Marilyn Monroe joue pleinement et irradie la pellicule. Et ce film ne serait pas ce qu'il est sans ce moment immortalisé à jamais aux côtés du strip-tease de gants de Rita Hayworth et de Marlene Dietrich chantant \"Ich bin von Kopf bis Fuß auf Liebe eingestellt\" ou encore d'Anita Ekberg dans la fontaine de Trevi : je parle bien sûr de la scène où Marilyn porte une robe blanche au-dessus d'une bouche d'aération de métro. Mythique.",
				"review_date_creation" => "10/01/2012",
				"review_last_update" => "08/02/2012",
				"review_link" => "",
				"review_like" => "2",
				"review_like_positive" => "1",
				"review_like_ratio" => "50",
				"review_comment" => rand(0,10)
			),
		);

		if (rand(0, 100) > $probability) return false;
		$return = array_merge(
			$review[rand(0, count($review) - 1)],
			$this->actions(),
			array( "is_review" => 1, "rating" => rand(1, 10) )
		);
		return $return;
	}

	public function listingProduct() {
		return array(
			"collection" => self::collection(),
			"product" => self::product(),
			"user" => self::user(),
			);
	}

	public function notifications() {
		$phrase = array(
				"vous a ajouté à la liste de ses éclaireurs",
				"a apprécié votre liste",
				"a apprécié votre critique",
				"a commenté l'avis de",
				"a commenté"
			);
		$date = array(
				"Le 25 mars 2012",
				"Le 24 mars 2012",
				"Le 22 mars 2012",
				"Le 20 mars 2012",
				"Le 16 mars 2012"
			);
		return array(
			"product" => self::product(),
			"user" => self::user(),
			"list" => self::lists(),
			"phrase" => $phrase[rand(0,count($phrase)-1)],
			"date" => $date[rand(0,count($date)-1)],
			);
	}

	public function product() {
		$rposter = "poster-".rand(1, 11);
		$product = array(
			"title" => "Millénium : Les Hommes qui n’aimaient pas les femmes",
			"original_title" => "The Girl with the Dragon Tatoo",
			"year" => "2012",
			"subtype_id" => rand(1,8),
			"type_id" => rand(1,6),
			"plot" => "
					Mikael Blomkvist, brillant journaliste d’investigation, est engagé par un des plus puissants industriels de Suède, Henrik Vanger, pour enquêter sur la disparition de sa nièce, Harriet, survenue des années auparavant. Vanger est convaincu qu’elle a été assassinée par un membre de sa propre famille.
					Lisbeth Salander, jeune femme rebelle mais enquêtrice exceptionnelle, est chargée de se renseigner sur Blomkvist, ce qui va finalement la conduire à travailler avec lui.
					Entre la jeune femme perturbée qui se méfie de tout le monde et le journaliste tenace, un lien de confiance fragile va se nouer tandis qu’ils suivent la piste de plusieurs meurtres. Ils se retrouvent bientôt plongés au cœur des secrets et des haines familiales, des scandales financiers et des crimes les plus barbares…
				",
			"poster" => "/img/tmp/".$rposter.".png",
			"reviews_total" => 54,
			"actors" => $this->casting(15),
			"directors" => $this->casting(2),
			"id" => "12131414",
			"id_big" => "1313131313"
		);
		return $product;
	}

	public function casting($max) {
		for ($x = 0; $x < rand(1, $max); $x++) {
			$casting[] = $this->contact();
		}
		return $casting;
	}

	public function contact() {
		$names = array(
			"Roger Moore",
			"David Fincher",
			"Kevin Spacey",
			"Arnold Schwarzenegger",
			"Bruce Lee",
			"Brad Pitt",
			"Angelina Jolie",
			"Elie Seimoun",
			"Frank Dubosc",
			"Robert de Niro",
			"Jean Reno",
			"Michel Blanc",
			"Chuck Norris",
			"Jet Li",
			"Jason Statham",
			"Jose Anigo",
			"Pape Diouf",
			"Kevin Kuipers",
			"Jesus Christ",
			"Yohan Spychala",
			"Joulse Petit-Kiki",
			"Amanda Lear",
			"Marc Ghorayeb",
			"Michou Labrute",
			"Sangoku",
			"Vanessa Paradis",
			"Thierry Lhermitte",
			"Kanye West",
			"Claude François",
		);
		return array(
			"name" => $names[rand(0, count($names) - 1)],
		);
	}

	public function index() {

		$index = array(
			"reviews" => 54,
			"reviews_scout" => 14,
			"reviews_positive" => 25,
			"reviews_negative" => 29,
			"scouts" => 92,
			"wishes" => 49,
			"lists" => 183,
			"ratings" => 567,
			"recommendations" => 15,
		);
		return $index;
	}

	public function review($probability = 100) {

		$user = $this->user();
		$collection = $this->collection();
		$product = $this->product();

		if (rand(0, 100) > $probability) return false;
		$return = array(
			"product" => $product,
			"user" => $user,
			"collection" => $collection
		);
		return $return;
	}

	public function comment() {
		$comment = array(
			"Une scène finale bienvenue qui ne fait pas plonger le film dans une grande médiocrité.",
			"si Neeson campe un personnage plutôt plaisant et le reste du casting n'aura pas cet honneur et se vera affublés de personnages un peu caricaturaux ou trop lisses.",
			"Ok !",
			"C'est clair, je le veux.",
			"...",
			"Ce cinquième épisode de la célèbre série de jeux de combat 3D à l'arme blanche est placé sous le signe du renouveau. 17 ans après les évènements de SoulCalibur IV, Ce cinquième épisode de la célèbre série de jeux de combat 3D à l'arme blanche est placé sous le signe du renouveau. 17 ans après les évènements de SoulCalibur IV...",
			"Wesh !!!! C'est-à-dire que c'est du commentaire de daube !",
		);
		return array(
			"comment" =>$comment[rand(0, count($comment) - 1)],
			"date_creation" => '2012-'.str_pad(rand(1,date("m")), 2, "0", STR_PAD_LEFT).'-'.str_pad(rand(1,date("d")), 2, "0", STR_PAD_LEFT).' 15:10:10',
		);
	}

	/**
	 * Users
	 * Products
	 * Feeds
	 *		- label
	 *		- date_creation
	 * Collections
	 */
	public function feed() {
		$sentences = array(
			"<a href=\"\" class=\"username\">Torpenn</a> a ajouté le film <a href=\"\" class=\"product movie\">Spartacus</a> à la liste <a href=\"\" class=\"list\">Top 15 Films de Porteur d'eau</a>",
			"<a href=\"\" class=\"username\">ElizaT</a> a attribué 6/10 au film <a href=\"\" class=\"product movie\">Spartacus</a>",
			"<a href=\"\" class=\"username\">Djibouti</a> a attribué 9/10 au jeu <a href=\"\" class=\"product game\">Final Fantasy XIII-2</a> sur PlayStation 3, il l'a recommandé",
			"<a href=\"\" class=\"username\">Tarask_Coral</a> a écrit <a href=\"\" class=\"review\">une critique</a> sur le jeu <a href=\"\" class=\"product tvShow\">Final Fantasy XIII-2</a> sur PlayStation 3 et lui a attribué 9/10, il l'a recommandé"
		);
		$collection = $this->actions();
		if (rand(0,100) > 70) {
			$collection = array_merge($collection, $this->collection());
		} else {
			if (rand(1,2) == 2) unset($collection["rating"]);
		}
		$feed = array(
			"label" => $sentences[rand(0,3)],
			"date_creation" => '2012-'.str_pad(rand(1,date("m")), 2, "0", STR_PAD_LEFT).'-'.str_pad(rand(1,date("d")), 2, "0", STR_PAD_LEFT).' 15:10:10',
		);
		$return =  array(
			"user" => $this->user(),
			"product" => $this->product(),
			"feed" => $feed,
			"collection" => $collection
		);
		if (rand(0,2) == 1) {
			for ($x = 0; $x < rand(1,3); $x++) {
				$comment[] = array(
					"user" => $this->user(),
					"comment" => $this->comment()
				);
			}
			$return["comments"] = $comment;
		}
		return $return;
	}

	public function scoutActivity() {

		if ($return["rating"]["count"] = rand(1,100)) {
			for($x = 0;$x < rand(1,8);$x++) {
				$return["rating"]["show"][] = array(
					"user" => $this->user(),
					"collection" => array("rating" => rand(1,10) )
				);
			}
		}
		if ($return["is_current"]["count"] = rand(1,100)) {
			for($x = 0;$x < rand(1,10);$x++) {
				$return["is_current"]["show"][] = array(
					"user" => $this->user(),
				);
			}
		}
		if ($return["is_recommend"]["count"] = rand(1,100)) {
			for($x = 0;$x < rand(1,10);$x++) {
				$return["is_recommend"]["show"][] = array(
					"user" => $this->user(),
				);
			}
		}
		if ($return["is_wish_list"]["count"] = rand(1,100)) {
			for($x = 0;$x < rand(1,10);$x++) {
				$return["is_wish_list"]["show"][] = array(
					"user" => $this->user(),
				);
			}
		}
		if ($return["is_shopping_list"]["count"] = rand(1,100)) {
			for($x = 0;$x < rand(1,10);$x++) {
				$return["is_shopping_list"]["show"][] = array(
					"user" => $this->user(),
				);
			}
		}

		return $return;
	}

	public function lists($probability = 100) {
		$rposter = "list-".rand(1, 8);
		$lists = array(
			array(
				"poster" => "/img/tmp/".$rposter.".jpeg",
				"list_title" => "Top 10 films d'action 2011",
				"list_description" =>
					"Certes, comme le souligne le grand Mr Raisin-ver, il y a un coté ridicule à vouloir faire passer une femme de 30 ans, au corps si... bref, à la faire passer pour un enfant de 12 ans, mais je ne sais pas.",
				"list_author" => self::user(),
				"list_date_creation" => "08/02/2012",
				"list_last_update" => "08/02/2012",
				"list_nb_product" => "12",
				"list_link" => "",
				"list_like" => "10",
				"list_like_positive" => "7",
				"list_like_ratio" => "70",
				"list_comment" => rand(0,10),
				"list_subtype" => rand(1,8)
			),
			array(
				"poster" => "/img/tmp/".$rposter.".jpeg",
				"list_title" => "Les lectures indispensables pour tout cinéphile",
				"list_description" =>
					"",
				"list_author" => self::user(),
				"list_date_creation" => "08/02/2012",
				"list_last_update" => "08/02/2012",
				"list_nb_product" => "25",
				"list_link" => "",
				"list_like" => "10",
				"list_like_positive" => "7",
				"list_like_ratio" => "70",
				"list_comment" => rand(0,10),
				"list_subtype" => rand(1,8)
			),
			array(
				"poster" => "/img/tmp/".$rposter.".jpeg",
				"list_title" => "Comment se faire passer pour un adolescent en révolte contre le système",
				"list_description" =>
					"Une comédie qui paraît assez légère de loin et qui en fait traite du sujet un peu malsain qu'est l'adultère.",
				"list_author" => self::user(),
				"list_date_creation" => "08/02/2012",
				"list_last_update" => "08/02/2012",
				"list_nb_product" => "32",
				"list_link" => "",
				"list_like" => "10",
				"list_like_positive" => "7",
				"list_like_ratio" => "70",
				"list_comment" => rand(0,10),
				"list_subtype" => rand(1,8)
			),
			array(
				"poster" => "/img/tmp/".$rposter.".jpeg",
				"list_title" => "Top 10 pires films d'action 2022",
				"list_description" =>
					"si Neeson campe un personnage plutôt plaisant et le reste du casting n'aura pas cet honneur et se vera affublés de personnages un peu caricaturaux ou trop lisses.",

				"list_author" => self::user(),
				"list_date_creation" => "08/02/2012",
				"list_last_update" => "08/02/2012",
				"list_nb_product" => "8",
				"list_link" => "",
				"list_like" => "10",
				"list_like_positive" => "7",
				"list_like_ratio" => "70",
				"list_comment" => rand(0,10),
				"list_subtype" => rand(1,8)
			),
		);
		if (rand(0, 100) > $probability) return false;
		$return = array_merge(
			$lists[rand(0, count($lists) - 1)],
			$this->actions()
		);
		return $return;
	}
}

?>
