<?php

namespace common\extensions\helper;

class Mail extends \lithium\template\Helper {

	public function css($rules) {
		$rules = explode(' ', $rules);
		$return = null;

		$width = '600px';
		$backgroundColor = '#fafafa';
		$linkColor = '#336699';
		$fontColor = '#202020';

		foreach ($rules as $rule) {
			switch ($rule) {
				case 'title': $value = 'color:' . $fontColor . '; display:block; font-family:Arial; font-size:26px; font-weight:bold; line-height:100%; margin-top:0; margin-right:0; margin-bottom:10px; margin-left:0; text-align:center;'; break;
				case 'smallTitle': $value = 'color:' . $fontColor . '; display:block; font-family:Arial; font-size:18px; font-weight:normal; line-height:100%; margin-top:0; margin-right:0; margin-bottom:10px; margin-left:0; text-align:center;'; break;
				case 'category': $value = 'color:' . $fontColor . '; display:block; font-family:Arial; font-size:14px; font-weight:bold; line-height:100%; margin-top:0; margin-right:0; margin-bottom:10px; margin-left:0; text-align:center; padding: 10px; border-bottom: 1px dotted #ddd; text-transform: uppercase; background-color: #eee;'; break;
				case 'td': $value = 'border-collapse:collapse;'; break;
				case 'font': $value = 'color:' . $fontColor . ';'; break;
				case 'li': $value = 'list-style-type: disc; list-style-position: inside; margin-left: 10px; display: list-item;'; break;
				case 'h4': $value = 'color:' . $fontColor . '; display:block; font-family:Arial; font-size:22px; font-weight:bold; line-height:100%; margin-top:0; margin-right:0; margin-bottom:10px; margin-left:0; text-align:left;'; break;
				case 'img': $value = 'border:0; height:auto; line-height:100%; outline:none; text-decoration:none;'; break;
				case 'imgRow': $value = 'text-align: center;'; break;
				case 'imgRowItem': $value = 'margin-right: 5px;'; break;
				case 'body': $value = 'width:100% !important; -webkit-text-size-adjust:none; margin:0; padding:0;'; break;
				case 'background': $value = 'height:100% !important; margin:0; padding:0; width:100% !important; background-color:' . $backgroundColor . ';'; break;
				case 'frame': $value = 'border: 1px solid #DDDDDD; background-color: #ffffff'; break;
				case 'preHeader': $value = 'width: ' . $width . ';'; break;
				case 'preHeaderContent': $value = 'color:#505050; font-family:Arial; font-size:10px; line-height:100%; text-align:center;'; break;
				case 'frameContent': $value = 'text-align:left;'; break;
				case 'footerFont': $value = 'color:#707070; font-family:Arial; font-size:12px; line-height:125%;'; break;
				case 'baseFont': $value = 'color:#505050; font-family:Arial; font-size:14px; line-height:150%;'; break;
				case 'smallFont': $value = 'color:#505050; font-family:Arial; font-size:11px; line-height:150%;'; break;
				case 'rating': $value = 'font-size: 30px;'; break;
				case 'link': $value = 'color:' . $linkColor . '; font-weight:normal; text-decoration:underline;'; break;
				case 'linkNodecoration': $value = 'color:' . $linkColor . '; font-weight:normal; text-decoration:none;'; break;
				case 'linkBlack': $value = 'color:' . $fontColor . ';'; break;
				case 'social': $value = 'background-color:' . $backgroundColor . '; border:0; text-align: center;'; break;
				case 'center': $value = 'text-align: center;'; break;
				case 'avatar': $value = 'border-radius: 50%; -webkit-border-radius: 50%; -moz-border-radius: 50%;'; break;
				case 'message': $value = 'background-color: whitesmoke; border-bottom: 1px solid #ccc;'; break;
				case 'noLeftPadding': $value = 'padding-left: 0;'; break;
				case 'widget': $value = 'width:90%; border:1px solid black; color:#505050; font-family:Arial; font-size:10px; line-height:100%; text-align:center; background-color: #F5F5F5; '; break;
				default: $value = '';
			}

			$return .= $value . ' ';
		}

		return $return;
	}


	public function convertLinks($input) {
		return str_replace('<a ', '<a style="' . $this->css('link') . '" ', $input);
	}


	public function convertTags($input) {
		return mb_ereg_replace('(>)', ' />', $input);
	}

}

?>