<?php

namespace common\extensions\helper;

class Queries extends \lithium\template\Helper {

	public function key($key, $i) {
		$keys = explode('|', $key);
		if (!is_array($keys) || count($keys) < 2) {
			$keys = array($key, 'lambda');
		}
		return $keys[$i];
	}

}

?>