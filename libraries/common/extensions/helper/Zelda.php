<?php

namespace common\extensions\helper;

use common\interfaces\ISingleton;
use common\util\Url;

use Exception;

/**
 * Main application link proxy.
 */
class Zelda extends \lithium\template\Helper implements ISingleton {

	/**
	 *
	 */
	private $_instance;

	/**
	 *
	 */
	public static function instance(array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty(self::$_instance)) {
			self::$_instance = new Zelda();
		}
		return self::$_instance;
	}

	/**
	 * [setEmailCode description]
	 * @param [type] $code [description]
	 */
	public function setEmailCode($code = null) {
		if (!empty($code)) {
			Url::addParam('mailCode', $code);
		} else {
			Url::flushParams();
		}
	}

	/**
	 * [agentAnswer description]
	 * @param  [type] $agentTopicId [description]
	 * @param  [type] $userId       [description]
	 * @param  [type] $value        [description]
	 * @return [type]               [description]
	 */
	public function agentAnswer($agentTopicId, $userId, $value) {
		return Url::url(array(
			'controller' => 'AgentAnswers',
			'action' => 'answer',
			'args' => array($agentTopicId, $userId, $value),
		));
	}

	/**
	 * [topicUsers description]
	 * @param  [type] $topicId [description]
	 * @return [type]          [description]
	 */
	public function topicUsers($topicId) {
		$route = array(
			'controller' => 'Topics',
			'action' => 'index',
			'args' => array($topicId),
		);

		return Url::url($route, array('#' => array('view')));
	}

	/**
	 * [exitTopic description]
	 * @param  [type] $topicId [description]
	 * @param  [type] $userId  [description]
	 * @return [type]          [description]
	 */
	public function exitTopic($topicId, $userId) {
		return Url::url(array(
			'controller' => 'TopicUsers',
			'action' => 'remove',
			'args' => array($topicId, $userId),
		));
	}

	/**
	 * [topic description]
	 * @param  [type] $topicId [description]
	 * @return [type]          [description]
	 */
	public function topic($topicId, $area = '', $action = '') {
		$route = array(
			'controller' => 'Topics',
			'action' => 'index',
		);

		$args = array('view', $topicId);

		if (!empty($area)) {
			$args[] = $area;
		}

		if (!empty($action)) {
			$args[] = $action;
		}

		return Url::url($route, array('#' => $args));
	}

	/**
	 * [auth description]
	 * @param  [type] $api [description]
	 * @return [type]      [description]
	 */
	public function auth($api = null) {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'login',
			'args' => !empty($api) ? array($api) : null,
		));
	}

	/**
	 * [register description]
	 * @return [type] [description]
	 */
	public function register() {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'register',
		));
	}

	/**
	 * [lostPassword description]
	 * @return [type] [description]
	 */
	public function lostPassword() {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'lostPassword',
		));
	}

	/**
	 * [register description]
	 * @return [type] [description]
	 */
	public function activate($code) {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'activate',
			'args' => array($code),
		));
	}

	/**
	 * [register description]
	 * @return [type] [description]
	 */
	public function logout() {
		return Url::url(array(
			'controller' => 'Auth',
			'action' => 'logout',
		));
	}

	/**
	 * [mailOpen description]
	 * @return [type] [description]
	 */
	public function mailOpen() {
		$rand = time() . rand(0,1000) . '.png';

		return Url::url(array(
			'controller' => 'Mails',
			'action' => 'open',
			'args' => array($rand),
		));
	}

	/**
	 * [agentShow description]
	 * @return [type] [description]
	 */
	public function agentShow($agentTopicId) {
		$rand = time() . rand(0,1000) . '.png';

		return Url::url(array(
			'controller' => 'Agents',
			'action' => 'show',
			'args' => array($agentTopicId, $rand),
		));
	}

	/**
	 * [simulations description]
	 * @param  [type] $topic [description]
	 * @param  [type] $user  [description]
	 * @return [type]        [description]
	 */
	public function simulations($topic = null, $user = null) {
		$args = array();
		if (!empty($topic)) {
			$args[] = $topic['id'];
		}
		if (!empty($user)) {
			$args[] = $user['id'];
		}

		return Url::url(array(
			'controller' => 'Simulations',
			'action' => 'index',
			'args' => $args,
		));
	}
}

?>