<?php

namespace common\extensions\command;

use common\models\Users;

class Simulator extends Command {

	protected $_user = null;

	/**
	 *
	 */
	protected function _user() {
		if (empty($this->_user)) {
			$this->_user = Users::testUser();
		}

		return $this->_user;
	}

}

?>