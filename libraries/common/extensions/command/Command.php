<?php

namespace common\extensions\command;

use common\interfaces\IDisplayer;

use common\analysis\Debugger;

use common\actions\responses\ActionResponse;

use common\util\String;
use common\util\ExceptionUtil;

use Exception;

class Command extends \lithium\console\Command implements IDisplayer {

	/**
	 * @var boolean debug
	 */
	public $debug = false;

	/**
	 * @var boolean debug
	 */
	public $test = true;

	/**
	 * @var boolean quiet
	 */
	public $quiet = false;

	/**
	 *
	 */
	protected $_params = null;


	/**
	 * @var int
	 */
	protected $_startTime;

	/**
	 * @var array
	 */
	protected $_executionTime;

	/**
	 *
	 */
	public function invokeMethod($method, $params = array()) {
		if (method_exists($this, $method)) {
			if ($this->_onBeforeAction($method)) {
				try {
					$status = parent::invokeMethod($method, $params);
				} catch (Exception $e) {
					$this->_onException($e);
					$status = false;
				}
				$this->_onAfterAction($status);
			} else {
				$this->_onAfterAction(false);
			}
		} else {
			$this->error($method . ' doesn\'t exist !');
		}

		$this->_onEndRun();
	}

	/**
	 *
	 */
	public function error($error = null, $options = array('nl' => 1)) {
		return parent::error($error, array('style' => 'red') + $options);
	}

	/**
	 *
	 */
	protected function _onException($e) {
		$this->out(ExceptionUtil::toString($e), array('style' => 'red'));
	}

	/**
	 *
	 */
	protected function _onEndRun() {
		if ($this->debug) {
			$this->out('[DEBUG] Execution time : ' . $this->_executionTime . " seconds", array('style' => 'yellow'));
			Debugger::afterRender();
			Debugger::display(true, array('console' => $this));
		}
	}

	/**
	 * Logging time
	 */
	protected function _onBeforeAction($method) {
		$this->_startTime = microtime(true);

		if ($this->test) {
			$this->header(get_called_class() . '::' . $method);
		}

		return true;
	}

	/**
	 * Determinate execution time
	 */
	protected function _onAfterAction($result) {
		$this->_executionTime = round(microtime(true) - $this->_startTime, 4);

		if ($this->test) {
			$this->outResult($result);
		}

		return true;
	}

	/**
	 *
	 */
	public function log($message) {
		if ($this->debug) {
			return Debugger::syslog($message, Debugger::SYSLOG_CRON);
		}
	}

	/**
	 * Override to disable on quiet option.
	 */
	public function out($output = null, $options = array('nl' => 1)) {
		if ($this->debug) {
			$this->log($output);
		}

		if (!$this->quiet) {
			return parent::out($output, $options);
		}
	}

	/**
	 *
	 */
	protected function _params(array $params = array()) {
		foreach ($params as $field => $default) {
			if (isset($this->{$field}) && $this->{$field} !== null) {
				$params[$field] = $this->{$field};
			}
		}
		$this->_params = $params;
		if ($this->test) {
			$this->outParams($this->_params);

			// @todo : change this in future
			$params['user'] = \common\models\Users::testUser();
		}
		return $params;
	}

	/**
	 * @interface IDisplayer
	 */
	public function startBlock($title = '') {
		if (!empty($title)) {
			$this->header($title);
		} else {
			$this->hr(30);
		}
	}

	/**
	 * @interface IDisplayer
	 */
	public function endBlock($title = '') {
		if (!empty($title)) {
			$this->header($title);
		} else {
			$this->hr(30);
		}
	}

	/**
	 * @interface IDisplayer
	 */
	public function outParams($data = null) {
		if (empty($data) || !is_array($data)) {
			$this->out('Empty params');
		} else {
			$this->_outArray($data, array('name' => 'Params'));
		}
	}

	/**
	 * @interface IDisplayer
	 */
	public function outResult($result) {
		$this->out('=> Result', array('style' => 'green'));
		$this->outData($result, array('style' => 'green'));
	}

	/**
	 * @interface IDisplayer
	 */
	public function outData($data, array $options = array()) {
		if ($data instanceof Exception) {
			$this->_onException($data);
		} else if ($data instanceof ActionResponse) {
			$this->outData($data->label(), array('style' => $data->success() ? 'green' : 'red'));
			if ($data->success()) {
				$this->outData($data->result(), $options);
			} else {
				$exception = $data->getException();
				if (!empty($exception)) {
					$this->outData($exception);
				}
			}
		} else if ($data instanceof \lithium\data\entity\Record) {
			$this->_outRecord($data, $options);
		} else if (is_array($data)) {
			$this->_outArray($data, $options);
		} else if (is_bool($data)) {
			$this->out(empty($data) ? 'FALSE' : 'TRUE', $options);
		} else {
			$this->out($data, $options);
		}
	}

	/**
	 *
	 */
	protected function _outArray(array $data, array $options = array()) {
		$defaults = array('name' => 'Array');
		$options += $defaults;

		$output = array(
			array($options['name']),
			array('------'),
		);
		foreach ($data as $key => $value) {
			$value = String::toString($value);
			$output[] = array($key, '=>', $value);
		}
		$this->columns($output);
	}

	/**
	 *
	 */
	protected function _outRecord($record, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (!($record instanceof \lithium\data\entity\Record)) {
			$this->error('not a record');
			return;
		}

		if (empty($record)) {
			$this->error('record is empty');
			return;
		}

		$title = $record->exists() ? 'R ' : 'T ';
		$title .= !empty($record->id) ? $record->id : '';
		$title .= ' |' . get_class($record);

		$schema = $record->schema();
		$fields = array();
		$values = array();
		foreach ($schema->fields() as $field => $fieldConfig) {
			$fields[] = $field;
			$values[] = isset($record->{$field}) ? $record->{$field} : null;
		}

		$output = array(
			$title,
			'-----------------',
			$fields,
			$values
		);

		$this->hr();
		$this->columns($output);
		$this->hr();
	}

	/**
	 *
	 */
	public function debugSql($r, $fields = null) {
		$outTable = array();
		$i = 0;

		foreach ($r->resource() as $k => $v) {
			if ($i == 0) {
				if ($fields == null) {
					$fields = array_keys($v);
				}

				$out[] = $fields;
				$out[] = array_fill(0,count($fields), '----');
			}

			$line = array();

			foreach( $fields as $vv) {
				$line[] = $v[$vv];
			}

			$out[] = $line;
			$i++;
		}

		$this->columns($out, array('style' => 'yellow'));
	}
}

?>