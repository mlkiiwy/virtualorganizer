<?php

namespace common\extensions\command;

use common\data\Semaphores;

use common\analysis\Debugger;

class Cron extends Command {

	protected static $_methodConfig = array();

	/**
	 *
	 */
	public function __construct(array $config = array()) {
		// Capture fatal error
		register_shutdown_function(array($this, 'onShutdown'));

		parent::__construct($config);
	}

	/**
	 * [onShutdown description]
	 * @return [type] [description]
	 */
	public function onShutdown() {
		if (!$this->_ended) {
			$this->_onAfterAction(false);
			$this->_ended = true;
		}
	}

	/**
	 *
	 */
	protected function _onBeforeAction($method) {
		if ($this->_canBeExecute($method) && parent::_onBeforeAction($method)) {
			// Indicate execution => blocks others executions
			$semaphoreData = $this->_getSemaphoreConfig($method);
			if (!empty($semaphoreData)) {
				Semaphores::create($semaphoreData);
			}

			return true;
		}

		return false;
	}

	/**
	 *
	 */
	protected function _onAfterAction($result) {
		// Delete semaphore if exist
		$semaphoreData = $this->_getSemaphoreConfig($this->_method);
		if (!empty($semaphoreData)) {
			Semaphores::delete($semaphoreData);
		}

		if ($result === false) {
			Debugger::syslog(get_class($this) . '::' . $this->_method . ' Execution end with an error', Debugger::SYSLOG_CRON, compact('result'));
		}

		return parent::_onAfterAction($result);
	}

	/**
	 *
	 */
	protected function _canBeExecute($method) {
		$methodConfig = static::_getMethodConfig($method);
		$can = true;

		if (empty($methodConfig['parallel'])) {
			$semaphoreData = $this->_getSemaphoreConfig($method);
			$semaphore = empty($semaphoreData) ? false : Semaphores::get($semaphoreData);

			if (!empty($semaphore)) {
				// Too much execution time ?
				if (!empty($semaphore['timestamp']) && (time() - $semaphore['timestamp'] > $methodConfig['maxExecutionTime'])) {
					$diff = time() - $semaphore['timestamp'];

					$this->error("Method : `$method` is already in execution but is out of time limit by `$diff` seconds");

					Debugger::syslog(get_class($this) . '::' . $this->_method . ' Execution exceded execution time by `$diff` seconds', Debugger::SYSLOG_CRON);

					// Delete the semaphore
					Semaphores::delete($semaphoreData);
				} else {
					$can = false;

					$this->error("Method : `$method` cannot be executed is already executed");
				}
			}
		}

		return $can;
	}

	/**
	 *
	 */
	protected function _onException($e) {
		Debugger::syslog(get_class($this) . '::' . $this->_method . ' Fire an Exception', Debugger::SYSLOG_CRON, array('message' => $e->getMessage()));
		return parent::_onException($e);
	}

	/**
	 *
	 */
	protected static function _getMethodConfig($method) {
		$defaults = array(
			'parallel' => true,
			'maxExecutionTime' => 3000,
		);

		$data = empty(static::$_methodConfig[$method]) ? array() : static::$_methodConfig[$method];
		$data += $defaults;

		return $data;
	}

	/**
	 * [_getSemaphoreConfig description]
	 * @param  [type] $method [description]
	 * @return [type]         [description]
	 */
	protected function _getSemaphoreConfig($method) {
		$methodConfig = static::_getMethodConfig($method);

		if ($methodConfig['parallel'] == true) {
			return false;
		}

		if (!empty($methodConfig['keyGenerator'])) {
			$func = $methodConfig['keyGenerator'];
			$key = $func($this);
		} else {
			$key = get_class($this) . '::' . $method;
		}

		$data = array(
			'key' => $key,
			'ttl' => '+1 Day',
			'data' => array('timestamp' => time()),
		);

		return $data;
	}
}

?>