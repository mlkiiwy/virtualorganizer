<?php

namespace common\extensions\exceptions;

/**
 *
 */
class ControllerParameterException extends \Exception {

	const REQUIRED = 1;
	const PARAM_EMPTY = 2;
	const TYPE = 3;

	public function __construct($paramName, $type = 'required', $message = '', $code = 0, Exception $previous = null) {
		$this->paramName = $paramName;
		$this->type = $type;
		parent::__construct($message, $code, $previous);
	}

	public $paramName = '';
	public $type;

	public $errors = array();
}

?>