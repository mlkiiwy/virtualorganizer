<?php

namespace common\extensions\tasks;

use google\appengine\api\taskqueue\PushTask;
use google\appengine\api\taskqueue\PushQueue;

use common\data\collection\RecordSet;
use common\data\entity\Record;

use common\util\Klass as KlassUtil;

use Exception;

class Tasks {

	/**
	 * [enabled description]
	 * @return [type] [description]
	 */
	public static function enabled() {
		return defined('IN_GOOGLE_APPENGINE') && IN_GOOGLE_APPENGINE;
	}

	/**
	 * [add description]
	 * @param [type] $value   [description]
	 * @param array  $options [description]
	 */
	public static function addAction($action, array $options = array()) {
		$actionName = KlassUtil::simpleClassname($action);
		$parameters = $action->input();

		foreach ($parameters as $name => $value) {
			if ($value instanceof Record) {
				$parameters[$name] = $value->id;
			} else if ($value instanceof RecordSet) {
				$parameters[$name] = $value->values('id');
			}
		}

		$pushTask = new PushTask('/tasks/action/' . $actionName . '.json', $parameters);
		return $pushTask->add();
	}

}
?>
