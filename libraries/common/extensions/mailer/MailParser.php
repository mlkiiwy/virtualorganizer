<?php

namespace common\extensions\mailer;

use common\util\Set;
use common\util\Mail as MailUtil;

class MailParser {

	const PLAINTEXT = 1;
	const HTML = 2;

	const PROVIDER_GMAIL = 'gmail';
	const PROVIDER_YAHOO = 'yahoo';

	/**
	 *
	 * @var boolean
	 */
	private $isImapExtensionAvailable = false;

	/**
	 *
	 * @var string
	 */
	private $emailRawContent;

	/**
	 *
	 * @var associative array
	 */
	protected $rawFields;

	/**
	 *
	 * @var array of string (each element is a line)
	 */
	protected $rawBodyLines;

	/**
	 *
	 * @param string $emailRawContent
	 */
	public function  __construct($emailRawContent) {
		$this->emailRawContent = $emailRawContent;

		$this->extractHeadersAndRawBody();

		if (function_exists('imap_open')) {
			$this->isImapExtensionAvailable = true;
		}
	}

	/**
	 * [extractHeadersAndRawBody description]
	 * @return [type] [description]
	 */
	private function extractHeadersAndRawBody() {
		$lines = preg_split("/(\r?\n|\r)/", $this->emailRawContent);

		$currentHeader = '';

		$i = 0;
		foreach ($lines as $line) {
			if(self::isNewLine($line)) {
				// end of headers
				$this->rawBodyLines = array_slice($lines, $i);
				break;
			}

			// start of new header
			if ($this->isLineStartingWithPrintableChar($line)) {
				preg_match('/([^:]+): ?(.*)$/', $line, $matches);
				$newHeader = strtolower($matches[1]);
				$value = $matches[2];
				$this->rawFields[$newHeader] = $value;
				$currentHeader = $newHeader;
			} else {
				// more lines related to the current header

				if ($currentHeader) {
					// to prevent notice from empty lines
					$this->rawFields[$currentHeader] .= substr($line, 1);
				}
			}
			$i++;
		}
	}

	/**
	 *
	 * @return string (in UTF-8 format)
	 * @throws Exception if a subject header is not found
	 */
	public function getSubject() {
		if (!isset($this->rawFields['subject'])) {
			throw new Exception("Couldn't find the subject of the email");
		}

		$ret = '';

		if ($this->isImapExtensionAvailable) {
			foreach (imap_mime_header_decode($this->rawFields['subject']) as $h) { // subject can span into several lines
				$charset = ($h->charset == 'default') ? 'US-ASCII' : $h->charset;
				$ret .=  iconv($charset, "UTF-8//TRANSLIT", $h->text);
			}
		} else {
			$ret = utf8_encode(iconv_mime_decode($this->rawFields['subject']));
		}

		return $ret;
	}

	/**
	 *
	 * @return array
	 */
	public function getCc() {
		if (!isset($this->rawFields['cc'])) {
			return array();
		}

		return MailUtil::extractEmails($this->rawFields['cc'], array('label' => true));
	}

	/**
	 *
	 * @return array
	 * @throws Exception if a to header is not found or if there are no recipient
	 */
	public function getTo() {
		if ( (!isset($this->rawFields['to'])) || (!count($this->rawFields['to']))) {
			throw new Exception("Couldn't find the recipients of the email");
		}
		return MailUtil::extractEmails($this->rawFields['to'], array('label' => true));
	}

	/**
	 *
	 * @return string
	 * @throws Exception if a to header is not found or if there are no expeditor
	 */
	public function getFrom() {
		if ( (!isset($this->rawFields['from'])) || (!count($this->rawFields['from']))) {
			throw new Exception("Couldn't find the expeditor of the email");
		}
		return MailUtil::extractEmail($this->rawFields['from'], array('label' => true));
	}

	/**
	 * [getProvider description]
	 * @return [type] [description]
	 */
	public function getProvider() {
		$from = $this->getFrom();
		$provider = null;

		if (!empty($from)) {
			$elements = explode('@', Set::first($from));
			$elements = !empty($elements[1]) ? explode('.', $elements[1]) : array();
			$provider = !empty($elements[0]) ? $elements[0] : null;
		}

		return $provider;
	}

	/**
	 * return string - UTF8 encoded
	 *
	 * Example of an email body
	 *
	 */
	public function getBody($returnType=self::PLAINTEXT) {
		$body = '';
		$detectedContentType = false;
		$contentTransferEncoding = null;
		$charset = 'ASCII';
		$waitingForContentStart = true;

		if ($returnType == self::HTML) {
			$contentTypeRegex = '/^Content-Type: ?text\/html/i';
		} else {
			$contentTypeRegex = '/^Content-Type: ?text\/plain/i';
		}

		// there could be more than one boundary
		preg_match_all('!boundary=(.*)$!mi', $this->emailRawContent, $matches);
		$boundaries = $matches[1];
		// sometimes boundaries are delimited by quotes - we want to remove them
		foreach($boundaries as $i => $v) {
			$boundaries[$i] = str_replace(array("'", '"'), '', $v);
		}

		foreach ($this->rawBodyLines as $line) {
			if (!$detectedContentType) {

				$line = trim($line);
				if (empty($line)) {
					continue;
				}

				if (preg_match($contentTypeRegex, $line, $matches)) {
					$detectedContentType = true;
				}

				if(preg_match('/charset=(.*)/i', $line, $matches)) {
					$charset = strtoupper(trim($matches[1], '"'));
				}

			} else if ($detectedContentType && $waitingForContentStart) {

				if(preg_match('/charset=(.*)/i', $line, $matches)) {
					$charset = strtoupper(trim($matches[1], '"'));
				}

				if ($contentTransferEncoding == null && preg_match('/^Content-Transfer-Encoding: ?(.*)/i', $line, $matches)) {
					$contentTransferEncoding = $matches[1];
				}

				if (self::isNewLine($line)) {
					$waitingForContentStart = false;
				}
			} else if ($detectedContentType && !$waitingForContentStart) {
				$skip = false;

				foreach ($boundaries as $boundary) {
					if (strpos($line, $boundary) !== false) {
						$skip = true;
					}
				}

				if (!$skip) {
					$body .= $line . "\n";
				} else {
					break;
				}
			}
		}

		if (!$detectedContentType) {
			// if here, we missed the text/plain content-type (probably it was
			// in the header), thus we assume the whole body is what we are after
			$body = implode("\n", $this->rawBodyLines);
		}

		// removing trailing new lines
		$body = preg_replace('/((\r?\n)*)$/', '', $body);

		if ($contentTransferEncoding == 'base64') {
			//var_dump($body);
			$body = base64_decode($body);
		} else if ($contentTransferEncoding == 'quoted-printable') {
			$body = quoted_printable_decode($body);
		}

		if($charset != 'UTF-8') {
			// FORMAT=FLOWED, despite being popular in emails, it is not
			// supported by iconv
			$charset = str_replace("FORMAT=FLOWED", "", $charset);

			$bodyCopy = $body;
			$body = iconv($charset, 'UTF-8//TRANSLIT', $body);

			if ($body === FALSE) { // iconv returns FALSE on failure
				$body = utf8_encode($bodyCopy);
			}
		}

		return $body;
	}

	/**
	 * @return string - UTF8 encoded
	 *
	 */
	public function getPlainBody(array $options = array()) {
		$defaults = array(
			'remove_quotes' => true,
		);
		$options += $defaults;

		$text = $this->getBody(self::PLAINTEXT);

		if ($options['remove_quotes']) {
			//get rid of any quoted text in the email body
			$lines = explode("\n",$text);
			$text = "";
			$from = $this->getFrom();
			$to = array_slice($this->getTo(), 0, 1);

			$toName = !empty($to) ? Set::getFirstKey($to) : '';
			$toEmail = !empty($to) ? Set::first($to) : '';
			$fromName = !empty($from) ? Set::getFirstKey($from) : '';
			$fromEmail = !empty($from) ? Set::first($from) : '';

			foreach($lines as $key => $value){

				//remove hotmail sig
				if($value == "_________________________________________________________________"){
					break;

				//original message quote
				} elseif(preg_match("/^-*(.*)Original Message(.*)-*/i",$value,$matches)){
					break;

				//check for date wrote string
				} elseif(preg_match("/^On(.*)wrote:(.*)/i",$value,$matches)) {
					break;

				//check for date wrote string french
				} elseif(preg_match("/^Le(.*)écrit :(.*)/i",$value,$matches)) {
					break;

				//check for From Name email section
				} elseif(preg_match("/^On(.*)$fromName(.*)/i",$value,$matches)) {
					break;

				//check for To Name email section
				} elseif(preg_match("/^On(.*)$toName(.*)/i",$value,$matches)) {
					break;

				//check for To Email email section
				} elseif(preg_match("/^(.*)$toEmail(.*)wrote:(.*)/i",$value,$matches)) {
					break;

				//check for From Email email section
				} elseif(preg_match("/^(.*)$fromEmail(.*)wrote:(.*)/i",$value,$matches)) {
					break;

				//check for quoted ">" section
				} elseif(preg_match("/^>(.*)/i",$value,$matches)){
					break;

				//check for date wrote string with dashes
				} elseif(preg_match("/^---(.*)On(.*)wrote:(.*)/i",$value,$matches)){
					break;

				//add line to body
				} else {
					$text .= "$value\n";
				}
			}

		}

		return trim($text);
	}

	/**
	 * return string - UTF8 encoded
	 */
	public function getHTMLBody(array $options = array()) {
		$defaults = array(
			'remove_quotes' => true,
		);
		$options += $defaults;

		$html = $this->getBody(self::HTML);

		if ($options['remove_quotes']) {

			$provider = $this->getProvider();
			switch ($provider) {
				case self::PROVIDER_GMAIL:
					$startQuotes = mb_strpos($html, '<div class="gmail_extra">');

					if ($startQuotes !== false) {
						$html = substr($html, 0, $startQuotes);
					}
					break;

				case self::PROVIDER_YAHOO:
					$startQuotes = mb_strpos($html, '<div class="yahoo_quoted"');
					if ($startQuotes !== false) {
						$html = substr($html, 0, $startQuotes);

						$bodyPos = mb_strpos($html, '<body>');
						$html = substr($html, $bodyPos + 6);
					}
					break;
			}
		}

		return $html;
	}

	/**
	 * N.B.: if the header doesn't exist an empty string is returned
	 *
	 * @param string $headerName - the header we want to retrieve
	 * @return string - the value of the header
	 */
	public function getHeader($headerName) {
		$headerName = strtolower($headerName);

		if (isset($this->rawFields[$headerName])) {
			return $this->rawFields[$headerName];
		}
		return '';
	}

	/**
	 *
	 * @param string $line
	 * @return boolean
	 */
	public static function isNewLine($line) {
		$line = str_replace("\r", '', $line);
		$line = str_replace("\n", '', $line);

		return (strlen($line) === 0);
	}

	/**
	 *
	 * @param string $line
	 * @return boolean
	 */
	private function isLineStartingWithPrintableChar($line) {
		return preg_match('/^[A-Za-z]/', $line);
	}
}

?>