<?php

namespace common\extensions\mailer;

use common\util\Url;
use common\util\Set;
use common\util\Html as HtmlUtil;
use common\util\ExceptionUtil;

use lithium\template\View;
use lithium\core\Environment;

use google\appengine\api\mail\Message as GMessage;

use li3_swiftmailer\mailer\Message;
use li3_swiftmailer\mailer\Transports;

use lithium\analysis\Logger;

use InvalidArgumentException;
use Exception;

/**
 *
 */
class Mail {

	const ENV_CONFIG_KEY = 'mail';

	/**
	 *
	 */
	public static function config() {
		return Environment::get(self::ENV_CONFIG_KEY);
	}

	/**
	 * [render description]
	 * @param  [type] $action  [description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function render($action, $data = array(), array $options = array()) {
		$defaults = array(
			'controller' => 'mails',
			'generateTextFromHtml' => true,
		);
		$options += $defaults;

		// Set Url on absolute mode
		Url::toggleAbsolute(true);

		// Add mailCode
		Url::addParam('mailCode', empty($data['email']['code']) ? '' : $data['email']['code']);

		// Build renderer
		$view  = new View(array(
			'view' => 'li3_twig\template\View',
			'loader' => 'li3_twig\template\Loader',
			'renderer' => 'li3_twig\template\view\adapter\Twig',
			'paths' => array(
				'template' => array(
					'{:library}/views/{:controller}/{:template}.{:type}',
					'{:library}/views/layouts',
				)
			)
		));

		// Render html
		try {
			$bodyHtml = $view->render(
				'template',
				$data,
				array(
					'controller' => $options['controller'],
					'template'=> $action,
					'type' => 'mail.html.twig'
				)
			);
		} catch(Exception $e) {

			Logger::write('error', ExceptionUtil::toString($e));

			if (get_class($e) == 'Twig_Error_Loader') {
				$bodyHtml = '';
			} else {
				throw $e;
			}
		}

		try {
			$bodyText = trim($view->render(
				'template',
				$data,
				array(
					'controller' => $options['controller'],
					'template'=> $action,
					'type' => 'mail.text.twig'
				)
			));
		} catch(Exception $e) {

			Logger::write('error', ExceptionUtil::toString($e));

			if (get_class($e) == 'Twig_Error_Loader') {
				$bodyText = '';
			} else {
				throw $e;
			}
		}

		if (empty($bodyText) && $options['generateTextFromHtml']) {
			$bodyText = HtmlUtil::toText($bodyHtml);
		}

		Url::flushParams();
		Url::toggleAbsolute();

		return compact('bodyHtml', 'bodyText');
	}

	/**
	 * [sendByTemplate description]
	 * @param  [type] $to      [description]
	 * @param  [type] $subject [description]
	 * @param  [type] $action  [description]
	 * @param  [type] $from    [description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function sendByTemplate($to, $subject, $action, $from, array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$content = static::render($action, $data);

		return static::send($to, $subject, $content, $from);
	}

	/**
	 * [send description]
	 * @param  [type]  $to       [description]
	 * @param  [type]  $subject  [description]
	 * @param  [type]  $content  [description]
	 * @param  boolean $from     [description]
	 * @param  boolean $bodyText [description]
	 * @param  array   $options  [description]
	 * @return [type]            [description]
	 */
	public static function send($to, $subject, $content, $from = false, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$config = self::config();
		$from = empty($from) ? $config['default_from'] : $from;

		if (empty($to)) {
			throw new Exception('to is empty');
		}

		if (empty($subject)) {
			throw new Exception('subject is empty');
		}

		if (empty($content)) {
			throw new Exception('content is empty');
		}

		if (is_array($content)) {
			$bodyText = empty($content['bodyText']) ? '' : $content['bodyText'];
			$bodyHtml = empty($content['bodyHtml']) ? '' : $content['bodyHtml'];
		} else {
			$bodyHtml = $content;
			$bodyText = null;
		}

		if (empty($bodyHtml)) {
			throw new Exception('bodyHtml is empty');
		}

		// For dev config
		$overrideTo = Environment::get('mail.overrideTo');
		if (!empty($overrideTo)) {
			if (is_array($to)) {
				$newTo = array();
				foreach ($to as $k => $v) {
					$newTo[$overrideTo] = $v;
				}
				$to = $newTo;
			} else {
				$to = $overrideTo;
			}
		}

		if (IN_GOOGLE_APPENGINE) {
			$to = Set::getFirstKey($to);

			$data = array(
				'sender' => $from,
				'to' => $to,
				'subject' => $subject,
				'textBody' => $bodyText,
				'htmlBody' => $bodyHtml,
			);

			$message = new GMessage($data);
			$message->setReplyTo($from);
			$message->send();

			return true;
		} else {
			// Build mailer
			$mailer = Transports::adapter('default');

			$message = Message::newInstance()
					->setFrom($from)
					->setTo($to)
					->setSubject($subject);

			$message->setBody($bodyHtml, 'text/html');

			if(!empty($bodyText)) {
				$message->addPart($bodyText);
			}

			return $mailer->send($message);
		}

	}
}

?>