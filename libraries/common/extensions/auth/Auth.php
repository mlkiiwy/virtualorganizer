<?php

namespace common\extensions\auth;

use \lithium\core\Environment;

use common\extensions\storage\Visitor;

use common\models\Users;
use common\models\UserTokens;
use common\models\Applications;

use Exception;

class Auth {

	const SESSION_KEY = 'Auth.user';
	const TOKEN_LIFETIME = 2592000; // 60 * 60 * 24 * 30 = 30 Days

	protected static $_config = array(
		'session' => 'lithium\storage\Session',
	);

	private static $_user = -1;

	/**
	 * [user description]
	 * @param  mixed   $user    [description]
	 * @param  array   $options [description]
	 * @return [type]           [description]
	 */
	public static function user($user = -1, array $options = array()) {
		$defaults = array(
			'applicationId' => Applications::WEBSITE,
		);
		$options += $defaults;

		if ($user === -1) {
			$user = static::getUser($options);
		} else {
			$user = static::setUser($user, $options);
		}

		return $user;
	}

	/**
	 * [setUser description]
	 * @param [type] $user    [description]
	 * @param array  $options [description]
	 */
	public static function setUser($user, array $options = array()) {
		$defaults = array(
			'replace' => false,
			'write_token_cookie' => true,
			'applicationId' => Applications::WEBSITE,
		);
		$options += $defaults;

		$actual = static::getUser();

		if (empty($user)) {
			if (!empty($actual)) {
				static::$_user = null;

				// Delete into session
				$session = static::$_config['session'];
				$session::delete(static::SESSION_KEY);

				if ($options['write_token_cookie']) {
					$token = Visitor::getUserToken();
					if (!empty($token)) {
						$token = UserTokens::firstByToken($token, $options['applicationId']);
						if (!empty($token)) {
							$token->invalidate();
						}
					}

					Visitor::deleteUserToken();
				}
			}
		} else {
			$user->checkSaved();

			if (!empty($actual)) {
				if ($actual->id != $user->id && !$options['replace']) {
					throw new Exception('user : ' . $actual->label() .
						' is connected, cannot connect ' . $user->label() .
						' => you must disconnect user first');
				}
			}

			static::$_user = $user;

			// Insert into session
			$session = static::$_config['session'];
			$session::write(static::SESSION_KEY, $user->data());

			if ($options['write_token_cookie']) {
				$token = UserTokens::build($user, $options['applicationId'], array('lifetime' => self::TOKEN_LIFETIME));

				// Write into cookie
				Visitor::setUserToken($token->token, $token->timestampExpiration());
			}
		}

		return static::$_user;
	}


	/**
	 * [getUser description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function getUser(array $options = array()) {
		$defaults = array(
			'refresh' => false,
			'restore_from_cookie' => true,
			'applicationId' => Applications::WEBSITE,
		);
		$options += $defaults;

		if (self::$_user !== -1 && !$options['refresh']) {
			return self::$_user;
		}

		$user = null;

		// From Session
		$session = static::$_config['session'];
		$user = $session::read(static::SESSION_KEY);

		// From Cookie
		if (empty($user) && $options['restore_from_cookie']) {
			$token = Visitor::getUserToken();
			if (!empty($token)) {
				$token = UserTokens::firstByToken($token, $options['applicationId']);
				$user = !empty($token->user) ? $token->user : null;
			}
		} else {
			$user = Users::buildFromArray($user);
		}

		self::$_user = $user;

		return $user;
	}

}
?>
