<?php

namespace common\extensions\oauth;

use \lithium\core\Environment;

use common\models\OAuthTokens;
use common\models\OAuthConsumers;

use OAuthProvider;
use OAuthException;
use Exception;

class Provider {

	const TOKEN_SIZE = 20;

	private $oauth;
	private $consumer;
	private $oauth_error;
	private $error;

	public function __construct() {

		/* create our instance */
		try {
			$this->oauth = new OAuthProvider();
		} catch(Exception $e) {
			$this->oauth_error = true;
			$this->oauth_error = $e;
			return;
		}

		/* setup check functions */
		$this->oauth->consumerHandler(array($this,'checkConsumer'));
		$this->oauth->timestampNonceHandler(array($this,'checkNonce'));
		$this->oauth->tokenHandler(array($this,'checkToken'));

		$this->oauth->setParam('url', NULL);
	}

	public static function createConsumer(){
		$key = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));
		$secret = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));
		return OAuthConsumers::createNew($key,$secret);
	}

	/**
	 * This function check the handlers that we added in the constructor
	 * and then checks for a valid signature
	 */
	public function checkRequest(){
		if( $this->oauth_error) {
			return false;
		}

		/* now that everything is setup we run the checks */
		try{
			$this->oauth->checkOAuthRequest();
		} catch(OAuthException $e){
			// echo OAuthProvider::reportProblem($e);
			$this->oauth_error = true;
			$this->error = $e;
		}

		return !$this->oauth_error;
	}

	/**
	 * This function is called when you are requesting a request token
	 * Basicly it disabled the tokenHandler check and force the oauth_callback parameter
	 */
	public function setRequestTokenQuery(){
		$this->oauth->isRequestTokenEndpoint(true);
		$this->oauth->addRequiredParameter("oauth_callback");
	}

	/**
	 * This function generates a Request token
	 * and save it in the db
	 * then returns the oauth_token, oauth_token_secret & the authentification url
	 * Please note that the authentification_url is not part of the oauth protocol but I added it to show you how to add extra parameters
	 */
	public function generateRequestToken() {

		if( $this->oauth_error){
			return OAuthProvider::reportProblem($this->error);
		}

		$token = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));
		$token_secret = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));

		$callback = $this->oauth->callback;

		OAuthTokens::createRequestToken($this->consumer, $token, $token_secret, $callback);

		$host = Environment::get('domain');
		$host = !empty($host['partners']) ? $host['partners'] : $host;
		$authentification_url = 'http://' . $host . '/oauth/authenticate';

		return "authentification_url=".$authentification_url."&oauth_token=".$token."&oauth_token_secret=".$token_secret."&oauth_callback_confirmed=true";
	}

	/**
	 * This function generates a Access token saves it in the DB and return it
	 * In that process it also removes the request token used to get that access token
	 */
	public function generateAccesstoken(){

		if( $this->oauth_error){
			return OAuthProvider::reportProblem($this->error);
		}

		$access_token = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));
		$secret = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));

		$token = OAuthTokens::findByToken($this->oauth->token);

		$token->changeToAccessToken($access_token,$secret);
		return "oauth_token=".$access_token."&oauth_token_secret=".$secret;
	}

	/**
	 * This function generates a verifier and returns it
	 */
	public function generateVerifier(){
		$verifier = sha1(OAuthProvider::generateToken(self::TOKEN_SIZE));
		return $verifier;
	}

	/* handlers */

	/**
	 * This function checks if the consumer exist in the DB and that it is active
	 * You can modify it at your will but you __HAVE TO__ set $provider->consumer_secret to the right value or the signature will fail
	 * It's called by OAuthCheckRequest()
	 * @param $provider
	 */
	public function checkConsumer($provider){
		$return = OAUTH_CONSUMER_KEY_UNKNOWN;

		$aConsumer = OAuthConsumers::findByKey($provider->consumer_key);

		if( is_object($aConsumer)){
			if( !$aConsumer->isActive()){
				$return = OAUTH_CONSUMER_KEY_REFUSED;
			} else {
				$this->consumer = $aConsumer;
				$provider->consumer_secret = $this->consumer->consumer_secret;
				$return = OAUTH_OK;
			}
		}

		return $return;
	}

	/**
	 * This function checks the token of the client
	 * Fails if token not found, or verifier not correct
	 * Once again you __HAVE TO__ set the $provider->token_secret to the right value or the signature will fail
	 * It's called by OAuthCheckRequest() unless the client is getting a request token
	 * @param unknown_type $provider
	 */
	public function checkToken($provider){
		$token = OAuthTokens::findByToken($provider->token);

		if( is_null($token)){ // token not found
			return OAUTH_TOKEN_REJECTED;
		} elseif($token->type_id == OAuthTokens::TYPE_REQUEST && $token->verifier != $provider->verifier){ // bad verifier for request token
			return OAUTH_VERIFIER_INVALID;
		} else {
			if( $token->type_id == OAuthTokens::TYPE_ACCESS){

			}
			$provider->token_secret = $token->token_secret;
			return OAUTH_OK;
		}
	}

	/**
	 * This function check both the timestamp & the nonce
	 * The timestamp has to be less than 5 minutes ago (this is not oauth protocol so feel free to change that)
	 * And the nonce has to be unknown for this consumer
	 * Once everything is OK it saves the nonce in the db
	 * It's called by OAuthCheckRequest()
	 * @param $provider
	 */
	public function checkNonce($provider){
		if( $this->oauth->timestamp < time() - 5*60){
			return OAUTH_BAD_TIMESTAMP;
		} elseif($this->consumer->hasNonce($provider->nonce,$this->oauth->timestamp)) {
			echo "Find => " . $provider->nonce . ' - ' . $this->oauth->timestamp." ";
			return OAUTH_BAD_NONCE;
		} else {
			$this->consumer->addNonce($this->oauth->nonce);
			return OAUTH_OK;
		}
	}

	public function getToken() {
		return OAuthTokens::findByToken($this->oauth->token);
	}

	public function getError() {
		if( $this->oauth_error){
			return OAuthProvider::reportProblem($this->error);
		}
		return "";
	}

}
?>
