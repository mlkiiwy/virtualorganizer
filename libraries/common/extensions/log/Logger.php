<?php

namespace common\extensions\log;

use common\interfaces\ISingleton;
use common\interfaces\IActionLogger;
use common\interfaces\IActionModel;
use common\interfaces\IActionUser;
use common\interfaces\ILoggableAction;

use common\models\ActionLogs;

class Logger implements ISingleton, IActionLogger {

	private static $_instance;

	/**
	 *
	 */
	public static function instance(array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty(self::$_instance)) {
			self::$_instance = new Logger();
		}
		return self::$_instance;
	}

	/**
	 *
	 */
	public function action($action, $success = null, $step = ActionLogs::STEP_NONE, $exception = null) {

		$data = array();

		$displayer = $action->displayer();

		if (!empty($displayer)) {
			switch ($step) {
				case ActionLogs::STEP_NONE:
					break;

				case ActionLogs::STEP_BEFORE:
					$displayer->startBlock('Start executing action ' . $action->label());
					$displayer->outParams($action->getParams());
					break;

				case ActionLogs::STEP_AFTER:
					if ($success) {
						$displayer->outResult($action->result());
					} else {
						$displayer->outData($exception);
					}
					$displayer->endBlock();
					break;
			}
		}

		// Analyse where write the log of action
		if ($action instanceof IActionModel) {
			$data['action_id'] = $action->modelId();
		}

		if (empty($data['action_id'])) {
			// Not an action so no log
			return false;
		}

		if ($action instanceof IActionUser) {
			$data['user_id'] = $action->userId();
		}

		if ($action instanceof ILoggableAction) {
			$data += $action->dataForLogger();
		}

		$log = ActionLogs::create($data);
		$log->input($action->input());
		$log->result($action->result());
		$log->setState($success);
		$log->setStep($step);
		$log->save();

		return $log;
	}
}

?>