<?php

use lithium\action\Dispatcher as ActionDispatcher;
use lithium\console\Dispatcher as ConsoleDispatcher;
use lithium\core\Libraries;

use lithium\core\Environment;

use common\analysis\Debugger;

$config = Libraries::get('common');

/// Debugger
Debugger::init(!empty($config['debugger']) ? $config['debugger'] : array());

// Alias for Debugger
function d($argument) {
	Debugger::d($argument);
}

/**
 * Define constants for CURL if not defined
 */

$constantsToDefine = array(
	'CURLOPT_CONNECTTIMEOUT' => 'CURLOPT_CONNECTTIMEOUT',
	'CURLOPT_RETURNTRANSFER' => 'CURLOPT_RETURNTRANSFER',
	'CURLOPT_TIMEOUT' => 'CURLOPT_TIMEOUT',
	'CURLOPT_USERAGENT' => 'CURLOPT_USERAGENT',
	'CURLOPT_POSTFIELDS' => 'CURLOPT_POSTFIELDS',
	'CURLOPT_URL' => 'CURLOPT_URL',
);

foreach ($constantsToDefine as $name => $value) {
	if (!defined($name)) {
		define($name, $value);
	}
}

/**
 * OAuthConfiguration
 */

\common\net\oauth\OAuthService::setConfig(Environment::get('oauth'));

?>