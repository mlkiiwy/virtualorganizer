<?php

namespace common\util;

use Exception;

class DataFake {

	/**
	 * [emailContent description]
	 * @return [type] [description]
	 */
	public static function emailContent(array $options = array()) {
		$defaults = array(
			'toEmail' => static::email(),
			'fromEmail' => static::email(),
			'subject' => static::subject(),
			'text' => static::p(),
			'html' => null,
		);
		$options += $defaults;

		$base = "Content-Type: multipart/alternative;
 boundary=\"==============={:codeBoundary}==\"
 MIME-Version: 1.0
To: {:toEmail}
From: {:fromEmail}
Cc:
Subject: {:subject}
Date: Fri, 31 Jan 2014 10:08:18 -0000

--==============={:codeBoundary}==
Content-Type: text/plain; charset=\"utf-8\"
MIME-Version: 1.0
Content-Transfer-Encoding: base64

{:textBase64}

--==============={:codeBoundary}==
Content-Type: text/html; charset=\"utf-8\"
MIME-Version: 1.0
Content-Transfer-Encoding: base64

{:textBase64}

--=================--";

		$codeBoundary = '0137610796119620094';

		$textBase64 = base64_encode($options['text']);
		$options['html'] = empty($options['html']) ? $options['text'] : $options['html'];
		$htmlBase64 = base64_encode($options['html']);

		$data = $options + compact('codeBoundary', 'textBase64', 'htmlBase64');

		return String::insert($base, $data);
	}

	/**
	 *
	 */
	public static function subject() {
		return self::string(array('prefix' => 'subject '));
	}

	/**
	 *
	 */
	public static function emails(array $options = array()) {
		$defaults = array('count' => 10);
		$options += $defaults;

		$emails = array();
		for ($i=0 ; $i < $options['count'] ; $i++) {
			$emails[] = self::email();
		}
		return $emails;
	}

	/**
	 *
	 */
	public static function email() {
		return self::string(array('length' => 6)) . '@' . self::string(array('length' => 6)) . '.com';
	}

	/**
	 *
	 */
	public static function message($user) {
		$message = 'Message de [' . $user->label() . '] ';
		return $message . self::p();
	}

	/**
	 *
	 */
	public static function string(array $options = array()) {
		$defaults = array(
			'length' => 20,
			'prefix' => '',
			'suffix' => '',
		);
		$options += $defaults;

		$str = md5(mt_rand());

		if ($options['length'] > 32) {
			throw new Exception('not implemented');
		}

		$length = $options['length'];
		$length -= mb_strlen($options['prefix']);
		$length -= mb_strlen($options['suffix']);

		if ($length <= 0) {
			throw new Exception('length must be superior to suffix + prefix length combined');
		}

		return $options['prefix'] . substr($str, 0, $length) . $options['suffix'];
	}

	/**
	 *
	 */
	public static function p() {
		return 'Quanta autem vis amicitiae sit, ex hoc intellegi maxime potest, quod ex infinita societate generis humani, quam conciliavit ipsa natura, ita contracta res est et adducta in angustum ut omnis caritas aut inter duos aut inter paucos iungeretur.';
	}
}

?>
