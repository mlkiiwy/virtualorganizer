<?php

namespace common\util;

use Exception;

/**
 *
 */
class Video {

	const TYPE_YOUTUBE = 1;
	const TYPE_DAILY = 2;
	const TYPE_VIMEO = 3;

	const EMBED_CODE_YOUTUBE = '<iframe width="{:width}" height="{:height}" src="http://www.youtube.com/embed/{:code}?autoplay={:autoplay}" frameborder="0" allowfullscreen></iframe>';
	const EMBED_CODE_DAILY = '<iframe frameborder="0" width="{:width}" height="{:height}" src="http://www.dailymotion.com/embed/video/{:code}?autoplay={:autoplay}"></iframe>';
	const EMBED_CODE_VIMEO = '<iframe src="http://player.vimeo.com/video/{:code}?autoplay={:autoplay}&wmode=transparent" width="{:width}" height="{:height}" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';

	const URL_YOUTUBE = 'http://www.youtube.com/embed/{:code}';
	const URL_DAILY = 'http://www.dailymotion.com/embed/video/{:code}';
	const URL_VIMEO = 'http://player.vimeo.com/video/{:code}';

	const YOUTUBE_URL_THUMBNAIL = 'http://img.youtube.com/vi/{:id}/0.jpg';
	const DAILY_URL_THUMBNAIL = 'http://www.dailymotion.com/thumbnail/video/{:id}';
	const VIMEO_URL_THUMBNAIL = 'http://vimeo.com/api/v2/video/{:id}.json';

	public static $_prefix = array(
			'yt' => self::TYPE_YOUTUBE,
			'dm' => self::TYPE_DAILY,
			'vm' => self::TYPE_VIMEO,
		);

	public static function getType($value) {
		$s = mb_strtolower(substr($value, 0,2));
		foreach( self::$_prefix as $prefix => $type) {
			if( $prefix === $s) {
				return $type;
			}
		}
		return false;
	}

	public static function getId($value) {
		return substr($value, 3);
	}

	public static function getThumbnailUrl($value) {
		$type = self::getType($value);
		$id = self::getId($value);
		if( empty($id)) {
			return false;
		}
		$url = false;
		switch($type) {
			case self::TYPE_YOUTUBE:
				$url = self::YOUTUBE_URL_THUMBNAIL;
				$url = String::insert($url, compact('id'));
			break;

			case self::TYPE_DAILY:
				$url = self::DAILY_URL_THUMBNAIL;
				$url = String::insert($url, compact('id'));
			break;

			case self::TYPE_VIMEO:
				$urlGet = self::VIMEO_URL_THUMBNAIL;
				$urlGet = String::insert($urlGet, compact('id'));
				try {
					$data = @file_get_contents($urlGet);
					$data = !empty($data) ? json_decode($data) : array();
					$url = !empty($data[0]->thumbnail_large) ? $data[0]->thumbnail_large : false;
				} catch(Exception $e) {

				}
			break;
		}

		return $url;
	}

	public static function getVideoEmbedCode($value, $width, $height, $autoplay = 0) {
		$type = self::getType($value);
		$code = self::getId($value);
		if( empty($code)) {
			return '';
		}
		$template = '';
		switch($type) {
			case self::TYPE_DAILY:
				$template = self::EMBED_CODE_DAILY;
			break;

			case self::TYPE_VIMEO:
				$template = self::EMBED_CODE_VIMEO;
			break;

			case self::TYPE_YOUTUBE:
				$template = self::EMBED_CODE_YOUTUBE;
			break;

			default:
				return '';
			break;
		}
		return String::insert($template, compact('code', 'width', 'height', 'autoplay'));
	}

	public static function getVideoUrl($value) {
		$type = self::getType($value);
		$code = self::getId($value);
		if( empty($code)) {
			return '';
		}
		$template = '';
		switch($type) {
			case self::TYPE_DAILY:
				$template = self::URL_DAILY;
			break;

			case self::TYPE_VIMEO:
				$template = self::URL_VIMEO;
			break;

			case self::TYPE_YOUTUBE:
				$template = self::URL_YOUTUBE;
			break;

			default:
				return '';
			break;
		}
		return String::insert($template, compact('code'));
	}
}

?>