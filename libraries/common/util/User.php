<?php

namespace common\util;

use common\models\Users;

class User {

	/**
	 * - Filters out spaces and replaces them with a `_` character.
	 * - Checks that the username is at least USERNAME_MIN_LENGTH.
	 * - Checks that the username is at most USERNAME_MAX_LENGTH.
	 * - Checks that the username is unique (should be taken out of this function...)
	 *
	 * @return Empty string if the username is not valid or else the validated username.
	 */
	public static function cleanAndValidUsername($username) {
		$replacement = '_';
		$username = String::cleanUsername($username, $replacement);
		$length = mb_strlen($username);

		if (empty($username) || $length < Users::USERNAME_MIN_LENGTH) {
			return '';
		}

		if ($length > Users::USERNAME_MAX_LENGTH) {
			$username = mb_substr($username, 0, Users::USERNAME_MAX_LENGTH);
			$username = trim($username, $replacement);
		}

		$user = Users::first(array('conditions' => array('username' => $username), 'fields' => array('id')));

		return empty($user) ? $username : '';
	}

	/**
	 * @see \common\models\Users::createFromApiData
	 */
	public static function generateUsername($data) {
		// First try: base
		$username = empty($data['username']) ? null : self::cleanAndValidUsername($data['username']);

		if (!empty($username)) {
			return $username;
		}

		// Second try: first + last name
		$usernameWithName = empty($data['first_name']) ? '' : $data['first_name'];
		$usernameWithName .= empty($data['last_name']) ? '' : $data['last_name'];

		$username = self::cleanAndValidUsername($usernameWithName);

		if (!empty($username)) {
			return $username;
		}

		if (empty($usernameWithName)) {
			$usernameWithName = empty($data['username']) ? null : $data['username'];
		}

		// Third try: second try with numbers appended
		if (!empty($usernameWithName)) {
			$usernameWithName = mb_substr($usernameWithName, 0, (Users::USERNAME_MAX_LENGTH - 2) );

			if (mb_strlen($usernameWithName) >= (Users::USERNAME_MIN_LENGTH - 1)) {
				$i = 1;
				do {
					$username = self::cleanAndValidUsername($usernameWithName . $i);
					$i++;
				} while (empty($username) && $i < 15);

				if (!empty($username)) {
					return $username;
				}
			}
		}

		// Fourth try: email
		if (!empty($data['email'])) {
			$username = self::cleanAndValidUsername($data['email']);

			if (!empty($username)) {
				return $username;
			}
		}

		// Fifth try: random username
		return self::cleanAndValidUsername(md5(time()));
	}
}

?>