<?php

namespace common\util;

use DateTime;
use Exception;

class Date {

	const PREG_DATETIME = '/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/';
	const PREG_DATE = '/^(\d{4})-(\d{2})-(\d{2})$/';
	const PREG_YEAR = '/^(\d{4})$/';

	const SQL_DATE = 'Y-m-d';
	const SQL_DATETIME = 'Y-m-d H:i:s';

	/**
	 * [now description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function now(array $options = array()) {
		$defaults = array('format' => self::SQL_DATETIME);
		$options += $defaults;

		return date($options['format'], strtotime('now'));
	}

	/**
	 *
	 */
	public static function sqlStr($strtotime = 'now', $format = 'Y-m-d H:i:s') {
		return date($format, strtotime($strtotime));
	}

	/**
	 *
	 */
	public static function sqlNow() {
		return self::sqlStr();
	}

	/**
	 *
	 */
	public static function sqlToday() {
		return self::sqlStr('now', 'Y-m-d');
	}

	/**
	 *
	 */
	public static function validBirthdate($date) {
		$date = explode('-', $date);

		if (count($date) != 3 || $date[0] > (date('Y') - 10) || $date[0] < 1910 ) {
			return false;
		}

		return checkdate($date[1], $date[2], $date[0]);
	}

	/**
	 *
	 */
	public static function compare($date1, $date2) {
		return strtotime($date1) - strtotime($date2);
	}

	/**
	 * $date = array($year, $month, $day)
	 */
	public static function isValidWiki($date) {
		// if ($date[0] > 0 && $date[1] > 0 && $date[2] > 0) {
		// 	return checkdate($date[1], $date[2], $date[0]);
		// }

		if ($date[0] > 0 && !($date[1] == 0 && $date[2] > 0)) {
			for ($i = 1; $i <= 2; $i++) {
				if ($date[$i] == 0) {
					$date[$i] = 1;
				}
			}

			return checkdate($date[1], $date[2], $date[0]);
		}

		return false;
	}

	/**
	 * @todo DateTime, timestamp
	 */
	public static function extract($data) {
		if (is_string($data)) {
			if (preg_match(self::PREG_DATETIME, $data)) {
				preg_match_all(self::PREG_DATETIME, $data, $extract);
			} elseif(preg_match(self::PREG_DATE, $data)) {
				preg_match_all(self::PREG_DATE, $data, $extract);
			} elseif (preg_match(self::PREG_YEAR, $data)) {
				preg_match_all(self::PREG_YEAR, $data, $extract);
			} else {
				throw new Exception('Unknown format');
			}

			$fields = array('Y', 'm', 'd', 'H', 'i', 's');
			$r = array();
			$i = 1;

			foreach ($fields as $key) {
				$r[$key] = !empty($extract[$i]) && !empty($extract[$i][0]) ? $extract[$i][0] : 0;
				$i++;
			}
		} else if ($data instanceof DateTime) {
			throw new Exception('TODO');
		} else {
			throw new Exception('TODO');
		}

		return $r;
	}

	protected static function _buildValidDateFromExtract($data) {
		foreach( $data as $k=>$v) {
			switch($k) {
				case 'Y':
				break;

				case 'm':
					if( empty($v) || $v > 12 || $v < 1) {
						$data[$k] = 1;
					}
				break;

				case 'd':
					if( empty($v) || $v > 31 || $v < 1) {
						$data[$k] = 1;
					}
				break;
				/*
				case 'H':
					if( $v > 24 || $v < 0) {
						$data[$k] = 0;
					}
				break;

				case 's':
				case 'i':
					if( empty($v) || $v > 60 || $v < 1) {
						$data[$k] = 1;
					}
				break;*/
			}
		}
		return $data['Y'].'-'.$data['m'].'-'.$data['d'].' '.$data['H'].':'.$data['i'].':'.$data['s'];
	}

	/**
	 *
	 */
	public static function format($d, $format = "%B%e, %Y %H:%M", $forceComplete = false) {
		setlocale(LC_TIME, "fr_FR.UTF-8");

		// Check date validity
		if (is_string($d)) {
			$data = self::extract($d);
			$d = self::_buildValidDateFromExtract($data);

			if( !$forceComplete && $data['d'] == 0 && String::containElements($format, array('%a', '%A', '%d', '%e', '%j', '%u', '%w', '%U', '%V', '%W'))) {
				$format = String::removeElements($format, array('%a', '%A', '%d', '%e', '%j', '%u', '%w', '%U', '%V', '%W'));
			}

			if( !$forceComplete && $data['m'] == 0 && String::containElements($format, array('%b', '%B', '%h', '%m', '%e'))) {
				$format = String::removeElements($format, array('%b', '%B', '%h', '%m', '%e'));
			}

			if( !$forceComplete && $data['Y'] == 0 && String::containElements($format, array('%Y'))) {
				$format = String::removeElements($format, array('%Y'));
			}

			// Modify format
			$format = trim($format);
			if( !empty($format) && $format[0] == ',') {
				$format = substr($format, 1);
			}
		}

		if (is_string($d)) {
			$d = new DateTime($d);
		}

		if ($d instanceof DateTime) {
			$d = $d->getTimestamp();
		}

		return strftime($format, $d);
	}

	/**
	 *
	 */
	public static function formatDateSc($input, $options = array()) {
		$defaults = array(
			'prefix' => true, // En... ou Le... (devant la date)
			'html_tag' => true, // <time/>
			'itemprop' => 'datePublished' // Microdata
		);
		$options += $defaults;

		if( empty($input)) {
			return '';
		}

		$data = self::extract($input);
		if ($options['prefix'] == true) {
			$output = $data['d'] == 0 ?  "en " : "le ";
		} else {
			$output = '';
		}

		if( !empty($data['m']) && $data['m'] > 0 && !empty($data['d']) && $data['d'] > 0 && $options['html_tag'] == true) {
			$output .= '<time datetime="' . htmlspecialchars($input) . '" itemprop="' . $options['itemprop'] . '">' . trim(self::format($input, '%e %B %Y')) . '</time>';
		} else {
			$output .= self::format($input, '%e %B %Y');
		}

		return $output;
	}

	/**
	 *
	 */
	public static function dayToWeek($date, $format = '%Y-%m-%d') {
		setlocale(LC_TIME, "fr_FR.UTF-8");
		$date = DateTime::createFromFormat('d-m-Y', $date);
		$dateBegin = strftime($format, $date->getTimestamp());

		$date->modify('+1 week');
		$dateEnd = strftime($format, $date->getTimestamp());

		return array($dateBegin, $dateEnd);
	}

	/**
	 * Affichage des heures en fonction du subtype_id
	 *
	 * Film, série, etc : 2 h 20 min.
	 * Track : 4:20
	 */
	public static function length($seconds, $subtype_id = 1) {
		if (is_array($seconds)) {
			$seconds = array_shift($seconds);
		}

		if (empty($seconds)) {
			return '';
		}

		$hours = floor($seconds / 3600);
		$minutes = floor(($seconds - ($hours*3600)) / 60);
		$seconds = $seconds - $hours * 3600 - $minutes * 60;

		if ($subtype_id == 8) {
			$string = '';
			if ($hours) {
				$string .= $hours . ':';
			}
			if ($minutes) {
				if ($hours) {
					$string .= str_pad($minutes, 2, '0', STR_PAD_LEFT) . ':';
				} else {
					$string .= $minutes . ':';
				}
			} else {
				$string = '0:';
			}
			if ($seconds) {
				$string .= str_pad($seconds, 2, '0', STR_PAD_LEFT);
			} else{
				$string .= '00';
			}
		} else {
			$string = '';
			if ($hours) {
				$string .= $hours . ' h ';
			}
			if ($minutes) {
				if ($hours) {
					$string .= str_pad($minutes, 2, '0', STR_PAD_LEFT) . ' min';
				} else {
					$string .= $minutes . ' min';
				}
			}
			if ($seconds) {
				if ($minutes) {
					$string .= str_pad($seconds, 2, '0', STR_PAD_LEFT) . ' s ';
				} else {
					$string .= $seconds . ' s';
				}
			}
		}

		return trim($string);
	}

	/**
	 * Input format must be Y-m-d
	 */
	public static function isSameYear($date1, $date2) {
		$d1 = DateTime::createFromFormat(self::SQL_DATE, $date1);
		$d2 = DateTime::createFromFormat(self::SQL_DATE, $date2);

		return $d1->format('Y') == $d2->format('Y');
	}

	/**
	 * Input format must be Y-m-d
	 */
	public static function year($date) {
		$date = DateTime::createFromFormat(self::SQL_DATE, $date);
		return $date->format('Y');
	}
}

?>