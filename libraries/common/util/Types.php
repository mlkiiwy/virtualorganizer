<?php

namespace common\util;

use common\data\collection\RecordSet;
use common\data\entity\Record;

use lithium\util\Validator;

use Exception;

class Types {

	const TYPE_STRING = 'string';
	const TYPE_INT = 'int';
	const TYPE_FLOAT = 'float';
	const TYPE_BOOLEAN = 'boolean';

	const TYPE_EMAIL = 'string_email';

	/**
	 * [isCollectionType description]
	 * @param  [type]  $type [description]
	 * @return boolean       [description]
	 */
	public static function isCollectionType($type) {
		return strpos($type, '[]') !== false;
	}

	/**
	 * [isPrimitive description]
	 * @param  [type]  $type [description]
	 * @return boolean       [description]
	 */
	public static function isPrimitive($type) {
		$primitives = array(
			self::TYPE_STRING,
			self::TYPE_INT,
			self::TYPE_BOOLEAN,
			self::TYPE_EMAIL,
			self::TYPE_FLOAT,
		);

		$pos = strpos($type, '_');
		if ($pos !== false) {
			$type = substr($type, 0, $pos);
		}

		return in_array($type, $primitives);
	}

	/**
	 * [extractTypeFromCollection description]
	 * @param  [type] $type [description]
	 * @return [type]       [description]
	 */
	public static function extractTypeFromCollection($type) {
		$posArray = strpos($type, '[]');
		if ($posArray !== false) {
			$type = substr($type, 0, $posArray);
		}
		return $type;
	}

	/**
	 *
	 */
	public static function test($input, $type, array $options = array()) {
		$defaults = array('tryToCast' => true, 'error' => 'exception');
		$options += $defaults;

		$isArray = static::isCollectionType($type);
		$type = static::extractTypeFromCollection($type);

		$stype = gettype($input);
		if ($isArray) {
			if (!is_array($input) && !$input instanceof ArrayAccess) {
				return self::_error("type not match excepted `array` get `$stype`", $options['error']);
			}
			foreach ($input as $key => $value) {
				$result = self::test($value, $type, $options);
				if ($result === false && $options['error'] === 'return') {
					unset($input[$key]);
				}
			}
			if (empty($input) && $options['error'] === 'return') {
				return false;
			}
			return $input;
		} else {
			switch ($type) {
				case self::TYPE_EMAIL:
					if (is_string($input)) {
						$input = $options['tryToCast'] ? trim((string) $input) : $input;
						if (Validator::isEmail($input)) {
							return $input;
						} else {
							return self::_error("`$input` is not a valid email", $options['error']);
						}
					} else {
						return self::_error("type not match excepted `string` (email) get `$stype`", $options['error']);
					}
					break;

				case self::TYPE_STRING:
					if (is_string($input)) {
						return $options['tryToCast'] ? (string) $input : $input;
					} else {
						return self::_error("type not match excepted `string` get `$stype`", $options['error']);
					}
					break;

				case self::TYPE_INT:
					if (is_numeric($input) || is_null($input)) {
						return $options['tryToCast'] ? (integer) $input : $input;
					} else {
						return self::_error("type not match excepted `int` get `$stype`", $options['error']);
					}
					break;

				case self::TYPE_FLOAT:
					if (is_float($input)) {
						return $options['tryToCast'] ? (float) $input : $input;
					} else {
						return self::_error("type not match excepted `float` get `$stype`", $options['error']);
					}
					break;

				case self::TYPE_BOOLEAN:
					if (is_bool($input)) {
						return $options['tryToCast'] ? (boolean) $input : $input;
					} else {
						return self::_error("type not match excepted `boolean` get `$stype`", $options['error']);
					}
					break;

				default:
					if ($input instanceof Record || $input instanceof RecordSet) {
						$model = $input->model();

						$simpleClassname = Klass::simpleClassname($model);

						if ($simpleClassname != $type) {
							return self::_error("type not match excepted `$type` get `$simpleClassname`", $options['error']);
						}
					}

					// TODO
					return $input;
					// throw new Exception("type not supported : `$stype`");
			}
		}

		return null;
	}

	/**
	 *
	 */
	protected static function _error($msg, $mode) {
		if ($mode == 'exception') {
			throw new Exception($msg);
		} else if ($mode == 'return') {
			return false;
		}
		return null;
	}
}

?>
