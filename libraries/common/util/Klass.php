<?php

namespace common\util;

use ReflectionClass;
use Exception;

class Klass {

	/**
	 *
	 */
	public static function _getReflection($klassName, array $options = array()) {
		return new ReflectionClass($klassName);
	}

	/**
	 *
	 */
	public static function hasInterface($klassName, $interfaceName, array $options = array()) {
		$reflection = self::_getReflection($klassName);
		return $reflection->implementsInterface($interfaceName);
	}

	/**
	 * [instance description]
	 * @param  [type] $klassName [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function instance($klassName, array $options = array()) {
		$fullKlassname = static::fullKlassname($klassName, $options);

		if ($fullKlassname === false) {
			throw new Exception("cannot instanciate : `$klassName`");
		}

		return new $fullKlassname;
	}

	/**
	 * [fullKlassname description]
	 * @param  [type] $klassName [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function fullKlassname($klassName, array $options = array()) {
		$defaults = array(
			'paths' => array(),
		);
		$options += $defaults;

		if (class_exists($klassName)) {
			return $klassName;
		}

		foreach ($options['paths'] as $path) {
			if (class_exists($path . '\\' . $klassName)) {
				return $path . '\\' . $klassName;
			}
		}

		return false;
	}

	/**
	 * [simpleClassname description]
	 * @param  [type] $instanceOrString [description]
	 * @return [type]           [description]
	 */
	public static function simpleClassname($instanceOrString) {
		$klassName = is_string($instanceOrString) ? $instanceOrString : get_class($instanceOrString);
		$split = explode('\\', $klassName);
		return Set::last($split);
	}
}

?>
