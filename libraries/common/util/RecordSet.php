<?php

namespace common\util;

use lithium\data\Entity;
use lithium\data\Collection;

use Exception;

/**
 *
 */
class RecordSet {

	/**
	 * [pickRandomElement description]
	 * @param  [type] $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function pickRandomElement($data, array $options = array()) {
		$defaults = array(
			'count' => 1
		);
		$options += $defaults;

		// Extract keys
		$keys = $data->keys();
		$keys = Set::pickRandomElement($keys, $options);

		if (count($keys) == 1) {
			return $data[$keys];
		} else {
			throw new Exception('not implemented');
		}
	}

	/**
	 * [toData description]
	 * @param  array  $elements [description]
	 * @return [type]           [description]
	 */
	public static function toData(array $elements) {
		$data = array();

		foreach ($elements as $name => $element) {
			if (!empty($element) && ($element instanceof Entity || $element instanceof Collection)) {
				$data[$name] = $element->data();
			} else {
				$data[$name] = $element;
			}
		}

		return $data;
	}

}

?>