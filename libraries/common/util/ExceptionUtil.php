<?php

namespace common\util;

use Exception;

class ExceptionUtil {

	public static function toString(Exception $e) {
		$string = 'Exception (' . get_class($e) . ') ';
		$string .= $e->getMessage();

		$file = $e->getFile();
		if (!empty($file)) {
			$string .= ' on `' . $file . '`';
		}

		$line = $e->getLine();
		if (!empty($line)) {
			$string .= ' at line `' . $line . '`';
		}

		return $string;
	}
}

?>