<?php

namespace common\util;

use Exception;

/**
 *
 */
class Html {

	/**
	 * [toText description]
	 * @param  [type] $html    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function toText($html, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$text = $html;
		$pos = strpos($html, '<body>');
		if ($pos !== false) {
			$text = substr($text, $pos);
		}
		$breaklines = array('<br>', '<BR>', '<BR/>', '<br/>', '<br />' ,'<BR />');
		foreach ($breaklines as $b) {
			$text = str_replace($b, '\n', $text);
		}
		$text = trim(strip_tags($text));

		return $text;
	}

}

?>