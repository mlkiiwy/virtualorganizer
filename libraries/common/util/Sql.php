<?php

namespace common\util;

use lithium\data\Connections;

use Exception;

/**
 * Helper functions for Sql interactions.
 * Consider merging some functions into the PDO adapter for li3.
 */
class Sql {

	protected static $_connection = null;

	public static function getConnection() {
		if (empty(self::$_connection)) {
			self::$_connection = Connections::get('default');
		}

		return self::$_connection;
	}

	public static function setConnection($config = 'default') {
		self::$_connection = Connections::get($config);
	}

	/**
	 * Execute une requête.
	 *
	 * {{{
	 * // Retourne les ressources
	 * Sql::execute("SELECT * FROM table", array()); // Recordset.
	 *
	 * // Retourne le nombre de lignes.
	 * Sql::execute("SELECT * FROM table WHERE id = {:id}", array('id' => 102), 'row_count'); // 1
	 *
	 * // Retourne le dernier identifiant inséré.
	 * Sql::execute("INSERT INTO table (label) values {:label}", array('label' => 'nanark'), 'last_insert_id'); // 231
	 * }}}
	 *
	 * @param string $sql Requête Sql.
	 * @param array $param
	 *	   Tableau associatif des paramètres.
	 *        Par défaut, on ajoute `array{'return' => 'resource'}` si la clé `return` est absente.
	 * @param array $return_value
	 *        Type de output. Par défaut le recordset.
	 *        - last_insert_id : dernier id inséré.
	 *        - row_count : nombre de lignes.
	 * @return mixed
	 */
	public static function execute($sql, $param = array(), $return_value = null, $connection = 'default') {
		if (!array_key_exists('return', $param)) {
			$param = array_merge($param, array('return' => 'resource'));
		}

		if ($connection != 'default') {
			self::setConnection($connection);
		}

		$db = self::getConnection();
		$r = $db->read($sql, $param);

		if ($connection != 'default') {
			self::setConnection();
		}

		switch ($return_value) {
			case 'last_insert_id':
				return $r->connection()->connection->lastInsertId();
			case 'row_count':
				return $r->resource()->rowCount();
			case 'boolean':
				return $r->resource()->rowCount() > 0;
		}

		return $r;
	}

	/**
	 * Démarre une transaction
	 *
	 * {{{
	 * Sql::begin();
	 * }}}
	 */
	public static function begin() {
		return self::execute("START TRANSACTION");
	}

	/**
	 * Effectue un ROLLBACK
	 *
	 * {{{
	 * Sql::rollback();
	 * }}}
	 */
	public static function rollback() {
		return self::execute("ROLLBACK");
	}

	/**
	 * Commit une transaction.
	 *
	 * {{{
	 * Sql::commit();
	 * }}}
	 */
	public static function commit() {
		return self::execute("COMMIT");
	}

	/**
	 * Exécute une création de table.
	 *
	 * {{{
	 * Sql::createTable('matable', 'CREATE TABLE (...)');
	 * }}}
	 */
	public static function createTable($name, $sql) {
		self::deleteTable($name);
		$result = self::execute($sql);
		return array('table' => $name, 'count' => $result->resource()->rowCount());
	}

	/**
	 * Exécute un DROP TABLE
	 *
	 * {{{
	 * Sql::deleteTable('ma_table');
	 * }}}
	 */
	public static function deleteTable($name) {
		return self::execute("DROP TABLE IF EXISTS `".$name."`");
	}

	/**
	 * Exécute un TRUNCATE TABLE
	 *
	 * {{{
	 * Sql::truncate('ma_table');
	 * }}}
	 */
	public static function truncate($name) {
		return self::execute("TRUNCATE TABLE `".$name."`");
	}

	/**
	 * Execute un ANALYZE TABLE.
	 */
	public static function analyze($name) {
		return self::execute("ANALYZE TABLE `".$name."`");
	}

	/**
	 * Escape
	 */
	public static function escape($inp) {
		return is_array($inp) ? array_map(__METHOD__, $inp) : !empty($inp) && is_string($inp)? str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp):$inp;
	}

	/**
	 * Escape
	 */
	public static function formatValue($value) {
		if (preg_match('/^[0-9]*$/', $value) == 1) {
			$return = $value;
		} else {
			$return = "'" . Sql::escape($value) . "'";
		}
		return $return;
	}

	/**
	 * Execute une requête de type SELECT et retourne le recordset
	 */
	public static function executeSelect($query, $param = array(), $connection = 'default') {
		$return = self::execute($query, $param, null, $connection);
		return is_object($return) ? $return->resource()->fetchAll() : $return;
	}

	/**
	 * Execute une requête de type SELECT et retourne la première ligne uniquement
	 */
	public static function executeSelectOne($query, $param = array(), $connection = 'default') {
		$return = self::execute($query, $param, null, $connection);
		return is_object($return) ? $return->resource()->fetch() : $return;
	}

	/**
	 * Execute une requête de type UPDATE et retourne le nombre de lignes affectées.
	 */
	public static function executeUpdate($query, $param = array(), $connection = 'default') {
		return self::execute($query, $param, 'row_count', $connection);
	}

	/**
	 * Execute une requête de type INSERT et retourne le nombre de lignes insérées.
	 *
	 * {{{
	 * // Insertion simple :
	 * Sql::executeInsert("INSERT INTO table (value) values (1)"); // 1
	 *
	 * // Insertion avec arguments :
	 * Sql::executeInsert("INSERT INTO table (value) SELECT value FROM table_from WHERE id = {:type}", array("type" => 1)); // 10
	 * }}}
	 *
	 * @param string $query Requête à exécuter.
	 * @param array $param Valeurs à insérer.
	 * @return int Nombre de lignes insérées.
	 */
	public static function executeInsert($query, $param = array(), $connection = 'default') {
		return self::execute($query, $param, 'row_count', $connection);
	}

	/**
	 * Execute une requête de type INSERT et retourne l'identifiant de la ligne insérée.
	 *
	 * {{{
	 * // Insertion simple :
	 * Sql::executeInsert("INSERT INTO table (value) values (1)"); // 123
	 * }}}
	 *
	 * @param string $query Requête à exécuter.
	 * @param array $param Valeurs à insérer.
	 * @return int Identifiant de la ligne insérée.
	 */
	public static function executeInsertId($query, $param = array(), $connection = 'default') {
		return self::execute($query, $param, 'last_insert_id', $connection);
	}

	/**
	 * Execute une requête de type INSERT à partir d'un tableau associatif.
	 * Retourne le nombre de lignes insérées.
	 *
	 * {{{
	 * $types = array(
	 *		array('id' => 1, 'label' => 'Movie', 'related_to' => 'sc_product' ),
	 *		array('id' => 2, 'label' => 'Book', 'related_to' => 'sc_product'),
	 *		);
	 * Sql::executeInsertArray("table", $types); // 2
	 * }}}
	 *
	 * @param string $table Table cible.
	 * @param array $data Valeurs à insérer.
	 * @return int Nombre de lignes insérées.
	 */
	public static function executeInsertArray($table, $data) {
		if (isset($data[0]) && is_array($data[0])) {
			$r = 0;

			foreach ($data as $data_line) {
				$r += self::executeInsertArray($table, $data_line);
			}

			return $r;
		} else {
			foreach ($data as $key => $value) {
				$keys[] = $key;
				$values[] = "{:". $key . "}";
				$replace[$key] = $value;
			}

			$query = "INSERT INTO `" . $table . "` (" . implode(",", $keys) . ") VALUES (" . implode(", ", $values) . ")";
			return self::execute($query, $replace, 'row_count');
		}
	}

	/**
	 *
	 */
	public static function removeAlias(array $conditions) {
		$newConditions = array();
		foreach( $conditions as $value => $select) {
			$pos = strpos($value, '.');
			if( $pos !== false) {
				$value = substr($value,$pos + 1);
			}
			$newConditions[$value] = $select;
		}
		return $newConditions;
	}

	/**
	 *
	 */
	public static function buildRecordSet($data, $klass) {
		$recordSet = call_user_func(array($klass, 'createRecordSet'));
		foreach( $data as $key => $element) {
			$recordSet[$key] = call_user_func(array($klass, 'create'), $element, array('exists' => true));
		}
		return $recordSet;
	}

	/**
	 * [executeFile description]
	 * @param  [type] $filepath [description]
	 * @return [type]           [description]
	 */
	public static function executeFile($filepath) {
		$content = file_get_contents($filepath);
		$requests = explode(';', $content);
		$good = true;
		foreach ($requests as $request) {
			if (!empty($request)) {
				$good = $good && static::execute($request);
			}
		}
		return $good;
	}
}

?>