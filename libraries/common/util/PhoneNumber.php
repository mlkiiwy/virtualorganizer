<?php

namespace common\util;

use common\models\PhoneTypes;

use Exception;

class PhoneNumber {

	const FRANCE_PREFIX = '+33';

	/**
	 * [clean description]
	 * @param  [type] $input   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function clean($input, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$output = trim($input);
		$output = strtolower($output);
		$output = str_replace(' ', '', $output);
		$output = str_replace('.', '', $output);
		$output = str_replace(',', '', $output);
		$output = str_replace('-', '', $output);
		$output = str_replace('_', '', $output);

		$output = static::extractPart($output);

		if ($output['country'] == self::FRANCE_PREFIX) {
			$output['number'] = strlen($output['number']) == 10 && $output['number'][0] == '0' ? substr($output['number'], 1) : $output['number'];

			if (strlen($output['number']) != 9) {
				return false;
			}
		}

		return $output['country'] . $output['number'];
	}

	/**
	 * [extractPart description]
	 * @param  [type] $input [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public static function extractPart($input, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$output = array(
			'country' => self::FRANCE_PREFIX,
			'number' => $input,
		);

		$plusPosition = strpos($input, '+');
		if ($plusPosition !== false) {
			$output['country'] = substr($input, $plusPosition, 3);
			$output['number'] = substr($input, $plusPosition + 3);
		}

		return $output;
	}

	/**
	 * [getPhoneTypeId description]
	 * @param  [type] $phoneNumber [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public static function getPhoneTypeId($phoneNumber, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$parts = static::extractPart($phoneNumber);
		$type = PhoneTypes::UNKNOW;

		if ($parts['country'] == self::FRANCE_PREFIX) {
			$value = empty($parts['number'][0]) ? 0 : $parts['number'][0];
			$type = in_array(intval($value), array(6,7)) ? PhoneTypes::MOBILE : PhoneTypes::CLASSIC;
		}

		return $type;

	}
}

?>