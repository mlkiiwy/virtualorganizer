<?php

namespace common\util;

use lithium\core\Environment;
use lithium\net\http\Router;

use Exception;

/**
 *
 */
class Url {

	/**
	 * [$_params description]
	 * @var array
	 */
	private static $_params = array();

	/**
	 *
	 */
	private static $_changedRoute = false;

	/**
	 *
	 */
	private static $_absolute = false;

	/**
	 * [addParam description]
	 * @param [type] $name    [description]
	 * @param [type] $value   [description]
	 * @param array  $options [description]
	 */
	public static function addParam($name, $value, array $options = array()) {
		self::$_params[$name] = $value;
	}

	/**
	 * [flushParams description]
	 * @return [type] [description]
	 */
	public static function flushParams() {
		self::$_params = array();
		return true;
	}

	/**
	 * Extracts the video ID from a Youtube, Vimeo or Dailymotion URL.
	 */
	public static function getVideoCode($url) {
		$parsed = parse_url($url);

		if (!empty($parsed['host'])) {
			switch ($parsed['host']) {
				case 'youtu.be':
				case 'www.youtube.com':
					preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);
					return 'yt_' . $matches[0];

				case 'vimeo.com':
				case 'www.vimeo.com':
					preg_match('#http://(www\.)?vimeo\.com/(\d+)#s', $url, $matches);
					return 'vm_' . $matches[2];

				case 'www.dailymotion.com':
					preg_match('#<object[^>]+>.+?http://www.dailymotion.com/swf/video/([A-Za-z0-9]+).+?</object>#s', $url, $matches);

					if( !isset($matches[1])) {
						preg_match('#http://www.dailymotion.com/video/([A-Za-z0-9]+)#s', $url, $matches);
					}

					if( !isset($matches[1])) {
						preg_match('#http://www.dailymotion.com/embed/video/([A-Za-z0-9]+)#s', $url, $matches);
					}

					return 'dm_' . $matches[1];
				default:
					return '';
			}
		}

		return '';
	}

	/**
	 * [url description]
	 * @param  [type] $url     [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function url($url, array $options = array()) {
		$defaults = array(
			'absolute' => false,
			'route' => 'www',
			'extra' => array(),
			'#' => array(),
		);
		$options += $defaults;

		$params = $options['extra'];
		$params += self::$_params;

		if (!empty(self::$_absolute)) {
			$options['absolute'] = true;
		}

		if (!empty(self::$_changedRoute)) {
			 $options['route'] = self::$_changedRoute;
		}

		$host = 'http://' . Environment::get('domain.' . $options['route']);
		$url = Router::match($url, null, array('absolute' => $options['absolute'], 'host' => $host));

		if (!empty($params)) {
			$url .= "?";
			$d = array();

			foreach ($params as $param => $value) {
				$d[] = $param . "=" . urlencode($value);
			}

			$url .= implode('&', $d);
		}

		if (!empty($options['#'])) {
			$url .= '#';

			$d = array();
			foreach ($options['#'] as $name => $value) {
				if (is_numeric($name)) {
					$d[] = urlencode($value);
				} else {
					$d[] .= urlencode($name) . '-' . urlencode($value);
				}
			}

			$url .= '/' . implode('/', $d);
		}

		return $url;
	}

	/**
	 *
	 */
	public static function toggleAbsolute($forceValue = -1) {

		if ($forceValue !== -1) {
			self::$_absolute = $forceValue;
		} else {
			self::$_absolute = !self::$_absolute;
		}

		return self::$_absolute;
	}

	/**
	 *
	 */
	public static function changeRoutes($app = null) {

		if (self::$_changedRoute == $app) {
			return false;
		}

		if (!empty($app)) {
			$file = dirname(LITHIUM_APP_PATH) . '/app_' . $app . '/config/routes.php';
		} else {
			$file = LITHIUM_APP_PATH . '/config/routes.php';
		}

		if (!is_file($file)) {
			throw new Exception('File ' . $file . ' does not exist.');
		}

		Router::reset();

		include $file;

		self::$_changedRoute = $app;

		return self::$_changedRoute;
	}
}

?>