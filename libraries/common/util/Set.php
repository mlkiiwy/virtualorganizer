<?php

namespace common\util;

use Exception;
use Iterator;

/**
 *
 */
class Set extends \lithium\util\Set {

	/**
	 *
	 */
	public static function getFirstKey($array) {
		if (is_array($array)) {
			foreach ($array as $key => $value) {
				return $key;
			}
		}
		return $array;
	}

	/**
	 * [first description]
	 * @param  [type] $array [description]
	 * @return [type]        [description]
	 */
	public static function first($array) {
		return static::arrayFirstValue($array);
	}

	/**
	 * [last description]
	 * @param  [type] $array [description]
	 * @return [type]        [description]
	 */
	public static function last($array) {
		return static::arrayLastValue($array);
	}

	/**
	 *
	 */
	public static function arrayLastValue($array) {
		if (count($array) > 0) {
			return self::arrayGetValueByIndex($array, count($array) - 1);
		}
		return false;
	}

	/**
	 *
	 */
	public static function arrayFirstValue($array) {
		return self::arrayGetValueByIndex($array);
	}

	/**
	 *
	 */
	public static function arrayGetValueByIndex($array, $index = 0) {
		if ($index >= count($array)) {
			return false;
		}
		$i = 0;
		foreach ($array as $k => $v) {
			if ($i == $index) {
				return $v;
			}
			$i++;
		}
		return false;
	}

	/**
	 *
	 */
	public static function requiredKeys(array $keys, array $data) {
		foreach ($keys as $key) {
			if (!isset($data[$key])) {
				throw new Exception('The parameter : ' . $key . ' is required');
			}
		}
	}

	/**
	 *
	 */
	public static function renameKeys(array $input, array $keysMapping) {
		foreach ($keysMapping as $source => $destination) {
			if (isset($input[$source])) {
				$input[$destination] = $input[$source];
				unset($input[$source]);
			}
		}
		return $input;
	}

	/**
	 *
	 */
	public static function reindexBy($datas, $key) {
		if (empty($datas)) {
			return array();
		}
		if (!$datas instanceof Iterator && !is_array($datas)) {
			throw new Exception("Data does not implement Iterator");
		}
		$newData = array();
		foreach ($datas as $value) {
			if (is_array($value) && isset($value[$key])) {
				$newData[$value[$key]] = $value;
			} else if (is_object($value) && isset($value->{$key})) {
				$newData[$value->{$key}] = $value;
			}
		}
		return $newData;
	}

	/**
	 *
	 */
	public static function getFieldValue($data, $field) {
		if (empty($data)) {
			return array();
		}
		if (!$data instanceof Iterator && !is_array($data)) {
			throw new Exception("Data not implement Iterator");
		}
		$newData = array();
		foreach ($data as $value) {
			if (is_array($value) && isset($value[$field])) {
				$newData[] = $value[$field];
			} else if (is_object($value) && isset($value->{$field})) {
				$newData[] = $value->{$field};
			}
		}
		return $newData;
	}

	/**
	 *
	 */
	public static function whitelist(&$data, $whitelist = array(), $required = false) {
		if ($required) {
			Set::requiredKeys($whitelist, $data);
		}

		foreach ($data as $field => $value) {
			if (!in_array($field, $whitelist)) {
				unset($data[$field]);
			}
		}
	}

	/**
	 *
	 */
	public static function blacklist(&$data, $blacklist = array()) {
		if (count($data) >= $blacklist) {
			foreach ($blacklist as $field) {
				if (isset($data[$field])) {
					unset($data[$field]);
				}
			}
		} else {
			foreach ($data as $field => $value) {
				if (in_array($field, $blacklist)) {
					unset($data[$field]);
				}
			}
		}

	}

	/**
	 *
	 */
	public static function valueFromPath(array $data, $path, $delimiter = '.') {
		$path = is_string($path) ? explode($delimiter, $path) : array();
		foreach( $path as $field) {
			if( !empty($data[$field])) {
				$data = $data[$field];
			} else {
				$data = null;
			}
		}
		return $data;
	}

	/**
	 *
	 */
	public static function groupBy(array $data, $path, $emptyGroup = 'empty') {
		$newData = array();
		foreach( $data as $value) {
			$fieldValue = self::valueFromPath($value, $path);
			$fieldValue = empty($fieldValue) ? $emptyGroup : $fieldValue;
			if( !isset($newData[$fieldValue])) {
				$newData[$fieldValue] = array();
			}
			$newData[$fieldValue][] = $value;
		}
		return $newData;
	}

	/**
	 *
	 */
	public static function getValues(array $data, $fields, $hash = true, $emptyValue = null) {
		$ret = array();
		foreach( $fields as $field) {
			if( $hash) {
				$ret[$field] = empty($data[$field]) ? $emptyValue : $data[$field];
			} else {
				$ret[] = empty($data[$field]) ? $emptyValue : $data[$field];
			}
		}
		return $ret;
	}


	/**
	 * Create a string from an array (it's recursive)
	 * @sample : "limit:20.page:1.filter:movies"
	 * @param array $input
	 * @param string $delimiter
	 * @return string
	 */
	public static function generateString(array $input = array(), array $options = array()) {
		$defaults = array('delimiter' => '.', 'space' => false);
		$options += $defaults;
		$arrayStr = array();
		foreach ($input as $key => $value) {
			if (is_array($value)) {
				$arrayStr[] = "{$key}:" . self::generateString($value, $options);
			} else if (is_object($value)) {
				if (method_exists($value, '__toString')) {
					$arrayStr[] = "{$key}:" . $value->__toString();
				}
			} else {
				$arrayStr[] = "{$key}:" . $value;
			}
		}

		$str = implode($options['delimiter'], $arrayStr);

		if ($options['space'] === false) {
			$str = str_replace(' ', '_', $str);
		}

		return $str;
	}

	/**
	 * [pickRandomElement description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function pickRandomElement(array $data = array(), array $options = array()) {
		$defaults = array(
			'count' => 1,
		);
		$options += $defaults;

		$count = count($data);

		if ($options['count'] > $count) {
			throw new Exception("cannot pick `" . $options['count'] . "` we have only $count elements");
		}

		if ($count === 0) {
			return null;
		} else if ($count == 1) {
			return self::arrayFirstValue($data);
		}

		$keys = array_keys($data);

		if ($options['count'] == 1) {
			$key = rand(0, $count - 1);
			return $data[$keys[$key]];
		} else {
			shuffle($keys);
			$keys = array_slice($keys, 0, $options['count']);
			$newArray = array();
			foreach ($keys as $key) {
				$newArray[$key] = $data[$key];
			}
			return $newArray;
		}
	}
}

?>