<?php

namespace common\util;

use Exception;

/**
 *
 */
class Model {

	/**
	 * [isModel description]
	 * @param  [type]  $klassname [description]
	 * @param  array   $options   [description]
	 * @return boolean            [description]
	 */
	public static function isModel($klassname, array $options = array()) {
		$fullKlassname = static::fullKlassname($klassname, $options);
		return !empty($fullKlassname) ? Klass::hasInterface($fullKlassname, 'common\interfaces\IResourceById') : false;
	}

	/**
	 * [findByIds description]
	 * @param  [type] $klassname [description]
	 * @param  [type] $ids       [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function findByIds($klassname, $ids, array $options = array()) {
		if (!static::isModel($klassname)) {
			throw new Exception("`$klassname is not a valid model`");
		}

		$klassname = static::fullKlassname($klassname);
		return $klassname::getByIds($ids);
	}

	/**
	 * [findById description]
	 * @param  [type] $klassname [description]
	 * @param  [type] $id        [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function findById($klassname, $id, array $options = array()) {
		if (!static::isModel($klassname)) {
			throw new Exception("`$klassname is not a valid model`");
		}

		$klassname = static::fullKlassname($klassname);
		return $klassname::getById($id);
	}

	/**
	 * [fullKlassname description]
	 * @param  [type] $klassname [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function fullKlassname($klassname, array $options = array()) {
		$defaults = array(
			'paths' => array(
				'\common\models'
			),
		);
		$options += $defaults;

		return Klass::fullKlassname($klassname, $options);
	}

}

?>