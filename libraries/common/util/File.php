<?php

namespace common\util;

use lithium\core\Environment;

use Exception;

class File {

	protected static $_root = null;

	/**
	 * Download a file into a temp directory
	 */
	public static function download($url, $destDir = null, $newFile = null) {
		if ($newFile === null) {
			// Generate a filePath
			$newFile = File::generateFileName('', $destDir);
		} else {
			$newFile = (empty($destDir)) ? self::tempDir().'/'.$newFile : $destDir.'/'.$newFile;
		}
		try {
			$handler = fopen($url, "r");
			$contents = '';
			if ($handler) {
				while (!feof($handler)) {
					$contents .= fread($handler, 8192);
				}
				fclose($handler);

				$handlew = fopen($newFile, "w");
				fwrite($handlew, $contents);
				fclose($handlew);
			} else {
				$newFile = false;
			}
		} catch (Exception $e) {
			$newFile = false;
		}
		return $newFile;
	}

	/**
	 *
	 */
	public static function generateFileName($extension = '', $root = null) {
		if (!$root) {
			$root = self::tempDir();
		}
		do {
			$filename = time().$extension;
		} while (is_file($root.'/'.$filename));

		return $root.'/'.$filename;
	}

	/**
	 *
	 */
	public static function tempDir($dir = null) {
		if (empty(self::$_root) && empty($dir)) {
			$dir = Environment::get('app.resources');
		}

		if (!empty($dir)) {
			if (!is_dir($dir) || !is_writable($dir)) {
				throw new Exception($dir.' is not a directory or is not writable');
			}
			self::$_root = $dir;
		}

		return self::$_root;
	}

	/**
	 *
	 */
	public static function tmpFilePath($filename) {
		return self::tempDir() . '/' . $filename;
	}

	/**
	 *
	 */
	public static function tryDelete($path) {
		try {
			$ret = false;
			if (is_dir($path)) {
				$ret = rmdir($path);
			} else {
				$ret = unlink($path);
			}
		} catch (Exception $e) {
			$ret = false;
		}
		return $ret;
	}

	/**
	 *
	 */
	public static function createDir($dirPath, $rights = 0777) {
		if (is_dir($dirPath)) {
			// Check rights ?
			// TODO
			return true;
		} else {
			return @mkdir($dirPath, $rights, true);
		}
	}

	/**
	 *
	 */
	public static function sizeHumanized($size) {
		$unit = array('b','kb','mb','gb','tb','pb');
		return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
	}
}

?>
