<?php

namespace common\util;

use common\extensions\mailer\MailParser;

use Exception;

class Mail {

	/**
	 * [parse description]
	 * @param  string $emailRawContent [description]
	 * @return [type]                  [description]
	 */
	public static function parse($emailRawContent = '', array $options = array()) {

		$defaults = array();
		$options += $defaults;

		if (empty($emailRawContent)) {
			throw new Exception('email raw content is empty');
		}

		$parser = new MailParser($emailRawContent);

		$data = array(
			'to' => $parser->getTo(),
			'from' => $parser->getFrom(),
			'cc' => $parser->getCc(),
			'subject' => $parser->getSubject(),
			'html' => $parser->getHTMLBody(),
			'text' => $parser->getPlainBody(),
		);

		return $data;
	}

	/**
	 * [extractEmails description]
	 * @param  [type] $string  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function extractEmails($string, array $options = array()) {
		$defaults = array(
			'label' => false,
		);
		$options += $defaults;

		// Split elements
		$emails = array();
		$elements = explode(',', $string);

		foreach ($elements as $element) {
			$email = null;
			$label = null;
			if (mb_strpos($element, '>') !== false) {
				preg_match("/<(.*)>/", $element, $matches);
				$email = !empty($matches[1]) ? $matches[1] : null;
				if ($options['label']) {
					preg_match("/\"(.*)\"/", $element, $matches);
					$label = !empty($matches[1]) ? $matches[1] : $email;
				}
			} else {
				preg_match("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/i", $element, $matches);
				$email = !empty($matches[0]) ? $matches[0] : null;
				if ($options['label']) {
					$label = $email;
				}
			}

			if (!empty($email)) {
				if (!empty($label)) {
					$emails[$label] = $email;
				} else {
					$emails[] = $email;
				}
			}
		}

		return $emails;
	}

	/**
	 * [extractEmail description]
	 * @param  [type] $string  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function extractEmail($string, array $options = array()) {
		$defaults = array(
			'label' => false,
		);
		$options += $defaults;

		$emails = static::extractEmails($string, $options);
		if ($options['label']) {
			return !empty($emails) ? array_slice($emails, 0, 1) : null;
		} else {
			return Set::first($emails);
		}
	}

}
?>