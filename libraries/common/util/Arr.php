<?php

namespace common\util;

use Exception;

class Arr {

	public static function union(array $array1, array $array2, $unique_value = false) {
		foreach ($array2 as $i => $value) {
			$array1[] = $value;
		}

		if ($unique_value) {
			$array1 = array_unique($array1);
		}

		return $array1;
	}

	/**
	 * Retourne un item d'un tableau à partir d'un chemin en dot
	 *
	 */
	public static function fromPath(&$context, $name) {
		$pieces = explode('.', $name);
		foreach ($pieces as $piece) {
			if (!is_array($context) || !array_key_exists($piece, $context)) {
				// error occurred
				return null;
			}
			$context = &$context[$piece];
		}
		return $context;
	}

	/**
	 * Retourne un tableau à partir des lignes filtrées d'un dictionnaire via la clé.
	 * 
	 * @param array $array : Tableau à parser
	 * @param mixed $keys : Clé à filtrer
	 * 		- @string : $array[$val]
	 * 		- @array : $array[$val1][$val2]...
	 * @param array $options
	 * 		- `unique` : applique un filtre unique sur les valeurs
	 */
	public static function filterFromKey(array $array, $keys, $options = array()) {
		if (empty($keys)) {
			throw new Exception('Need filters to filter...');
		}
		if (!is_array($keys)) {
			$keys = array($keys);
		}

		$return = array();

		foreach ($array as $row) {
			$value = $row;
			foreach ($keys as $key) {
				if (!empty($value[$key])) {
					$value = $value[$key];
				}
			}
			if (!empty($value)) {
				$return[] = $value;
			}
		}

		if (!empty($options['unique'])) {
			$return = array_unique($return);
		}

		return $return;
	}
}

?>