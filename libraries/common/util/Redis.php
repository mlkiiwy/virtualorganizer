<?php

namespace common\util;

use Exception;

use lithium\core\Environment;
use lithium\storage\Cache;
use lithium\util\Inflector;
use common\analysis\Debugger;

class Redis {

	/**
	 * Open ou get current Redis connection
	 *
	 * @param string $config
	 */
	public static function connection($config = 'default') {
		return Cache::adapter($config)->connection;
	}

	/**
	 * Multiple get
	 *
	 *		array(
	 *			'LongTerm' => array(
	 *				'key' => 'APP_WWW.ProductsController.product_421678.LongTerm.array'
	 * 				'hashKey' => 'toto' // Hash
	 *			),
	 *			'MidTerm' => array(
	 *				'key' => 'APP_WWW.ProductsController.product_421678.MidTerm.array'
	 *			)
	 *		) // Ou :
	 * 		array(
	 * 			'LongTerm' => 'APP_WWW.ProductsController.product_421678.LongTerm.array'
	 * 			'MidTerm' => 'APP_WWW.ProductsController.product_421678.MidTerm.array'
	 * 		)
	 * 		// Returns :
	 * 		array(
	 * 			'LongTerm' => {data},
	 * 			'MidTerm' => {data}
	 *		)
	 *
	 * @param array $keys
	 * @param array $options
	 * 		- `config` string : connection
	 * 		- `filter` string
	 * @return array
	 */
	public static function mGet($keys, array $options = array()) {
		$defaults = array('config' => 'read_only', 'filter' => 'unserialize');
		$options += $defaults;

		$debug = Debugger::log('GET : ' . json_encode($keys), Debugger::TYPE_CACHE);

		try {
			$transaction = Redis::connection($options['config']);
			$transaction->multi();

			foreach ($keys as $key => $values) {
				if (!is_array($values)) {
					$keyString = $values;
					$values = array();

				} elseif (!empty($values['key'])) {
					$keyString = $values['key'];

				} else {
					throw new Exception("Key is missing");

				}

				// Hash
				if (!empty($values['hashKey'])) {
					$transaction->hGet($keyString, $values['hashKey']);

				// String
				} else {
					$transaction->get($keyString);

				}
			}

			$return = $transaction->exec();
			$return = array_combine(array_keys($keys), $return);
			$return = array_map($options['filter'], $return);

		} catch (Exception $e) {
			$return = array();
		}

		Debugger::end($debug);

		return $return;

	}

	/**
	 * Single get
	 *
	 *		Redis::get(array(
	 *				'key' => 'APP_WWW.ProductsController.product_421678.LongTerm.array'
	 * 				'hashKey' => 'toto' // Hash
	 *		)); // Ou :
	 * 		Redis::get('APP_WWW.ProductsController.product_421678.LongTerm.array');
	 * 		// Returns LongTerm
	 *
	 * @param array $keys
	 * @param string $tag
	 * @param array $options
	 * 		- `config` string : connection
	 * 		- `filter` string
	 * @return array
	 */
	public static function get($key, array $options = array()) {
		$defaults = array('config' => 'read_only', 'filter' => 'unserialize');
		$options += $defaults;

		$return = Redis::mGet(array($key), $options);
		$return = reset($return);

		return $return;

	}

	/**
	 * Multiple set with TTL options.
	 *
	 * 		Redis::mSet(
	 * 			array(
	 * 				'LongTerm' => {data},
	 * 				'MidTerm' => {data}
	 *			),
	 *			array(
	 *				'LongTerm' => array(
	 *					'key' => 'APP_WWW.ProductsController.product_421678.LongTerm.array'
	 *					'ttl' => '+20 hours'
	 *				),
	 *				'MidTerm' => array(
	 *					'key' => 'APP_WWW.ProductsController.product_421678.MidTerm.array'
	 *					'ttl' => null // No TTL
	 *				)
	 *			)
	 *		);
	 *
	 * @param array $toCache : Data to save
	 * @param array $keys
	 * @param array $options
	 * 		- `config` string : connection
	 * @return array
	 */
	public static function mSet(array $toCache, array $keys, array $options = array()) {
		$defaults = array('config' => 'default', 'filter' => 'serialize');
		$options += $defaults;

		$debug = Debugger::log('SET : ' . json_encode($keys), Debugger::TYPE_CACHE);

		try {
			$transaction = Redis::connection($options['config']);
			$transaction->multi();

			foreach ($keys as $key => $keyOptions) {
				if (!is_array($keyOptions)) {
					$keyOptions = array('key' => $keyOptions);
				}

				$value = (!empty($toCache[$key])) ? call_user_func($options['filter'], $toCache[$key]) : null;

				if (empty($keyOptions['key'])) {
					throw new Exception("Invalid options, key is missing");
				}

				$ttl = (!empty($keyOptions['ttl'])) ? $keyOptions['ttl'] : null;
				if ($ttl) {
					$ttl = (preg_match('/^[0-9]*$/i', $ttl) != 1) ? strtotime($ttl) : intval($ttl);
				}

				// Hashkey
				if (!empty($keyOptions['hashKey'])) {
					$transaction->hSet($keyOptions['key'], $keyOptions['hashKey'], $value);

				// String
				} else {
					$transaction->set($keyOptions['key'], $value);

				}

				// Set TTL
				if ($ttl) {
					$transaction->expireAt($keyOptions['key'], $ttl);
				}

			}

			$transaction->exec();
		} catch (Exception $e) {
			$transaction = false;
		}

		Debugger::end($debug);

		return $transaction;

	}

	/**
	 * Single set with TTL options.
	 *
	 * 		Redis::set(
	 * 			{data},
	 *			array(
	 *				'key' => 'APP_WWW.ProductsController.product_421678.LongTerm.array'
	 *				'ttl' => '+20 hours'
	 *			)
	 *		); // Save to longTerm
	 * 		Redis::set({data}, 'APP_WWW.ProductsController.product_421678.LongTerm.array'); // Save to longTerm
	 *
	 * @param mixed $toCache : Data to save
	 * @param mixed $keys
	 * @param array $options
	 * 		- `config` string : connection
	 * @return array
	 */
	public static function set($toCache, $key, array $options = array()) {
		$defaults = array('config' => 'default', 'filter' => 'serialize');
		$options += $defaults;

		return Redis::mSet(array($toCache), array($key), $options);

	}

	/**
	 * Build a normalized cache key using `APP.Class.params[].tag.array` pattern.
	 *
	 * 		Cache::buildKey(array('product' => 1, 'user' => 2), array('class' => 'ProductsController', 'tag' => 'LongTerm'));
	 * 		// Returns :
	 * 		APP_WWW.ProductsController.product_1.user_2.LongTerm.array
	 *
	 * @param array $params
	 * @param array $options
	 * 		- `tag`: required
	 * 		- `class`: required
	 * 		- `app`: optional (default: SC_APP_NAME)
	 * @return string
	 */
	public static function buildKey(array $params, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($options['class'])) {
			throw new Exception("Class is missing");
		}

		$app = (!empty($options['app'])) ? $options['app'] : SC_APP_NAME;
		$tag = (!empty($options['tag'])) ? '.'. Inflector::camelize($options['tag'], true) : null;

		$paramsOutput = array();

		foreach ($params as $i => $value) {
			$paramsOutput[] .= $i . '_' . $value;
		}

		$paramString = implode('.', $paramsOutput);

		$class = explode('\\', $options['class']);
		$class = end($class);

		$return = Redis::_getEnv() . mb_strtoupper($app) . '.' . $class . '.' . $paramString . $tag . '.array';
		return $return;
	}

	/**
	 * Build multiple normalized cache options.
	 *
	 *		Redis::buildOptions(array(
	 *			'tags' => array(
	 *				'LongTerm' => array('hashKey' => 'toto'), // Hash key
	 *				'MidTerm' => '+24 hours'
	 *			),
	 *			'class' => get_class(),
	 *			'params' => array('product' => 1)
	 *		));
	 * 		// Returns :
	 * 		array(
	 * 			'LongTerm' => array(
	 * 				'key' => 'APP_WWW.ProductsController.product_1.LongTerm.array',
	 * 				'ttl' => null,
	 * 				'hashKey' => 'toto'
	 * 			),
	 * 			'MidTerm' => array(
	 * 				'key' => 'APP_WWW.ProductsController.product_1.MidTerm.array',
	 * 				'ttl' => '+24 hours'
	 * 			)
	 *		)
	 *
	 * @param array $options
	 * 		- `tags` mixed : required
	 * 		- `params` array : required
	 * 		- `class` string : required
	 * 		- `app` string : optional (default: SC_APP_NAME)
	 * @return array
	 */
	public static function buildOptions(array $options = array()) {
		$defaults = array('app' => SC_APP_NAME);
		$options += $defaults;

		$return = array();

		if (empty($options['tags'])) {
			throw new Exception("Tags are missing");
		}

		if (empty($options['class'])) {
			throw new Exception("Class is missing");
		}

		if (empty($options['params']) || !is_array($options['params'])) {
			throw new Exception("Params are invalid or missing");
		}

		$tags = (is_array($options['tags'])) ? $options['tags'] : array($options['tags']);
		unset($options['tags']);

		foreach ($tags as $tag => $values) {
			if (is_int($tag)) {
				$tag = $values;
				$values = array();
			}
			if (!is_array($values) && $values) {
				$values = array('ttl' => $values);
			}
			$key = Redis::buildKey($options['params'], array('tag' => $tag) + $options);
			$return[$tag] = compact('key') + $values;
		}

		return $return;
	}

	/**
	 *
	 */
	private static function _getEnv() {
		switch (Environment::get()) {
			case 'production':
				$namespace = 'Prod.';
				break;

			case 'preproduction':
				$namespace = 'Preprod.';
				break;

			default:
				$namespace = 'Dev.';
		}

		return $namespace;
	}
}

?>
