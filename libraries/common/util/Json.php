<?php

namespace common\util;

use common\interfaces\IJsonnable;

use Exception;

class Json {

	/**
	 *
	 */
	public static function encode($data, array $options = array()) {
		$defaults = array('return' => 'json');
		$options += $defaults;

		if (is_array($data)) {
			foreach ($data as $key => $value) {
				$data[$key] = self::encode($value, array('return' => 'data') + $options);
			}
			return $options['return'] == 'json' ? json_encode($data) : $data;
		} else if (is_object($data) && $data instanceof IJsonnable) {
			return $data->toJson($options);
		} else {
			return $options['return'] == 'json' ? json_encode($data) : $data;
		}
	}

	/**
	 *
	 */
	public static function decode($string, array $options = array()) {
		if (!is_string($string)) {
			throw new Exception('cannot decode, data is not a string');
		}
		$data = json_decode($string, true);
		// todo : analyse data, recursively and recreate if possible elements from json (IJsonnable)
		return $data;
	}
}

?>
