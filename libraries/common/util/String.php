<?php

namespace common\util;

use Exception;
use lithium\util\Inflector;

/**
 * Extends the Lithium String library with useful functions.
 */
class String extends \lithium\util\String {

	const EQUAL_STRICT = 'equal_strict';
	const EQUAL_CLEAN = 'equal_clean';
	const EQUAL_SLUG = 'equal_slug';
	const EQUAL_SLUG_BASE = 'equal_slug_base';

	const SLUG_MAX_LENGTH = 60;

	/**
	 * Creates a slug suitable for urls.
	 * @todo check notices for encoding issues...
	 */
	public static function slug($str) {
		$str = Inflector::slug($str, '_');
		$str = self::cleanStr($str);

		$str = trim($str, '_');
		$str = trim($str, '-');

		if (strlen($str) > self::SLUG_MAX_LENGTH) {
			$str = substr($str, 0, self::SLUG_MAX_LENGTH);
			$str = trim($str, '_');
			$str = trim($str, '-');
		}

		$str = rawurlencode($str);

		return empty($str) ? 'n_a' : $str;
	}

	/**
	 *
	 */
	public static function cleanStr($str) {
		@$str = iconv("UTF-8", "ASCII//IGNORE", $str);
		return $str;
	}

	/**
	 *
	 */
	public static function cleanUsername($str, $replacement = '_') {
		$map = array(
			'/[^\w\s-]/u' => ' ',
			'/\\s+/u' => $replacement,
			str_replace(':rep', preg_quote($replacement, '/'), '/^[:rep]+|[:rep]+$/') => ''
		);

		$str = preg_replace(array_keys($map), array_values($map), $str);

		return trim($str, $replacement);
	}

	/**
	 * Prépare un tableau en valeur MD5 identique au trigger des param_index.
	 */
	public static function md5(array $values, array $fields = array()) {
		$return = null;
		if (!$fields) {
			$fields = array_keys($values);
		}
		foreach ($fields as $field) {
			$val = (!empty($values[$field])) ? $values[$field] : '';
			$return .= '|' . $field . ':' . $val;
		}
		return md5($return);
	}

	/**
	 *
	 */
	public static function oldSlugArticle($s, $to='html', $ms_compat=false, $strip_html=false){
		if( is_array($s) === true):
			while(list($k,$v) = each($s)):
				$s[$k] = convert($v, $to, $ms_compat);
			endwhile;
			return $s;
		else:
			if( $to == 'html'):
				$s = htmlspecialchars($s);
				if( $ms_compat == true):
					$s = convert_ms2html($s);
				endif;
			elseif($to == 'plain'):
				$s = html_entity_decode($s);
				$s = strip_tags($s);
			elseif($to == 'seo'):
				$s = trim(strtolower($s));
				$s = strtr(utf8_decode($s), utf8_decode('ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ'), 'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn');
				$s = preg_replace('#[^a-z0-9_]#', '_', $s);
				$s = preg_replace('`_+`', '_', $s);
				$s = trim($s, '_');
			elseif($to == 'seo2'):
				$s = trim(strtolower($s));
				$s = str_replace(' ', '-', $s);

				// MG: On garde l'url en unicode, on enlève les caractères interdits par contre
				$search = array(
					'/[\?\+\:\/\"\'\&\#]/',
					'`_+`',
					'`-+`',
					'`\-+`',
					'`_\-`',
					'`\-_`'
				);
				$s = preg_replace($search, '-', $s);
				$s = rawurlencode($s);
				$s = trim($s, '_');
				$s = trim($s, '-');
			elseif($to == 'seo3'):
				$s = trim(strtolower($s));
				$s = strtr(utf8_decode($s), utf8_decode('ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ'), 'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn');
			elseif($to == 'seo4'):
				$s = trim(strtolower($s));
				$s = strtr(utf8_decode($s), utf8_decode('ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ'), 'aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn');
				$s = preg_replace('#[^a-z0-9_\-]#', '-', $s);
			elseif($to == 'sql'):
				if( $strip_html == true):
					$s = html_entity_decode($s);
				endif;
				if( $ms_compat == true):
					$s = convert_ms2html($s);
				endif;
			elseif($to == 'url'):
				$s = urlencode($s);
			endif;

			return $s;
		endif;
	}

	/**
	 *
	 */
	public static function equal($str1, $str2, $mode = self::EQUAL_STRICT) {
		switch($mode) {
			default:
			case self::EQUAL_STRICT:
				$ret = ($str1 === $str2);
			break;

			case self::EQUAL_CLEAN:
				$ret = (trim(strtolower($str1)) == trim(strtolower($str2)));
			break;

			case self::EQUAL_SLUG:
				$ret = (self::slug($str1) == self::slug($str2));
			break;

			case self::EQUAL_SLUG_BASE:
				$ret = (Inflector::slug($str1) == Inflector::slug($str2));
			break;
		}

		return $ret;
	}

	/**
	 *
	 */
	public static function isUrl($url) {
		// Simple (very simple...)
		return substr($url,0,4) === 'http';
	}

	/**
	 *
	 */
	public static function extractClassName($str) {
		$e = explode("\\", $str);
		return !empty($e) ? $e[count($e) -1] : '';
	}

	/**
	 *
	 */
	public static function containElements($str, array $elements, $all = false) {
		$containAll = true;
		foreach( $elements as $element) {
			if( strpos($str, $element) !== false) {
				if( !$all) {
					return true;
				}
			} else {
				$containAll = false;
			}
		}
		return $containAll;
	}

	/**
	 *
	 */
	public static function removeElements($str, array $elements) {
		foreach( $elements as $element) {
			$str = str_replace($element, '', $str);
		}

		return $str;
	}

	/**
	 *
	 */
	public static function extractMainDomain($domain) {
		$data = explode('.', $domain);
		return (count($data) >= 2) ? $data[count($data)-2] . '.' . $data[count($data) - 1] : $domain;
	}

	/**
	 *
	 */
	public static function titleCase($str) {
		return mb_convert_case($str, MB_CASE_TITLE);
	}

	/**
	 *
	 */
	public static function escape($value) {
		return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
	}

	/**
	 *
	 */
	public static function endsWith($haystack, $needle) {
		$length = strlen($needle);
		return $length == 0 ? true : (substr($haystack, -$length) === $needle);
	}

	/**
	 *
	 */
	public static function startsWith($haystack, $needle) {
		return !strncmp($haystack, $needle, strlen($needle));
	}

	/**
	 *
	 */
	public static function toString($input, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (is_object($input)) {
			return '(object) ' . get_class($input);
		} else if (is_array($input)) {
			return '(array) ' . count($input);
		} else {
			return (string) $input;
		}
	}
}

?>