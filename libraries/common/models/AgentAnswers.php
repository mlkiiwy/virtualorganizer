<?php

namespace common\models;

use Exception;

class AgentAnswers extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_agent_answer');

	const VALUE_NO_ANSWER = 0;
	const VALUE_YES_OR_POSITIVE = 1;
	const VALUE_NO_OR_NEGATIVE = -1;
	const VALUE_MAYBE = 2;

	public $belongsTo = array(
		'AgentTopics' => array(
			'key' => array('agent_topic_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	/**
	 * [answerForAgentAndUser description]
	 * @param  [type] $agentTopicId [description]
	 * @param  [type] $userId       [description]
	 * @return [type]               [description]
	 */
	public static function answerForAgentAndUser($agentTopicId, $userId) {
		$conditions = array(
			'agent_topic_id' => $agentTopicId,
			'user_id' => $userId,
		);

		return static::first(compact('conditions'));
	}

	/**
	 * [build description]
	 * @param  [type] $agentTopicId [description]
	 * @param  [type] $userId       [description]
	 * @param  [type] $value        [description]
	 * @return [type]               [description]
	 */
	public static function build($agentTopicId, $userId, $value) {

		$agentAnswer = static::answerForAgentAndUser($agentTopicId, $userId);
		$agentAnswer = empty($agentAnswer) ? static::create() : $agentAnswer;

		$agentAnswer->agent_topic_id = $agentTopicId;
		$agentAnswer->user_id = $userId;
		$agentAnswer->value = $value;

		if (!$agentAnswer->save()) {
			throw new Exception('cannot build AgentAnswers');
		}

		return $agentAnswer;
	}

	/**
	 * [allForAgentTopicId description]
	 * @param  [type] $agentTopicId [description]
	 * @return [type]               [description]
	 */
	public static function allForAgentTopicId($agentTopicId) {
		$conditions = array(
			'agent_topic_id' => $agentTopicId,
		);

		return static::all(compact('conditions'));
	}
}

?>