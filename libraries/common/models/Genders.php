<?php

namespace common\models;

class Genders extends \common\data\ConstantModels {

	const MALE = 1;
	const FEMALE = 2;
	const UNKNOW = 3;

	protected $_meta = array('source' => 'vo_gender');

	/**
	 *
	 */
	protected static $_constants = array(
		self::MALE => 'Male',
		self::FEMALE => 'Female',
		self::UNKNOW => 'Unknow',
	);

}

?>