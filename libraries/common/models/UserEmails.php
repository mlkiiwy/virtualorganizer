<?php

namespace common\models;

use common\data\constants\State;

use common\interfaces\IBuildable;

use Exception;

class UserEmails extends \common\data\BaseModels implements IBuildable {

	protected $_meta = array('source' => 'vo_user_email');

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Creators' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_creation' => 'id'),
		),
	);

	/**
	 * [firstByEmail description]
	 * @param  [type] $email   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function firstByEmail($email, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('email' => $email);
		$with = array('Users');

		return static::first(compact('conditions', 'with'));
	}

	/**
	 * [allByEmails description]
	 * @param  [type] $emails  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function allByEmails($emails, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('email' => $emails);
		$with = array('Users');

		return static::all(compact('conditions', 'with'));
	}

	/**
	 * [build description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build(array $data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($data['user_id'])) {
			throw new Exception("user_id is required");
		}

		if (empty($data['email'])) {
			throw new Exception("email is required");
		}

		if (empty($data['user_id_creation'])) {
			$data['user_id_creation'] = $data['user_id'];
		}

		$userEmail = static::create($data);

		try {
			return $userEmail->save() ? $userEmail : false;
		} catch (Exception $e) {
			return false;
		}
	}
}

?>