<?php

namespace common\models;

class AgentReasks extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_agent_reask');

	public $belongsTo = array(
		'AgentTopics' => array(
			'key' => array('agent_topic_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'AgentReaskReasons' => array(
			'key' => array('agent_reask_reason_id' => 'id'),
		),
		'Messages' => array(
			'key' => array('message_id' => 'id'),
		),
	);

	/**
	 * [findByAgentTopicId description]
	 * @param  [type] $agentTopicId [description]
	 * @param  array  $options      [description]
	 * @return [type]               [description]
	 */
	public static function findByAgentTopicId($agentTopicId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('AgentReasks.agent_topic_id' => $agentTopicId);

		return static::all(compact('conditions'));
	}

	/**
	 * [build description]
	 * @param  [type] $message [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build($message, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$message->checkSaved();

		$data = array(
			'agent_topic_id' => $message->agent_topic_id,
			'user_id' => $message->user_dest_id,
			'agent_reask_reason_id' => $message->content['agent_reask_reason_id'],
		);

		$reask = static::create($data);
		$reask->related('message', $message);

		return $reask->save() ? $reask : null;
	}

}

?>