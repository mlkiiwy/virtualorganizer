<?php

namespace common\models\exceptions;

/**
 *
 */
class TopicCannotAddMessageException extends \Exception {
	
	const MESSAGE_EMPTY = 'message_empty';
	const USER_IS_NOT_IN_TOPIC = 'user_id_not_in_topic';
	const PERMISSION_DENIED_FOR_USER = 'permission_denied_for_user';

}
?>