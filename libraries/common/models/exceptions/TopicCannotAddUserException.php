<?php

namespace common\models\exceptions;

/**
 *
 */
class TopicCannotAddUserException extends \Exception {
	
	const PERMISSION_DENIED = 'permission_denied';
	const USER_ALREADY_ADDED = 'user_already_added';

}
?>