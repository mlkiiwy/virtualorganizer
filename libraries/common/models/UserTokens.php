<?php

namespace common\models;

use common\extensions\storage\Visitor;

use common\data\constants\State;

use DateTime;
use DateInterval;

class UserTokens extends \common\data\BaseModels {

	const GEN_MD5_PREFIX = 'md5_pre';
	const GEN_MD5_SUFFIX = 'md5_suffix';

	protected $_meta = array('source' => 'vo_user_token');

	public $belongsTo = array(
		'Applications' => array(
			'key' => array('application_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	/**
	 * [firstByToken description]
	 * @param  [type] $token         [description]
	 * @param  [type] $applicationId [description]
	 * @param  array  $options       [description]
	 * @return [type]                [description]
	 */
	public static function firstByToken($token, $applicationId = false, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if ($applicationId === false) {
			$applicationId = Applications::getCurrentId();
		}

		$conditions = array(
			'UserTokens.application_id' => $applicationId,
			'UserTokens.token' => $token,
			'UserTokens.state' => State::ACTIVE,
			'UserTokens.date_expiration' => array('<' => (object) 'NOW()'),
			'Users.state' => State::ACTIVE,
		);

		$with = array('Users');

		return static::first(compact('conditions', 'with'));
	}

	/**
	 * [build description]
	 * @param  [type] $user          [description]
	 * @param  [type] $applicationId [description]
	 * @param  array  $options       [description]
	 * @return [type]                [description]
	 */
	public static function build($user, $applicationId = Applications::WEBSITE, array $options = array()) {
		global $_SERVER;

		$defaults = array(
			'timelife' => 60 * 60 * 24 * 30, // 30 Days
		);
		$options += $defaults;

		$user->checkSaved();

		// First invalidate all
		static::invalidateTokensForUserAndApplicationId($user, $applicationId);

		$browser = ini_get("browscap") ? get_browser(null, true) : null;

		if (empty($browser['browser'])) {
			$browser = empty($_SERVER['HTTP_USER_AGENT']) ? 'unknow' : $_SERVER['HTTP_USER_AGENT'];
		} else {
			$browser = $browser['browser'] . ' version ' . $browser['version'] . ' platform ' . $browser['platform'];
		}

		$dateExpiration = new DateTime();
		$dateExpiration->add(new DateInterval('PT' . $options['lifetime'] . 'S'));

		$host = function_exists('php_uname') ? php_uname('n') : '';

		// Next create a new token
		$data = array(
			'application_id' => $applicationId,
			'user_id' => $user->id,
			'state' => State::ACTIVE,
			'token' => md5(self::GEN_MD5_PREFIX . time() . rand(0,1000) . $user->id . $applicationId . self::GEN_MD5_SUFFIX),
			'ip' => Visitor::getIp(),
			'host' => $host,
			'user_agent' => $browser,
			'date_expiration' => $dateExpiration->format('Y-m-d H:i:s'),
		);
		$userToken = static::create($data);

		return $userToken->save() ? $userToken : false;
	}

	/**
	 * [invalidateTokensForUserAndApplicationId description]
	 * @param  [type] $user          [description]
	 * @param  [type] $applicationId [description]
	 * @return [type]                [description]
	 */
	public static function invalidateTokensForUserAndApplicationId($user, $applicationId = Applications::WEBSITE) {
		return static::update(
			array('state' => State::DELETED),
			array('user_id' => $user->id, 'application_id' => $applicationId)
		);
	}

	/**
	 * [timestampExpiration description]
	 * @param  [type] $userToken [description]
	 * @return [type]            [description]
	 */
	public function timestampExpiration($userToken) {
		return strtotime($userToken->date_expiration);
	}

	/**
	 * [invalidate description]
	 * @param  [type] $userToken [description]
	 * @return [type]            [description]
	 */
	public function invalidate($userToken) {
		$userToken->state = State::DELETED;
		return $userToken->save();
	}
}

?>