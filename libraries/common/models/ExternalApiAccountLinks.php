<?php

namespace common\models;

use common\data\constants\State;

class ExternalApiAccountLinks extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_external_api_account_link');

	public $belongsTo = array(
		'ExternalApis' => array(
			'key' => array('gen_external_api_id' => 'id'),
		),
		'ExternalApiAccounts' => array(
			'key' => array('external_api_account_id' => 'id'),
		),
		'ExternalApiAccountRelateds' => array(
			'to' => 'common\models\ExternalApiAccounts',
			'key' => array('external_api_account_id_related' => 'id'),
		),
		'LinkTypes' => array(
			'key' => array('link_type_id' => 'id'),
		),
	);
}

?>