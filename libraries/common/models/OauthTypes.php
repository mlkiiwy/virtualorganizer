<?php

namespace common\models;

class OauthTypes extends \common\data\ConstantModels {

	const OAUTH_1 = 1;
	const OAUTH_2 = 2;

	protected $_meta = array('source' => 'vo_oauth_type');

	/**
	 *
	 */
	protected static $_constants = array(
		self::OAUTH_1 => 'OAuth 1',
		self::OAUTH_2 => 'OAuth 2',
	);

}

?>