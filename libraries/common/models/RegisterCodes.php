<?php

namespace common\models;

use Exception;

class RegisterCodes extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_register_code');

	const STATE_ACTIVE = 0;
	const STATE_USED = 1;

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Creators' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_creation' => 'id'),
		),
	);

	/**
	 * [setUsed description]
	 * @param [type] $code    [description]
	 * @param [type] $user    [description]
	 * @param array  $options [description]
	 */
	public static function setUsed($code, $user, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		return static::update(
			array('user_id' => $user->id, 'state' => self::STATE_USED),
			array('code' => $code, 'state' => self::STATE_ACTIVE)
		);
	}

	/**
	 * [isCodeActive description]
	 * @param  [type]  $code    [description]
	 * @param  array   $options [description]
	 * @return boolean          [description]
	 */
	public static function isCodeActive($code, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'RegisterCodes.code' => $code,
			'RegisterCodes.state' => self::STATE_ACTIVE,
		);

		$code = static::first(compact('conditions'));

		return !empty($code);
	}
}

?>