<?php

namespace common\models;

class AgentReaskReasons extends \common\data\ConstantModels {

	const AFTER_FIRST_READING = 1;
	const BASIC_AFTER_SOME_TIME = 2;

	protected $_meta = array('source' => 'vo_agent_reask_reason');

	/**
	 *
	 */
	protected static $_constants = array(
		self::AFTER_FIRST_READING => 'Reask after first reading of message',
		self::BASIC_AFTER_SOME_TIME => 'Reask after some times',
	);

}

?>