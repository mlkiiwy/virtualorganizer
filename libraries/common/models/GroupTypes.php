<?php

namespace common\models;

class GroupTypes extends \common\data\ConstantModels {

	const PRIVE = 1;
	const SHARED = 2;

	protected $_meta = array('source' => 'vo_group_type');

	/**
	 *
	 */
	protected static $_constants = array(
		self::PRIVE => 'Private group of contact',
		self::SHARED => 'Shared Group',
	);

}

?>