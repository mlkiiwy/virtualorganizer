<?php

namespace common\models;

use common\data\constants\State;

use Exception;

class ExternalApiAccounts extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_external_api_account');

	public $belongsTo = array(
		'ExternalApis' => array(
			'key' => array('external_api_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	protected static $_jsonFields = array('json' => array('other'));

	/**
	 * [firstByUid description]
	 * @param  [type] $uid     [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function firstByUidAndApi($uid, $apiId, array $options = array()) {
		$defaults = array(
			'with' => array('Users'),
		);
		$options += $defaults;

		$with = $options['with'];
		$conditions = array(
			'ExternalApiAccounts.uid' => $uid,
			'ExternalApiAccounts.external_api_id' => $apiId,
		);

		return static::first(compact('conditions', 'with'));
	}

	/**
	 * [firstByUserIdAndApiId description]
	 * @param  [type] $userId  [description]
	 * @param  [type] $apiId   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function firstByUserIdAndApiId($userId, $apiId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'ExternalApiAccounts.user_id' => $userId,
			'ExternalApiAccounts.external_api_id' => $apiId,
		);

		return static::first(compact('conditions'));
	}

	/**
	 * [build description]
	 * @param  [type] $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build($data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($data['uid'])) {
			throw new Exception('uid is required');
		}

		if (empty($data['external_api_id'])) {
			throw new Exception('external_api_id is required');
		}

		$account = static::firstByUidAndApi($data['uid'], $data['external_api_id']);

		if (empty($account)) {
			// Doesn't exist so we create it
			$account = static::create(array('state' => State::DRAFT) + $data);
			$account->save();
		}

		return $account;
	}

	/**
	 * [getToken description]
	 * @param  [type] $externalApiAccount [description]
	 * @param  array  $options            [description]
	 * @return [type]                     [description]
	 */
	public function getToken($externalApiAccount, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$externalApiAccount->checkSaved();

		return ExternalApiTokens::firstActiveTokenForAccount($externalApiAccount->id);
	}
}

?>