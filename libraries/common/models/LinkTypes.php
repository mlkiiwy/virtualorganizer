<?php

namespace common\models;

class LinkTypes extends \common\data\ConstantModels {

	const FOLLOW = 1;
	const FRIENDS = 2;

	protected $_meta = array('source' => 'vo_link_type');

	/**
	 *
	 */
	protected static $_constants = array(
		self::FOLLOW => 'Follow',
		self::FRIENDS => 'Friends',
	);

}

?>