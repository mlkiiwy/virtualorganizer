<?php

namespace common\models;

use lithium\core\Environment;

use common\actions\ActionReadMessage;
use common\actions\ActionSendEmail;

use common\interfaces\IBlockContainer;

use common\data\constants\BlockContainerTypes;

use common\extensions\mailer\Mail;

use Exception;

class Emails extends \common\data\BaseModels implements IBlockContainer {

	protected $_meta = array('source' => 'vo_email');

	const STATE_PENDING = 0;
	const STATE_SEND = 1;
	const STATE_OPEN = 2;
	const STATE_CLICK = 3;

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	public $hasOne = array(
		'Messages' => array(
			'key' => array('message_id' => 'id'),
		),
	);

	protected static $_jsonFields = array('content' => array('blocks', 'topicData', 'isFirstEmailOfTopic'));

	/**
	 *
	 */
	public static function build($user, $message = null, array $options = array()) {
		$defaults = array(
			'data' => array(),
		);
		$options += $defaults;

		$email = static::create($options['data']);

		if (!empty($message)) {
			$email->related('message', $message);
			$email->email_type_id = EmailTypes::TOPIC;
		} else {
			$email->email_type_id = EmailTypes::SYSTEM;
		}

		$email->related('user', $user);

		$email->populateIsFirstEmailOfTopic();
		$email->populateCode();
		$email->populateSubject();
		$email->populateFrom();
		$email->populateTo();
		$email->populateContent();

		$email->state = self::STATE_PENDING;

		return $email;
	}

	/**
	 * [populateIsFirstEmailOfTopic description]
	 * @param  [type] $email   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function populateIsFirstEmailOfTopic($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (isset($email->isFirstEmailOfTopic) && !$options['refresh']) {
			return $email->isFirstEmailOfTopic;
		}

		if ($email->email_type_id == EmailTypes::TOPIC) {
			$message = $email->related('message');
			$mails = static::allByTopicIdAndUserId($message->topic_id, $email->user_id,
				array('fields' => array('Emails.id')));
			$email->isFirstEmailOfTopic = count($mails) == 0;
		}

		return $email->isFirstEmailOfTopic;
	}

	/**
	 * [allByTopicIdAndUserId description]
	 * @param  [type] $topicId [description]
	 * @param  [type] $userId  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function allByTopicIdAndUserId($topicId, $userId, array $options = array()) {
		$defaults = array('fields' => array('Emails'));
		$options += $defaults;

		$with = array('Messages');
		$conditions = array(
			'Messages.topic_id' => $topicId,
			'Emails.user_id' => $userId,
		);
		$fields = $options['fields'];

		return static::all(compact('conditions', 'with', 'fields'));
	}

	/**
	 *
	 */
	public function populateCode($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (!empty($email->code) && empty($options['refresh'])) {
			return $email->code;
		}

		$user = $email->related('user');
		if (empty($user)) {
			throw new Exception('cannot populate code, user is empty');
		}

		switch ($email->email_type_id) {
			case EmailTypes::TOPIC:
				$message = $email->related('message');

				if (empty($message)) {
					throw new Exception('cannot populate code, message is empty');
				}

				$str = 'message:' . $message->id . 'user:' . $user->id;
				break;

			case EmailTypes::SYSTEM:
				$str = 'user:' . $user->id . time() . rand(0,1000);
				break;
		}

		$email->code = md5($str);

		return $email->code;
	}

	/**
	 *
	 */
	public function populateSubject($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (!empty($email->subject) && empty($options['refresh'])) {
			return $email->subject;
		}

		if ($email->email_type_id == EmailTypes::TOPIC) {
			$message = $email->related('message');
			if (empty($message)) {
				throw new Exception('cannot populate subject, message is empty');
			}

			$value = $message->subject();
			$user = $email->related('user');

			$email->subject = $value . ' [De : ' . $user->label() . ']';

			if (!$email->populateIsFirstEmailOfTopic()) {
				$email->subject = 'Re : ' . $email->subject;
			}
		}

		return $email->subject;
	}

	/**
	 *
	 */
	public function populateFrom($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (!empty($email->from) && empty($options['refresh'])) {
			return $email->from;
		}

		if ($email->email_type_id == EmailTypes::TOPIC) {
			$message = $email->related('message');
			if (empty($message)) {
				throw new Exception('cannot populate from, message is empty');
			}

			$topic = $message->related('topic');
			$email->from = $topic->getEmailFrom();
		} else {
			$email->from = Environment::get('mail.default_from');
		}

		return $email->from;
	}

	/**
	 *
	 */
	public function populateTo($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (!empty($email->to) && empty($options['refresh'])) {
			return $email->to;
		}

		$user = $email->related('user');
		if (empty($user)) {
			throw new Exception('cannot populate to, user is empty');
		}

		$value = $user->main_email;
		$email->to = $value;

		return $email->to;
	}

	/**
	 *
	 */
	public function populateContent($email, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (!empty($email->content) && empty($options['refresh'])) {
			return $email->content;
		}

		if ($email->email_type_id == EmailTypes::TOPIC) {
			$message = $email->related('message');
			if (empty($message)) {
				throw new Exception('cannot populate content, message is empty');
			}

			$topic = $message->related('topic');
			$userMessageFrom = $message->related('user');
			$userMessageDest = $message->related('user_dest');
			$userTopicCreator = $topic->related('user');

			if ($email->populateIsFirstEmailOfTopic()) {
				$email->addBlock('firstMessage', array('userTopicCreator' => $userTopicCreator->data()));
			}

			// Integrate topic
			$email->topicData = $topic->data();

			$agents = $topic->getAgents();

			foreach ($agents as $agent) {
				$agent->insertBlock($email, $userMessageDest);
			}

			$data = $message->content;
			if (!empty($userMessageFrom)) {
				$data += array('userFrom' => $userMessageFrom->data());
			}
			$data += array('userDest' => $userMessageDest->data());

			$email->addBlock($message->content['template'], $data);

			$email->addBlock('footerBar', array());
		}

		return $email->content;
	}

	/**
	 *
	 */
	public function send($email, array $options = array()) {
		$defaults = array(
			'delayed' => false,
		);
		$options += $defaults;

		$email->checkSaved();

		if ($email->state != self::STATE_PENDING) {
			throw new Exception('cannot send an email already sent');
		}

		if ($options['delayed']) {
			$action = new ActionSendEmail(compact('email'));
			return $action->delayed(array('parent' => $this));
		} else {
			try {
				$r = Mail::sendByTemplate(
					$email->to,
					$email->subject,
					'byBlocks',
					$email->from,
					array('email' => $email->data())
				);

				if (empty($r)) {
					throw new Exception("error when sending by template");
				}

				$email->state = self::STATE_SEND;
				return $email->save();
			} catch (Exception $e) {
				throw $e;
				return false;
			}
		}

	}

	/**
	 * @interface IBlockContainer
	 */
	public function addBlock($email, $name, array $data, $position = false, array $options = array()) {
		$defaults = array(
			'append' => true,
			'create' => true,
		);
		$options += $defaults;

		$blocks = $email->getBlocks($options);

		// @todo : gerer la position

		if (!isset($blocks[$name]) && $options['create']) {
			$blocks[$name] = array();
		} else {
			throw new Exception("blocks `$name` not already created");
		}

		if ($options['append']) {
			$blocks[$name] += $data;
		} else {
			$blocks[$name] = $data;
		}

		$email->blocks = $blocks;

		return $blocks[$name];
	}

	/**
	 *  @interface IBlockContainer
	 */
	public function getBlocks($email, array $options = array()) {
		$defaults = array(
			'create' => true,
		);
		$options += $defaults;

		$blocks = empty($email->blocks) ? array() : $email->blocks;

		if (empty($blocks) && $options['create']) {
			$blocks = $email->blocks = array();
		}

		return $blocks;
	}

	/**
	 * @interface IBlockContainer
	 * [blockContainerType description]
	 * @param  [type] $email [description]
	 * @return [type]        [description]
	 */
	public function blockContainerType($email) {
		return BlockContainerTypes::EMAIL;
	}

	/**
	 * [action description]
	 * @param  [type] $email    [description]
	 * @param  [type] $actionId [description]
	 * @param  array  $options  [description]
	 * @return [type]           [description]
	 */
	public function action($email, $actionId = Actions::OPEN_EMAIL, array $options = array()) {
		$defaults = array(
			'propagate' => true,
		);
		$options += $defaults;

		switch ($actionId) {
			case Actions::OPEN_EMAIL:
				if ($email->state == self::STATE_SEND) {
					$email->state = self::STATE_OPEN;
				} else {
					return false;
				}
				break;

			case Actions::CLICK_EMAIL:
				if (in_array($email->state, array(self::STATE_SEND, self::STATE_OPEN))) {
					$email->state = self::STATE_CLICK;
				} else {
					return false;
				}
				break;

			default:
				throw new Exception("action id `$actionId` has not effect on email");
				break;
		}

		if ($options['propagate']) {
			$this->_propagateOnAction($email, $actionId);
		}

		return $email->save();
	}

	/**
	 * [_propagateOnAction description]
	 * @param  [type] $email    [description]
	 * @param  [type] $actionId [description]
	 * @param  array  $options  [description]
	 * @return [type]           [description]
	 */
	protected function _propagateOnAction($email, $actionId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		switch ($actionId) {
			case Actions::OPEN_EMAIL:
			case Actions::CLICK_EMAIL:
				$message = $email->related('message');
				if (empty($message)) {
					break;
				}

				$user = $email->related('user');
				if (!$message->isReadedBy($user)) {
					$action =  new ActionReadMessage(compact('message'));
					$action->user($user);
					$result = $action->execute();

					if ($result->success()) {
						return true;
					} else {
						throw $result->getException();
					}
				}

				break;

			default:
				break;
		}

		return false;
	}

	/**
	 * [isSended description]
	 * @param  [type]  $email [description]
	 * @return boolean        [description]
	 */
	public function isSended($email) {
		return $email->state == self::STATE_SEND;
	}

}

?>