<?php

namespace common\models;

use common\data\constants\State;

class ExternalApiTokens extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_external_api_token');

	public $belongsTo = array(
		'ExternalApis' => array(
			'key' => array('external_api_id' => 'id'),
		),
		'ExternalApiAccounts' => array(
			'key' => array('external_api_account_id' => 'id'),
		),
		'OauthTypes' => array(
			'key' => array('oauth_type_id' => 'id'),
		),
	);

	/**
	 * [firstActiveTokenForAccount description]
	 * @param  [type] $externalApiAccountId [description]
	 * @param  array  $options              [description]
	 * @return [type]                       [description]
	 */
	public static function firstActiveTokenForAccount($externalApiAccountId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'external_api_account_id' => $externalApiAccountId,
			'state' => State::ACTIVE,
			'date_expiration' => array('>' => (object) 'NOW()'),
		);

		return static::first(compact('conditions'));
	}

	/**
	 * [desactivate description]
	 * @param  [type] $externalApiToken [description]
	 * @param  array  $options          [description]
	 * @return [type]                   [description]
	 */
	public function desactivate($externalApiToken, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$externalApiToken->state = State::DELETED;

		return $externalApiToken->save();
	}

	/**
	 * [build description]
	 * @param  [type] $account   [description]
	 * @param  array  $tokenData [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function build($account, array $tokenData = array(), array $options = array()) {
		$defaults = array(
			'oauthTypeId' => OauthTypes::OAUTH_2,
		);
		$options += $defaults;

		$data = array(
			'external_api_id' => $account->external_api_id,
			'external_api_account_id' => $account->id,
			'state' => State::ACTIVE,
			'oauth_type_id' => $options['oauthTypeId'],
			'access_token' => $tokenData['access_token'],
			'secret' => empty($tokenData['secret']) ? '' : $tokenData['secret'],
			'date_expiration' => $tokenData['date_expiration'],
		);

		return static::create($data);
	}

	/**
	 * [desactivateExpiredTokens description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function desactivateExpiredTokens(array $options = array()) {
		return static::update(
			array('state' => State::DELETED),
			array(
				'state' => State::ACTIVE,
				'date_expiration' => array('<=' => (object) 'NOW()')
			)
		);
	}
}

?>