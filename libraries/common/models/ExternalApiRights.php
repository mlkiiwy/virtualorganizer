<?php

namespace common\models;

class ExternalApiRights extends \common\data\ConstantModels {

	const FOLLOWED = 1;
	const FOLLOWERS = 2;
	const FRIENDS = 3;
	const CONTACTS = 4;

	protected $_meta = array('source' => 'vo_external_api_right');

	/**
	 *
	 */
	protected static $_constants = array(
		self::FOLLOWED => 'Accounts followed',
		self::FOLLOWERS => 'Accounts following you',
		self::FRIENDS => 'Accounts friends (bidirectionnal)',
		self::CONTACTS => 'Contacts (emails)',
	);

}

?>