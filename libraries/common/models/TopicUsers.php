<?php

namespace common\models;

class TopicUsers extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_topic_user');

	const STATE_ACTIVE = 1;

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Topics' => array(
			'key' => array('topic_id' => 'id'),
		),
	);

	/**
	 * [findByTopicId description]
	 * @param  [type]  $topicId [description]
	 * @param  boolean $state   [description]
	 * @param  array   $options [description]
	 * @return [type]           [description]
	 */
	public static function findByTopicId($topicId, $state = false, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('TopicUsers.topic_id' => $topicId);

		if ($state !== false) {
			$conditions['TopicUsers.state'] = $state;
		}

		return static::all(compact('conditions'));
	}

	/**
	 * [populateStatusLabel description]
	 * @param  [type] $topicUser [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public function populateStatusLabel($topicUser, array $options = array()) {
		$defaults = array('refresh' => false);
		$options += $defaults;

		if (isset($topicUser->statusLabel) && !$options['refresh']) {
			return $topicUser->statusLabel;
		}

		$statusLabel = array('type' => 'unknow', 'label' => 'Unknow');
		$details = array();

		$set = false;
		if (!empty($topicUser->agentData)) {
			foreach ($topicUser->agentData as $agentTopicId => $data) {
				$value = false;
				if (!empty($data['answer'])) {
					switch ($data['answer']['value']) {
						case AgentAnswers::VALUE_YES_OR_POSITIVE:
							$value = array('type' => 'yes', 'label' => 'Yes');
							break;

						case AgentAnswers::VALUE_NO_OR_NEGATIVE:
							$value = array('type' => 'no', 'label' => 'No');
							break;

						case AgentAnswers::VALUE_MAYBE:
							$value = array('type' => 'maybe', 'label' => 'Maybe');
							break;

						case AgentAnswers::VALUE_NO_ANSWER:
							$value = array('type' => 'no_answer', 'label' => 'No answer');
							break;
					}
				}

				if (!empty($value)) {
					if ($statusLabel['type'] != 'unknow' && $statusLabel['type'] != $value['type']) {
						$statusLabel = array('type' => 'multiple', 'label' => 'Multiple');
					} else {
						$statusLabel = $value;
					}
					$details[$agentTopicId] = $value;
				}

			}
		} else {
			// TODO => use email data instead (open / read / click)
		}

		$statusLabel['details'] = $details;
		$topicUser->statusLabel = $statusLabel;

		return $topicUser->statusLabel;
	}
}

?>