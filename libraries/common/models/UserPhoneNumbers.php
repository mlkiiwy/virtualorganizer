<?php

namespace common\models;

use common\data\constants\State;

use common\interfaces\IBuildable;

use Exception;

class UserPhoneNumbers extends \common\data\BaseModels implements IBuildable {

	protected $_meta = array('source' => 'vo_user_phone_number');

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Creators' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_creation' => 'id'),
		),
	);

	/**
	 * [firstByPhoneNumber description]
	 * @param  [type] $phoneNumber   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function firstByPhoneNumber($phoneNumber, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('phone_number' => $phoneNumber);
		$with = array('Users');

		return static::first(compact('conditions', 'with'));
	}

	/**
	 * [allByPhoneNumbers description]
	 * @param  [type] $phoneNumbers  [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function allByPhoneNumbers($phoneNumbers, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('phone_number' => $phoneNumbers);
		$with = array('Users');

		return static::all(compact('conditions', 'with'));
	}

	/**
	 * [build description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build(array $data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$defaultsData = array(
			'phone_type_id' => PhoneTypes::UNKNOW,
		);
		$data += $defaultsData;

		if (empty($data['user_id'])) {
			throw new Exception("user_id is required");
		}

		if (empty($data['phone_number'])) {
			throw new Exception("phone_number is required");
		}

		if (empty($data['user_id_creation'])) {
			$data['user_id_creation'] = $data['user_id'];
		}

		$userPhoneNumber = static::create($data);

		try {
			return $userPhoneNumber->save() ? $userPhoneNumber : false;
		} catch (Exception $e) {
			return false;
		}
	}
}

?>