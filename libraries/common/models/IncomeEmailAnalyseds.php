<?php

namespace common\models;

use common\util\Html as HtmlUtil;
use common\util\Set;

use Exception;

class IncomeEmailAnalyseds extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_income_email_analysed');

	const STATE_PENDING = 0;
	const STATE_TREATED = 1;
	const STATE_ERROR = -1;

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Topics' => array(
			'key' => array('topic_id' => 'id'),
		),
	);

	/**
	 * [build description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build(array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$newData = array(
			'subject' => empty($data['subject']) ? '' : $data['subject'],
		);

		if (empty($data['html']) && empty($data['text'])) {
			throw new Exception("cannot build analysed email : email has no content (html or text)");
		}

		if (empty($data['html'])) {
			$data['html'] = $data['text'];
		} else if (empty($data['text'])) {
			$data['text'] = HtmlUtil::toText($data['html']);
		}

		$newData['content_html'] = $data['html'];
		$newData['content_text'] = $data['text'];

		$incomeEmailAnalysed = static::create($newData);

		// First we search the expeditor
		$userFrom = empty($data['userFrom']) ? null : $data['userFrom'];
		$emailFrom = empty($data['from']) ? null : Set::first($data['from']);

		if (empty($userFrom) && !empty($emailFrom)) {
			$userEmail = UserEmails::firstByEmail($emailFrom);
			if (!empty($userEmail)) {
				$userFrom = $userEmail->related('user');
			}
		}

		if (empty($userFrom)) {
			throw new Exception("cannot build analysed email : userFrom cannot be found for : `$emailFrom`");
		}

		$incomeEmailAnalysed->related('user', $userFrom);

		// Second we search the topic associed
		$emailsToTestForTopic = empty($data['to']) ? array() : $data['to'];
		$topic = null;
		foreach ($emailsToTestForTopic as $emailTopic) {
			$topic = Topics::firstByEmailIdentifier($emailTopic);
			if (!empty($topic)) {
				break;
			}
		}

		if (empty($topic)) {
			throw new Exception("cannot build analysed email : topic cannot be found for : `" . implode(';' , $emailsToTestForTopic) . '`');
		}

		$incomeEmailAnalysed->related('topic', $topic);

		return ($incomeEmailAnalysed->save()) ? $incomeEmailAnalysed : null;
	}

	/**
	 * [getMessage description]
	 * @param  [type] $incomeEmailAnalysed [description]
	 * @param  array  $options             [description]
	 * @return [type]                      [description]
	 */
	public function getMessage($incomeEmailAnalysed, array $options = array()) {
		$defaults = array('html' => true);
		$options += $defaults;

		if ($options['html'] && !empty($incomeEmailAnalysed->content_html)) {
			$ret = $incomeEmailAnalysed->content_html;
		}

		return $incomeEmailAnalysed->content_text;
	}

}

?>