<?php

namespace common\models;

class Messages extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_message');

	public $belongsTo = array(
		'Topics' => array(
			'key' => array('topic_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'AgentTopics' => array(
			'key' => array('agent_topic_id' => 'id'),
		),
		'MessageTypes' => array(
			'key' => array('message_type_id' => 'id'),
		),
		'UserDests' => array(
			'to' => '\common\models\Users',
			'key' => array('user_dest_id' => 'id'),
		),
	);

	public $hasMany = array(
		'MessageStatusUsers' => array(
			'key' => array('id' => 'message_id'),
		),
	);

	protected static $_jsonFields = array('json' => array('content'));

	/**
	 *
	 */
	public static function build($topic, $message, $from, $to = null, array $options = array()) {
		$defaults = array('propagate' => true, 'copyForSender' => false);
		$options += $defaults;

		if (empty($from)) {
			throw new Exception('from of type Users or AgentTopics is required');
		}

		$messageTypeId = $from->model() == 'common\models\Users' ? MessageTypes::FROM_USER : MessageTypes::FROM_AGENT;

		if (is_string($message)) {
			$message = array(
				'template' => 'message',
				'message' => $message,
			);
		}

		$message = static::create(array('content' => $message, 'message_type_id' => $messageTypeId));
		$message->related('topic', $topic);

		switch ($messageTypeId) {
			case MessageTypes::FROM_USER:
				$message->related('user', $from);
				break;

			case MessageTypes::FROM_AGENT:
				$message->related('agent_topic', $from);
				break;
		}

		if (!empty($to)) {
			$message->related('user_dest', $to);
		}

		if ($message->save()) {
			$message->buildMessageStatusUsers($options);

			if (!empty($options['propagate'])) {
				$message->propagate();
			}
		}

		return $message;
	}

	/**
	*
	*/
	public function buildMessageStatusUsers($message, array $options = array()) {
		$defaults = array('copyForSender' => false);
		$options += $defaults;

		$message->checkSaved();
		$topic = $message->related('topic');

		$status = MessageStatusUsers::createRecordSet();
		if (empty($message->user_dest_id)) {
			$topicUsers = $topic->populateUsers();
			foreach ($topicUsers as $topicUser) {
				// Not add user who sent the message
				if (!$options['copyForSender'] && $topicUser->user_id == $message->user_id) {
					continue;
				}
				$e = MessageStatusUsers::create(array(
					'message_id' => $message->id,
					'user_id' => $topicUser->user_id,
					'state' => MessageStatusUsers::STATE_PENDING,
				));
				if ($e->save()) {
					$status[] = $e;
				} else {
					throw new Exception('cannot save MessageStatusUsers');
				}
			}
		} else {
			$e = MessageStatusUsers::create(array(
				'message_id' => $message->id,
				'user_id' => $message->user_dest_id,
				'state' => MessageStatusUsers::STATE_PENDING,
			));
			if ($e->save()) {
				$status[] = $e;
			} else {
				throw new Exception('cannot save MessageStatusUsers');
			}
		}
		$message->message_status_users = $status;

		return $status;
	}

	/**
	 *
	 */
	public function propagate($message, array $options = array()) {
		$defaults = array(

		);
		$options += $defaults;

		$message->checkSaved();

		foreach ($options as $value) {
			switch ($value) {
				default:
					break;
			}
		}

		return true;
	}

	/**
	 *
	 */
	public function subject($message, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$message->checkSaved();

		$topic = $message->related('topic');
		return $topic->subject;
	}

	/**
	 * [isReadedBy description]
	 * @param  [type]  $message [description]
	 * @param  [type]  $user    [description]
	 * @param  array   $options [description]
	 * @return boolean          [description]
	 */
	public function isReadedBy($message, $user, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$message->checkSaved();
		$user->checkSaved();

		$messageStatus = MessageStatusUsers::firstByMessageIdAndUserId($message->id, $user->id);
		return $messageStatus->state == MessageStatusUsers::STATE_READ;
	}

	/**
	 * [action description]
	 * @param  [type] $message  [description]
	 * @param  [type] $actionId [description]
	 * @param  [type] $user     [description]
	 * @param  array  $options  [description]
	 * @return [type]           [description]
	 */
	public function action($message, $actionId = Actions::READ_MESSAGE, $user = null, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$message->checkSaved();

		$executed = false;

		switch ($actionId) {
			case Actions::READ_MESSAGE:
				if (empty($user)) {
					throw new Exception("user is required to apply action `$actionId` on a message");
				}

				$user->checkSaved();
				$messageStatus = MessageStatusUsers::firstByMessageIdAndUserId($message->id, $user->id);

				if (empty($messageStatus)) {
					throw new Exception("cannot find a message status for message : `" . $message->id . "` and user `" . $user->id . "`");
				}

				if ($messageStatus->state == MessageStatusUsers::STATE_SEND) {
					$messageStatus->state = MessageStatusUsers::STATE_READ;
				} else {
					throw new Exception("Cannot set state open because actual state of messageStatus is : `" . $messageStatus->state . "`");
				}

				$executed = $messageStatus->save();
				break;

			default:
				throw new Exception("action id `$actionId` has not effect on message");
				break;
		}

		return $executed;
	}
}

?>