<?php

namespace common\models;

use common\data\constants\State;

use Exception;

class AgentTopics extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_agent_topic');

	const STATE_PENDING = 0;
	const STATE_ACTIVE = 1;
	const STATE_INACTIVE = -1;

	public $belongsTo = array(
		'Topics' => array(
			'key' => array('topic_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Agents' => array(
			'key' => array('agent_id' => 'id'),
		),
	);

	protected static $_jsonFields = array('json' => array('data_agent'));

	/**
	 * [allActivesToExecute description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function allActivesToExecute(array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'AgentTopics.state' => State::ACTIVE,
			'AgentTopics.date_next_execution' => array('<=' => (object) 'NOW()'),
		);

		return static::all(compact('conditions'));
	}

	/**
	 *
	 */
	public static function findByTopicId($topicId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('topic_id' => $topicId);

		return static::all(compact('conditions'));
	}

	/**
	 *
	 */
	public static function findByTopicIdAndAgentId($topicId, $agentId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('topic_id' => $topicId, 'agent_id' => $agentId);

		return static::all(compact('conditions'));
	}

	/**
	 *
	 */
	public static function build($topic, $user, $agentId, $agentData = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$topic->checkSaved();
		$user->checkSaved();

		$topicId = $topic->id;

		/*
		=> On ne bloque pas la possibilité de plusieurs agent du même type pour le moment
		$currentAgent = $topic->getAgentById($agentId);

		if (!empty($currentAgent)) {
			throw new Exception("topic `$topicId` have already an agent : `$agentId`");
		}
		*/

		$agent = Agents::getById($agentId);

		if (empty($agent)) {
			throw new Exception("Agent : `$agentId` not exists");
		}

		$agentTopic = self::create();
		$agentTopic->related('topic', $topic);
		$agentTopic->related('agent', $agent);
		$agentTopic->related('user', $user);
		$agentTopic->state = State::ACTIVE;
		$agentTopic->dataAgent($agentData);

		$agentTopic->getAgentInstance()->populateEndExecution();
		$agentTopic->getAgentInstance()->populateNextExecution();

		if (!$agentTopic->save()) {
			throw new Exception("cannot save AgentTopics");
		}

		return $agentTopic;
	}

	/**
	 *
	 */
	public function dataAgent($agentTopic, array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$agentTopic->data_agent = $data;
	}

	/**
	 * [getAgentInstance description]
	 * @param  [type] $agentTopic [description]
	 * @return [type]             [description]
	 */
	public function getAgentInstance($agentTopic) {
		$klass = Agents::agentKlassById($agentTopic->agent_id);
		if (!empty($klass)) {
			return $klass::instance($agentTopic);
		}

		throw new Exception('cannot instanciate agent for agentId : ' . $agentTopic->agent_id);
	}

	/**
	 * [insertBlock description]
	 * @param  [type] $agentTopic      [description]
	 * @param  [type] $blockContainer [description]
	 * @param  array  $options         [description]
	 * @return [type]                  [description]
	 */
	public function insertBlock($agentTopic, $blockContainer, $user = null, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$agent = $agentTopic->getAgentInstance();
		$block = $agent->getBlock($user);

		if (empty($block)) {
			return false;
		} else {
			return $blockContainer->addBlock($block['name'], $block['data'], $block['position']);
		}
	}

	/**
	 * [isAdmin description]
	 * @param  [type]  $agentTopic       [description]
	 * @param  [type]  $userInstanceOrId [description]
	 * @param  array   $options          [description]
	 * @return boolean                   [description]
	 */
	public function isAdmin($agentTopic, $userInstanceOrId, array $options = array()) {
		$userId = is_numeric($userInstanceOrId) ? $userInstanceOrId : $userInstanceOrId->id;
		return $agentTopic->user_id == $userId;
	}

	/**
	 * [isMember description]
	 * @param  [type]  $agentTopic [description]
	 * @param  [type]  $user       [description]
	 * @param  array   $options    [description]
	 * @return boolean             [description]
	 */
	public function isMember($agentTopic, $user, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$topic = $agentTopic->related('topic');
		$topicUsers = $topic->populateUsers();

		foreach ($topicUsers as $topicUser) {
			if ($topicUser->state == State::ACTIVE && $topicUser->user_id = $user->id) {
				return true;
			}
		}
		return false;
	}

	/**
	 * [setAnswer description]
	 * @param [type] $agentTopic [description]
	 * @param [type] $userId     [description]
	 * @param [type] $answer     [description]
	 */
	public function setAnswer($agentTopic, $userId, $answer) {
		return AgentAnswers::build($agentTopic->id, $userId, $answer);
	}

	/**
	 * [mergeUserStatus description]
	 * @param  [type] $agentTopic [description]
	 * @param  [type] $topicUsers [description]
	 * @param  array  $options    [description]
	 * @return [type]             [description]
	 */
	public function mergeUserStatus($agentTopic, $topicUsers, array $options = array()) {
		$agent = $agentTopic->getAgentInstance();
		$usersData = $agent->userData();

		foreach ($usersData as $userId => $data) {
			foreach ($topicUsers as $topicUser) {
				if ($topicUser->user_id == $userId) {
					$temp = $topicUser->agentData;
					$temp[$agentTopic->id] = $data;
					$topicUser->agentData = $temp;
				}
			}
		}

		return $topicUsers;
	}

	/**
	 * [allTopicUsersActives description]
	 * @param  [type] $agentTopic [description]
	 * @param  array  $options    [description]
	 * @return [type]             [description]
	 */
	public function allTopicUsersActives($agentTopic, array $options = array()) {
		return TopicUsers::findByTopicId($agentTopic->topic_id, State::ACTIVE);
	}

	/**
	 * [getTotalLifeTime description]
	 * @param  [type] $agentTopic [description]
	 * @return [type]             [description]
	 */
	public function getTotalLifeTime($agentTopic) {
		$time = strtotime($agentTopic->date_end_execution) - strtotime($agentTopic->date_creation);
		return $time <= 0 ? 0 : $time;
	}

	/**
	 * [addMessage description]
	 * @param [type] $agentTopic [description]
	 * @param [type] $message    [description]
	 * @param [type] $user       [description]
	 * @param array  $options    [description]
	 */
	public function addMessage($agentTopic, $message, $user = null, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (!empty($user)) {
			$user->checkSaved();
		}

		$message = Messages::build($agentTopic->related('topic'), $message, $agentTopic, $user, $options);

		if (!empty($message) && $message->checkSaved()) {
			$agentTopic->getAgentInstance()->onMessageAdded($message);
		}

		return $message;
	}

}

?>