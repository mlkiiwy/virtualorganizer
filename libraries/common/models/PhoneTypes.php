<?php

namespace common\models;

class PhoneTypes extends \common\data\ConstantModels {

	const MOBILE = 1;
	const CLASSIC = 2;
	const UNKNOW = 3;

	protected $_meta = array('source' => 'vo_phone_type');

	/**
	 *
	 */
	protected static $_constants = array(
		self::MOBILE => 'Mobile Phone',
		self::CLASSIC => 'Static basic phone',
		self::UNKNOW => 'Unknow',
	);

}

?>