<?php

namespace common\models;

use common\actions\ActionSendMessageToUser;

class MessageStatusUsers extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_message_status_user');

	const STATE_PENDING = 0;
	const STATE_SEND = 1;
	const STATE_READ = 2;

	public $belongsTo = array(
		'Messages' => array(
			'key' => array('message_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	/**
	 * [findByTopicIdAndStatus description]
	 * @param  [type] $topicId [description]
	 * @param  [type] $state   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function findByTopicIdAndStatus($topicId, $state, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'Messages.topic_id' => $topicId,
			'MessageStatusUsers.state' => $state,
		);

		$with = array('Messages', 'Users');

		return static::all(compact('conditions', 'with'));
	}

	/**
	 * [firstByMessageIdAndUserId description]
	 * @param  [type] $messageId [description]
	 * @param  [type] $userId    [description]
	 * @param  array  $options   [description]
	 * @return [type]            [description]
	 */
	public static function firstByMessageIdAndUserId($messageId, $userId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array(
			'MessageStatusUsers.message_id' => $messageId,
			'MessageStatusUsers.user_id' => $userId,
		);

		return static::first(compact('conditions'));
	}

	/**
	 * [send description]
	 * @param  [type] $messageStatus [description]
	 * @param  array  $options       [description]
	 * @return [type]                [description]
	 */
	public function send($messageStatus, array $options = array()) {
		$defaults = array(
			'direct' => false,
			'parent' => null,
		);
		$options += $defaults;

		$messageStatus->checkSaved();

		$action = new ActionSendMessageToUser(compact('messageStatus'));

		if ($options['direct']) {
			$action->delayed($options);
		}

		return $action;
	}

}

?>