<?php

namespace common\models;

use common\data\constants\State;

use common\models\exceptions\TopicCannotCreateException;
use common\models\exceptions\TopicCannotAddUserException;
use common\models\exceptions\TopicCannotAddMessageException;

use lithium\core\Environment;

use Exception;

class Topics extends \common\data\BaseModels {

	const MAIL_IDENTIFIER_PREFIX = 'topic';

	protected $_meta = array('source' => 'vo_topic');

	public $hasMany = array(
		'TopicUsers' => array(
			'key' => array('id' => 'topic_id'),
		),
	);

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	public function getEmailFrom($topic) {
		return $topic->getEmailIdentifier() . '@' . Environment::get('domain.email');
	}

	/**
	 *
	 */
	public function getEmailIdentifier($topic) {
		$topic->checkSaved();

		return self::MAIL_IDENTIFIER_PREFIX . $topic->id;
	}

	/**
	 * [extractTopicIdFromMail description]
	 * @param  [type] $email [description]
	 * @return int       [description]
	 */
	public static function extractTopicIdFromMail($email) {
		$mailParts = explode('@', $email);
		return !empty($mailParts[0]) ? substr($mailParts[0], strlen(self::MAIL_IDENTIFIER_PREFIX)) : null;
	}

	/**
	 *
	 */
	public static function firstByEmailIdentifier($email, array $options = array()) {
		$id = self::extractTopicIdFromMail($email);
		return empty($id) ? null : static::getById($id);
	}

	/**
	 *
	 */
	public static function createByUser($user, array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($user)) {
			throw new TopicCannotCreateException(
				TopicCannotCreateException::USER_REQUIRED
			);
		}

		$topic = static::create($data);
		$topic->related('user', $user);

		if ($topic->save()) {
			$topic->addUser($user);
		} else {
			throw new Exception('cannot save topic');
		}

		return $topic;
	}


	/**
	 *
	 */
	public function populateUsers($topic, array $options = array()) {
		$defaults = array(
			'refresh' => false,
			'with_users' => false,
		);
		$options += $defaults;

		$topic->checkSaved();

		if (!isset($topic->users)) {
			$topic->users = TopicUsers::findByTopicId($topic->id);
		}

		if ($options['with_users']) {
			$topic->users->related('user');
		}

		return $topic->users;
	}

	/**
	 *
	 */
	public function hasUser($topic, $user) {
		$users = $topic->populateUsers();

		if (empty($user)) {
			throw new Exception('a valid user is required');
		}

		$topic->checkSaved();
		$user->checkSaved();

		$have = false;
		foreach ($users as $topicUser) {
			if ($topicUser->user_id == $user->id) {
				return true;
			}
		}

		return $have;
	}

	/**
	 *
	 */
	public function canAddUser($topic, $user) {
		$users = $topic->populateUsers();

		$topic->checkSaved();
		$user->checkSaved();

		// @todo

		return true;
	}

	/**
	 *
	 */
	public function canUserAddMessage($topic, $user) {
		$users = $topic->populateUsers();

		$topic->checkSaved();
		$user->checkSaved();

		// @todo

		return true;
	}

	/**
	 *
	 */
	public function addUser($topic, $user, array $options = array()) {
		$defaults = array(
			'count' => true,
		);
		$options += $defaults;

		if (!$topic->hasUser($user)) {
			if ($topic->canAddUser($user)) {
				$topicUser = TopicUsers::create(array(
					'topic_id' => $topic->id,
					'user_id' => $user->id,
					'state' => State::ACTIVE,
					'topic_id_mode' => TopicModes::CLASSIC,
				));

				if ($topicUser->save()) {
					$topic->users[] = $topicUser;

					if ($options['count']) {
						$topic->gen_count_users++;
						$topic->save(null, array('whitelist' => array('gen_count_users')));
					}

				} else {
					throw new Exception('cannot save TopicUser');
				}
			} else {
				throw new TopicCannotAddUserException(
					TopicCannotAddUserException::PERMISSION_DENIED
				);
			}
		} else {
			throw new TopicCannotAddUserException(
				TopicCannotAddUserException::USER_ALREADY_ADDED
			);
		}
	}

	/**
	 *
	 */
	public function addMessage($topic, $user, $message, $messageTypeId, array $options = array()) {
		$defaults = array(
			'auto_add_user' => false,
			'propagate' => true,
			'copyForSender' => false,
			'count' => true,
		);
		$options += $defaults;

		$topic->checkSaved();
		$user->checkSaved();

		if (empty($message)) {
			throw new TopicCannotAddMessageException(
				TopicCannotAddMessageException::MESSAGE_EMPTY
			);
		}

		if (!$topic->hasUser($user)) {
			if ($options['auto_add_user']) {
				$topic->addUser($user);
			} else {
				throw new TopicCannotAddMessageException(
					TopicCannotAddMessageException::USER_IS_NOT_IN_TOPIC
				);
			}
		}

		if ($topic->canUserAddMessage($user)) {

			$message = Messages::build($topic, $message, $user, null, $options);

			if (empty($message)) {
				throw new Exception('cannot save message');
			} else if ($options['count']) {
				$topic->gen_count_messages++;
				$topic->save(null, array('whitelist' => array('gen_count_messages')));
			}
		} else {
			throw new TopicCannotAddMessageException(
				TopicCannotAddMessageException::PERMISSION_DENIED_FOR_USER
			);
		}

		return $message;
	}

	/**
	 * @return MessageStatusUsers[]
	 */
	public function findMessageStatusUsersByStatus($topic, $status, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$topic->checkSaved();

		return MessageStatusUsers::findByTopicIdAndStatus($topic->id, $status);
	}

	/**
	 * @return AgentTopics[]
	 */
	public function getAgents($topic, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		return AgentTopics::findByTopicId($topic->id);
	}

	/**
	 * @return AgentTopics[]
	 */
	public function getAgentById($topic, $agentId, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		return AgentTopics::findByTopicIdAndAgentId($topic->id, $agentId);
	}

	/**
	 * [allByUser description]
	 * @param  [type] $userId  [description]
	 * @param  [type] $state   [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function allByUser($userId, $state = null, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		// TODO : use state
		if ($state != null) {
			throw new Exception('not implemented');
		}

		$conditions = array(
			'TopicUsers.user_id' => $userId,
		);
		$with = array('TopicUsers');
		return static::all(compact('conditions', 'with'));
	}
}

?>