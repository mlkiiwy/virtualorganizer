<?php

namespace common\models;

class EmailTypes extends \common\data\ConstantModels {

	const TOPIC = 1;
	const SYSTEM = 2;

	protected $_meta = array('source' => 'vo_email_type');

	/**
	 *
	 */
	protected static $_constants = array(
		self::TOPIC => 'Email based on a message of a topic',
		self::SYSTEM => 'Email system (registering process, etc.)',
	);

}

?>