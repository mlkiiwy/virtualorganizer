<?php

namespace common\models;

use common\util\Mail as MailUtil;

use Exception;

class IncomeEmails extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_income_email');

	const STATE_PENDING = 0;
	const STATE_TREATED = 1;
	const STATE_ERROR = -1;

	/**
	 * [analyse description]
	 * @param  [type] $incomeEmail [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function analyse($incomeEmail, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		return MailUtil::parse($incomeEmail->content);
	}

}

?>