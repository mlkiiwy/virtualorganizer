<?php

namespace common\models;

use lithium\util\Validator;

use common\data\constants\State;
use common\data\constants\CreationState;

use common\interfaces\IUser;
use common\interfaces\IBuildable;

use common\util\Date;
use common\util\Set;

class Users extends \common\data\BaseModels implements IUser, IBuildable {

	const DEFAULT_GRAIN = 'grain';

	protected $_meta = array('source' => 'vo_user');

	public $hasMany = array(
		'UserEmails' => array(
			'key' => array('id' => 'user_id'),
		),
	);

	protected static $_testUserData = array(
		'id' => 1,
		'email' => 'labrut@gmail.com',
		'password' => 'jupiter69moumoune',
		'state_admin' => 1,
	);

	protected static $_testUser = null;

	/**
	 * [testUser description]
	 * @return [type] [description]
	 */
	public static function testUser() {
		if (empty(self::$_testUser)) {
			$user = static::first(self::$_testUserData['id']);
			if (empty($user)) {
				$user = static::build(self::$_testUserData);
				$user->save();
				$user->activate();
			}
			self::$_testUser = $user;
		}
		return self::$_testUser;
	}

	/**
	 * [getNewValidationCode description]
	 * @param  [type] $grain [description]
	 * @return [type]        [description]
	 */
	public static function getNewValidationCode($grain = null) {
		$grain = empty($grain) ? self::DEFAULT_GRAIN : $grain;
		return md5($grain . time() . self::DEFAULT_GRAIN);
	}

	/**
	 * [label description]
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function label($user) {
		return $user->label;
	}

	/**
	 * [build description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build(array $data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if (empty($data['email'])) {
			if (!empty($data['emails'])) {
				$data['email'] = Set::first($data['emails']);
			}
			throw new Exception("email is required");
		}

		if (!empty($data['emails'])) {

			foreach ($data['emails'] as $inputEmail) {
				if (!Validator::isEmail($inputEmail)) {
					throw new Exception("`$inputEmail` is not a valid email");
				}
			}

			$userEmail = UserEmails::allByEmails($data['emails']);
		} else {

			if (!Validator::isEmail($data['email'])) {
				throw new Exception("`" . $data['email'] . "` is not a valid email");
			}

			$userEmail = UserEmails::firstByEmail($data['email']);
		}

		if (empty($userEmail)) {

			// Formating data
			$data['main_email'] = $data['email'];
			unset($data['email']);

			if (empty($data['label'])) {
				$data['label'] = $data['main_email'];
			}

			if (empty($data['login'])) {
				$data['login'] = $data['main_email'];
			}

			if (empty($data['password'])) {
				$data['password'] = md5($data['main_email'] . time());
			} else {
				$data['password'] = md5($data['password']);
			}

			if (empty($data['validation_code'])) {
				$data['validation_code'] = static::getNewValidationCode($data['main_email']);
			}

			if (empty($data['application_id_creation'])) {
				$data['application_id_creation'] = Applications::getCurrentId();
			}

			if (!empty($data['phone_number']) || !empty($data['phone_numbers'])) {
				if (empty($data['phone_number'])) {
					$data['phone_number'] = Set::first($data['phone_numbers']);
				}

				if (is_array($data['phone_number'])) {
					$data['main_phone_number'] = $data['phone_number']['number'];
				} else {
					$data['main_phone_number'] = $data['phone_number'];
				}
			}

			$user = static::create($data);
			if (!$user->save()) {
				throw new Exception("cannot save user");
			} else {
				// Attach main email
				$user->addEmail($data['main_email']);

				if (!empty($data['phone_number'])) {
					$user->addPhoneNumber($data['phone_number']);
				}
			}
		} else {
			$user = $userEmail->user;
		}

		if (!empty($data['emails'])) {
			foreach ($data['emails'] as $email) {
				if ($user->main_email != $email) {
					$user->addEmail($email);
				}
			}
		}

		if (!empty($data['phone_numbers'])) {
			foreach ($data['phone_numbers'] as $number) {
				if ($user->main_phone_number != $number) {
					$user->addPhoneNumber($number);
				}
			}
		}

		return $user;
	}

	/**
	 * [addEmail description]
	 * @param [type] $email   [description]
	 * @param array  $options [description]
	 */
	public function addEmail($user, $email, array $options = array()) {
		$defaults = array(
			'state' => State::ACTIVE,
			'user_id_creation' => $user->id,
		);
		$options += $defaults;

		$user->checkSaved();

		$main = $user->main_email == $email;
		$state = $options['state'];
		$data = compact('email', 'main') + array('user_id' => $user->id);

		return UserEmails::build($data);
	}

	/**
	 * [addPhoneNumber description]
	 * @param [type] $phoneNumber   [description]
	 * @param array  $options [description]
	 */
	public function addPhoneNumber($user, $phoneNumber, array $options = array()) {
		$defaults = array(
			'state' => State::ACTIVE,
			'user_id_creation' => $user->id,
		);
		$options += $defaults;

		$user->checkSaved();

		$number = !empty($phoneNumber['number']) ? $phoneNumber['number'] : $phoneNumber;

		$main = $user->main_phone_number == $phoneNumber;
		$state = $options['state'];
		$data = array(
			'user_id' => $user->id,
			'main' => $main,
			'phone_number' => $phoneNumber,
			'phone_type_id' => empty($phoneNumber['phone_type_id']) ? PhoneTypes::UNKNOW : $phoneNumber['phone_type_id']
		);

		return UserPhoneNumbers::build($data);
	}

	/**
	 * [activate description]
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function activate($user) {
		if ($user->state != State::ACTIVE) {
			$user->state = State::ACTIVE;
			$user->date_activation = Date::sqlNow();

			return $user->save();
		}

		return false;
	}

	/**
	 * [comparePassword description]
	 * @param  [type] $user  [description]
	 * @param  [type] $input [description]
	 * @return [type]        [description]
	 */
	public function comparePassword($user, $input) {
		return $user->password == md5($input);
	}

	/**
	 * [firstByEmailAndPassword description]
	 * @param  [type] $email    [description]
	 * @param  [type] $password [description]
	 * @param  array  $options  [description]
	 * @return [type]           [description]
	 */
	public static function firstByEmailAndPassword($email, $password, array $options = array()) {
		$user = UserEmails::firstByEmail($email);
		return !empty($user) && $user->related('user')->comparePassword($password) ? $user->related('user') : false;
	}

	/**
	 * [firstByValidationCode description]
	 * @param  [type] $code    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function firstByValidationCode($code, array $options = array()) {
		$defaults = array(
			'state' => State::DRAFT,
		);
		$options += $defaults;

		$conditions = array(
			'Users.state' => $options['state'],
			'Users.validation_code' => $code,
		);

		return static::first(compact('conditions'));
	}
}

?>