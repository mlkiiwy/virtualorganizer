<?php

namespace common\models;

class ExternalApis extends \common\data\ConstantModels {

	const FACEBOOK = 1;
	const GOOGLE = 2;

	const FACEBOOK_SLUG = 'facebook';
	const GOOGLE_SLUG = 'google';

	protected $_meta = array('source' => 'vo_external_api');

	/**
	 *
	 */
	protected static $_constants = array(
		self::FACEBOOK => array('label' => 'Facebook', 'slug' => self::FACEBOOK_SLUG),
		self::GOOGLE => array('label' => 'Google', 'slug' => self::GOOGLE_SLUG),
	);

}

?>