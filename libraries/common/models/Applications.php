<?php

namespace common\models;

class Applications extends \common\data\ConstantModels {

	const WEBSITE = 1;

	protected $_meta = array('source' => 'vo_application');

	/**
	 *
	 */
	protected static $_constants = array(
		self::WEBSITE => 'Website',
	);

	protected static $_current = null;

	/**
	 * [current description]
	 * @param  boolean $application [description]
	 * @param  array   $options     [description]
	 * @return [type]               [description]
	 */
	public static function current($application = false, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		if ($application !== false) {
			if (is_numeric($application)) {
				$application = static::create(array('id' => $application), array('exist' => true));
			}

			self::$_current = $application;
		}

		return self::$_current;
	}

	/**
	 * [getCurrentId description]
	 * @return [type] [description]
	 */
	public static function getCurrentId() {
		$current = static::current();
		return $current ? $current->id : null;
	}

}

?>