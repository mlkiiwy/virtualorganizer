<?php

namespace common\models;

use common\data\constants\State;

class ExternalApiRightByApis extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_external_api_right_by_api');

	public $belongsTo = array(
		'ExternalApis' => array(
			'key' => array('external_api_id' => 'id'),
		),
		'ExternalApiRights' => array(
			'key' => array('external_api_right_id' => 'id'),
		),
	);
}

?>