<?php

namespace common\models;

class ActionLogs extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_log_action');

	const STATE_UNKNOW = 0;
	const STATE_SUCCESS = 1;
	const STATE_FAILED = -1;

	const STEP_NONE = 0;
	const STEP_BEFORE = 1;
	const STEP_AFTER = 2;

	protected static $_jsonFields = array('json' => array('input', 'result'));

	/**
	 *
	 */
	public function input($log, $data) {
		$log->input = $data;
	}

	/**
	 *
	 */
	public function result($log, $data) {
		$log->result = $data;
	}

	/**
	 *
	 */
	public function setState($log, $value = null) {
		if ($value === null) {
			$log->state = self::STATE_UNKNOW;
		} else {
			$log->state = empty($value) ? self::STATE_FAILED : self::STATE_SUCCESS;
		}
	}

	/**
	 *
	 */
	public function setStep($log, $value = null) {
		if ($value === null) {
			$log->step = self::STEP_NONE;
		} else {
			$log->step = $value;
		}
	}
}

?>