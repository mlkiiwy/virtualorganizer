<?php

namespace common\models;

use common\data\constants\State;

class ExternalApiRightAccountValues extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_external_api_right_account_value');

	public $belongsTo = array(
		'ExternalApiAccounts' => array(
			'key' => array('external_api_account_id' => 'id'),
		),
		'ExternalApiRights' => array(
			'key' => array('external_api_right_id' => 'id'),
		),
	);
}

?>