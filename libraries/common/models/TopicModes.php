<?php

namespace common\models;

class TopicModes extends \common\data\ConstantModels {

	const CLASSIC = 1;

	protected $_meta = array('source' => 'vo_topic_mode');

	/**
	 *
	 */
	protected static $_constants = array(
		self::CLASSIC => 'Classic topic',
	);

}

?>