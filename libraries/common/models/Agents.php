<?php

namespace common\models;

class Agents extends \common\data\ConstantModels {

	protected $_meta = array('source' => 'vo_agent');

	const SIMPLE_QUESTION = 1;

	/**
	 *
	 */
	protected static $_constants = array(
		self::SIMPLE_QUESTION => 'Simple question agent (Yes / No / Maybe)',
	);

	/**
	 * [agentKlassById description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public static function agentKlassById($id) {
		switch ($id) {
			case self::SIMPLE_QUESTION:
				return 'common\agents\SimpleQuestionAgent';
		}

		return false;
	}
}

?>