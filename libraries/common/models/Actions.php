<?php

namespace common\models;

class Actions extends \common\data\ConstantModels {

	protected $_meta = array('source' => 'vo_action');

	const CREATE_TOPIC = 1;
	const ADD_USER_TO_TOPIC = 2;
	const ADD_MESSAGE_TO_TOPIC = 3;
	const CREATE_USER = 4;
	const SEND_PENDING_MESSAGE_OF_TOPIC = 5;
	const SEND_MESSAGE_TO_USER = 6;
	const CREATE_SIMPLE_QUESTION_AGENT = 7;
	const CREATE_INCOMING_EMAIL = 8;
	const ANALYSE_INCOMING_EMAIL = 9;
	const ADD_MESSAGE_TO_TOPIC_FROM_EMAIL = 10;
	const OPEN_EMAIL = 11;
	const CLICK_EMAIL = 12;
	const READ_MESSAGE = 13;
	const ACTIVATE_USER = 14;
	const REFRESH_EXTERNAL_API_ACCESS_TOKEN = 15;
	const CLEAN_EXTERNAL_API_TOKENS = 16;
	const ADD_EMAIL_TO_USER = 17;
	const CONNECT_USER = 18;
	const LINK_EXTERNAL_API_ACCOUNT_TO_USER = 19;
	const SYNC_EXTERNAL_API_ACCOUNT_FRIENDS = 20;
	const LOGIN_USER = 21;
	const SEND_ACTIVATION_EMAIL = 22;
	const DISCONNECT_USER = 23;
	const SEND_EMAIL = 24;
	const ANSWER_TO_AN_AGENT_TOPIC = 25;
	const SHOW_AGENT = 26;
	const CREATE_AGENT_EXECUTION_TASKS = 27;
	const AGENT_EXECUTION = 28;
	const ADD_AGENT_MESSAGE_TO_TOPIC = 29;

	/**
	 *
	 */
	protected static $_constants = array(
		self::CREATE_TOPIC => 'Create a topic',
		self::ADD_USER_TO_TOPIC => 'Add a user to a topic',
		self::ADD_MESSAGE_TO_TOPIC => 'Add a message to a topic',
		self::CREATE_USER => 'Create a user',
		self::SEND_PENDING_MESSAGE_OF_TOPIC => 'Create ActionSendMessageToUser to execute',
		self::SEND_MESSAGE_TO_USER => 'Send a message to a particular user from a topic',
		self::CREATE_SIMPLE_QUESTION_AGENT => 'Create a new instance of simple question agent associed to a topic',
		self::CREATE_INCOMING_EMAIL => 'Create an incoming email into base',
		self::ANALYSE_INCOMING_EMAIL => 'Analyse an incoming email and store result into base (as analysed email)',
		self::ADD_MESSAGE_TO_TOPIC_FROM_EMAIL => 'Take an analysed email and insert it into a topic by using ADD_MESSAGE_TO_TOPIC action',
		self::OPEN_EMAIL => 'Open an email',
		self::CLICK_EMAIL => 'Click a link into an email',
		self::READ_MESSAGE => 'Read a message',
		self::ACTIVATE_USER => 'Activation user account',
		self::REFRESH_EXTERNAL_API_ACCESS_TOKEN => 'Refresh external api access token',
		self::CLEAN_EXTERNAL_API_TOKENS => 'Clean external api tokens',
		self::ADD_EMAIL_TO_USER => 'Add an email to a user account',
		self::CONNECT_USER => 'Connect an user',
		self::LINK_EXTERNAL_API_ACCOUNT_TO_USER => 'Link an external api account to an user',
		self::SYNC_EXTERNAL_API_ACCOUNT_FRIENDS => 'Synchronise Friends data (contacts, friends, etc.) of an api external account',
		self::LOGIN_USER => 'Login an user with basic credentials',
		self::SEND_ACTIVATION_EMAIL => 'Send an activation email to an user',
		self::DISCONNECT_USER => 'Disconnect an user',
		self::SEND_EMAIL => 'Send an email',
		self::ANSWER_TO_AN_AGENT_TOPIC => 'Answer to an agent topic',
		self::SHOW_AGENT => 'Show an agent to a user',
		self::CREATE_AGENT_EXECUTION_TASKS => 'Create delayed tasks for each activated agent',
		self::AGENT_EXECUTION => 'Execute an agent',
		self::ADD_AGENT_MESSAGE_TO_TOPIC => 'Add an agent message to a topic to a particular user or not',
	);
}

?>