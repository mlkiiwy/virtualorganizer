<?php

namespace common\models;

use common\data\constants\GroupUserState;

class GroupUsers extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_group_user');

	public $belongsTo = array(
		'Groups' => array(
			'key' => array('group_id' => 'id'),
		),
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
	);

	/**
	 * [build description]
	 * @param  [type] $group   [description]
	 * @param  [type] $user    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function build($group, $user, array $options = array()) {
		$defaults = array(
			'state' => GroupUserState::MEMBER,
		);
		$options += $defaults;

		$data = array(
			'group_id' => $group->id,
			'user_id' => $user->id,
			'state' => $options['state'],
		);

		$groupUser = static::create($data);

		return $groupUser->save() ? $groupUser : false;
	}
}

?>