<?php

namespace common\models;

use common\data\constants\State;
use common\data\constants\CreationState;

class UserContacts extends \common\data\BaseModels {

	protected $_meta = array('source' => 'vo_user_email');

	public $belongsTo = array(
		'Users' => array(
			'key' => array('user_id' => 'id'),
		),
		'Contacts' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_contact' => 'id'),
		),
		'Creators' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_creation' => 'id'),
		),
	);

	public $hasOne = array(
		'ExternalApis' => array(
			'key' => array('external_api_id' => 'id'),
		),
	);

	/**
	 * [connect description]
	 * @param  [type] $owner       [description]
	 * @param  [type] $userContact [description]
	 * @param  array  $base        [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public static function connect($owner, $contact, array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$defaultsData = array(
			'state' => State::DRAFT,
			'user_id_creation' => $owner->id,
			'creation_state' => CreationState::BASIC,
			'external_api_id' => null,
			'gen_count_message' => 0,
			'gen_count_topic' => 0,
			'gen_count_group' => 0,
			'gen_friend_force' => 0,
			'gen_affinity' => 0,
		);
		$data += $defaultsData;

		$data['user_id'] = $owner->id;
		$data['user_id_contact'] = $contact->id;

		if ($data['user_id'] == $data['user_id_contact']) {
			throw new Exception('cannot connect itself');
		}

		$userContact = static::create($data);
		return $userContact->save() ? $userContact : false;
	}
}

?>