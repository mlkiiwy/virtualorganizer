<?php

namespace common\models;

use common\data\constants\State;
use common\data\constants\CreationState;

class Groups extends \common\data\BaseModels {

	const DEFAULT_LABEL = 'Unamed group';

	protected $_meta = array('source' => 'vo_group');

	public $belongsTo = array(
		'GroupTypes' => array(
			'key' => array('group_type_id' => 'id'),
		),
		'Creators' => array(
			'to' => 'common\model\Users',
			'key' => array('user_id_creation' => 'id'),
		),
	);

	/**
	 * [build description]
	 * @param  [type]  $creator [description]
	 * @param  boolean $label   [description]
	 * @param  [type]  $members [description]
	 * @param  array   $data    [description]
	 * @param  array   $options [description]
	 * @return [type]           [description]
	 */
	public static function build($creator, $label = false, $members = null, array $data = array(), array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$baseData = array(
			'group_type_id' => GroupTypes::PRIVE,
			'state' => State::DRAFT,
			'creation_state' => CreationState::BASIC,
			'user_id_creation' => $creator->id,
		);
		$data += $baseData;

		$data['user_id_creation'] = $creator->id;

		if ($label === false) {
			$label = self::DEFAULT_LABEL;
		}

		$group = static::create($data);

		if ($group->save()) {

			if (!empty($members)) {
				foreach ($members as $member) {
					$group->add($member);
				}
			}
		}

		return empty($group->id) ? false : $group;
	}

	/**
	 * [add description]
	 * @param [type] $group   [description]
	 * @param [type] $user    [description]
	 * @param array  $options [description]
	 */
	public function add($group, $user, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		return GroupUsers::build($group, $user, $options);
	}
}

?>