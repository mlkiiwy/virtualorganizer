<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionReadMessage extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'message' => array(
			'type' => 'Messages',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::READ_MESSAGE,
		'label' => 'Action read a message',
		'result' => array(
			'type' => 'boolean',
			'name' => 'readMessageStatus',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$message = $params['message'];
		$user = $this->user();
		return $message->action(Actions::READ_MESSAGE, $user);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>