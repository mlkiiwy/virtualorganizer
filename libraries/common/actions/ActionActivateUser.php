<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;

class ActionActivateUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'user' => array(
			'type' => 'Users',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::ACTIVATE_USER,
		'label' => 'Activate a user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'activated',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = $params['user'];
		return $user->activate();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$params = $this->getParams();
		$data = array(
			'user_id' => $params['user']->id,
		);
		return $data + parent::dataForLogger();
	}
}

?>