<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Emails;

class ActionOpenEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'Emails',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::OPEN_EMAIL,
		'label' => 'Action open email',
		'result' => array(
			'type' => 'boolean',
			'name' => 'openEmailResult',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$email = $params['email'];
		return $email->action(Actions::OPEN_EMAIL);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>