<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

use Exception;

class ActionAnswerToAgentTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'agentTopic' => array(
			'type' => 'AgentTopics',
			'required' => true,
		),
		'user' => array(
			'type' => 'Users',
			'required' => true,
		),
		'answer' => array(
			'type' => 'int',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::ANSWER_TO_AN_AGENT_TOPIC,
		'label' => 'Answer to an agent topic',
		'result' => array(
			'type' => 'boolean',
			'name' => 'answered',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$agentTopic = $params['agentTopic'];
		$answererUser = $params['user'];
		$answer = $params['answer'];

		$user = $this->user();

		// Security check
		if ($agentTopic->isAdmin($user)) {
			// Do nothing
		} else if ($answererUser->id != $user->id) {
			throw new Exception('security error : cannot answer for ' . $answererUser->label . ' you are not admin of this agent');
		}

		// Save the answer
		return $agentTopic->setAnswer($answererUser->id, $answer);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>