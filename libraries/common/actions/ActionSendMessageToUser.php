<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\MessageStatusUsers;
use common\models\Emails;

use Exception;

class ActionSendMessageToUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'messageStatus' => array(
			'type' => 'MessageStatusUsers',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::SEND_MESSAGE_TO_USER,
		'label' => 'Send a message of a topic to a particular user',
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$messageStatus = $params['messageStatus'];

		if ($messageStatus->state != MessageStatusUsers::STATE_PENDING) {
			throw new Exception('message already sent');
		}

		$user = $messageStatus->related('user');
		$message = $messageStatus->related('message');

		// For now we only sent message via email
		$email = Emails::build($user, $message);
		if ($email->save()) {
			$email->send(array('delayed' => true));
		} else {
			throw new Exception('cannot save the email');
		}

		$messageStatus->state = MessageStatusUsers::STATE_SEND;
		$messageStatus->save();

		return $email;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
			'topic_id' => $this->resultId(),
		);
		return $data + parent::dataForLogger();
	}
}

?>