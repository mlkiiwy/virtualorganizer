<?php

namespace common\actions\util;

use common\interfaces\IUseResult;

use Exception;

class UseResult implements IUseResult {

	const PREFIX_PARENT = 'parent';
	const PREFIX_PRE_ACTION = 'pre';

	/**
	 *
	 */
	protected $_key;

	/**
	 *
	 */
	protected $_prefix;

	/**
	 *
	 */
	public function __construct($string) {
		$data = explode('.', $string);

		if (count($data) == 2) {
			$this->_key = $data[1];
			$this->_prefix = $data[0];
		} else {
			$this->_key = $string;
		}
	}

	/**
	 *
	 */
	public function isParent() {
		return $this->_prefix == self::PREFIX_PARENT;
	}

	/**
	 *
	 */
	public function isPreAction() {
		return $this->_prefix == self::PREFIX_PRE_ACTION;
	}

	/**
	 *
	 */
	public function key() {
		return $this->_key;
	}
}

?>