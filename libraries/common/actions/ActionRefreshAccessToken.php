<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;
use common\models\ExternalApiTokens;

class ActionRefreshAccessToken extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'externalApiAccount' => array(
			'type' => 'ExternalApiAccounts',
			'required' => true,
		),
		'infoToken' => array(
			'type' => 'array',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::REFRESH_EXTERNAL_API_ACCESS_TOKEN,
		'label' => 'Refresh access token',
		'result' => array(
			'type' => 'ExternalApiTokens',
			'name' => 'token',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$infoToken = $params['infoToken'];
		$account = $params['externalApiAccount'];

		$activeToken = $account->getToken();

		if (!empty($activeToken)) {
			if ($activeToken->access_token == $infoToken['access_token']) {
				$activeToken->date_expiration = $infoToken['date_expiration'];
				$activeToken->save();
			} else {
				$activeToken->desactivate();

				$activeToken = ExternalApiTokens::build($account, $infoToken);
				$activeToken->save();
			}
		} else {
			$activeToken = ExternalApiTokens::build($account, $infoToken);
			$activeToken->save();
		}

		return $activeToken;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$input = $this->input();
		$user = $this->user();
		$data = array(
			'user_id' => $user->id,
		);
		return $data + parent::dataForLogger();
	}
}

?>