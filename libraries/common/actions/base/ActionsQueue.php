<?php

namespace common\actions\base;

use common\interfaces\IChainableAction;
use common\interfaces\IActionQueue;

use Exception;

class ActionsQueue extends BaseAction implements IActionQueue {

	/**
	 *
	 */
	protected $_queue = array();

	/**
	 * @interface IActionQueue
	 */
	public function add($action, $inputForAction = array(), array $options = array()) {
		$defaults = array('result' => null);
		$options += $defaults;

		if ($action instanceof IChainableAction) {
			$action->parentQueue($this);
		}
		$this->_queue[] = array(
			'action' => $action,
			'input' => $inputForAction,
			'result' => $options['result'],
		);
	}

	/**
	 *
	 */
	protected function _execute(array $params) {
		$allResults = array();
		$results = array();
		$preAction = null;
		foreach ($this->_queue as $key => $actionData) {
			$action = $actionData['action'];

			$action->displayer($this->displayer());

			if (!empty($actionData['input'])) {
				$action->input($actionData['input']);
			}

			$user = $this->user();
			if (!empty($user)) {
				$action->user($user);
			}

			if (!empty($preAction) && $action instanceof IChainableAction) {
				$action->preAction($preAction);
			}

			$response = $action->execute();

			$allResults[$key] = $response->result();

			if ($response->success()) {
				if (!empty($actionData['result']) && $response->success() ) {
					$labelResult = $actionData['result'];
					$result = $response->result();
					$results[$labelResult] = ($action instanceof IActionQueue) ? $result['all'] : $result;
				}
			} else {
				// Error stop queue
				$results['all'] = $allResults;
				$label = $action->label();
				$error = $response->getException()->getMessage();
				$file = $response->getException()->getFile();
				if (!empty($file)) {
					$error .= " on `$file` at line " . $response->getException()->getLine();
				}
				throw new Exception("Action [$key] `$label` stop queue execution => `$error`");
			}

			$preAction = $action;
			$results['all'] = $allResults;

			$this->_setResult($results);
		}

		return $results;
	}
}

?>