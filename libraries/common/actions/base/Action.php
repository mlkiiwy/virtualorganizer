<?php

namespace common\actions\base;

use common\interfaces\IActionModel;

use common\models\Actions as ActionsModel;

use common\util\Klass as KlassUtil;

use Exception;

class Action extends BaseAction implements IActionModel {

	/**
	 *
	 */
	protected $_baseConfig = array(
		'user' => false,
		'logger' => '\common\extensions\log\Logger',
		'id' => null,
	);

	/**
	 * [createByName description]
	 * @param  [type] $name    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function createByName($name, array $options = array()) {
		$defaults = array(
			'paths' => array('common\actions', 'common\actions\queue'),
		);
		$options += $defaults;

		return KlassUtil::instance($name, $options);
	}

	/*
	* @interface IActionModel
	*/
	public function model() {
		$modelId = $this->modelId();
		return new ActionsModel(array('id' => $modelId), array('exist' => true));
	}

	/*
	* @interface IActionModel
	*/
	public function modelId() {
		$config = $this->config();
		if (empty($config['id'])) {
			throw new Exception('Action model is not defined in configuration');
		}
		return $config['id'];
	}
}

?>