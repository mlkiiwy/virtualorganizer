<?php

namespace common\actions\base;

use common\actions\responses\ActionResponse;

use common\interfaces\IUser;
use common\interfaces\ILabel;
use common\interfaces\IAction;
use common\interfaces\IConfigurable;
use common\interfaces\IActionUser;
use common\interfaces\ILoggableAction;
use common\interfaces\IChainableAction;
use common\interfaces\IUseResult;
use common\interfaces\IActionQueue;
use common\interfaces\IDisplayer;

use common\models\ActionLogs;

use common\util\Model as ModelUtil;
use common\util\Types;

use common\extensions\tasks\Tasks;

use Exception;

class BaseAction implements IAction, IChainableAction, IActionUser, ILoggableAction, ILabel, IConfigurable {

	/**
	 *
	 */
	protected $_params = array();

	/**
	 *
	 */
	protected $_baseConfig = array(
		'user' => false,
		'logger' => '\common\extensions\log\Logger',
	);

	/**
	 *
	 */
	protected $_config = array();

	/**
	 *
	 */
	protected $_input = array();

	/**
	 *
	 */
	protected $_user = null;

	/**
	 *
	 */
	protected $_result = null;

	/**
	* IAction
	*/
	protected $_lastAction = null;

	/**
	* IActionQueue
	*/
	protected $_parentQueue = null;

	/**
	 * @var IDisplayer
	 */
	protected $_displayer = null;

	/**
	 *
	 */
	public function __construct(array $input = array()) {
		$this->input($input);
	}

	/**
	* @interface IAction
	*/
	public function input(array $input = null) {
		if ($input !== null) {
			$this->_input = $input;
		}
		return $this->_input;
	}

	/**
	*
	*/
	public function getParams(array $options = array()) {
		$defaults = array('tryLoadRecord' => true);
		$options += $defaults;

		$input = $this->input();
		$result = array();

		// Checking inputs
		foreach ($this->_params as $name => $config) {
			// Check value
			$value = null;
			$isset = false;
			if (!empty($config['required']) && !isset($input[$name]) && !isset($config['default'])) {
				throw new Exception("parameter error : `$name` missing");
			}
			if (isset($input[$name])) {
				$isset = true;
				$value = $input[$name];
			} else if (isset($config['default'])) {
				$isset = true;
				$value = $config['default'];
			}

			// Search value if needed
			if ($value instanceof IUseResult) {
				// Need to search in parent or last result
				if ($value->isParent()) {
					$parent = $this->parentQueue();
					if (empty($parent)) {
						throw new Exception('cannot search result in parent : parent is empty');
					}
					$value = $parent->getResult($value->key(), array('notIsset' => 'error'));
				} else if ($value->isPreAction()) {
					$pre = $this->preAction();
					if (empty($pre)) {
						throw new Exception('cannot search result in pre-action : pre-action is empty');
					}
					$value = $pre->getResult($value->key(), array('notIsset' => 'error'));
				}
			}

			// Checking value type
			if (!empty($config['type'])) {

				if ($options['tryLoadRecord'] && !is_null($value) && !Types::isPrimitive($config['type'])) {

					if (Types::isCollectionType($config['type']) && Types::test($value, 'int[]', array('error' => 'return')) !== false) {
						$pType = Types::extractTypeFromCollection($config['type']);

						if (ModelUtil::isModel($pType)) {
							$tryValue = ModelUtil::findByIds($pType, $value);

							if ($tryValue !== false) {
								$value = $tryValue;
								$this->_input[$name] = $value;
							}
						}
					} else if (ModelUtil::isModel($config['type']) && Types::test($value, 'int', array('error' => 'return')) !== false) {
						$tryValue = ModelUtil::findById($config['type'], $value);

						if ($tryValue !== false) {
							$value = $tryValue;
							$this->_input[$name] = $value;
						}
					}
				}

				$value = Types::test($value, $config['type'], array('tryToCast' => true, 'error' => 'exception'));
			}

			if ($isset) {
				$result[$name] = $value;
			}
		}

		return $result;
	}

	/**
	* @interface TODO
	*/
	public function getResult($key = null, array $options = array()) {
		$defaults = array('notIsset' => null);
		$options += $defaults;

		$result = $this->result();
		$value = null;
		if (!empty($key) && is_array($result)) {
			if (!isset($result[$key]) && $options['notIsset'] == 'error') {
				throw new Exception("no result : `$key`");
			}
			$value = isset($result[$key]) ? $result[$key] : null;
		} else if (empty($key)) {
			$value = $result;
		}

		return $value;
	}

	/**
	* @interface IConfigurable
	*/
	public function config() {
		return $this->_config + $this->_baseConfig;
	}

	/**
	* @interface IAction
	*/
	public function result() {
		return $this->_result;
	}

	/**
	* @interface IAction
	*/
	public function resultId() {
		$result = $this->result();
		if (is_object($result) && !empty($result->id)) {
			return $result->id;
		} else if (is_array($result) && !empty($result['id'])) {
			return $result['id'];
		}
		return null;
	}

	/**
	 *
	 */
	public function logger() {
		$config = $this->config();
		if (empty($config['logger'])) {
			throw new Exception('no logger defined in configuration of action');
		}
		$class = $config['logger'];
		return $class::instance();
	}

	/**
	 * [displayer description]
	 * @param  IDisplayer $displayer [description]
	 * @return [type]                [description]
	 */
	public function displayer(IDisplayer $displayer = null) {
		if (!empty($displayer)) {
			$this->_displayer = $displayer;
		}
		return $this->_displayer;
	}

	/**
	* @interface IAction
	*/
	public function execute(array $options = array()) {
		$defaults = array(
			'displayer' => null,
			'user' => false,
		);
		$options += $defaults;

		if (!empty($options['user'])) {
			$this->user($options['user']);
		}

		if (!empty($options['displayer'])) {
			$this->displayer($options['displayer']);
		}

		try {
			$params = $this->_onBeforeExecute();
			$result = $this->_execute($params);
		} catch(Exception $e) {
			$this->_onAfterExecute(false, $e);
			return $this->_error($e);
		}

		if ($result instanceof ActionResponse) {
			if ($result->success()) {
				$this->_setResult($result->result());
			} else {
				$this->_onAfterExecute(false, $result->getException());
				return $this->_error($result->getException());
			}
		} else if (is_bool($result) && $result === false) {
			$this->_onAfterExecute(false, 'An error occured when executing the action');
			return $this->_error('Result is false');
		} else {
			$this->_setResult($result);
		}
		$this->_onAfterExecute(true);
		return $this->_success();
	}

	/**
	 *
	 */
	protected function _setResult($result) {
		$this->_result = $result;
	}

	/**
	 *
	 */
	protected function _execute(array $params) {
		throw new Exception('not implemented');
	}

	/**
	 *
	 */
	protected function _onBeforeExecute() {
		$user = $this->user();
		if (!empty($this->_config['user']) && empty($user)) {
			throw new Exception('user required for executing this action');
		}

		$params = $this->getParams();

		$logger = $this->logger();
		$logger->action($this, null, ActionLogs::STEP_BEFORE);

		return $params;
	}

	/**
	 *
	 */
	protected function _onAfterExecute($success = false, $exception = null) {
		$logger = $this->logger();
		$logger->action($this, $success, ActionLogs::STEP_AFTER);
	}

	/**
	 *
	 */
	protected function _success() {
		return new ActionResponse($this, ActionResponse::TYPE_SUCCESS, $this->_result);
	}

	/**
	 *
	 */
	protected function _error($data = null) {
		$response = new ActionResponse($this, ActionResponse::TYPE_ERROR);
		if (!empty($data)) {
			if ($data instanceof Exception) {
				$response->setException($data);
			}
		}
		return $response;
	}

	/**
	* @interface IChainableAction
	*/
	public function parentQueue(IActionQueue $actionQueue = null) {
		if ($actionQueue !== null) {
			$this->_parentQueue = $actionQueue;
		}
		return $this->_parentQueue;
	}

	/**
	* @interface IChainableAction
	*/
	public function preAction(IChainableAction $lastAction = null) {
		if ($lastAction !== null) {
			$this->_lastAction = $lastAction;
		}
		return $this->_lastAction;
	}

	/**
	* @interface ILabel
	*/
	public function label() {
		$config = $this->config();
		return empty($config['label']) ? __CLASS__ : $config['label'];
	}

	/**
	* @interface IActionUser
	*/
	public function user($user = null, array $options = array()) {
		$defaults = array(
			'propagate' => true,
		);
		$options += $defaults;

		if ($user !== null) {
			$this->_user = $user;

			if ($options['propagate'] && !empty($this->_parentQueue)) {
				$this->_parentQueue->user($user);
			}
		}
		return $this->_user;
	}

	/**
	* @interface IActionUser
	*/
	public function userId() {
		$user = $this->user();
		return empty($user) ? null : $user->id;
	}

	/*
	* @interface ILoggableAction
	*/
	public function dataForLogger() {
		$data = array();
		$userId = $this->userId();
		if (!empty($userId)) {
			$data['user_id_owner'] = $userId;
		}
		return $data;
	}

	/**
	 * Push the action into a task queue to be treated asynchronously
	 * @return [type] [description]
	 */
	public function delayed(array $options = array()) {
		$defaults = array(
			'parent' => null,
			'executeIfDisabled' => true,
		);
		$options += $defaults;

		if (Tasks::enabled()) {
			return Tasks::addAction($this);
		} else if ($options['executeIfDisabled']) {
			if (!empty($options['parent']) && $options['parent'] instanceof IAction) {
				$this->displayer($options['parent']->displayer());
			}
			return $this->execute();
		} else {
			return null;
		}
	}

}

?>