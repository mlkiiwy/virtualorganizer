<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Emails;

class ActionClickEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'Emails',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CLICK_EMAIL,
		'label' => 'Action click into an email',
		'result' => array(
			'type' => 'boolean',
			'name' => 'clickEmailResult',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$email = $params['email'];
		return $email->action(Actions::CLICK_EMAIL);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>