<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\IncomeEmails;

class ActionCreateIncomingEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'content' => array(
			'type' => 'string',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CREATE_INCOMING_EMAIL,
		'label' => 'Handle an create an incoming email',
		'result' => array(
			'type' => 'IncomeEmails',
			'name' => 'incomeEmail',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$email = IncomeEmails::create($params);

		if ($email->save()) {
			$action = new ActionAnalyseIncomeEmail(array('incomeEmail' => $email));
			$action->delayed(array('parent' => $this));
		}

		return $email;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>