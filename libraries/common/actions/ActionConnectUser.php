<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;

use common\data\constants\State;

use common\extensions\auth\Auth;

use Exception;

class ActionConnectUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'user' => array(
			'type' => 'Users',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CONNECT_USER,
		'label' => 'Connect an user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'connected',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = $params['user'];

		if ($user->state != State::ACTIVE) {
			throw new Exception('User `' . $user->label . '` is not active');
		}

		$user = Auth::user($user);

		if (!empty($user)) {
			// Need to set user
			$this->user($user, array('propagate' => true));
		}

		return !empty($user);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$user = $this->user();
		$data = array(
			'user_id' => !empty($user) ? $user->id : null,
		);
		return $data + parent::dataForLogger();
	}
}

?>