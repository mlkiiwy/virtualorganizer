<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionSendEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'Emails',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::SEND_EMAIL,
		'label' => 'Send an email',
		'result' => array(
			'type' => 'boolean',
			'name' => 'sended',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$email = $params['email'];

		return $email->send();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>