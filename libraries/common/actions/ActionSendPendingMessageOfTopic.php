<?php

namespace common\actions;

use common\actions\base\Action;
use common\actions\base\ActionsQueue;

use common\models\Actions;
use common\models\MessageStatusUsers;

class ActionSendPendingMessageOfTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'topic' => array(
			'type' => 'Topics',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::SEND_PENDING_MESSAGE_OF_TOPIC,
		'label' => 'Create ActionSendMessageToUser to execute',
		'result' => array(
			'type' => 'ActionsQueue',
			'name' => 'actions',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$topic = $params['topic'];

		$messageStatus = $topic->findMessageStatusUsersByStatus(MessageStatusUsers::STATE_PENDING);

		$tasks = array();
		foreach ($messageStatus as $value) {
			$actionSending = $value->send();
			$tasks[] = $actionSending->delayed(array('parent' => $this));
		}

		return $tasks;
	}

	/*
	* @todo
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>