<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionAddEmailToUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'string',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::ADD_EMAIL_TO_USER,
		'label' => 'Add an email to a user account',
		'result' => array(
			'type' => 'boolean',
			'name' => 'added',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$email = $params['email'];
		$user = $this->user();

		return $user->addEmail($email);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$input = $this->input();
		$data = array(
			'user_id' => $input['user']->id,
		);
		return $data + parent::dataForLogger();
	}
}

?>