<?php

namespace common\actions;

use common\actions\base\BaseAction;

class ActionTestResultExecute extends BaseAction {

	/**
	 *
	*/
	protected $_params = array(
		'action' => array(
			'type' => 'IAction',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'label' => 'Execute an action on param',
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$action = $params['action'];
		$action->user($this->user());
		$response = $action->execute();

		if ($response->success()) {
			return $response->result();
		} else {
			throw $response->getException();
		}
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>