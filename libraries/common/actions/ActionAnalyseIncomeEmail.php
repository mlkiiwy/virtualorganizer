<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\IncomeEmails;
use common\models\IncomeEmailAnalyseds;

use Exception;

class ActionAnalyseIncomeEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'incomeEmail' => array(
			'type' => 'IncomeEmails',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::ANALYSE_INCOMING_EMAIL,
		'label' => 'Analyse an incoming email',
		'result' => array(
			'type' => 'IncomeEmailAnalyseds',
			'name' => 'incomeEmailAnalysed',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$incomeEmail = $params['incomeEmail'];
		$incomeEmailAnalysed = null;

		try {
			$data = $incomeEmail->analyse();
			$incomeEmailAnalysed = IncomeEmailAnalyseds::build($data);
			if (empty($incomeEmailAnalysed)) {
				throw new Exception('cannot build income analysed email');
			}
			$incomeEmail->state = IncomeEmails::STATE_TREATED;
			if ($incomeEmail->save()) {
				$action = new ActionAddMessageToTopicFromEmail(compact('incomeEmailAnalysed'));
				$action->delayed(array('parent' => $this));
			}

		} catch (Exception $e) {
			$incomeEmail->state = IncomeEmails::STATE_ERROR;
			$incomeEmail->save();

			throw $e;
		}

		return $incomeEmailAnalysed;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>