<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\ExternalApiAccounts;

use common\data\constants\State;

class ActionLinkExternalApiAccountToUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'externalApiAccount' => array(
			'type' => 'ExternalApiAccounts',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::LINK_EXTERNAL_API_ACCOUNT_TO_USER,
		'label' => 'Link an external api account to an user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'linked',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$externalApiAccount = $params['externalApiAccount'];

		$user = $this->user();
		$account = ExternalApiAccounts::firstByUserIdAndApiId($user->id, $externalApiAccount->external_api_id);

		if (!empty($account) && $account->id != $externalApiAccount) {
			throw new Exception("cannot associate an `" . $externalApiAccount->related('api')->label . "` account to user " . $user->label() .
				" because this user has already an account associated");
		}

		$externalApiAccount->related('user', $user);
		$externalApiAccount->state = State::ACTIVE;

		return $externalApiAccount->save();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$input = $this->input();
		$user = $this->user();
		$data = array(
			'user_id' => $user->id,
		);
		return $data + parent::dataForLogger();
	}
}

?>