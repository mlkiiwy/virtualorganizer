<?php

namespace common\actions\responses;

use Exception;

class ActionResponse {

	const TYPE_UNSET = 0;
	const TYPE_SUCCESS = 1;
	const TYPE_ERROR = -1;

	/**
	 *
	 */
	protected $_action = null;

	/**
	 *
	 */
	protected $_type = 0;

	/**
	 *
	 */
	protected $_result = null;

	/**
	 *
	 */
	protected $_exception = null;

	/**
	 *
	 */
	public function __construct($action, $type, $result = null) {
		$this->_action = $action;
		$this->_type = $type;
		$this->_result = $result;
	}

	/**
	 *
	 */
	public function success() {
		return $this->_type === self::TYPE_SUCCESS;
	}

	/**
	 *
	 */
	public function result() {
		return $this->_result;
	}

	/**
	 *
	 */
	public function label() {
		$successString = $this->success() ? '[SUCCESS]' : '[FAILED]';
		return get_class($this->_action) . ' > ActionResponse ' . $successString;
	}

	/**
	 *
	 */
	public function setException(Exception $e) {
		$this->_exception = $e;
	}

	/**
	 *
	 */
	public function getException() {
		return $this->_exception;
	}
}

?>