<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;

use common\extensions\auth\Auth;

use Exception;

class ActionLoginUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'string',
			'required' => true,
		),
		'password' => array(
			'type' => 'string',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::LOGIN_USER,
		'label' => 'Login an user with basic credentials',
		'result' => array(
			'type' => 'boolean',
			'name' => 'logged',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = Users::firstByEmailAndPassword($params['email'], $params['password']);

		if (empty($user)) {
			throw new Exception('Login or Password incorrect');
		}

		$connect = new ActionConnectUser(compact('user'));
		$result = $connect->execute();

		if ($result->success()) {
			$this->user($user, array('propagate' => true));
			return true;
		} else {
			throw $result->getException();
		}
	}

	/*
	*
	*/
	public function dataForLogger() {
		$user = $this->user();
		$data = array(
			'user_id' => !empty($user) ? $user->id : null,
		);
		return $data + parent::dataForLogger();
	}
}

?>