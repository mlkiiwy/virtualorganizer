<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionAddAgentMessageToTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'agentTopic' => array(
			'type' => 'AgentTopics',
			'required' => true,
		),
		'message' => array(
			'type' => 'array',
			'required' => true,
		),
		'user' => array(
			'type' => 'Users',
			'required' => false,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::ADD_AGENT_MESSAGE_TO_TOPIC,
		'label' => 'Add an agent message to a topic to a particular user or not',
		'result' => array(
			'type' => 'Messages',
			'name' => 'message',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$agentTopic = $params['agentTopic'];
		$message = $params['message'];
		$user = $params['user'];

		$message = $agentTopic->addMessage($message, $user);

		if (!empty($message)) {
			foreach ($message->message_status_users as $messageStatus) {
				$messageStatus->send(array('parent' => $this, 'direct' => true));
			}
		}

		return $message;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$params = $this->getParams();

		$data = array(
			'message_id' => $this->resultId(),
		);
		return $data + parent::dataForLogger();
	}
}

?>