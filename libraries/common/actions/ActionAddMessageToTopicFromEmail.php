<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\IncomeEmailAnalyseds;

class ActionAddMessageToTopicFromEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'incomeEmailAnalysed' => array(
			'type' => 'IncomeEmailAnalyseds',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::ADD_MESSAGE_TO_TOPIC_FROM_EMAIL,
		'label' => 'Take an analysed email and insert it into a topic by using ADD_MESSAGE_TO_TOPIC action',
		'result' => array(
			'type' => 'Messages',
			'name' => 'message',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$incomeEmailAnalysed = $params['incomeEmailAnalysed'];

		$action = new ActionAddMessageToTopic(array(
			'message' => $incomeEmailAnalysed->getMessage(),
			'topic' => $incomeEmailAnalysed->related('topic'),
		));
		$action->user($incomeEmailAnalysed->related('user'));

		$result = $action->execute();

		if ($result->success()) {
			$incomeEmailAnalysed->state = IncomeEmailAnalyseds::STATE_TREATED;
			$incomeEmailAnalysed->save();
		}

		return $result;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$params = $this->getParams();

		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>