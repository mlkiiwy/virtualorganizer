<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionCleanExternalApiTokens extends Action {

	/**
	 *
	*/
	protected $_params = array();

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CLEAN_EXTERNAL_API_TOKENS,
		'label' => 'Clean external api tokens',
		'result' => array(
			'type' => 'boolean',
			'name' => 'cleaned',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		return ExternalApiTokens::desactivateExpiredTokens();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>