<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\ExternalApiAccounts;
use common\models\UserContacts;
use common\models\Groups;

use common\data\constants\State;
use common\data\constants\CreationState;

use common\util\ExceptionUtil;

use common\interfaces\IOAuthEmailContactsService;
use common\interfaces\IOAuthFriendsService;

use Exception;

class ActionSyncExternalApiAccountFriends extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'externalApiAccount' => array(
			'type' => 'ExternalApiAccounts',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::SYNC_EXTERNAL_API_ACCOUNT_FRIENDS,
		'label' => 'Synchronise Friends data (contacts, friends, etc.) of an api external account',
		'result' => array(
			'type' => 'boolean',
			'name' => 'synchronized',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$externalApiAccount = $params['externalApiAccount'];

		$token = $externalApiAccount->getToken();
		if (empty($token)) {
			throw new Exception('cannot sync friends from external api account `' . $externalApiAccount->id . '` not active token');
		}

		try {
			$service = OAuthService::getService($externalApiAccount->related('api')->slug);
			$service->accessToken($token->access_token);

			$contacts = array();
			if ($service instanceof IOAuthEmailContactsService) {
				$contacts = $service->getEmailContacts();

				$users = array();
				$defaults = array(
					'external_api_id_creation_import' => $externalApiAccount->external_api_id,
					'creation_state' => CreationState::FROM_API_IMPORT,
				);
				// For each contacts we create an user
				foreach ($contacts as $contact) {
					$user = Users::build($defaults + $contact);

					$users[$user->id] = $user;
				}

				// Link contacts
				$userOfExternalAccount = $externalApiAccount->related('user');
				foreach ($users as $user) {
					UserContacts::connect(
						$userOfExternalAccount,
						$user,
						array(
							'state' => State::ACTIVE,
							'creation_state' => CreationState::FROM_API_IMPORT,
							'external_api_id' => $externalApiAccount->external_api_id,
						)
					);
				}

				// Create groups
				// @todo : create action and action queue for this

				// First isolate groups
				$userByGroups = array();
				foreach ($users as $user) {
					foreach ($user['groups'] as $groupName) {
						$userByGroups[$groupName][] = $user;
					}
				}

				// Second create groups
				foreach ($userByGroups as $group => $members) {
					$label = 'Import from ' . $externalApiAccount->related('api')->label . ' ' . $group;

					Group::build(
						$userOfExternalAccount,
						$label,
						$members,
						array('creation_state' => CreationState::FROM_API_IMPORT)
					);
				}

			} else if ($service instanceof IOAuthFriendsService) {
				$friends = $service->getEmailContacts();

				// Create external api account for each contact
				$externalAccounts = array();
				foreach ($contacts as $contact) {
					$externalAccount = ExternalApiAccounts::build($contact);

					// Create a user if data is here
					if (!empty($externalAccount['email']) || !empty($externalAccount['emails'])) {

						// $user = ...;

						// Associate user to api_account

					}

					// Associate in groups



					$externalAccounts[$externalAccount->id] = $externalAccount;
				}

				// Create relationship between accounts

				// Create relationship between users

			}
		} catch (Exception $e) {
			throw new Exception('cannot sync friends : ' . ExceptionUtil::toString($e));
		}


		return $externalApiAccount->save();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$input = $this->input();
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>