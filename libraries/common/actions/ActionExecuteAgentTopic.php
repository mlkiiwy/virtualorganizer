<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;

class ActionExecuteAgentTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'agentTopic' => array(
			'type' => 'AgentTopics',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::AGENT_EXECUTION,
		'label' => 'Execute an agent',
		'result' => array(
			'type' => 'boolean',
			'name' => 'executed',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$agentTopic = $params['agentTopic'];
		return $agentTopic->getAgentInstance()->execute();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>