<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\AgentAnswers;

use Exception;

class ActionShowAgent extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'agentTopic' => array(
			'type' => 'AgentTopics',
			'required' => true,
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::SHOW_AGENT,
		'label' => 'Show an agent to a user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'showed',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$agentTopic = $params['agentTopic'];
		$user = $this->user();

		if (!$agentTopic->isMember($user)) {
			throw new Exception('user ' . $user->label . ' is not linked with this topic agent');
		}

		// Save the answer
		return $agentTopic->setAnswer($user->id, AgentAnswers::VALUE_NO_ANSWER);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
		);
		return $data + parent::dataForLogger();
	}
}

?>