<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;

use common\extensions\auth\Auth;

class ActionDisconnectUser extends Action {

	/**
	 *
	*/
	protected $_params = array();

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::DISCONNECT_USER,
		'label' => 'Disconnect an user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'disconnected',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		if (Auth::user(null) === null) {
			$this->user(null, array('propagate' => true));
			return true;
		} else {
			return false;
		}
	}

	/*
	*
	*/
	public function dataForLogger() {
		$user = $this->user();
		$data = array(
			'user_id' => !empty($user) ? $user->id : null,
		);
		return $data + parent::dataForLogger();
	}
}

?>