<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Emails;

use common\data\constants\State;

use Exception;

class ActionSendActivationEmail extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'user' => array(
			'type' => 'Users',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::SEND_ACTIVATION_EMAIL,
		'label' => 'Send an activation email to an user',
		'result' => array(
			'type' => 'boolean',
			'name' => 'sended',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = $params['user'];

		if ($user->state == State::ACTIVE) {
			throw new Exception('User `' . $user->label . '` already active');
		}

		$email = Emails::build($user);
		$email->subject = 'Activate your account';
		$email->addBlock('activation', array());
		return $email->save();
	}

	/*
	*
	*/
	public function dataForLogger() {
		$user = $this->user();
		$data = array(
			'user_id' => !empty($user) ? $user->id : null,
		);
		return $data + parent::dataForLogger();
	}
}

?>