<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Agents;
use common\models\AgentTopics;

class ActionCreateSimpleQuestionAgent extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'question' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'On va boire un verre ?',
		),
		'maxDateTime' => array(
			'type' => 'string',
			'required' => false,
			'default' => '',
		),
		'topic' => array(
			'type' => 'Topics',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::CREATE_SIMPLE_QUESTION_AGENT,
		'label' => 'Create a new instance of simple question agent associed to a topic',
		'result' => array(
			'type' => 'AgentTopics',
			'name' => 'agent',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = $this->user();

		$topic = $params['topic'];
		$question = $params['question'];
		$maxDateTime = $params['maxDateTime'];

		$agentTopic = AgentTopics::build($topic, $user, Agents::SIMPLE_QUESTION, compact('question', 'maxDateTime'));

		return $agentTopic;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>