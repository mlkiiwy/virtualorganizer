<?php

namespace common\actions;

use common\actions\base\CronAction;

use common\models\Actions;
use common\models\AgentTopics;

class ActionCreateAgentExecutionTasks extends CronAction {

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CREATE_AGENT_EXECUTION_TASKS,
		'label' => 'Create delayed tasks for each activated agent',
		'result' => array(
			'type' => 'int',
			'name' => 'countTasksCreated',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$agents = AgentTopics::allActivesToExecute();

		$i = 0;
		foreach ($agents as $agent) {
			$i += $agent->getAgentInstance()->delayed() ? 1 : 0;
		}

		return $i;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(

		);
		return $data + parent::dataForLogger();
	}
}

?>