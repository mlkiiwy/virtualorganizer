<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Topics;

class ActionAddUserToTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'user' => array(
			'type' => 'Users',
			'required' => true,
		),
		'topic' => array(
			'type' => 'Topics',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::ADD_USER_TO_TOPIC,
		'label' => 'Add an user to a topic',
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$topic = $params['topic'];
		return $topic->addUser($params['user']);
	}

	/*
	*
	*/
	public function dataForLogger() {
		$input = $this->input();
		$data = array(
			'topic_id' => $input['topic']->id,
			'user_id' => $input['user']->id,
		);
		return $data + parent::dataForLogger();
	}
}

?>