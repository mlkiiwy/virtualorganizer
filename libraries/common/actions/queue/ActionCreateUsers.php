<?php

namespace common\actions\queue;

use common\actions\base\ActionsQueue;

use common\actions\ActionCreateUser;

class ActionCreateUsers extends ActionsQueue {

	/**
	 *
	*/
	protected $_params = array(
		'emails' => array(
			'type' => 'string[]',
			'required' => true,
			'default' => array()
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => null,
		'label' => 'Create multiple users',
	);

	/**
	 *
	*/
	public function _onBeforeExecute() {
		$input = parent::_onBeforeExecute();

		// Create sub-actions
		foreach ($input['emails'] as $email) {
			$this->add(new ActionCreateUser, compact('email'));
		}

		return array();
	}
}

?>