<?php

namespace common\actions\queue;

use common\actions\base\ActionsQueue;

use common\models\ExternalApiAccounts;
use common\models\UserEmails;

use common\data\constants\State;

use common\actions\ActionCreateUser;
use common\actions\ActionActivateUser;
use common\actions\ActionRefreshAccessToken;
use common\actions\ActionAddEmailToUser;
use common\actions\ActionConnectUser;
use common\actions\ActionLinkExternalApiAccountToUser;

use common\actions\util\UseResult;

class ActionLoginProcessFromApiData extends ActionsQueue {

	/**
	 *
	*/
	protected $_params = array(
		'userInfo' => array(
			'type' => 'array',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => null,
		'label' => 'Process authentification and/or linking from external api based on api userInfo',
	);

	/**
	 *
	*/
	public function _onBeforeExecute() {
		$input = parent::_onBeforeExecute();

		$userInfo = $input['userInfo'];
		$infoToken = empty($userInfo['info_token']) ? null : $userInfo['info_token'];

		$externalApiAccount = ExternalApiAccounts::build($userInfo);

		$userOfEmail = null;
		if (!empty($userInfo['email'])) {
			$userEmail = UserEmails::firstByEmail($userInfo['email']);
			$userOfEmail = empty($userEmail->id) ? null : $userEmail->related('user');
		}

		// Associate user if not
		$userLinked = $externalApiAccount->state != State::DELETED ? $externalApiAccount->related('user') : null;
		$userExec = $this->user();

		$case = null;
		$actions = array();

		if (empty($userLinked)) {
			if (empty($userExec)) {
				if (empty($userOfEmail)) {
					$case = 'link_to_new_user_not_connected';

					$actions = array(
						'create' => array('email' => $userInfo['email'], 'external_api_id_creation' => $externalApiAccount->external_api_id),
						'activate' => array('user' => new UseResult('parent.user')),
						'connect' => array('user' => new UseResult('parent.user')),
						'link' => array('user' => new UseResult('parent.user'), 'externalApiAccount' => $externalApiAccount),
						'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
					);

				} else {
					$case = 'link_to_existing_user_not_connected';

					$actions = array(
						'connect' => array('user' => $userOfEmail),
						'link' => array('user' => $userOfEmail, 'externalApiAccount' => $externalApiAccount),
						'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
					);
				}
			} else {
				if (empty($userOfEmail) || $userOfEmail->id == $userExec->id) {
					$case = 'link_to_existing_user_connected';

					$actions = array(
						'link' => array('user' => $userExec, 'externalApiAccount' => $externalApiAccount),
						'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
					);
				} else {
					throw new Exception("cannot link `$api` account to user (" . $userExec->label() . ") : " .
						" email of the account (" . $userInfo['email'] . ") is already associed to account : (" . $userOfEmail->label() . ")");
				}
			}
		} else {
			if (empty($userExec)) {
				if (empty($userOfEmail) || $userOfEmail->id == $userLinked->id) {
					$case = 'login_with_existing_linked_account';

					$actions = array(
						'connect' => array('user' => $userLinked),
						'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
					);

				} else {
					throw new Exception("connection problem with `$api` account, on `$api` " .
						"you have the email `" . $userInfo['email'] . "` setted but this email is linked to `" . $userEmail->label() . "`" .
						" account here, or your account is : `" . $userLinked->label() . "` => you must contact administrator to made an account fusion");
				}
			} else {
				if (empty($userOfEmail)) {
					if ($userLinked->id != $userExec->id) {
						throw new Exception("cannot link `$api` account to user (" . $userExec->label() . ") : " .
							" the `$api` account is already associed to account : (" . $userLinked->label() . ")");
					} else {
						$case = 'add_email_to_user';

						$actions = array(
							'add_email_to_user' => array('email' => $userInfo['email']),
							'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
						);
					}
				} else {
					if ($userLinked->id == $userExec->id && $userExec->id == $userOfEmail->id) {
						$case = 'refresh_access_token';

						$actions = array(
							'refresh_access_token' => array('externalApiAccount' => $externalApiAccount, 'infoToken' => $infoToken),
						);
					} else if ($userLinked->id != $userExec->id) {
						throw new Exception("cannot link `$api` account to user (" . $userExec->label() . ") : " .
							" the `$api` account is already associed to account : (" . $userLinked->label() . ")");
					} else {
						throw new Exception("link problem with `$api` account, on `$api` " .
							"you have the email `" . $userInfo['email'] . "` setted but this email is linked to `" . $userEmail->label() . "`" .
							" account here, or your account is : `" . $userLinked->label() . "` => you must contact administrator to made an account fusion");
					}
				}
			}
		}

		foreach ($actions as $name => $params) {
			switch ($name) {
				case 'create':
					$this->add(new ActionCreateUser, $params,
						array('result' => 'user'));
					break;

				case 'activate':
					$this->add(new ActionActivateUser, $params,
						array('result' => 'activated'));
					break;

				case 'refresh_access_token':
					$this->add(new ActionRefreshAccessToken, $params,
						array('result' => 'accessTokenRefreshed'));
					break;

				case 'add_email_to_user' :
					$this->add(new ActionAddEmailToUser, $params,
						array('result' => 'emailAdded'));
					break;

				case 'connect':
					$this->add(new ActionConnectUser, $params,
						array('result' => 'connected'));
					break;

				case 'link':
					$this->add(new ActionLinkExternalApiAccountToUser, $params,
						array('result' => 'linked'));
					break;
			}
		}

		return array();
	}
}

?>