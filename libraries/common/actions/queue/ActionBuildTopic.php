<?php

namespace common\actions\queue;

use common\actions\base\ActionsQueue;

use common\actions\ActionCreateTopic;
use common\actions\ActionAddMessageToTopic;
use common\actions\ActionCreateSimpleQuestionAgent;

use common\actions\util\UseResult;

class ActionBuildTopic extends ActionsQueue {

	/**
	 *
	*/
	protected $_params = array(
		'subject' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'Default subject',
		),
		'emails' => array(
			'type' => 'string[]',
			'required' => true,
			'default' => array()
		),
		'message' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'Default message',
		),
		'agents' => array(
			'type' => 'string[]',
			'required' => false,
			'default' => array(),
		),
		'agentParams' => array(
			'type' => 'array',
			'required' => false,
			'default' => array(),
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => null,
		'label' => 'Build a topic (create topic, create users, add users to topic, add first message to topic)',
	);

	/**
	 *
	*/
	public function _onBeforeExecute() {
		$input = parent::_onBeforeExecute();

		$subject = $input['subject'];
		$message = $input['message'];
		$emails = $input['emails'];
		$agents = $input['agents'];
		$agentParams = $input['agentParams'];

		$this->add(new ActionCreateTopic, array('subject' => $subject),
			array('result' => 'topic'));
		$this->add(new ActionCreateUsers, array('emails' => $emails),
			array('result' => 'users'));
		$this->add(new ActionAddUsersToTopic,
			array('users' => new UseResult('parent.users'), 'topic' => new UseResult('parent.topic')));

		foreach ($agents as $agent) {
			$param = empty($agentParams[$agent]) ? array() : $agentParams[$agent];
			switch ($agent) {
				case 'question':
					$this->add(new ActionCreateSimpleQuestionAgent, $param +
						array('topic' => new UseResult('parent.topic')));
					break;
			}
		}

		$this->add(new ActionAddMessageToTopic,
			array('message' => $message, 'topic' => new UseResult('parent.topic'), 'copyForSender' => true));


		return array();
	}
}

?>