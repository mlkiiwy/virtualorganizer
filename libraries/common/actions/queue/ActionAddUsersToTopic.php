<?php

namespace common\actions\queue;

use common\actions\base\ActionsQueue;

use common\actions\ActionAddUserToTopic;

class ActionAddUsersToTopic extends ActionsQueue {

	/**
	 *
	*/
	protected $_params = array(
		'users' => array(
			'type' => 'Users[]',
			'required' => true,
			'default' => array()
		),
		'topic' => array(
			'type' => 'Topics',
			'required' => true,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => null,
		'label' => 'Add multiple users to a topic',
	);

	/**
	 *
	*/
	public function _onBeforeExecute() {
		$input = parent::_onBeforeExecute();
		
		// Create sub-actions
		$topic = $input['topic'];
		foreach ($input['users'] as $user) {
			$this->add(new ActionAddUserToTopic, compact('user', 'topic'));
		}

		return array();
	}
}

?>