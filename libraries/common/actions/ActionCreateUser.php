<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Users;

class ActionCreateUser extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'email' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'test@test.com'
		),
		'external_api_id_creation' => array(
			'type' => 'int',
			'required' => false,
			'default' => null,
		),
		'password' => array(
			'type' => 'string',
			'required' => false,
			'default' => '',
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => false,
		'id' => Actions::CREATE_USER,
		'label' => 'Create a new user',
		'result' => array(
			'type' => 'IUser',
			'name' => 'user',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = Users::build($params);
		return $user;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
			'user_id' => $this->resultId(),
		);
		return $data + parent::dataForLogger();
	}
}

?>