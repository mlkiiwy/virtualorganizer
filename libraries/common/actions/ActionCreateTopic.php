<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Topics;

class ActionCreateTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'subject' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'Default subject'
		)
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::CREATE_TOPIC,
		'label' => 'Create a new topic by an user',
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$user = $this->user();

		$topic = Topics::createByUser($user, $params);

		return $topic;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$data = array(
			'topic_id' => $this->resultId(),
		);
		return $data + parent::dataForLogger();
	}
}

?>