<?php

namespace common\actions;

use common\actions\base\Action;

use common\models\Actions;
use common\models\Messages;
use common\models\MessageTypes;

class ActionAddMessageToTopic extends Action {

	/**
	 *
	*/
	protected $_params = array(
		'message' => array(
			'type' => 'string',
			'required' => true,
			'default' => 'Default message'
		),
		'topic' => array(
			'type' => 'Topics',
			'required' => true,
		),
		'copyForSender' => array(
			'type' => 'boolean',
			'default' => false,
		),
	);

	/**
	 *
	 */
	protected $_config = array(
		'user' => true,
		'id' => Actions::ADD_MESSAGE_TO_TOPIC,
		'label' => 'Add a message to a topic from a user',
		'result' => array(
			'type' => 'Messages',
			'name' => 'message',
		)
	);

	/**
	 *
	*/
	public function _execute(array $params) {
		$topic = $params['topic'];
		$copyForSender = $params['copyForSender'];

		$message = $topic->addMessage($this->user(), $params['message'],
			MessageTypes::FROM_USER ,compact('copyForSender'));

		if (!empty($message)) {
			// Create delayed action of sending
			$delayedAction = new ActionSendPendingMessageOfTopic(compact('topic'));
			$delayedAction->delayed(array('parent' => $this));
		}

		return $message;
	}

	/*
	*
	*/
	public function dataForLogger() {
		$params = $this->getParams();

		$data = array(
			'topic_id' => $params['topic']->id,
			'message_id' => $this->resultId(),
		);
		return $data + parent::dataForLogger();
	}
}

?>