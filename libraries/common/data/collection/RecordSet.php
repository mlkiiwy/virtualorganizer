<?php

namespace common\data\collection;

class RecordSet extends \lithium\data\collection\RecordSet {

	/**
	 * [resetIndex description]
	 * @return [type] [description]
	 */
	public function resetIndex() {
		$this->_index = array_keys($this->_data);
		$this->_data = array_values($this->_data);
	}

	/**
	 * [values description]
	 * @param  [type] $fieldName [description]
	 * @return [type]            [description]
	 */
	public function values($fieldName) {
		$data = array();
		foreach ($this as $key => $content) {
			if (is_array($fieldName)) {
				$line = array();
				foreach ($fieldName as $field) {
					if (isset($content->{$field})) {
						$line[$field] = $content->{$field};
					}
				}
				if (!empty($line)) {
					$data[$key] = $line;
				}
			} else if (isset($content->{$fieldName})) {
				$data[$key] = $content->{$fieldName};
			}
		}
		return $data;
	}
}

?>