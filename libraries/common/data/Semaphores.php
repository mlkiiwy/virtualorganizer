<?php

namespace common\data;

use lithium\storage\Cache as CacheStorage;

use common\util\Cache as CacheUtil;

use Exception;

/**
 *
 */
class Semaphores {

	/**
	 * [_input description]
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	protected static function _input($data) {
		$defaults = array(
			'key' => null,
			'ttl' => '+1 Hour',
			'data' => array(1),
		);

		if (is_array($data)) {
			$data += $defaults;
		} else if (is_string($data)) {
			$key = $data;
			$data = $defaults;
			$data['key'] = $key;
		}

		if (empty($data['key'])) {
			throw new Exception("key arg is missing");
		}

		// Generate cache key
		$keyInfo = array(
			'class' => 'array',
			'name' => 'Semaphore::' . $data['key'],
		);

		$cacheKey = CacheUtil::generateKey($keyInfo);
		$data['key'] = $cacheKey;

		return $data;
	}

	/**
	 * [create description]
	 * @param  [type] $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function create($data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$data = static::_input($data);

		return CacheStorage::write('default', $data['key'], $data['data'], $data['ttl']);
	}

	/**
	 * [get description]
	 * @param  [type] $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function get($data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$data = static::_input($data);

		return CacheStorage::read('default', $data['key']);
	}

	/**
	 * [delete description]
	 * @param  [type] $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function delete($data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$data = static::_input($data);

		return CacheStorage::delete('default', $data['key']);
	}

}

?>