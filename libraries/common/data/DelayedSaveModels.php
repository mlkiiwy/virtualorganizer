<?php

namespace common\data;

use common\data\source\database\adapter\MySql;

/**
 *
 */
class DelayedSaveModels extends BaseModels {

	/**
	 * Override to delay save
	 */
	public function save($entity, $data = null, array $options = array()) {
		$defaults = array('delayed_insert' => true);
		$options += $defaults;
		return parent::save($entity, $data, $options);
	}

	/**
	 * Override to delay update
	 */
	public static function update($data, $conditions = array(), array $options = array()) {
		$defaults = array('delayed_update' => false);
		$options += $defaults;
		return parent::update($data, $conditions, $options);
	}
}

?>