<?php

namespace common\data\model;

use lithium\data\Source;

/**
 *
 */
class Query extends \lithium\data\model\Query {

	/**
	 *
	 */
	public function __construct(array $config = array()) {
		$defaults = array('useIndex' => array());
		parent::__construct($config + $defaults);
	}

	/**
	 *
	 */
	public function export(Source $dataSource, array $options = array()) {
		$results = parent::export($dataSource, $options);

		if (!empty($this->_config['useIndex'])) {
			if (is_array($this->_config['useIndex'])) {
				$indexes = implode(',', $this->_config['useIndex']);
			} elseif (is_string($this->_config['useIndex'])) {
				$indexes = trim($this->_config['useIndex']);
			}

			$results['alias'] .= ' USE INDEX (' . $indexes . ')';
		}
		return $results;
	}
}

?>