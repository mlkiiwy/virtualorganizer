<?php

namespace common\data\entity;

use common\interfaces\IJsonnable;

class Record extends \lithium\data\entity\Record implements IJsonnable {

	/**
	 * Override to automatically unpack json on entity creation.
	 */
	public function __construct(array $config = array()) {
		parent::__construct($config);

		$model = $this->model();
		if (is_subclass_of($model, 'common\interfaces\IWithJsonModel')) {
			$this->extractJsonFields();
		}
	}

	/**
	 *
	 */
	public function toJson(array $options = array()) {
		$defaults = array('return' => 'json');
		$options += $defaults;

		$data = array(
			'_IJsonnable' => true,
			'class' => $this->model(),
			'data' => $this->data(),
		);
		return $options['return'] == 'json' ? json_encode($data) : $data;
	}
}

?>