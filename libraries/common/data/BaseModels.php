<?php

namespace common\data;

use DateTime;
use Exception;

use common\interfaces\IJsonModel;
use common\interfaces\IEnhancedModel;
use common\interfaces\IBuildFromArray;

use common\interfaces\IResourceById;
use common\interfaces\IResourceByField;

use common\interfaces\IWithJsonModel;

use common\util\Sql;
use common\util\Json;
use common\util\Set;
use common\util\Klass as ClassUtil;

/**
 *
 */
class BaseModels extends \lithium\data\Model implements IJsonModel, IEnhancedModel, IResourceById, IResourceByField, IWithJsonModel, IBuildFromArray {

	/**
	 * Override on a per-model basis if needed.
	 */
	protected static $_jsonFields = array();

	/**
	 *
	 */
	public function save($entity, $data = null, array $options = array()) {
		$defaults = array(
			'refreshDates' => true,
			'keepDateLastUpdate' => false,
			'generateJson' => true,
		);
		$options += $defaults;

		if ($options['refreshDates']) {
			$entity->refreshDates($options);
		}

		if (!empty($options['generateJson'])) {
			$entity->generateJsonFields(is_array($options['generateJson']) ? $options['generateJson'] : array());
		}

		return parent::save($entity, $data, $options);
	}


	/**
	 * @interface IWithJsonModel
	 */
	public function jsonFields($entity, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$schema = self::schema();
		$fields = array();
		$schemaFields = $schema->fields();
		foreach ($schemaFields as $key => $value) {
			if (strpos($key, 'json') !== false) {
				$fields[$key] = array();
			}
		}

		$fields = array_merge($fields, static::$_jsonFields);
		$fields = array_unique($fields, SORT_REGULAR);

		return $fields;
	}

	/**
	 * @interface IWithJsonModel
	 */
	public function generateJsonFields($entity, array $options = array()) {
		$defaults = array('clear' => true, 'merge' => true);
		$options += $defaults;

		$jsonFields = $entity->jsonFields();
		$jsonData = array();

		foreach ($jsonFields as $key => $value) {
			if (is_array($value)) {
				if (empty($options['clear'])) {
					$jsonData[$key] = $entity->extractJsonFields($key, array('merge' => false));
				}
				foreach ($value as $entityField) {
					if (isset($entity->{$entityField})) {
						$jsonData[$key][$entityField] = $entity->{$entityField};
					}
				}
			}
		}

		if ($options['merge']) {
			foreach ($jsonData as $key => $value) {
				$entity->{$key} = json_encode($value);
			}
		}

		return $jsonData;
	}

	/**
	 * @interface IWithJsonModel
	 */
	public function extractJsonFields($entity, $jsonField = null, array $options = array()) {
		$defaults = array('merge' => true, 'erase' => true);
		$options += $defaults;

		$jsonFields = $entity->jsonFields();
		$data = array();

		if (!empty($jsonField) && !empty($jsonFields[$jsonField])) {
			$jsonFields = array($jsonField => $jsonFields[$jsonField]);
		} else if (!empty($jsonField)) {
			throw new Exception("no json field name `$jsonField` on this entity");
		}

		foreach ($jsonFields as $key => $value) {
			$json = empty($entity->{$key}) ? '' : $entity->{$key};
			$data[$key] = json_decode($json, true);
		}

		if (!empty($options['merge'])) {
			foreach ($data as $content) {
				if (is_array($content)) {
					foreach ($content as $field => $value) {
						if (!isset($entity->{$field}) || !empty($options['erase'])) {
							$entity->{$field} = $value;
						}
					}
				}
			}
		}

		return empty($jsonField) ? $data : $data[$jsonField];
	}

	/**
	 * Automatically update `'date_creation'`, `'date_last_update'`
	 */
	public function refreshDates($entity, array $options = array()) {
		$defaults = array('keepDateLastUpdate' => false);
		$options += $defaults;

		$fields = array_keys($entity->schema()->fields());
		$dateObject = new DateTime();
		$date = $dateObject->format('Y-m-d H:i:s');

		if (!$entity->exists() && in_array('date_creation', $fields)) {
			if (empty($entity->date_creation) || $entity->date_creation == 'CURRENT_TIMESTAMP') {
				$entity->date_creation = $date;
				if (!empty($options['whitelist']) && !in_array('date_creation', $options['whitelist'])) {
					$options['whitelist'][] = 'date_creation';
				}
			}
		}

		if (in_array('date_last_update', $fields)) {
			if ($options['keepDateLastUpdate'] && !empty($entity->date_last_update) && $entity->date_last_update != 'CURRENT_TIMESTAMP') {
				//
			} else {
				$entity->date_last_update = $date;
			}

			if (!empty($options['whitelist']) && !in_array('date_last_update', $options['whitelist'])) {
				$options['whitelist'][] = 'date_last_update';
			}

			if (!empty($user) && in_array('user_id_last_update', $fields)) {
				$entity->user_id_last_update = $user['id'];

				if (!empty($options['whitelist']) && !in_array('user_id_last_update', $options['whitelist'])) {
					$options['whitelist'][] = 'user_id_last_update';
				}
			}
		}
	}

	/**
	 *
	 */
	public static function createRecordSet(array $data = array(), array $options = array()) {
		$defaults = array('class' => 'set');
		$options += $defaults;

		return static::connection()->item(get_called_class(), $data, $options);
	}

	/**
	 *
	 */
	public static function createRecords(array $data, array $options = array()) {
		$defaults = array('exists' => true);
		$options += $defaults;
		$recordset = self::createRecordSet();
		foreach($data as $e) {
			$recordset[] = static::create($e, $options);
		}
		return $recordset;
	}

	/**
	 *
	 */
	public function fieldChanges($entity, $options = array()) {
		$defaults = array();
		$options += $defaults;

		$export = $entity->export();
		$changes = array();

		$start = $export['data'];
		$end = $export['update'];
		$schema = $entity->schema();

		foreach( $end as $field => $value) {

			if( isset($start[$field])) {
				$beforeValue = $start[$field];
			} else {
				$beforeValue = isset($schema[$field]['default']) ? $schema[$field]['default'] : null;
			}

			if( !is_array($value) &&
				 !is_object($value) &&
				 !empty($schema[$field]) &&
				 $beforeValue != $value
				) {
				$changes[$field] = compact('value');
				if( $export['exists']) {
					$changes[$field]['before'] = $beforeValue;
				}
			}
		}

		return $changes;
	}

	/**
	 *
	 */
	public static function truncate() {
		$table = static::meta('source');
		return Sql::truncate($table);
	}

	/**
	 *
	 */
	public function checkSaved($entity, array $options = array()) {
		$defaults = array('error' => 'exception');
		$options += $defaults;

		if (empty($entity->id)) {
			switch ($options['error']) {
				case 'exception':
					throw new Exception("required to be saved before use");
					break;
			}

			return false;
		}

		return true;
	}

	/**
	 *
	 */
	protected function _relationConfig($fieldNameOrType) {
		$relations = static::relations();
		$config = Set::arrayFirstValue(array_filter($relations, function($i) use ($fieldNameOrType) {
			return $i->data('fieldName') === $fieldNameOrType || $i->data('to') === $fieldNameOrType;
		}));

		if (empty($config)) {
			throw new Exception("cannot get relation config for `$fieldNameOrType`");
		}

		return $config;
	}

	/**
	 * @interface IEnhancedModel
	 */
	public function populateRelatedId($entity, $fieldNameOrType, array $options = array()) {
		$config = static::_relationConfig($fieldNameOrType);

		if ($config->data('type') == 'hasMany') {
			return false;
		}

		$keys = $config->data('key');
		$relatedField = Set::getFirstKey($keys);
		$targetField = Set::arrayFirstValue($keys);
		$fieldName = $config->data('fieldName');

		if ($relatedField != 'id') {
			$targetEntity = empty($entity->{$fieldName}) ? null : $entity->{$fieldName};
			$value = empty($targetEntity->{$targetField}) ? null : $targetEntity->{$targetField};
			$entity->{$relatedField} = $value;
		} else {
			return false;
		}

		return $value;
	}

	/**
	 * @interface IEnhancedModel
	 */
	public function related($entity, $fieldNameOrType, $value = false, array $options = array()) {
		$defaults = array('load' => true);
		$options += $defaults;

		$config = static::_relationConfig($fieldNameOrType);
		$fieldName = $config->data('fieldName');

		if ($value !== false) {
			// Write
			$entity->{$fieldName} = $value;
			$entity->populateRelatedId($fieldNameOrType);
		} else {
			// Read
			$value = isset($entity->{$fieldName}) ? $entity->{$fieldName} : 'not_loaded';

			if ($value === 'not_loaded' && $options['load']) {
				$value = $entity->loadRelated($fieldNameOrType);
			}
		}

		return $value;
	}

	/**
	 * @interface IEnhancedModel
	 */
	public function loadRelated($entity, $fieldNameOrType, array $options = array()) {
		$defaults = array('null' => true);
		$options += $defaults;

		$config = static::_relationConfig($fieldNameOrType);
		$fieldName = $config->data('fieldName');
		$model = $config->data('to');
		$type = $config->data('type');
		$keys = $config->data('key');
		$relatedField = Set::getFirstKey($keys);
		$targetField = Set::arrayFirstValue($keys);

		switch ($type) {
			case 'belongsTo':
			case 'hasOne':
					$value = null;
					if ($targetField == 'id') {
						$targetId = empty($entity->{$relatedField}) ? null : $entity->{$relatedField};

						if (empty($targetId)) {
							if (empty($options['null'])) {
								throw new Exception("cannot load : `$fieldNameOrType` because field `$relatedField` is not set");
							}
						} else {
							if (ClassUtil::hasInterface($model, 'common\interfaces\IResourceById')) {
								$value = $model::getById($targetId);
							}
						}
					} else if ($relatedField == 'id') {
						$entity->checkSaved();

						$entityId = $entity->id;

						if (ClassUtil::hasInterface($model, 'common\interfaces\IResourceByField')) {
							$value = $model::getByField($targetField, $entityId);
						}
					}

					if (empty($value) && !$options['null']) {
						throw new Exception("cannot load `$fieldNameOrType`");
					}

					$value = $entity->related($fieldNameOrType, $value);
				break;

			case 'hasMany':
				throw new Exception('');
				break;
		}

		return $value;
	}

	/**
	 * @interface IResourceById
	 */
	public static function getById($id, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('id' => $id);
		return self::first(compact('conditions'));
	}

	/**
	 * @interface IResourceById
	 */
	public static function getByIds($ids, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array('id' => $ids);
		return self::all(compact('conditions'));
	}

	/**
	 * @interface IResourceByField
	 */
	public static function getByField($field, $value, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$conditions = array($field => $value);
		return self::first(compact('conditions'));
	}

	/**
	 * [buildFromArray description]
	 * @param  array  $data    [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function buildFromArray(array $data, array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$relations = static::relations();
		$relatedEntities = array();
		foreach ($relations as $relation) {
			$fieldName = $relation->data('fieldName');
			$model = $relation->data('to');

			if (isset($data[$fieldName])) {
				$subData = $data[$fieldName];
				unset($data[$fieldName]);

				if (is_array($subData)) {
					$relatedEntities[$fieldName] = $model::buildFromArray($subData,$options);
				} else if (empty($subData)) {
					$relatedEntities[$fieldName] = null;
				} else {
					throw new Exception("key `$fieldName` must be null or instance of `$model`
						`$subData` given instead");
				}
			}
		}

		$entity = static::create($data, array('exists' => !empty($data['id'])));

		foreach ($relatedEntities as $key => $value) {
			$entity->related($key, $value);
		}

		return $entity;
	}
}

?>
