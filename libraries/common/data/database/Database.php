<?php

namespace common\data\database;

use lithium\core\Environment;

use common\models\ActionLogs;
use common\models\Actions;
use common\models\Agents;
use common\models\AgentTopics;
use common\models\AgentAnswers;
use common\models\Emails;
use common\models\Messages;
use common\models\MessageTypes;
use common\models\MessageStatusUsers;
use common\models\Topics;
use common\models\TopicUsers;
use common\models\TopicModes;
use common\models\Users;
use common\models\IncomeEmails;
use common\models\IncomeEmailAnalyseds;
use common\models\UserEmails;
use common\models\UserContacts;
use common\models\Groups;
use common\models\GroupTypes;
use common\models\GroupUsers;
use common\models\Applications;
use common\models\UserTokens;
use common\models\ExternalApis;
use common\models\ExternalApiAccounts;
use common\models\ExternalApiAccountLinks;
use common\models\LinkTypes;
use common\models\OauthTypes;
use common\models\ExternalApiTokens;
use common\models\ExternalApiRights;
use common\models\ExternalApiRightByApis;
use common\models\ExternalApiRightAccountValues;
use common\models\Genders;
use common\models\PhoneTypes;
use common\models\UserPhoneNumbers;
use common\models\EmailTypes;
use common\models\AgentReaskReasons;

use common\util\Sql as SqlUtil;

use Exception;

/**
 *
 */
class Database {

	/**
	 * [reset description]
	 * @return [type] [description]
	 */
	public static function reset() {
		if (SqlUtil::executeFile(Environment::get('app.config_dir') . '/base.sql')) {

			ActionLogs::truncate();
			Topics::truncate();
			Users::truncate();
			Messages::truncate();
			TopicUsers::truncate();
			MessageStatusUsers::truncate();
			Emails::truncate();
			IncomeEmails::truncate();
			IncomeEmailAnalyseds::truncate();
			AgentTopics::truncate();
			AgentAnswers::truncate();
			UserEmails::truncate();
			UserPhoneNumbers::truncate();
			UserContacts::truncate();
			Groups::truncate();
			GroupUsers::truncate();
			UserTokens::truncate();
			ExternalApiAccounts::truncate();
			ExternalApiAccountLinks::truncate();
			ExternalApiTokens::truncate();
			ExternalApiRightAccountValues::truncate();

			ExternalApiRightByApis::truncate();

			ExternalApiRights::generate(array('reset' => true));
			OauthTypes::generate(array('reset' => true));
			LinkTypes::generate(array('reset' => true));
			ExternalApis::generate(array('reset' => true));
			Applications::generate(array('reset' => true));
			GroupTypes::generate(array('reset' => true));
			Actions::generate(array('reset' => true));
			TopicModes::generate(array('reset' => true));
			MessageTypes::generate(array('reset' => true));
			Agents::generate(array('reset' => true));
			Genders::generate(array('reset' => true));
			PhoneTypes::generate(array('reset' => true));
			EmailTypes::generate(array('reset' => true));
			AgentReaskReasons::generate(array('reset' => true));

			Users::testUser();
			return true;
		} else {
			return false;
		}
	}

}

?>