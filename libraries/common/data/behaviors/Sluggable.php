<?php

namespace common\data\behaviors;

/**
 *
 */
interface Sluggable {

	/**
	 *
	 */
	public function slug($entity);

	/**
	 *
	 */
	public function guessSlug($entity);
}

?>