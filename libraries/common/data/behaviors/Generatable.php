<?php

namespace common\data\behaviors;

/**
 *
 */
interface Generatable {

	/**
	 *
	 */
	public function generate($entity, array $options = array());

	/**
	 *
	 */
	public function propagateGeneration($entity, array $options = array());
}

?>