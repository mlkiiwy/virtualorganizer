<?php

namespace common\data\behaviors;

/**
 *
 */
interface Memcacheable {

	/**
	 *
	 */
	public static function cachePrefix();

	/**
	 *
	 */
	public static function cacheTTL();

	/**
	 *
	 */
	public static function cache($ids, array $options = array());

	/**
	 *
	 */
	public static function cacheEntities($ids);

	/**
	 *
	 */
	public static function cacheFull($ids);

	/**
	 *
	 */
	public static function invalidateCache($ids);
}

?>