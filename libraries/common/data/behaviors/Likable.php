<?php

namespace common\data\behaviors;

/**
 *
 */
interface Likable {

	/**
	 *
	 */
	public static function refreshLikes($listId, $rating, $previousRating);

}

?>