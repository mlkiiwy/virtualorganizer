<?php

namespace common\data\behaviors;

/**
 *
 */
interface Loggable {

	/**
	 *
	 */
	public function log($entity, $level, $message, array $options = array());
}

?>