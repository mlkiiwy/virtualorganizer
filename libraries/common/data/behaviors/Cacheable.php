<?php

namespace common\data\behaviors;

/**
 *
 */
interface Cacheable {

	/**
	 *
	 */
	public function regenerateCache($entity, array $options = array());

	/**
	 *
	 */
	public function propagateCache($entity, $cache, array $options = array());
}

?>