<?php

namespace common\data\constants;

/**
 *
 */
class CreationState extends Enum {

	const BASIC = 0;
	const FROM_API_IMPORT = 1;

}

?>
