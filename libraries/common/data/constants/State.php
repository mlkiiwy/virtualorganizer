<?php

namespace common\data\constants;

/**
 *
 */
class State extends Enum {

	const DRAFT = 0;
	const ACTIVE = 1;
	const DELETED = -1;

}

?>
