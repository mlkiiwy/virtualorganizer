<?php

namespace common\data;

use common\interfaces\IConstantModel;

/**
 *
 */
class ConstantModels extends BaseModels implements IConstantModel {

	/**
	 *
	 */
	protected static $_constants = array();

	/**
	 *
	 */
	public static function generate(array $options = array()) {
		$defaults = array('reset' => false);
		$options += $defaults;

		if ($options['reset']) {
			static::truncate();
		}

		$constants = static::$_constants;
		foreach ($constants as $id => $data) {

			if (is_string($data)) {
				$data = array('label' => $data);
			}

			$data['id'] = $id;
			$model = static::create($data);
			$model->save();
		}
	}

}

?>
