<?php

namespace common\data\source\database\adapter;

use Exception;
use RuntimeException;

use PDO;
use PDOException;

use InvalidArgumentException;
use lithium\core\ConfigException;
use lithium\core\NetworkException;
use lithium\data\model\QueryException;

use lithium\core\Environment;

use common\util\Set;
use common\util\Sql;
use common\util\String;

/**
 *
 */
class MySql extends \lithium\data\source\database\adapter\MySql {

	/**
	 *
	 */
	const INSERT_QUEUE_FILENAME = 'delayed_insert.csv';
	const UPDATE_QUEUE_FILENAME = 'delayed_update.csv';

	/**
	 *
	 */
	const SEPARATOR = ';';

	/**
	 *
	 */
	const INSERT_TEMPLATE = 'INSERT IGNORE INTO {:source} ({:fields}) VALUES {:values}';
	const UPDATE_TEMPLATE = 'UPDATE {:source} SET {:fields} {:conditions}';

	/**
	 * [connect description]
	 * @return [type] [description]
	 */
	public function connect() {
		if (!$this->_config['dsn']) {
			if (!empty($this->_config['socket'])) {
				$socket = $this->_config['socket'];
				$dsn = "mysql:unix_socket=%s;dbname=%s";
				$this->_config['dsn'] = sprintf($dsn, $socket, $this->_config['database']);
			} else {
				$host = $this->_config['host'];
				list($host, $port) = explode(':', $host) + array(1 => "3306");
				$dsn = "mysql:host=%s;port=%s;dbname=%s";
				$this->_config['dsn'] = sprintf($dsn, $host, $port, $this->_config['database']);
			}
		}

		$this->_isConnected = false;
		$config = $this->_config;

		if (!$config['database']) {
			throw new ConfigException('No Database configured');
		}
		if (!$config['dsn']) {
			throw new ConfigException('No DSN setup for DB Connection');
		}
		$dsn = $config['dsn'];

		$options = $config['options'] + array(
			PDO::ATTR_PERSISTENT => $config['persistent'],
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
		);

		try {
			$this->connection = new PDO($dsn, $config['login'], $config['password'], $options);
		} catch (PDOException $e) {
			preg_match('/SQLSTATE\[(.+?)\]/', $e->getMessage(), $code);
			$code = $code[1] ?: 0;
			switch (true) {
			case $code === 'HY000' || substr($code, 0, 2) === '08':
				if (!empty($config['socket'])) {
					$msg = "Unable to connect to socket `{$config['socket']}`." . $e->getMessage();
				} else {
					$msg = "Unable to connect to host `{$config['host']}`.";
				}
				throw new NetworkException($msg, null, $e);
			case in_array($code, array('28000', '42000')):
				$msg = "Host connected, but could not access database `{$config['database']}`.";
				throw new ConfigException($msg, null, $e);
			}
			throw new ConfigException("An unknown configuration error has occured.", null, $e);
		}
		$this->_isConnected = true;

		if ($this->_config['encoding']) {
			$this->encoding($this->_config['encoding']);
		}
		if ($this->_isConnected) {
			$info = $this->connection->getAttribute(PDO::ATTR_SERVER_VERSION);
			$this->_useAlias = (boolean) version_compare($info, "4.1", ">=");
		}

		return $this->_isConnected;
	}

	/**
	 * Overriden to delay insert if asked
	 */
	public function create($query, array $options = array()) {
		try {
			if (!empty($options['delayed_insert'])) {
				$queryData = $query->export($this);
				$queryData = Set::getValues($queryData, array('fields', 'source', 'values'));

				if ($options['delayed_insert'] === true) {
					return static::addToInsertQueue($queryData);
				} else {
					return static::addToInsertQueue($queryData, $options['delayed_insert']);
				}
			}
		} catch (DelayedInsertException $e) {
			return parent::create($query, $options);
		}

		return parent::create($query, $options);
	}

	/**
	 * Overriden to delay update if asked
	 */
	public function update($query, array $options = array()) {
		try {
			if (!empty($options['delayed_update'])) {
				$queryData = $query->export($this);
				$queryData = Set::getValues($queryData, array('fields', 'source', 'conditions'));

				if ($options['delayed_update'] === true) {
					return static::addToUpdateQueue($queryData);
				} else {
					return static::addToUpdateQueue($queryData, $options['delayed_update']);
				}
			}
		} catch (DelayedInsertException $e) {
			return parent::update($query, $options);
		}

		return parent::update($query, $options);
	}

	/**
	 *
	 */
	public static function addToInsertQueue($data, $filename = self::INSERT_QUEUE_FILENAME) {
		try {
			return self::_addToQueue($data, $filename);
		} catch (Exception $e) {
			return new DelayedInsertException($e->getMessage());
		}
	}

	/**
	 *
	 */
	public static function addToUpdateQueue($data, $filename = self::UPDATE_QUEUE_FILENAME) {
		try {
			return self::_addToQueue($data, $filename);
		} catch (Exception $e) {
			return new DelayedUpdateException($e->getMessage());
		}
	}

	/**
	 *
	 */
	protected static function _addToQueue($data, $filename) {
		$path = Environment::get('delayed_insert.path');

		$fh = @fopen($path . $filename, 'a+');

		if ($fh === false) {
			throw new Exception('Cannot open/create queue file.');
		}

		$encodedData = json_encode($data);

		if (fputcsv($fh, array($encodedData), self::SEPARATOR) === false) {
			throw new Exception('Cannot write query into queue.');
		}

		@fclose($fh);

		return true;
	}

	/**
	 *
	 */
	protected static function _retreiveTreatmentFile($queueFile) {
		$path = Environment::get('delayed_insert.path');
		$pathinfo = pathinfo($queueFile);
		$treatmentFile = $path . $pathinfo['filename'] . '_treatment.' . $pathinfo['extension'];

		if (!is_file($treatmentFile)) {
			if (!is_file($queueFile)) {
				throw new RuntimeException("Queue file {$queueFile} does not exist.");
			}

			if (rename($queueFile, $treatmentFile) === false) {
				throw new Exception("Cannot rename queue file {$queueFile}.");
			}
		}

		return $treatmentFile;
	}

	/**
	 * Remove file
	 */
	protected static function _removeTreatmentFile($treatmentFile) {
		@unlink($treatmentFile);
	}

	/**
	 * @todo remove lines from treatment file when they are processed, use current connections...
	 */
	public static function processDelayedInsertQueue($filename = self::INSERT_QUEUE_FILENAME, $callback = null) {
		$total = 0;
		$path = Environment::get('delayed_insert.path');
		$queueFile = $path . $filename;

		try {
			$treatmentFile = self::_retreiveTreatmentFile($queueFile);
		} catch (RuntimeException $e) {
			return $total;
		}

		$fh = @fopen($treatmentFile, 'r');

		if ($fh === false) {
			throw new Exception("Cannot open treatment file {$treatmentFile}.");
		}

		$hashPerTable = array();

		while (($line = fgetcsv($fh, 0, self::SEPARATOR)) !== false) {
			$data = json_decode($line[0], true);

			$key = md5(serialize($data['source']) . serialize($data['fields']));

			if (!empty($callback) && is_callable($callback)) {
				$data = call_user_func($callback, $data);
			}

			if (!isset($hashPerTable[$key])) {
				$hashPerTable[$key] = $data + array('lines' => array());
			}

			if (!is_array($data['values'])) {
				$data['values'] = array($data['values']);
			}

			foreach ($data['values'] as $values) {
				$hashPerTable[$key]['lines'][] = '(' . $values . ')';
			}
		}

		@fclose($fh);

		foreach ($hashPerTable as $data) {
			$data['values'] = implode(',', $data['lines']);

			$query = trim(String::insert(self::INSERT_TEMPLATE, $data, array('clean' => true)));

			$total += Sql::execute($query, array(), 'row_count');
		}

		self::_removeTreatmentFile($treatmentFile);

		return $total;
	}

	/**
	 *
	 */
	public static function processDelayedUpdateQueue($filename = self::UPDATE_QUEUE_FILENAME, $callback = null) {
		$total = 0;
		$path = Environment::get('delayed_insert.path');
		$queueFile = $path . $filename;

		try {
			$treatmentFile = self::_retreiveTreatmentFile($queueFile);
		} catch (RuntimeException $e) {
			return $total;
		}

		$fh = @fopen($treatmentFile, 'r');

		if ($fh === false) {
			throw new Exception("Cannot open treatment file {$treatmentFile}.");
		}

		while (($line = fgetcsv($fh, 0, self::SEPARATOR)) !== false) {
			$data = json_decode($line[0], true);

			$query = trim(String::insert(self::UPDATE_TEMPLATE, $data, array('clean' => true)));

			$total += Sql::execute($query, array(), 'row_count');
		}

		@fclose($fh);

		self::_removeTreatmentFile($treatmentFile);

		return $total;
	}
}

?>