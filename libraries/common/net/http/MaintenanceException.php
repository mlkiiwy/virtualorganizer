<?php

namespace common\net\http;

/**
 * Simple 404 exception.
 */
class MaintenanceException extends \RuntimeException {
	protected $code = 503;
}
?>