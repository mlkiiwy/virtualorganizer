<?php

namespace common\net\http;

use common\analysis\Debugger;

/**
 *
 */
class Router extends \lithium\net\http\Router {

	/**
	 * Override to log time spent processing the incoming request.
	 */
	public static function process($request) {
		$id = Debugger::log('Router::process', 'LITHIUM');
		$result = parent::process($request);
		Debugger::end($id);
		return $result;
	}
}

?>