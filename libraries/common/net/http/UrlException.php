<?php

namespace common\net\http;

/**
 * Simple 404 exception.
 */
class UrlException extends \RuntimeException {
	protected $code = 404;
}
?>