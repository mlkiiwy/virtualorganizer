<?php

namespace common\net\http;

/**
 * Simple 401 exception.
 */
class PrivateUrlException extends \RuntimeException {
	protected $code = 401;
}

?>