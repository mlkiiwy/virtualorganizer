<?php

namespace common\net\http;

/**
 * Simple 404 exception.
 */
class GoneUrlException extends \RuntimeException {
	protected $code = 410;
}

?>