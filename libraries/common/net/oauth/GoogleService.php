<?php

namespace common\net\oauth;

use common\models\ExternalApis;
use common\models\Genders;
use common\models\PhoneTypes;

use common\util\PhoneNumber as PhoneNumberUtil;

use common\extensions\storage\Visitor;

set_include_path(get_include_path() . PATH_SEPARATOR . LITHIUM_LIBRARY_PATH . '/google-api-php-client/src/');
require 'Google/Client.php';
require 'Google/Service/Oauth2.php';

use Google_Client;
use Google_Service_Oauth2;
use Google_Http_Request;

class GoogleService extends OAuthService {

	public function __construct() {
		$config = static::getConfig($this->slug());

		$this->_service = new Google_Client();
		$this->_service->setClientId($config['appId']);
		$this->_service->setClientSecret($config['secret']);
		$this->_service->setState('offline'); 	// usefull ???

		foreach ($config['baseScope'] as $scope) {
			$this->_service->addScope($scope);
		}

		$this->accessToken(-1, array('init_by_visitor' => true));
	}

	/**
	 * [accessToken description]
	 * @param  [type] $accessToken [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function accessToken($accessToken = -1, array $options = array()) {
		$defaults = array('init_by_visitor' => false);
		$options += $defaults;

		if ($accessToken === -1) {
			// Read
			$accessToken = $this->_service->getAccessToken();

			if (empty($accessToken) && $options['init_by_visitor']) {
				$accessToken = Visitor::get('oauth.' . $this->slug() . '.access_token');

				if (!empty($accessToken)) {
					$this->_service->setAccessToken($accessToken);
				}
			}
		} else {
			$this->_service->setAccessToken($accessToken);
			Visitor::set('oauth.' . $this->slug() . '.access_token', $accessToken);
		}

		return $accessToken;
	}

	/**
	 * [slug description]
	 * @return [type] [description]
	 */
	public function slug() {
		return ExternalApis::GOOGLE_SLUG;
	}

	/**
	 * [isAuthentificated description]
	 * @return boolean [description]
	 */
	public function isAuthentificated() {
		return $this->_service->getAccessToken() && !$this->_service->isAccessTokenExpired();
	}

	/**
	 * [getUrlForAuthentificate description]
	 * @param  [type] $callbackUrl [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function getUrlForAuthentificate($callbackUrl, array $options = array()) {
		$config = static::getConfig($this->slug());

		$this->_service->setRedirectUri($callbackUrl);

		return $this->_service->createAuthUrl();
	}

	/**
	 * [getUserInfo description]
	 * @return [type] [description]
	 */
	public function getUserInfo() {
		$infos = new Google_Service_Oauth2($this->_service);

		$userInfos = $infos->userinfo->get();

		$gender = Genders::UNKNOW;
		switch ($userInfos->gender) {
			case 'male' :
				$gender = Genders::MALE;
				break;

			case 'female' :
				$gender = Genders::FEMALE;
				break;
		}

		// Format infos
		$data = array(
			'external_api_id' => ExternalApis::FACEBOOK,
			'uid' => $userInfos->id,
			'username' => $userInfos->name,
			'first_name' => $userInfos->givenName,
			'last_name' => $userInfos->familyName,
			'avatar' => $userInfos->picture,
			'email' => $userInfos->email,
			'other' => array(
				'gender' => $gender,
				'locale' => $userInfos->locale,
			),
		);

		// $accessToken = json_decode($this->accessToken(), true);

		$data['info_token'] = array(
			'access_token' => $this->accessToken(),
			'date_expiration' => date('Y-m-d H:i:s', $this->getAccessTokenExpiration()),
		);

		return $data;
	}

	/**
	 * [onAuthorizeCallback description]
	 * @param  [type] $request [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function onAuthorizeCallback($request, $callbackUrl, array $options = array()) {
		$this->_service->setRedirectUri($callbackUrl);

		if (!empty($request->query['code'])) {
			$this->_service->authenticate($request->query['code']);
			$accessToken = $this->_service->getAccessToken();

			if (!empty($accessToken)) {
				$this->accessToken($accessToken);
			}
		}
	}

	/**
	 * [getAccessTokenExpiration description]
	 * @return [type] [description]
	 */
	public function getAccessTokenExpiration() {
		$accessToken = json_decode($this->accessToken(), true);
		return $accessToken['created'] + $accessToken['expires_in'];
	}

	/**
	 * [getEmailContacts description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function getEmailContacts(array $options = array()) {
		$defaults = array();
		$options += $defaults;

		$accessToken = json_decode($this->accessToken(), true);
		$request = new Google_Http_Request("https://www.google.com/m8/feeds/contacts/default/full?max-results=9999&alt=json&v=3.0&oauth_token=" . $accessToken['access_token']);
		$result = $this->_service->execute($request);

		$contacts = array();
		if (!empty($result) && !empty($result['feed']['entry'])) {
			foreach ($result['feed']['entry'] as $entry) {

				$emails = array();
				if (!empty($entry['gd$email'])) {
					foreach ($entry['gd$email'] as $email) {
						$emails[] = array(
							'email' => $email['address'],
							'main' => !empty($email['primary']),
						);
					}
				}

				$phones = array();
				if (!empty($entry['gd$phoneNumber'])) {
					foreach ($entry['gd$phoneNumber'] as $phone) {
						$type = !empty($phone['rel']) ? $phone['rel'] : 'unknow';
						$type = $type != 'unknow' ? substr($type, strpos($type, '#') + 1) : $type;
						$number = PhoneNumberUtil::clean($phone['$t']);
						$phoneTypeId = PhoneNumberUtil::getPhoneTypeId($number);

						if ($phoneTypeId == PhoneTypes::UNKNOW && !empty($type)) {
							switch ($type) {
								case 'mobile':
									$phoneTypeId = PhoneTypes::MOBILE;
									break;
							}
						}
						$phones[] = array(
							'number' => $number,
							'type' => $type,
							'phone_type_id' => $phoneTypeId,
						);
					}
				}

				$groups = array();
				if (!empty($entry['gContact$groupMembershipInfo'])) {
					foreach ($entry['gContact$groupMembershipInfo'] as $group) {
						$name = $group['href'];
						$name = substr($name, strpos($name, 'base/') + 5);
						$groups[] = $name;
					}
				}

				$contact = array(
					'external_api_id' => ExternalApis::GOOGLE,
					'label' => !empty($entry['title']['$t']) ? $entry['title']['$t'] : '',
					'first_name' => !empty($entry['gd$name']['gd$familyName']['$t']) ? $entry['gd$name']['gd$familyName']['$t'] : '',
					'last_name' => !empty($entry['gd$name']['gd$givenName']['$t']) ? $entry['gd$name']['gd$givenName']['$t'] : '',
					'emails' => $emails,
					'phone_numbers' => $phones,
					'groups' => $groups,
				);

				$contacts[] = $contact;
			}
		}

		return $contacts;
	}
}
?>