<?php

namespace common\net\oauth;

use common\models\ExternalApis;
use common\models\Genders;

use common\extensions\storage\Visitor;

use common\util\String;

require 'libraries/facebook-php-sdk-http-stream/src/facebook.php';
use Facebook;

class FacebookService extends OAuthService {

	const AVATAR_URL_TEMPLATE = 'http://graph.facebook.com/{:uid}/picture';

	public function __construct() {
		$config = static::getConfig(ExternalApis::FACEBOOK_SLUG);

		$this->_service = new Facebook(array(
			'appId'  => $config['appId'],
			'secret' => $config['secret'],
			'allowSignedRequest' => false,
		));

		$this->accessToken(-1, array('init_by_visitor' => true));
	}

	/**
	 * [accessToken description]
	 * @param  [type] $accessToken [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function accessToken($accessToken = -1, array $options = array()) {
		$defaults = array('init_by_visitor' => false);
		$options += $defaults;

		if ($accessToken === -1) {
			// Read
			$accessToken = $this->_service->getAccessToken();

			if (empty($accessToken) && $options['init_by_visitor']) {
				$accessToken = Visitor::get('oauth.' . $this->slug() . '.access_token');

				if (!empty($accessToken)) {
					$this->_service->setAccessToken($accessToken);
				}
			}
		} else {
			$this->_service->setAccessToken($accessToken);
			Visitor::set('oauth.' . $this->slug() . '.access_token', $accessToken);
		}

		return $accessToken;
	}

	/**
	 * [slug description]
	 * @return [type] [description]
	 */
	public function slug() {
		return ExternalApis::FACEBOOK_SLUG;
	}

	/**
	 * [isAuthentificated description]
	 * @return boolean [description]
	 */
	public function isAuthentificated() {
		return $this->_service->getUser() !== 0;
	}

	/**
	 * [getUrlForAuthentificate description]
	 * @param  [type] $callbackUrl [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function getUrlForAuthentificate($callbackUrl, array $options = array()) {
		$config = static::getConfig(ExternalApis::FACEBOOK_SLUG);

		$scope = implode(', ', $config['baseScope']);

		return $this->_service->getLoginUrl(array(
			'scope' => $scope,
			'redirect_uri' => $callbackUrl,
		));
	}

	/**
	 * [getUserInfo description]
	 * @return [type] [description]
	 */
	public function getUserInfo() {
		$userInfos = $this->_service->api('/me');

		$gender = Genders::UNKNOW;
		switch ($userInfos['gender']) {
			case 'male' :
				$gender = Genders::MALE;
				break;

			case 'female' :
				$gender = Genders::FEMALE;
				break;
		}

		// Format infos
		$data = array(
			'external_api_id' => ExternalApis::FACEBOOK,
			'uid' => $userInfos['id'],
			'username' => $userInfos['name'],
			'first_name' => $userInfos['first_name'],
			'last_name' => $userInfos['last_name'],
			'avatar' => static::getAvatarUrl($userInfos['id']),
			'email' => $userInfos['email'],
			'other' => array(
				'gender' => $gender,
			),
		);

		unset($userInfos['id']);
		unset($userInfos['name']);
		unset($userInfos['first_name']);
		unset($userInfos['last_name']);
		unset($userInfos['link']);
		unset($userInfos['gender']);
		unset($userInfos['email']);
		$userInfos['slug'] = $userInfos['username'];
		unset($userInfos['username']);

		$data['other'] += $userInfos;

		$data['info_token'] = array(
			'access_token' => $this->accessToken(),
			'date_expiration' => date('Y-m-d H:i:s', $this->getAccessTokenExpiration()),
		);

		return $data;
	}

	/**
	 * [onAuthorizeCallback description]
	 * @param  [type] $request [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function onAuthorizeCallback($request, $callbackUrl, array $options = array()) {
		if ($this->isAuthentificated()) {
			$accessToken = $this->_service->getAccessToken();

			if (!empty($accessToken)) {
				$this->accessToken($accessToken);
			}
		}
	}

	/**
	 * [getAccessTokenExpiration description]
	 * @return [type] [description]
	 */
	public function getAccessTokenExpiration() {
		$info = $this->_service->api('debug_token', array('input_token' => $this->_service->getAccessToken()));
		return empty($info['data']['expires_at']) ? false : $info['data']['expires_at'];
	}

	/**
	 * [getAvatarUrl description]
	 * @param  [type] $uid     [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public static function getAvatarUrl($uid, array $options = array()) {
		$defaults = array('size' => null);
		$options += $defaults;

		$url = String::insert(self::AVATAR_URL_TEMPLATE, compact('uid'));

		if (!empty($options['size'])) {
			$url .= '?width=' . $options['size'] . '&amp;height=' . $options['size'];
		}

		return $url;
	}
}
?>