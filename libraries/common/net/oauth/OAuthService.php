<?php

namespace common\net\oauth;

use Exception;

class OAuthService {

	protected static $_servicesInstances = array();

	protected static $_config = array();

	protected $_service = null;

	/**
	 * [getService description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public static function getService($name) {
		if (empty(self::$_servicesInstances[$name])) {
			$klass = static::_getServiceClass($name);

			if (empty($klass)) {
				throw new Exception("no oauth service found for `$name`");
			}

			self::$_servicesInstances[$name] = new $klass;
		}

		return self::$_servicesInstances[$name];
	}

	/**
	 * [getConfig description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	public static function getConfig($name) {
		if (empty(self::$_config[$name])) {
			throw new Exception("no service config for `$name`");
		}

		return self::$_config[$name];
	}

	/**
	 * [setConfig description]
	 * @param [type] $data [description]
	 */
	public static function setConfig($data) {
		self::$_config = $data;
	}

	/**
	 * [_getServiceClass description]
	 * @param  [type] $name [description]
	 * @return [type]       [description]
	 */
	protected static function _getServiceClass($name) {
		$klasses = array(
			'facebook' => '\common\net\oauth\FacebookService',
			'google' => '\common\net\oauth\GoogleService',
		);

		return empty($klasses[$name]) ? null : $klasses[$name];
	}

	/**
	 * [isAuthentificated description]
	 * @return boolean [description]
	 */
	public function isAuthentificated() {
		return false;
	}

	/**
	 * [getUrlForAuthentificate description]
	 * @param  [type] $callbackUrl [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function getUrlForAuthentificate($callbackUrl, array $options = array()) {
		throw new Exception('must be implemented');
	}

	/**
	 * [getUserInfo description]
	 * @return [type] [description]
	 */
	public function getUserInfo() {
		throw new Exception('must be implemented');
	}

	/**
	 * [onAuthorizeCallback description]
	 * @param  [type] $request [description]
	 * @param  array  $options [description]
	 * @return [type]          [description]
	 */
	public function onAuthorizeCallback($request, $callbackUrl, array $options = array()) {
		throw new Exception('must be implemented');
	}

	/**
	 * [accessToken description]
	 * @param  [type] $accessToken [description]
	 * @param  array  $options     [description]
	 * @return [type]              [description]
	 */
	public function accessToken($accessToken = null, array $options = array()) {
		throw new Exception('must be implemented');
	}

	/**
	 * [slug description]
	 * @return [type] [description]
	 */
	public function slug() {
		throw new Exception('must be implemented');
	}

	/**
	 * [getVisitorPrefix description]
	 * @return [type] [description]
	 */
	protected function getVisitorPrefix() {
		return 'oauth.' . $this->slug();
	}

	/**
	 * [getAccessTokenExpiration description]
	 * @return [type] [description]
	 */
	public function getAccessTokenExpiration() {
		throw new Exception('must be implemented');
	}
}
?>