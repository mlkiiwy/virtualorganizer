<?php

namespace common\tests\cases\util;

use Exception;

use common\util\String;

class StringTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testSlug() {
		$str = 'Hello World';
		$expected = 'Hello_World';
		$result = String::slug($str);
		$this->assertEqual($expected, $result);

		$str = 'Hello World!';
		$expected = 'Hello_World';
		$result = String::slug($str);
		$this->assertEqual($expected, $result);

		$str = '†';
		$expected = 'n_a';
		$result = String::slug($str);
		$this->assertEqual($expected, $result);

		$str = '올드보이';
		$expected = 'n_a';
		$result = String::slug($str);
		$this->assertEqual($expected, $result);

		$str = 'Test avec des - et des _';
		$expected = 'Test_avec_des_et_des';
		$result = String::slug($str);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function cleanStr() {
		throw new Exception('Todo');
	}

	/**
	 *
	 */
	public function testCleanUsername() {

		$tests = array(
			'Kévin' => 'Kévin',
			'Филипповицх' => 'Филипповицх',
			'Sens critique' => 'Sens_critique',
			'   Sens critique  _' => 'Sens_critique',
			'RockLDN' => 'RockLDN',
			'' => '',
			'†' => '',
			'올드보이' => '올드보이',
			'Test avec des - et des _' => 'Test_avec_des_-_et_des',
			'-ML-' => '-ML-'
		);

		foreach( $tests as $input => $expected) {
			$result = String::cleanUsername($input);
			$this->assertEqual($expected, $result);
		}
	}

	/**
	 *
	 */
	public function testEqual() {
		$str = 'Hello world, my name is SensCritique.';
		$expected = true;
		$result = String::equal($str, 'Hello world, my name is SensCritique.');
		$this->assertEqual($expected, $result);

		$str = 'Hello world, my name is SensCritique.';
		$expected = true;
		$result = String::equal($str, 'Hello world, my name is senscritique.', String::EQUAL_CLEAN);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testIsUrl() {
		$str = 'http://www.google.com';
		$expected = true;
		$result = String::isUrl($str);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testExtractClassName() {
		$str = '\\common\\models\\Users';
		$expected = 'Users';
		$result = String::extractClassName($str);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testContainElements() {
		$str = 'Hello world, my name is SensCritique.';
		$expected = true;
		$result = String::containElements($str, array('SensCritique'));
		$this->assertEqual($expected, $result);

		$str = 'Hello world, my name is SensCritique.';
		$expected = false;
		$result = String::containElements($str, array('Senscritique'));
		$this->assertEqual($expected, $result);

		$str = 'Hello world, my name is SensCritique.';
		$expected = false;
		$result = String::containElements($str, array('SensCritiquee'));
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testRemoveElements() {
		$str = 'Hello world, my name is SensCritique.';
		$expected = 'Hello     SensCritique.';
		$result = String::removeElements($str, array('world,', 'my', 'name', 'is'));
		$this->assertEqual($expected, $result);
	}
	/**
	 *
	 */
	public function testExtractMainDomain() {
		$str = 'www.senscritique.com';
		$expected = 'senscritique.com';
		$result = String::extractMainDomain($str);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testTitleCase() {
		$str = 'hello world';
		$expected = 'Hello World';
		$result = String::titleCase($str);
		$this->assertEqual($expected, $result);

		$str = 'coucou c\'est moi!';
		$expected = 'Coucou C\'est Moi!';
		$result = String::titleCase($str);
		$this->assertEqual($expected, $result);

		$str = 'ça va?';
		$expected = 'Ça Va?';
		$result = String::titleCase($str);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testCleanSeeksQuery() {
		$str = 'Matrix';
		$expected = 'Matrix';
		$result = String::cleanSeeksQuery($str);
		$this->assertEqual($expected, $result);

		$str = 'Matrix 2 le retour';
		$expected = 'Matrix+2+le+retour';
		$result = String::cleanSeeksQuery($str);
		$this->assertEqual($expected, $result);
	}
}

?>