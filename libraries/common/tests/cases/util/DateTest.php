<?php

namespace common\tests\cases\util;

use Exception;

use common\util\Date;

class DateTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testSqlNow() {
		$expected = date('Y-m-d H:i:s');
		$result = Date::sqlNow();
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testSqlStr() {
		$expected = date('Y-m-d H:i:s');
		$result = Date::sqlStr();
		$this->assertEqual($expected, $result);

		$expected = date('Y-m-d H:i:s', strtotime('yesterday'));
		$result = Date::sqlStr('yesterday');
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testSqlToday() {
		$expected = date('Y-m-d');
		$result = Date::sqlToday();
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testValidBirthdate() {
		$result = Date::validBirthdate('2000-05-31');
		$this->assertTrue($result);

		$result = Date::validBirthdate('2020-05-31');
		$this->assertFalse($result);

		$result = Date::validBirthdate('1987-05-31');
		$this->assertTrue($result);

		$result = Date::validBirthdate('1987-05-35');
		$this->assertFalse($result);

		$result = Date::validBirthdate('1987-14-01');
		$this->assertFalse($result);
	}

	/**
	 *
	 */
	public function testCompare() {
		$date1 = '2010-01-01';
		$date2 = '2011-01-01';

		$expected = true;
		$result = Date::compare($date1, $date2) < 0;
		$this->assertEqual($expected, $result);

		$date1 = '2011-01-01';
		$date2 = '2010-01-01';

		$expected = false;
		$result = Date::compare($date1, $date2) < 0;
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testIsValidWiki() {
		$date = array(2011, 1, 1);

		$expected = true;
		$result = Date::isValidWiki($date);
		$this->assertEqual($expected, $result);

		$date = array(2011, 1, 0);

		$expected = true;
		$result = Date::isValidWiki($date);
		$this->assertEqual($expected, $result);

		$date = array(2011, 0, 0);

		$expected = true;
		$result = Date::isValidWiki($date);
		$this->assertEqual($expected, $result);

		$date = array(2011, 1, 32);

		$expected = false;
		$result = Date::isValidWiki($date);
		$this->assertEqual($expected, $result);

		$date = array(2011, 13, 0);

		$expected = false;
		$result = Date::isValidWiki($date);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testExtract() {
		$date = '2012-01-05';

		$expected = array('Y' => '2012', 'm' => '01', 'd' => '05', 'H' => '00', 'i' => '00', 's' => '00');
		$result = Date::extract($date);
		$this->assertEqual($expected, $result);

		$date = '2012-01-05 22:30:00';

		$expected = array('Y' => '2012', 'm' => '01', 'd' => '05', 'H' => '22', 'i' => '30', 's' => '00');
		$result = Date::extract($date);
		$this->assertEqual($expected, $result);

		$date = '2012';

		$expected = array('Y' => '2012', 'm' => '00', 'd' => '00', 'H' => '00', 'i' => '00', 's' => '00');
		$result = Date::extract($date);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testFormat() {
		$date = '2012-01-05';

		$expected = 'janvier 5, 2012 00:00';
		$result = Date::format($date);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testFormatDateSc() {
		$date = '2012-01-05';

		$expected = 'le <time datetime="2012-01-05" itemprop="datePublished">5 janvier 2012</time>';
		$result = Date::formatDateSc($date);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testDayToWeek() {
		$date = '05-01-2012';

		$expected = array('2012-01-05', '2012-01-12');
		$result = Date::dayToWeek($date);
		$this->assertEqual($expected, $result);
	}
}

?>