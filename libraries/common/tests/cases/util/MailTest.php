<?php

namespace common\tests\cases\util;

use common\util\Mail as MailUtil;

use Exception;

class MailTest extends \lithium\test\Unit {

	public function testExtractEmails() {
		$tests = array(
			'=?UTF-8?B?TWlrYcOrbCBMQUJSVVQgKC1NTC0p?= <labrut@gmail.com>' => array(
				'labrut@gmail.com',
			),
			'"topic1@vo-project.appspotmail.com" <topic1@vo-project.appspotmail.com>' => array(
				'topic1@vo-project.appspotmail.com',
			),
			'=?UTF-8?B?TWlrYcOrbCBMQUJSVVQgKC1NTC0p?= <labrut@gmail.com>, robert@test.com' => array(
				'labrut@gmail.com', 'robert@test.com',
			),
			'"topic1@vo-project.appspotmail.com" <topic1@vo-project.appspotmail.com>, "topic2@vo-project.appspotmail.com" <topic2@vo-project.appspotmail.com>' => array(
				'topic1@vo-project.appspotmail.com', 'topic2@vo-project.appspotmail.com',
			),
		);

		foreach ($tests as $value => $expected) {
			$this->assertEqual($expected, MailUtil::extractEmails($value));
		}

		$tests = array(
			'=?UTF-8?B?TWlrYcOrbCBMQUJSVVQgKC1NTC0p?= <labrut@gmail.com>' => array(
				'labrut@gmail.com' => 'labrut@gmail.com',
			),
			'"topic1@vo-project.appspotmail.com" <topic1@vo-project.appspotmail.com>' => array(
				'topic1@vo-project.appspotmail.com' => 'topic1@vo-project.appspotmail.com',
			),
			'"Robert" <topic1@vo-project.appspotmail.com>' => array(
				'Robert' => 'topic1@vo-project.appspotmail.com',
			),
			'"topic1" <topic1@vo-project.appspotmail.com>, "topic2" <topic2@vo-project.appspotmail.com>' => array(
				'topic1' => 'topic1@vo-project.appspotmail.com',
				'topic2' => 'topic2@vo-project.appspotmail.com',
			),
		);

		foreach ($tests as $value => $expected) {
			$this->assertEqual($expected, MailUtil::extractEmails($value, array('label' => true)));
		}
	}

	public function testExtractEmail() {
		$tests = array(
			'=?UTF-8?B?TWlrYcOrbCBMQUJSVVQgKC1NTC0p?= <labrut@gmail.com>' => 'labrut@gmail.com',
			'"topic1@vo-project.appspotmail.com" <topic1@vo-project.appspotmail.com>' => 'topic1@vo-project.appspotmail.com',
		);

		foreach ($tests as $value => $expected) {
			$this->assertEqual($expected, MailUtil::extractEmail($value));
		}

		$tests = array(
			'=?UTF-8?B?TWlrYcOrbCBMQUJSVVQgKC1NTC0p?= <labrut@gmail.com>' => array(
				'labrut@gmail.com' => 'labrut@gmail.com',
			),
			'"topic1@vo-project.appspotmail.com" <topic1@vo-project.appspotmail.com>' => array(
				'topic1@vo-project.appspotmail.com' => 'topic1@vo-project.appspotmail.com',
			),
			'"Robert" <topic1@vo-project.appspotmail.com>' => array(
				'Robert' => 'topic1@vo-project.appspotmail.com',
			),
		);

		foreach ($tests as $value => $expected) {
			$this->assertEqual($expected, MailUtil::extractEmail($value, array('label' => true)));
		}
	}

}

?>