<?php

namespace common\tests\cases\util;

use Exception;

use common\models\PhoneTypes;

use common\util\PhoneNumber as PhoneNumberUtil;

class PhoneNumberTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testClean() {
		$tests = array(
			'+33678474785' => '+33678474785',
			'  +33 678 47 4785 ' => '+33678474785',
			'+33.6.7.8474785  ' => '+33678474785',
			'06 78 47 47 85' => '+33678474785',
			'06.7847 4785' => '+33678474785',
			'16.7847-4785' => false,
			'4785' => false,
			'+44787878' => '+44787878',
		);

		foreach ($tests as $input => $output) {
			$this->assertEqual($output, PhoneNumberUtil::clean($input));
		}
	}

	/**
	 *
	 */
	public function testTypes() {
		$tests = array(
			'+33678474785' => PhoneTypes::MOBILE,
			'+44787878' => PhoneTypes::UNKNOW,
			'+33344221701' => PhoneTypes::CLASSIC,
		);

		foreach ($tests as $input => $output) {
			$this->assertEqual($output, PhoneNumberUtil::getPhoneTypeId($input));
		}
	}
}

?>