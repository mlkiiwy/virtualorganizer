<?php

namespace common\tests\cases\util;

use Exception;

use common\util\Set;

class SetTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testArrayLastValue() {
		$a = array('Hello', 'World', 'My', 'Name', 'Is', 'Slim', 'Shady');

		$expected = end($a);
		$result = Set::arrayLastValue($a);
		$this->assertEqual($expected, $result);

		$a = array('Hello' => 1, 'World' => 2, 'My' => 3, 'Name' => 4, 'Is' => 5, 'Slim' => 6, 'Shady' => 7);

		$expected = end($a);
		$result = Set::arrayLastValue($a);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testArrayFirstValue() {
		$a = array('Hello', 'World', 'My', 'Name', 'Is', 'Slim', 'Shady');

		$expected = reset($a);
		$result = Set::arrayFirstValue($a);
		$this->assertEqual($expected, $result);

		$a = array('Hello' => 1, 'World' => 2, 'My' => 3, 'Name' => 4, 'Is' => 5, 'Slim' => 6, 'Shady' => 7);

		$expected = reset($a);
		$result = Set::arrayFirstValue($a);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testArrayGetValueByIndex() {
		$a = array('Hello', 'World', 'My', 'Name', 'Is', 'Slim', 'Shady');

		$expected = $a[3];
		$result = Set::arrayGetValueByIndex($a, 3);
		$this->assertEqual($expected, $result);

		$a = array('Hello' => 1, 'World' => 2, 'My' => 3, 'Name' => 4, 'Is' => 5, 'Slim' => 6, 'Shady' => 7);

		$expected = $a['Name'];
		$result = Set::arrayGetValueByIndex($a, 3);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testRequiredKeys() {
		$closure = function () {
			$a = array('first' => 1, 'second' => 2);
			$keys = array('third');
			Set::requiredKeys($keys, $a);
		};
		$this->assertException(true, $closure);

		$closure = function () {
			$a = array('first' => 1, 'second' => 2);
			$keys = array('first', 'second', 'third');
			Set::requiredKeys($keys, $a);
		};
		$this->assertException(true, $closure);

		$a = array('first' => 1, 'second' => 2);
		$keys = array('first');
		$result = Set::requiredKeys($keys, $a);
		$this->assertNull($result);

		$a = array('first' => 1, 'second' => 2);
		$keys = array('first', 'second');
		$result = Set::requiredKeys($keys, $a);
		$this->assertNull($result);
	}

	/**
	 *
	 */
	public function testRenameKeys() {
		$a = array('first' => 1, 'second' => 2, 'third' => 3);
		$mapping = array('first' => 'fourth', 'third' => 'fifth');

		$expected = array('fourth' => 1, 'second' => 2, 'fifth' => 3);
		$result = Set::renameKeys($a, $mapping);
		$this->assertEqual($expected, $result);

		$a = array('first' => 1, 'second' => 2, 'third' => 3);
		$mapping = array('first' => 'second', 'third' => 'first');

		$expected = array('second' => 1, 'first' => 3);
		$result = Set::renameKeys($a, $mapping);
		$this->assertEqual($expected, $result);

		$a = array('first' => 1, 'second' => 2, 'third' => 3);
		$mapping = array('first' => 'second', 'second' => 'first');

		$expected = array('first' => 1, 'third' => 3);
		$result = Set::renameKeys($a, $mapping);
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testReindexBy() {
		$a = array(
			0 => array('id' => 123, 'label' => 'Coucou moi'),
			1 => array('id' => 234, 'label' => 'Coucou toi')
		);

		$expected = array(
			123 => array('id' => 123, 'label' => 'Coucou moi'),
			234 => array('id' => 234, 'label' => 'Coucou toi')
		);
		$result = Set::reindexBy($a, 'id');
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testGetFieldValue() {
		$a = array(
			0 => array('id' => 123, 'label' => 'Coucou moi'),
			1 => array('id' => 234, 'label' => 'Coucou toi')
		);

		$expected = array(123, 234);
		$result = Set::getFieldValue($a, 'id');
		$this->assertEqual($expected, $result);
	}

	/**
	 *
	 */
	public function testWhitelist() {
		$input = array('label' => 'Hello world', 'label2' => 'Hello you', 'id' => 666);

		$expected = array('label' => 'Hello world', 'id' => 666);
		Set::whitelist($input, array('label', 'id'));
		$this->assertEqual($expected, $input);
	}

	/**
	 *
	 */
	public function testBlacklist() {
		$input = array('label' => 'Hello world', 'label2' => 'Hello you', 'id' => 666);

		$expected = array('label2' => 'Hello you');
		Set::blacklist($input, array('label', 'id'));
		$this->assertEqual($expected, $input);
	}
}

?>