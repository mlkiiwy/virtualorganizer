<?php

namespace common\tests\cases\util;

use common\util\Arr;

class ArrTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testUnion() {
		$a = array('hello', 'world', 'my');
		$b = array('my', 'name', 'is', 'slim');

		$expected = array_merge($a, $b);
		$result = Arr::union($a, $b);
		$this->assertEqual($expected, $result);

		$expected = array_merge($b, $a);
		$result = Arr::union($b, $a);
		$this->assertEqual($expected, $result);

		$expected = array_unique(array_merge($a, $b));
		$result = Arr::union($a, $b, true);
		$this->assertEqual($expected, $result);

		$expected = array_unique(array_merge($b, $a));
		$result = Arr::union($b, $a, true);
		$this->assertEqual($expected, $result);
	}
}

?>