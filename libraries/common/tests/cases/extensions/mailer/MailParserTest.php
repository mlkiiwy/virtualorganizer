<?php

namespace common\tests\cases\extensions\mailer;

use common\extensions\mailer\MailParser;

use Exception;

class MailParserTest extends \lithium\test\Unit {

	/**
	 *
	 */
	public function testParse() {
		$tests = array(
			array(
				'file' => '1.gmail.txt',
				'from' => array('labrut@gmail.com' => 'labrut@gmail.com'),
				'to' => array(
					'topic1@vo-project.appspotmail.com' => 'topic1@vo-project.appspotmail.com'
				),
				'provider' => 'gmail',
				'cc' => array(),
				'subject' => 'Re: Subject du jour',
				'html' => '<div dir="ltr">Ca va ?</div>',
				'text' => 'Ca va ?',
			),
			array(
				'file' => '1.yahoo.txt',
				'from' => array('mikael.labrut@yahoo.fr' => 'mikael.labrut@yahoo.fr'),
				'to' => array(
					'topic1@vo-project.appspotmail.com' => 'topic1@vo-project.appspotmail.com'
				),
				'provider' => 'yahoo',
				'cc' => array(),
				'subject' => 'Re: Subject du jour',
				'html' => '<div style="color:#000; background-color:#fff; font-family:HelveticaNeue, Helvetica Neue, Helvetica, Arial, Lucida Grande, sans-serif;font-size:12pt"><div><span>Ouais ????</span></div> <div class="qtdSeparateBR"><br><br></div>',
				'text' => 'Ouais ????',
			),
		);



		foreach ($tests as $email) {

			$filepath = dirname(__FILE__) . '/' . $email['file'];
			$content = file_get_contents($filepath);
			if ($content === false) {
				throw new Exception('cannot open file : ' . $filepath);
				return;
			}

			$parser = new MailParser($content);

			$this->assertEqual($email['from'], $parser->getFrom());
			$this->assertEqual($email['to'], $parser->getTo());
			$this->assertEqual($email['provider'], $parser->getProvider());
			$this->assertEqual($email['cc'], $parser->getCc());
			$this->assertEqual($email['subject'], $parser->getSubject());
			$this->assertEqual($email['html'], $parser->getHTMLBody());
			$this->assertEqual($email['text'], $parser->getPlainBody());
		}
	}
}

?>