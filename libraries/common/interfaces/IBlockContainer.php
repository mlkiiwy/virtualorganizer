<?php

namespace common\interfaces;

interface IBlockContainer {

	public function addBlock($container, $name, array $data, $position = false, array $options = array());
	public function getBlocks($container, array $options = array());
	public function blockContainerType($container);

}
?>