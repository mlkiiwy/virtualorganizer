<?php

namespace common\interfaces;

interface IActionLogger {

	public function action($action, $success = null, $step = ActionLogs::STEP_NONE, $exception = null);

}

?>