<?php

namespace common\interfaces;

interface IChainableAction {
	public function parentQueue(IActionQueue $actionQueue = null);
	public function preAction(IChainableAction $lastAction = null);
}
?>