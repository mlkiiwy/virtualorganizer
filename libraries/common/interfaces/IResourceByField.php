<?php

namespace common\interfaces;

interface IResourceByField {
	public static function getByField($field, $value, array $options = array());
}
?>