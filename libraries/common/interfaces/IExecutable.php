<?php

namespace common\interfaces;

interface IExecutable {
	public function execute(array $options = array());
}
?>