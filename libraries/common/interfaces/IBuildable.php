<?php

namespace common\interfaces;

interface IBuildable {

	public static function build(array $data, array $options = array());
}
?>