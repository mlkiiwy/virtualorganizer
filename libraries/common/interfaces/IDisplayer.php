<?php

namespace common\interfaces;

interface IDisplayer {

	public function startBlock($title = '');
	public function outParams($data = null);
	public function outResult($result);
	public function outData($data, array $options = array());
	public function endBlock($title = '');
}
?>