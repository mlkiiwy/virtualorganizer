<?php

namespace common\interfaces;

interface IResourceById {
	public static function getById($id, array $options = array());
	public static function getByIds($ids, array $options = array());
}
?>