<?php

namespace common\interfaces;

interface ITaskable extends IExecutable {
	public function delayed(array $options = array());
}
?>