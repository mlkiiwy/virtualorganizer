<?php

namespace common\interfaces;

/**
 *
 */
interface IOAuthEmailContactsService {

	/**
	 *
	 */
	public function getEmailContacts(array $options = array());

}

?>