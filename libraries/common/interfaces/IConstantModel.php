<?php

namespace common\interfaces;

interface IConstantModel {

	public static function generate(array $options = array());
}
?>