<?php

namespace common\interfaces;

/**
 *
 */
interface IWithJsonModel {

	/**
	 *
	 */
	public function jsonFields($entity, array $options = array());

	/**
	 *
	 */
	public function generateJsonFields($entity, array $options = array());

	/**
	 *
	 */
	public function extractJsonFields($entity, $key = null, array $options = array());

}

?>