<?php

namespace common\interfaces;

interface IConfigurable {
	public function config();
}
?>