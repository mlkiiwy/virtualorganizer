<?php

namespace common\interfaces;

interface IAction extends ITaskable {

	public function input(array $input = null);
	public function result();

	public function displayer(IDisplayer $displayer = null);
}
?>