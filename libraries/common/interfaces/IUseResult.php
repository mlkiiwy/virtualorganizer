<?php

namespace common\interfaces;

interface IUseResult {
	public function isParent();
	public function isPreAction();
	public function key();
}
?>