<?php

namespace common\interfaces;

interface ISingleton {

	public static function instance();

}
?>