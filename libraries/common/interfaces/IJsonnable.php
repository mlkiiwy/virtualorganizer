<?php

namespace common\interfaces;

interface IJsonnable {

	public function toJson(array $options = array());
}
?>