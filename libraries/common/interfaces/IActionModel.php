<?php

namespace common\interfaces;

interface IActionModel {

	public function model();
	public function modelId();
}
?>