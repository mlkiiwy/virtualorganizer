<?php

namespace common\interfaces;

interface ILoggableAction {

	public function dataForLogger();

}
?>