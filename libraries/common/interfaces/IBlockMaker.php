<?php

namespace common\interfaces;

interface IBlockMaker {

	public function hasBlock($user = null);
	public function getBlock($user = null, array $options = array());

}
?>