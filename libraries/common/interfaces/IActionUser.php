<?php

namespace common\interfaces;

interface IActionUser {

	public function user($user = null, array $options = array());
	public function userId();
}
?>