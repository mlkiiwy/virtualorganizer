<?php

namespace common\interfaces;

interface IEnhancedModel {

	public function refreshDates($entity, array $options = array());

	public static function createRecordSet(array $data = array(), array $options = array());
	public static function createRecords(array $data, array $options = array());

	public function fieldChanges($entity, $options = array());

	public function checkSaved($entity, array $options = array());

	public function related($entity, $relationName, $value = false, array $options = array());
	public function populateRelatedId($entity, $fieldNameOrType, array $options = array());
	public function loadRelated($entity, $fieldNameOrType, array $options = array());

}
?>