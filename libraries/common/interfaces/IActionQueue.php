<?php

namespace common\interfaces;

interface IActionQueue {

	public function add($action, $inputForAction = array(), array $options = array());

}
?>