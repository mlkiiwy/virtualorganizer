<?php

namespace li3_assets\extensions\command;

use lessc;

use lithium\core\Libraries;
use lithium\net\http\Media;
use lithium\storage\Cache;

use li3_assets\extensions\helper\Optimize;
use li3_assets\util\CSS;

/**
 *
 */
class Assets extends \lithium\console\Command {

	/**
	 *
	 */
	protected $_removedFiles = 0;
	protected $_compiledFiles = 0;
	protected $_processedDirectories = 0;

	/**
	 *
	 */
	public function run() {
		$webroot = Media::webroot();
		$cssRoot = rtrim(Media::asset(null, 'css', array('suffix' => false)), DIRECTORY_SEPARATOR);
		$webroot = Media::webroot() . $cssRoot;

		$this->_cleanDirectory($webroot, '.less.cache');
		$this->_cleanDirectory($webroot, '.less.css');
		$this->_cleanDirectory($webroot . '/' . $this->_getOptimizedDirectory(), '.css');

		$this->out('Processed ' . $this->_processedDirectories . ' directories.');
		$this->out('Removed ' . $this->_removedFiles . ' files.');

		$this->_processedDirectories = 0;
		$this->_processDirectory($webroot);

		$this->out('Processed ' . $this->_processedDirectories . ' directories.');
		$this->out('Compiled ' . $this->_compiledFiles . ' files.');

		return true;
	}

	/**
	 *
	 */
	public function flushOptimized() {
		$webroot = Media::webroot(true) . '/lib';
		$this->_cleanDirectory($webroot . '/' . $this->_getOptimizedDirectory(), '.css');
		$this->_cleanDirectory($webroot . '/' . $this->_getOptimizedDirectory(), '.js');
		$this->_clearCache();
		return true;
	}

	/**
	 *
	 */
	protected function _cleanDirectory($dir, $suffix) {
		$this->out("Cleaning {$dir} from files with suffix `{$suffix}`");

		$files = array_diff(scandir($dir), array('..', '.', '.DS_Store', 'optimized'));
		$this->_processedDirectories++;

		foreach ($files as $file) {
			if (is_dir($dir . '/' . $file)) {
				$this->_cleanDirectory($dir . '/' . $file, $suffix);
			} elseif (strpos($file, $suffix) !== false) {
				if (unlink($dir . '/' . $file)) {
					$this->out("Removed file {$file}");
					$this->_removedFiles++;
				} else {
					$this->error("Error while removing {$file}");
				}
			}
		}
	}

	/**
	 *
	 */
	protected function _processDirectory($dir) {
		$this->out('Scanning ' . $dir);

		$files = array_diff(scandir($dir), array('..', '.', '.DS_Store', 'optimized'));
		$this->_processedDirectories++;

		foreach ($files as $file) {
			if (is_dir($dir . '/' . $file)) {
				$this->_processDirectory($dir . '/' . $file);
			} elseif ($this->_isLessFile($file)) {
				$this->_compileFile($dir . '/' . $file);
			}
		}
	}

	/**
	 *
	 */
	protected function _compileFile($lessFile) {
		$this->out('Compiling ' . $lessFile);

		CSS::compileLess($lessFile, $lessFile . '.css');

		$this->_compiledFiles++;
	}

	/**
	 *
	 */
	protected function _isLessFile($file) {
		$isLessFile = strpos($file, '.less') !== false;
		$isLessFile = $isLessFile && strpos($file, '.less.cache') === false;
		$isLessFile = $isLessFile && strpos($file, 'less.css') === false;
		$isLessFile = $isLessFile && strpos($file, '.dist') === false;

		return $isLessFile;
	}

	/**
	 *
	 */
	protected function _getOptimizedDirectory() {
		$library = Libraries::get('li3_assets');
		return $library['config']['css']['output_directory'];
	}

	/**
	 *
	 */
	protected function _clearCache() {
		$cache = Cache::adapter('default')->connection;
		$keys = $cache->keys('*' . Optimize::CACHE_PREFIX . '*');

		if (!empty($keys)) {
			$count = $cache->del($keys);
			$this->out('Deleted ' . $count . ' asset keys.');
		} else {
			$this->out('No asset keys to delete.');
		}
	}
}

?>