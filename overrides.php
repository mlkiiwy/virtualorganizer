<?php

function override_tempnam($dir, $prefix){
	do {
		$fix = substr(md5($prefix . time() . rand(0,10000)), 0, 6);
		$filename = $dir . '/' . $prefix . $fix;
	} while (file_exists($filename));

	return $filename;
}

function override_glob($path, $regex) {
	$files = array();
	if ($handle = opendir($path)) {
		while (false !== ($entry = readdir($handle))) {
			if (preg_match($regex, $entry)) {
				$files[] = $path . '/' . $entry;
			}
		}
		closedir($handle);
	}
	return $files;
}

?>