function applyChmod(){
	local target=$1
	local full_path=$(echo $target | cut -d"|" -f1);
	local rights=$(echo $target | cut -d"|" -f2);
	local recursive=1

	if [ "${full_path: -1}" == "/" ]
	then
		recursive=0
	fi

	# find -P $path -type f -print | xargs chmod $option $rights

	if [ -d $full_path ]
	then
		if [ $recursive == 0 ]
		then
			cmd="chmod $rights $full_path*"
			echo "   >   $cmd" # Pour le debug
			eval "$cmd"
		else
			cmd="find -P $full_path -type f -print | xargs chmod $rights"
			echo "   >   $cmd" # Pour le debug
			eval "$cmd"
			cmd="find -P $full_path -type d -print | xargs chmod $rights"
			echo "   >   $cmd" # Pour le debug
			eval "$cmd"
		fi
	fi
}
