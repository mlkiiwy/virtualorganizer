#!/bin/bash

source tools/lib.sh

# Applique des droits chmod aux répertoires du site.
#
# Syntaxe : `chemin du répertoire`|`code chmod`
#
# Si le chemin se finit par un `/`, seuls les fichiers à sa racine seront affectés,
# sinon l'application sera récursive.
#
# Exemple :
#		'libraries|755' // Applique 755 à tous les fichiers et répertoires de libraries
#		'libraries/|755' // Applique 755 à tous les fichiers à la racine de libraries

# Applications concernées
apps=(
	'app_mail'
)

# Dossiers à affecter dans les applications
# Format : Valeur|droits
folders_in_apps=(
	'/|755'
	'/config|755'
	'/controllers|755'
	'/extensions|755'
	'/libraries|755'
	'/models|755'
	'/tests|755'
	'/util|755'
	'/views|755'
	'/webroot|755'
	'/resources|777'
	'/webroot/lib|777'
)
# Dossiers supplémentaires
folders_from_root=(
	'./|755'
	'libraries/|755'
	'libraries/sc_common|755'
	'libraries/li3_assets|755'
	'tools|755'
)

echo "Updating files and folder rights..."

for app in "${apps[@]}"
do
	for target in "${folders_in_apps[@]}"
	do
		applyChmod $app$target
	done
done

for target in "${folders_from_root[@]}"
do
	applyChmod $target
done

echo "Done."