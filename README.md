virtualorganizer
================

Prototype d'Application web "assistant" à l'organisation d'un événement.

=> Au sein d'une mailing-list temporaire simulée, l'application ajoute des "widgets" (questions / compte rendus), relance automatiquement les non-répondant, etc. Elle fait ainsi ce que vous feriez vous même au sein d'une mailing list pour l'organisation d'une soirée par exemple.

Serveur : 

- PHP avec Framework Lithium (li3) 

- Avec un développement sous forme d'"Actions atomiques". (pour répartition dans le cloud des traitements)

Base :

- MySQL 

Front :

- AngularJS (voir : app_mail / webroot / lib / )

- Template généré par le serveur sous Twig